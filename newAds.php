<?php 
/*========================================================= 
 *profile Page 
 * ========================================================= 
 */
ob_start();
session_start();
include 'inti.php';

$PageTitel='New Ads';
if(isset($_SESSION['useruser'])){
    if($_SERVER['REQUEST_METHOD']=='POST'&&$_POST['user_token'] == $_SESSION['user_token']){
      $err_arr=array();
        $name=  filter_var($_POST['name'],FILTER_SANITIZE_STRING);
        $des=filter_var($_POST['des'],FILTER_SANITIZE_STRING);
        $pric=filter_var($_POST['pri'],FILTER_SANITIZE_NUMBER_INT);
        $country=filter_var($_POST['cont'],FILTER_SANITIZE_STRING);
        $stat=filter_var($_POST['status'],FILTER_SANITIZE_NUMBER_INT);
        $categ=filter_var($_POST['categ'],FILTER_SANITIZE_NUMBER_INT);
        $tags=filter_var($_POST['tags'],FILTER_SANITIZE_STRING);
       
        
        //Begin error check
        if(strlen($name)<4){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Name must Less Than 4 Character';}
    if(strlen($des)<10){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Description  must Less Than 10 Character';}
   if(strlen($country)===''){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Please Choose country ';}
   if(empty($pric)){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Price must be not empty';}
   if(empty($stat)){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Status must be not empty';}
   if(empty($categ)){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Category must be not empty';}
  // if(empty($imgFile)){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Please choose images';}
    if(count($_FILES["user_files"]["name"]) < 4){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Choose at least four images';}
 
    //updata database
                   if(empty($err_arr))   //after validation no error
                       {
           
               //make image uploading
  $userpic='';
  $valid_image_check = array("image/gif", "image/jpeg", "image/jpg", "image/png", "image/bmp");
  if (count($_FILES["user_files"]) > 3) {
    $folderName = "layout/Img/items/";
for ($i = 0; $i < count($_FILES["user_files"]["name"]); $i++) {

      if ($_FILES["user_files"]["name"][$i] <> "") {

        $image_mime = strtolower(image_type_to_mime_type(exif_imagetype($_FILES["user_files"]["tmp_name"][$i])));
        // if valid image type then upload
        if (in_array($image_mime, $valid_image_check)) {

          $ext = explode("/", strtolower($image_mime));
          $ext1 = strtolower(end($ext));
          $filename = rand(10000, 990000) . '_' . time() . '.' . $ext1;
          $filepath = $folderName . $filename;
          
          if (!move_uploaded_file($_FILES["user_files"]["tmp_name"][$i], $filepath)) {
            $emsg .= "Failed to upload <strong>" . $_FILES["user_files"]["name"][$i] . "</strong>. <br>";
            $counter++;
          } else {
            $smsg .= "<strong>" . $_FILES["user_files"]["name"][$i] . "</strong> uploaded successfully. <br>";

            $magicianObj = new imageLib($filepath);
            $magicianObj->resizeImage(100, 100);
           // $magicianObj->saveImage($folderName . 'thumb/' . $filename, 100);
            
            $userpic.=$filename.',';

          }
        } 
      }
    }


   
  } else{
      
      
  }
                       
              
                       
  //  $userpic=implode(",",$imgarr);              
              
                       
                       
                       
                       
                       
        
                
                       //end image uploading update
                       
                       
                       
                       
                       
                       //Insert Items
                       try{
                     
           try{
 $statm=$con->prepare('INSERT INTO items (item_name,description,price,add_date,country_name,image,status,category_id,member_id,tags)VALUES(:zname,:zdes,:zpri,now(),:zcon,:zimg,:zst,:zcat,:zmem,:ztags)' );
  $statm->execute(array("zname"=>$name,"zdes"=>$des,"zpri"=>$pric,"zcon"=>$country,'zimg'=>$userpic,"zst"=>$stat,"zcat"=>$categ,"zmem"=>$_SESSION['uid'],"ztags"=>$tags));
           }  catch (PDOException $e){
               echo $e->getMessage();
           }
                      //echo success message
                        if($statm){
                            $mesg_track="-(User Add new Item )=>";
                    UpMsgLog($mesg_track,$_SESSION['uid'], $_SESSION['log']);
                        $msg= 'Item successfuly inserted';
                        unset($_SESSION['user_token']);
                        //  $mesg_track.="-(User add New Item with name $name)=>";
                      //    $_SESSION['activity'].=$mesg_track;

                         //   UpMsgLog($_SESSION['activity'],$_SESSION['uid']);
                        
                        }       
                       }catch(PDOException $e)
                             {
                                  echo $e->getMessage();    
                              }
                       
                       }
   }
  
?>
<h1 class="text-center">create New Ads</h1>
<div class="new-ad block">
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                create New Ad
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-8">
                    <!Add form!>
                   
                    <div class="container">
                        <?php 
                         // create unique token
         $form_token = uniqid();
 
        // commit token to session
         $_SESSION['user_token'] = $form_token;
                        
                        ?>
                         <form class="form-horizontal add-form" enctype="multipart/form-data" action="<?php echo $_SERVER[PHP_SELF];?>" method="POST">
                             <input type="hidden" name="user_token" value="<?php echo  $_SESSION['user_token'];  ?>" />
                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_nm")?></label>
                                 <div class="col-sm-10 col-md-5">
                                     <input pattern=".{4,}" title="Enter at least 4 character" type="text" name="name" 
                                            class="form-control l-name" 
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Name Of The Item"/>
                                 </div>
                             </div>
                          
               <!Start Description!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_des")?></label>
                                 <div class="col-sm-10 col-md-5">
                                     <input pattern=".{10,}" title="Description not less than 10 character"type="text" name="des"
                                            class="form-control l-des"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Description Of The Item"/>
                                 </div>
                             </div>
            <!Start Price!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_price")?></label>
                                 <div class="col-sm-10 col-md-5">
                                     <input type="text" name="pri"
                                            class="form-control l-price"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Price Of The Item"/>
                                 </div>
                             </div>
          <!Start country made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_country")?></label>
                                 <div class="col-sm-10 col-md-5">
                             <!Start country made!>
                             <select name="cont" required="required"><option value="Pick a Country">Choose Country</option><option value="Afghanistan">Afghanistan</option><option value="Albania">Albania</option>
                                          <option value="Algeria">Algeria</option>
                                                  <option value="American Samoa">American Samoa</option>
                                                  <option value="Andorra">Andorra</option>
                                                  <option value="Angola">Angola</option>
                                                  <option value="Anguilla">Anguilla</option>
                                                  <option value="Antarctica">Antarctica</option>
                                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                  <option value="Argentina">Argentina</option>
                                                  <option value="Armenia">Armenia</option>
                                                  <option value="Aruba">Aruba</option>
                                                  <option value="Australia">Australia</option>
                                                  <option value="Austria">Austria</option>
                                                  <option value="Azerbaijan">Azerbaijan</option>
                                                  <option value="Bahamas">Bahamas</option>
                                                  <option value="Bahrain">Bahrain</option>
                                                  <option value="Bangladesh">Bangladesh</option>
                                                  <option value="Barbados">Barbados</option>
                                                  <option value="Belarus">Belarus</option>
                                                  <option value="Belgium">Belgium</option>
                                                  <option value="Belize">Belize</option>
                                                  <option value="Benin">Benin</option>
                                                  <option value="Bermuda">Bermuda</option>
                                                  <option value="Bhutan">Bhutan</option>
                                                  <option value="Bolivia">Bolivia</option>
                                                  <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                  <option value="Botswana">Botswana</option>
                                                  <option value="Bouvet Island">Bouvet Island</option>
                                                  <option value="Brazil">Brazil</option>
                                                  <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                  <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                  <option value="Bulgaria">Bulgaria</option>
                                                  <option value="Burkina Faso">Burkina Faso</option>
                                                  <option value="Burundi">Burundi</option>
                                                  <option value="Cambodia">Cambodia</option>
                                                  <option value="Cameroon">Cameroon</option>
                                                  <option value="Canada">Canada</option>
                                                  <option value="Cape Verde">Cape Verde</option>
                                                  <option value="Cayman Islands">Cayman Islands</option>
                                                  <option value="Central African Republic">Central African Republic</option>
                                                  <option value="Chad">Chad</option>
                                                  <option value="Chile">Chile</option>
                                                  <option value="China">China</option>
                                                  <option value="Christmas Island">Christmas Island</option>
                                                  <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                  <option value="Colombia">Colombia</option>
                                                  <option value="Comoros">Comoros</option>
                                                  <option value="Congo">Congo</option>
                                                  <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                  <option value="Cook Islands">Cook Islands</option>
                                                  <option value="Costa Rica">Costa Rica</option>
                                                  <option value="Cote D&#39;ivoire">Cote D'ivoire</option>
                                                  <option value="Croatia">Croatia</option>
                                                  <option value="Cuba">Cuba</option>
                                                  <option value="Cyprus">Cyprus</option>
                                                  <option value="Czech Republic">Czech Republic</option>
                                                  <option value="Denmark">Denmark</option>
                                                  <option value="Djibouti">Djibouti</option>
                                                  <option value="Dominica">Dominica</option>
                                                  <option value="Dominican Republic">Dominican Republic</option>
                                                  <option value="Ecuador">Ecuador</option>
                                                  <option value="Egypt">Egypt</option>
                                                  <option value="El Salvador">El Salvador</option>
                                                  <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                  <option value="Eritrea">Eritrea</option>
                                                  <option value="Estonia">Estonia</option>
                                                  <option value="Ethiopia">Ethiopia</option>
                                                  <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                  <option value="Faroe Islands">Faroe Islands</option>
                                                  <option value="Fiji">Fiji</option>
                                                  <option value="Finland">Finland</option>
                                                  <option value="France">France</option>
                                                  <option value="French Guiana">French Guiana</option>
                                                  <option value="French Polynesia">French Polynesia</option>
                                                  <option value="French Southern Territories">French Southern Territories</option>
                                                  <option value="Gabon">Gabon</option>
                                                  <option value="Gambia">Gambia</option>
                                                  <option value="Georgia">Georgia</option>
                                                  <option value="Germany">Germany</option>
                                                  <option value="Ghana">Ghana</option>
                                                  <option value="Gibraltar">Gibraltar</option>
                                                  <option value="Greece">Greece</option>
                                                  <option value="Greenland">Greenland</option>
                                                  <option value="Grenada">Grenada</option>
                                                  <option value="Guadeloupe">Guadeloupe</option>
                                                  <option value="Guam">Guam</option>
                                                  <option value="Guatemala">Guatemala</option>
                                                  <option value="Guinea">Guinea</option>
                                                  <option value="Guinea-bissau">Guinea-bissau</option>
                                                  <option value="Guyana">Guyana</option>
                                                  <option value="Haiti">Haiti</option>
                                                  <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                  <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                  <option value="Honduras">Honduras</option>
                                                  <option value="Hong Kong">Hong Kong</option>
                                                  <option value="Hungary">Hungary</option>
                                                  <option value="Iceland">Iceland</option>
                                                  <option value="India">India</option>
                                                  <option value="Indonesia">Indonesia</option>
                                                  <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                  <option value="Iraq">Iraq</option>
                                                  <option value="Ireland">Ireland</option>
                                                  <option value="Israel">Israel</option>
                                                  <option value="Italy">Italy</option>
                                                  <option value="Jamaica">Jamaica</option>
                                                  <option value="Japan">Japan</option>
                                                  <option value="Jordan">Jordan</option>
                                                  <option value="Kazakhstan">Kazakhstan</option>
                                                  <option value="Kenya">Kenya</option>
                                                  <option value="Kiribati">Kiribati</option>
                                                  <option value="Korea, Democratic People&#39;s Republic of">Korea, Democratic People's Republic of</option>
                                                  <option value="Korea, Republic of">Korea, Republic of</option>
                                                  <option value="Kuwait">Kuwait</option>
                                                  <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                  <option value="Lao People&#39;s Democratic Republic">Lao People's Democratic Republic</option>
                                                  <option value="Latvia">Latvia</option>
                                                  <option value="Lebanon">Lebanon</option>
                                                  <option value="Lesotho">Lesotho</option>
                                                  <option value="Liberia">Liberia</option>
                                                  <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                  <option value="Liechtenstein">Liechtenstein</option>
                                                  <option value="Lithuania">Lithuania</option>
                                                  <option value="Luxembourg">Luxembourg</option>
                                                  <option value="Macao">Macao</option>
                                                  <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                  <option value="Madagascar">Madagascar</option>
                                                  <option value="Malawi">Malawi</option>
                                                  <option value="Malaysia">Malaysia</option>
                                                  <option value="Maldives">Maldives</option>
                                                  <option value="Mali">Mali</option>
                                                  <option value="Malta">Malta</option>
                                                  <option value="Marshall Islands">Marshall Islands</option>
                                                  <option value="Martinique">Martinique</option>
                                                  <option value="Mauritania">Mauritania</option>
                                                  <option value="Mauritius">Mauritius</option>
                                                  <option value="Mayotte">Mayotte</option>
                                                  <option value="Mexico">Mexico</option>
                                                  <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                  <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                  <option value="Monaco">Monaco</option>
                                                  <option value="Mongolia">Mongolia</option>
                                                  <option value="Montenegro">Montenegro</option>
                                                  <option value="Montserrat">Montserrat</option>
                                                  <option value="Morocco">Morocco</option>
                                                  <option value="Mozambique">Mozambique</option>
                                                  <option value="Myanmar">Myanmar</option>
                                                  <option value="Namibia">Namibia</option>
                                                  <option value="Nauru">Nauru</option>
                                                  <option value="Nepal">Nepal</option>
                                                  <option value="Netherlands">Netherlands</option>
                                                  <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                  <option value="New Caledonia">New Caledonia</option>
                                                  <option value="New Zealand">New Zealand</option>
                                                  <option value="Nicaragua">Nicaragua</option>
                                                  <option value="Niger">Niger</option>
                                                  <option value="Nigeria">Nigeria</option>
                                                  <option value="Niue">Niue</option>
                                                  <option value="Norfolk Island">Norfolk Island</option>
                                                  <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                  <option value="Norway">Norway</option>
                                                  <option value="Oman">Oman</option>
                                                  <option value="Pakistan">Pakistan</option>
                                                  <option value="Palau">Palau</option>
                                                  <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                  <option value="Panama">Panama</option>
                                                  <option value="Papua New Guinea">Papua New Guinea</option>
                                                  <option value="Paraguay">Paraguay</option>
                                                  <option value="Peru">Peru</option>
                                                  <option value="Philippines">Philippines</option>
                                                  <option value="Pitcairn">Pitcairn</option>
                                                  <option value="Poland">Poland</option>
                                                  <option value="Portugal">Portugal</option>
                                                  <option value="Puerto Rico">Puerto Rico</option>
                                                  <option value="Qatar">Qatar</option>
                                                  <option value="Reunion">Reunion</option>
                                                  <option value="Romania">Romania</option>
                                                  <option value="Russian Federation">Russian Federation</option>
                                                  <option value="Rwanda">Rwanda</option>
                                                  <option value="Saint Helena">Saint Helena</option>
                                                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                  <option value="Saint Lucia">Saint Lucia</option>
                                                  <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                  <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                  <option value="Samoa">Samoa</option>
                                                  <option value="San Marino">San Marino</option>
                                                  <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                  <option value="Saudi Arabia">Saudi Arabia</option>
                                                  <option value="Senegal">Senegal</option>
                                                  <option value="Serbia">Serbia</option>
                                                  <option value="Seychelles">Seychelles</option>
                                                  <option value="Sierra Leone">Sierra Leone</option>
                                                  <option value="Singapore">Singapore</option>
                                                  <option value="Slovakia">Slovakia</option>
                                                  <option value="Slovenia">Slovenia</option>
                                                  <option value="Solomon Islands">Solomon Islands</option>
                                                  <option value="Somalia">Somalia</option>
                                                  <option value="South Africa">South Africa</option>
                                                  <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                  <option value="South Sudan">South Sudan</option>
                                                  <option value="Spain">Spain</option>
                                                  <option value="Sri Lanka">Sri Lanka</option>
                                                  <option value="Sudan">Sudan</option>
                                                  <option value="Suriname">Suriname</option>
                                                  <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                  <option value="Swaziland">Swaziland</option>
                                                  <option value="Sweden">Sweden</option>
                                                  <option value="Switzerland">Switzerland</option>
                                                  <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                  <option value="Taiwan, Republic of China">Taiwan, Republic of China</option>
                                                  <option value="Tajikistan">Tajikistan</option>
                                                  <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                  <option value="Thailand">Thailand</option>
                                                  <option value="Timor-leste">Timor-leste</option>
                                                  <option value="Togo">Togo</option>
                                                  <option value="Tokelau">Tokelau</option>
                                                  <option value="Tonga">Tonga</option>
                                                  <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                  <option value="Tunisia">Tunisia</option>
                                                  <option value="Turkey">Turkey</option>
                                                  <option value="Turkmenistan">Turkmenistan</option>
                                                  <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                  <option value="Tuvalu">Tuvalu</option>
                                                  <option value="Uganda">Uganda</option>
                                                  <option value="Ukraine">Ukraine</option>
                                                  <option value="United Arab Emirates">United Arab Emirates</option>
                                                  <option value="United Kingdom">United Kingdom</option>
                                                  <option value="United States">United States</option>
                                                  <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                  <option value="Uruguay">Uruguay</option>
                                                  <option value="Uzbekistan">Uzbekistan</option>
                                                  <option value="Vanuatu">Vanuatu</option>
                                                  <option value="Venezuela">Venezuela</option>
                                                  <option value="Viet Nam">Viet Nam</option>
                                                  <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                  <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                  <option value="Western Sahara">Western Sahara</option>
                                                  <option value="Yemen">Yemen</option>
                                                  <option value="Zambia">Zambia</option>
                                                  <option value="Zimbabwe">Zimbabwe</option>
                                          </select>
                                            <!End Start country made!>

                                 </div>
                             </div>
                          
          <!Start status made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_stat")?></label>
                                 <div class="col-sm-10 col-md-5">
                                     <select name="status" required="required">
                                         <option value="0">..</option>
                                         <option value="1">New</option>
                                         <option value="2">Like New</option>
                                         <option value="3">Used</option>
                                         <option value="4">Old</option>
                                     </select>
                                            
                                 </div>
                             </div>
       
    
       
      <!Start Category Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Admin_CAT")?></label>
                                 <div class="col-sm-10 col-md-5">
                                     <select name="categ" required="required">
                                         <option value="0">..</option>
                                        <?php
                                        
                                        foreach (getAll("categories","Name") as $cats){
                                            echo "<option value='".$cats['ID']."'>".$cats['Name']."</option>";
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
        <!Start Tags!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">Add 4 images</label>
                                 <div class="col-sm-10 col-md-5">
                                     <input type="file" name="user_files[]"
                                            class="form-control filechooserimage"
                                            accept="image/*"
                                             multiple="true"
                                            
                                             />
                                           
                                 </div>
                             </div> 
       <!Start Tags!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">Tags</label>
                                 <div class="col-sm-10 col-md-5">
                                     <input type="text" name="tags"
                                            class="form-control"
                                            autocomplete="off" 
                                            placeholder="Seprate Tags with comma(..,..)"/>
                                 </div>
                             </div> 
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" 
                                            value="<?php echo lang("itm_add")?>" 
                                            class="btn btn-primary btn-sm" />
                                 </div>
                             </div>
                         </form>
                      </div>
                    
                    
                    
                    
                    </div>
                        <div class="col-md-4">
                             <div class="thumbnail item-box live-preview">
                                 <span class="price">$</span>
                              <img class="img-responsive" src="layout/img/av.png" alt=""/>
                                  <div class="caption">
                                  <h3></h3>
                                  <p></p>
                            </div>   
                              
                      </div>
                            <div class="Add_item_errormesg"></div>
                    </div>
                </div>
                <?php
                 if(!empty($err_arr)){
             foreach ($err_arr as $err){
                 echo '<div class="alert alert-danger">';
                 echo $err.'<br>';
                 echo '</div>';
             }
         }
          if(isset($msg)){
            //  echo '<script language="javascript" type="text/javascript">swal({   title: "Auto close alert!",   text: "I will close in 2 seconds.",   timer: 2000,   showConfirmButton: false });</script>';
             echo '<div class="alert alert-success">'.$msg.'</div>';
         }
                ?>
             </div>
        </div>
    </div>
</div>



    
<?php }else{
     header('Location:login.php');
     exit;
}
       include $tpl.'Footer.php';
       ob_end_flush();
        

