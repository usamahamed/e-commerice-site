<?php 
/*========================================================= 
 *profile Page 
 * ========================================================= 
 */
ob_start();
session_start();
include 'inti.php';
$PageTitel='Profile';
if(isset($_SESSION['useruser'])){  
    $getUser=$con->prepare('SELECT * FROM Users WHERE username=?');
    $getUser->execute(array($sessionuser));
    $info=$getUser->fetch();
    $do=isset($_GET['do'])?$_GET['do']:'manage';
     if($do=="manage"){
?>
<h1 class="text-center">My Profile</h1>

         <form id="msform"  enctype="multipart/form-data"  action="?do=update" method="POST">
  <!-- progressbar -->
  <ul id="progressbar">
    <li class="active">Account Setup</li>
    <li>Social Profiles</li>
    <li>Personal Details</li>
  </ul>
  <!-- fieldsets -->
  <fieldset>
    <h2 class="fs-title">Basic Information</h2>
    <div class="exiteditprofile" >x</div>
    <h3 class="fs-subtitle">This is step 1</h3>
     <input type="text" name="uname" placeholder="User Name"  value="<?php echo $info['UserName'];?>" />
     <input type="password" name="pass" placeholder="Enter New Password"  />
     <input type="password" name="cpass" placeholder="Confirm New Password"  />
     <input type="text" name="raddr" placeholder="register Adress" value="<?php echo $info['RegisterAddress'];?>" />
     <input type="text" name="saddr" placeholder="shipping address"value="<?php echo $info['shippingaddress'];?>"  />
          <input type="text" name="usercountry" placeholder="country" value="<?php echo $info['country'];?>" />

     <input type="text" name="email" placeholder="Email" value="<?php echo $info['Email'];?>"/>
     
     <input type="number" name="zcode" placeholder="Zip code" value="<?php echo $info['Zipcode'];?>" />
    <input type="text" name="phone" placeholder="phone" value="<?php echo $info['Tel'];?>" />
    
    <input type="button" name="next" class="next next11 action-button" value="Next" />
  </fieldset>
  <fieldset>
    <h2 class="fs-title">Social Profiles</h2>
        <div class="exiteditprofile" >x</div>

    <h3 class="fs-subtitle">Your presence on the social network</h3>
        <input type="text" name="facebook" placeholder="Facebook" value="<?php echo $info['Facebook'];?>"  />
     <input type="text" name="Website" placeholder="Website" value="<?php echo $info['Website'];?>"  />

    <input type="text" name="twitter" placeholder="Twitter" value="<?php echo $info['Twitter'];?>" />
    <input type="text" name="Youtube" placeholder="Youtube" value="<?php echo $info['Youtube'];?>" />
    <input type="text" name="Instagram" placeholder="Instagram" value="<?php echo $info['Instagram'];?>" />
    <input type="button" name="previous" class="previous action-button" value="Previous" />
    <input type="button" name="next" class="next next11 action-button" value="Next" />
  </fieldset>
  <fieldset>
    <h2 class="fs-title">Personal Details</h2>
        <div class="exiteditprofile" >x</div>

    <h3 class="fs-subtitle">We will never sell it</h3>
     <input type="text" name="fname" placeholder="Full name" value="<?php echo $info['FullName'];?>" />
     <input type="text" name="Occupation" placeholder="Occupation" value="<?php echo $info['Occupation'];?>"  />
    <input type="text" name="SupplierType" placeholder="Supplier Type" value="<?php echo $info['SupplierType'];?>"  />
    <textarea name="Bio" placeholder="Bio" value="<?php echo $info['Bio'];?>" ></textarea>
        <select name="Favourite" required="required">
                                        <?php
                                        $statm1=$con->prepare('SELECT * FROM categories');
                                        $statm1->execute();
                                        $c= $statm1->fetchAll();
                                        foreach ($c as $cats){
                                           // echo "<option value='".$cats['ID']."'>".$cats['Name']."</option>";
                                            
                                            echo "<option value='".$cats['Name']."'";
                                              if($info['favgategory']===$cats['Name']){
                                              echo 'selected';
                                              
                                              }
                                            echo ">"; echo $cats['Name']."</option>";
                                            
                                        }
                                        ?>
                                     </select>
       <input class="input-group" type="file" name="user_image" accept="image/*" />

    <input type="button" name="previous" class="previous action-button" value="Previous" />
    <input type="submit" name="btnsave" class="action-button" value="Save" />
  </fieldset>
</form> 


    <div class="container">
 
            <div id="mainwrap">

                <header class="profile-tap-header">
    
      <ul id="menu">
        <li><a class="profile" href="#profile" title="Profile">Profile</a><span>Some information about me</span></li>
        <li><a class="resume" href="#resume" title="Resume">Community</a><span>Community information</span></li>
        
        <li><a class="contact" href="#contact" title="Contact">account Mange</a><span>Mange your account</span></li>
      </ul>
    </header>
    <div style="clear:both"></div>
    <div id="content">
        

      <div id="profile" class="section section12">
          <div class="row">
        <div class="column1 col-md-4">
          <table id="bioinfo">
            <tbody>
              <tr>
                <td class="odd"><i class="fa fa-unlock-alt fa-fw"></i>Log Name</td>
                <td class="even"><?php echo $info['UserName'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-envelope-o fa-fw"></i>Email</td>
                <td class="even"><?php echo $info['Email'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-user fa-fw"></i>Full Name</td>
                <td class="even"><?php echo $info['FullName'].'<br>';?></td>
              </tr>
             <tr>
                <td class="odd"><i class="fa fa-automobile fa-fw"></i>Register Address</td>
                <td class="even"><?php echo $info['RegisterAddress'].'<br>';?></td>
              </tr>
             <tr>
                <td class="odd"><i class="fa fa-crosshairs fa-fw"></i>shipping address </td>
                <td class="even"><?php echo $info['shippingaddress'].'<br>';?></td>
              </tr>
             <tr>
                <td class="odd"><i class="fa fa-apple fa-fw"></i>Zip code </td>
                <td class="even"><?php echo $info['Zipcode'].'<br>';?></td>
              </tr>
            <tr>
                <td class="odd"><i class="fa fa-cc-discover fa-fw"></i>country </td>
                <td class="even"><?php echo $info['country'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-calendar fa-fw"></i>Register Date</td>
                <td class="even"><?php echo $info['Date'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-tags fa-fw"></i>Favourite Category</td>
                <td class="even"><?php echo $info['favgategory'].'<br>';?></td>
              </tr>
              
            </tbody>
          </table>
            <br>
             <!a href="profile.php?do=edit"class="btn btn-primary"Edit Information</a>
  
            <br>
          <div class="sharewith"><span>Social:</span> 
           <i class="fa fa-facebook-square fa-2x"></i>
           <i class="fa fa-twitter fa-2x"></i>
           <i class="fa fa-google-plus fa-2x"></i>
           <i class="fa fa-pinterest fa-2x"></i>
       </div>
        </div>
              <div class=" col-md-1"></div>
              <div class=" col-md-4">
                       
                  
                      <table id="bioinfo">
            <tbody>
              <tr>
                <td class="odd"><i class="fa fa-phone fa-fw"></i>Phone</td>
                <td class="even"><?php echo $info['Tel'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-suitcase"></i>Supplier Type</td>
                <td class="even"><?php echo $info['SupplierType'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-jpy fa-fw"></i>Occupation</td>
                <td class="even"><?php echo $info['Occupation'].'<br>';?></td>
              </tr>
             <tr>
                <td class="odd"><i class="fa fa-bitcoin fa-fw"></i>Bio</td>
                <td class="even p_des"><?php echo $info['Bio'].'<br>';?></td>
              </tr>
             <tr>
                <td class="odd"><i class="fa fa-wechat fa-fw"></i>Website</td>
                <td class="even"><?php echo $info['Website'].'<br>';?></td>
              </tr>
             <tr>
                <td class="odd"><i class="fa fa-facebook fa-fw"></i>Facebook </td>
                <td class="even"><?php echo $info['Facebook'].'<br>';?></td>
              </tr>
            <tr>
                <td class="odd"><i class="fa fa-twitter fa-fw"></i>Twitter </td>
                <td class="even"><?php echo $info['Twitter'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-youtube fa-fw"></i>Youtube</td>
                <td class="even"><?php echo $info['Youtube'].'<br>';?></td>
              </tr>
              <tr>
                <td class="odd"><i class="fa fa-instagram fa-fw"></i>Instagram</td>
                <td class="even"><?php echo $info['Instagram'].'<br>';?></td>
              </tr>
              
            </tbody>
          </table>
                  
                   
                  
                  
                  
              </div>
              <div class=" col-md-2">
                  <div class="img-photo">
                  <img src="layout/Img/<?php echo $info['img'];?>" alt="profile"/>
                         <a href="#"class="edit_profile_personal btn btn-primary">Edit Information</a>

                  </div>
              </div>

          </div>
      </div>

      <div id="resume" class="section">
<div class="home-stat">
        <div class="container text-center">
                <div class="row">
                    <div class="col-md-3">
                        <div class="stat st-members">
                            <div class="info">
                                <i class="fa fa-envelope"></i>
                            Unread Message
                            <span><a href="messages.php?uid='<?php echo $_SESSION['uid']?>'"><?php echo countitemswhere("ID","messages",$id);?></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-Pending">
                            <div class="info">
                                <i class="fa fa-cart-plus"></i>
                            Cart<span><a href="cart.php?do=showcart" >
                                     <?php echo countitemswhere1("ID","cart",$id,"AND Quantity!=0");?></a></span></div></div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-items">
                            <div class="info">
                                <i class="fa fa-tag"></i>
                                <div class="info">
                                <i class="fa fa-heart"></i>
                                Wish list<span><a href="cart.php?do=showwishlist"><?php echo countitemswhere1("ID","cart",$id,"AND wishlist=1");?></a></span></div></div></div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-Comments">
                            <div class="info">
                                <i class="fa fa-tags"></i>
                                Tags<span><a href="comments.php"><?php echo countitems('c_id','comments')?></a></span></div></div>
                    </div>  
                      </div>
                </div>
</div>
      </div>

      <div id="contact" class="section">
     
          
          
          
          
          
      </div>
    </div>

   
    
  </div>
        
        
        
        
        

</div>

<div id="MY_ITEMS" class="ads block">
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Adds
                </div>
            <div class="panel-body">
                
        <?php
        if(!empty(getitms('member_id',$info['userID']))){
            echo '<div class="row">'; 
        foreach (getitms('member_id',$info['userID'],1) as $itm){
            
              echo '<figure class="Item_container_home hover1">';

            echo '<div class="image">';
                echo '<img src="layout/img/test.jpg" alt=""/><i class="ion-ios-basketball-outline"></i></div>';
                  echo '<figcaption>';
                  echo '<h3><a href="Item_details.php?itemid='.$itm['item_id'].'">'.$itm['item_name'].'</a></h3>';
                            echo '<p class="warp_content">'.$itm['description'].'</p>';
                            echo ' <div class="price">$'.$itm['price'].'</div>';
                                     echo '<fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" checked /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
  </fieldset>';
                            echo '<a href="#" class="add-to-cart">Add to Cart</a>';
                        echo '<div class="date">Added '.$itm['add_date'].'</div>';
               

                                echo '</figcaption>';
                                 echo ' </figure>';
            
            
            
            
            
            
            
            
            ///////////////////////////////////////////
          /*  echo '<div class="col-sm-6 col-md-3">';
                echo '<div class="thumbnail item-box">';
                if($itm['Approve_itm']==0){echo '<span class="Approve-item">Wating Approval..</span>';}
                  echo '<span class="price">'.$itm['price'].'</span>';
                    echo '<img class="img-responsive" src="layout/img/av.png" alt=""/>';
                        echo '<div class="caption">';
                            echo '<h3><a href="Item_details.php?itemid='.$itm['item_id'].'">'.$itm['item_name'].'</a></h3>';
                            echo '<p>'.$itm['description'].'</p>';
                             echo '<div class="date">'.$itm['add_date'].'</div>';
                        echo '</div>';  
                        
                echo '</div>';
                
                   echo '</div>';
        */}
        
        }  else {
    echo 'There is no Ads create <a href="newAds.php">New Ads</a>';
}
echo '</div>';

        ?>
            </div>
        </div>
    </div>
</div>

<div class="comns block">
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Latest Comments
            </div>
            <div class="panel-body">
                <?php
                $statm11=$con->prepare("SELECT comment  FROM comments where User_id=?");
            
            $statm11->execute(array($info['userID']));//select all items expect admin
            $comments=$statm11->fetchAll();
            if(!empty($comments)){
                foreach ($comments as $com){
                    echo '<p class="lead">'.$com['comment'].'</p>';
                }
            }  else {
            echo 'There is no comments';
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
     }

    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='update'){
             if(isset($_POST['btnsave'])){
                  echo '<h1 class="text-center"> update Iteme</h1>';
            echo '<div class="container">';
                $id=$_SESSION['uid'];
                $usr_nam=filter_input(INPUT_POST, 'uname');
                $pass=filter_input(INPUT_POST, 'pass');
                $pass1=filter_input(INPUT_POST, 'cpass');
                $raddr=filter_input(INPUT_POST, 'raddr');
                $saddr=filter_input(INPUT_POST, 'saddr');
                $email=filter_input(INPUT_POST, 'email');
                $zcode=filter_input(INPUT_POST, 'zcode');
                $country=filter_input(INPUT_POST, 'usercountry');
                $phone=filter_input(INPUT_POST, 'phone');
                $twitter=filter_input(INPUT_POST, 'twitter');
                $facebook=filter_input(INPUT_POST, 'facebook');
                $Youtube=filter_input(INPUT_POST, 'Youtube');
                $Instagram=filter_input(INPUT_POST, 'Instagram');
                $Website=filter_input(INPUT_POST, 'Website');
                $Fullname=filter_input(INPUT_POST, 'fname');
                $Occupation=filter_input(INPUT_POST, 'Occupation');
                $SupplierType=filter_input(INPUT_POST, 'SupplierType');
                $Bio=filter_input(INPUT_POST, 'Bio');
                $Favourite=filter_input(INPUT_POST, 'Favourite');
                $imgFile = $_FILES['user_image']['name'];
		$tmp_dir = $_FILES['user_image']['tmp_name'];
		$imgSize = $_FILES['user_image']['size'];
                    //make image uploading
                        $statm8=$con->prepare('SELECT * FROM Users WHERE userID=?');
                         $uid=$_SESSION['uid'];
                         $statm8->execute(array($uid));
                          $userimg=$statm8->fetch(PDO::FETCH_ASSOC);
                          $oldimg=$userimg['img'];
                          	if($imgFile)
                	{
			$upload_dir = 'layout/Img/'; // upload directory	
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			$userpic = rand(1000,1000000).".".$imgExt;
			if(in_array($imgExt, $valid_extensions))
			{			
				if($imgSize < 5000000)
				{
					unlink($upload_dir.$userimg['img']);
					move_uploaded_file($tmp_dir,$upload_dir.$userpic);
				}
				else
				{
					$errMSG = "Sorry, your file is too large it should be less then 5MB";
				}
			}
			else
			{
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";		
			}	
		}
		else
		{
			// if no image selected the old image remain as it is.
			$userpic = $oldimg ;// old image from database
                        
		}
                       
                       
                       
                       
                       //end image uploading update            
                 //validate the form
                $errorarray=array();
                if(empty($usr_nam)){$errorarray[]="Name must be <strong>not Empty</strong>";}
                if(empty($email)){$errorarray[]="Description must be <strong>not Empty</strong>";}
                if(empty($Fullname)){$errorarray[]="Price must be <strong>not Empty</strong>";}
                if(empty($Favourite)){$errorarray[]="country must be <strong>not Empty</strong>";}
                if($pass!=$pass1){$errorarray[]="password comfirm <strong>not matched</strong>";}
                
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\">'.$err.'</div><br>';
                   }
                   if(empty($errorarray)){
         
                 $statm11=$con->prepare("UPDATE Users SET img=? ,UserName=?,Email=?,FullName=?,favgategory=?,RegisterAddress=?,shippingaddress=?,Zipcode=?,Tel=? ,SupplierType=?,Occupation=?,Bio=?,Website=?,Facebook=?,Twitter=?,Youtube=?,Instagram=?,country=?  WHERE userID=?");
     $statm11->execute(array($userpic,$usr_nam,$email,$Fullname,$Favourite,$raddr,$saddr,$zcode,$phone,$SupplierType,$Occupation,$Bio,$Website,$facebook,$twitter,$Youtube, $Instagram,$country,$id));
     $mesg_track="-(User update his profile info )=>";
                    UpMsgLog($mesg_track,$_SESSION['uid'], $_SESSION['log']);
     //echo success message
                echo '<div class="container"><br><br><br><br><br>';

                   $msg= '<br><br><div class="alert alert-success">'.$statm11->rowCount().'record update</div>';
                     Redirect($msg,"back",3);
                     echo '</div>';           
     }
              
   
                       
            }//end if not post 
            else{
                     echo '<div class="container"><br><br><br>';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }    
     
?>
    
<?php }else{
     header('Location:login.php');
     exit;
}
       include $tpl.'Footer.php';
       ob_end_flush();
        
