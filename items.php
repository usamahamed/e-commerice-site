<?php 
/*========================================================= 
 *profile Page 
 * ========================================================= 
 */
ob_start();
session_start();
include 'inti.php';
$PageTitel='Show Items';
 $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
        $statm=$con->prepare('SELECT items.* ,categories.Name AS CAT_NM,Users.UserName AS US_NM FROM items '
                . 'INNER JOIN categories ON categories.ID=items.category_id INNER JOIN Users ON  Users.userID=items.member_id WHERE item_id=? AND Approve_itm=1');
        $statm->execute(array($itemid));
        $count=$statm->rowCount();            
        if($count>0){
        $items=$statm->fetch();
?>
<h1 class="text-center"><?php echo $items['item_name'] ?></h1>
      <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img class="img-responsive img-thumbnail center-block" src="layout/img/av.png" alt=""/>
                </div>
                <div class="col-md-8 item-information">
                    <h2><?php echo $items['item_name']?></h2>
                    <p class="lead p_des"><?php echo $items['description']?></p>
                   <ul class="list-unstyled">
                       <li><i class="fa fa-calendar fa-fw"></i> <span>Added Date: </span><?php echo $items['add_date']?></li>
                    <li><span><i class="fa fa-money fa-fw"></i>Price: </span>$<?php echo $items['price']?></li>
                 <li><span><i class="fa fa-building fa-fw"></i>Made in: </span><?php echo $items['country_name']?></li>
                 <li><span><i class="fa fa-tags fa-fw"></i>Category is: </span> <a href='categories.php?pageid=<?php echo $items['category_id'];?>'><?php echo $items['CAT_NM']?></a></li>
                <li> <span><i class="fa fa-user fa-fw"></i>Add by: </span><a href=""><?php echo $items['US_NM']?></a></li>
                   </ul>
                </div>
            </div>
          <?php if(isset($_SESSION['useruser'])){ ?>
                    <!Start Comments Show!>
           <hr class="C-hr">
          <div class="row">
              <div class="col-md-offset-3">
                  <div class="add-comment">
                  <h3>Add your comment</h3>
                  <form action="<?php echo $_SERVER['PHP_SELF'].'?itemid='.$items['item_id'] ?>" method="POST" >
                      <textarea name="comment"class="form-control" required="required"></textarea>
                      <input class="btn btn-primary"type="submit" value="Add comment"/>
                  </form>
                  <?php if($_SERVER['REQUEST_METHOD']=='POST'){
                      $comment=  filter_var($_POST['comment'],FILTER_SANITIZE_STRING);
                 $User_id=$_SESSION['uid'];
                 $item_id=$items['item_id'];
                 if(!empty($comment)){
                     $stat=$con->prepare('INSERT INTO comments(comment,status,date,item_id,User_id) VALUES (:zcom,0,now(),:zitem,:zuser)');
                     $stat->execute(array('zcom'=>$comment,'zitem'=>$item_id,zuser=>$User_id));
                      $itm=$items['item_name'];
                     if($stat){
                         echo '<div class="alert alert-success">Comment Sucess add</div>';
                    // $mesg_track.="-(User add comment in item $itm)=>";
                    //  $_SESSION['activity'].=$mesg_track;

                    // UpMsgLog($_SESSION['activity'],$_SESSION['uid']);
                     }
                 }
                  }?>
                  </div>
              </div>
          </div>
          <?php }else { echo '<a href="login.php">Login</a> or <a href="login.php">Register</a> to add comment';} ?>
                    <hr class="C-hr">
   <?php  
               $statm=$con->prepare("SELECT comments.*,Users.UserName AS UN1 FROM comments INNER JOIN Users ON Users.userID=comments.User_id WHERE item_id=? AND status=1 ORDER BY c_id DESC");
               $statm->execute(array($items['item_id']));//select all items expect admin
               $coms=$statm->fetchAll();
              
                  ?>
          <!Start Comments Show!>
          
           <?php   
           foreach ($coms as $c){?>
          <div class="com-box">
               <div class="row">
              <div class="col-sm-2 text-center">
                  <img class="img-responsive img-thumbnail img-circle center-block" src="layout/img/av.png" alt=""/>
                  <?php echo $c['UN1']?></div>
                   <div class="col-sm-10"><p class="lead"><?php echo $c['comment']?></p></div>

            
             </div>
          </div>
          <hr class="C-hr">
          
         <?php  }
             ?>
               
            
    
          </div>
      </div>

    
<?php 
        }//Not count >0 no such item 
        else{
            echo 'no such ID Or comment is not approved';
        }
       include $tpl.'Footer.php';
       ob_end_flush();
        
