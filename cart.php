<?php 
/*========================================================= 
 *profile Page 
 * ========================================================= 
 */
ob_start();
session_start();
//include 'inti.php';
$PageTitel='cart';
    $do=isset($_GET['do'])?$_GET['do']:'manage';

 
if(isset($_SESSION['useruser'])){  
    include 'inti.php';
     /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){
            $uid=$_SESSION['uid'];
              $itmid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
              $statment1=$con->prepare("SELECT * FROM cart WHERE uid=$uid AND itmid=$itmid ");
          $statment1->execute();
          $count1=$statment1->rowCount();
          if($count1===1){
              $statm=$con->prepare("UPDATE cart SET Quantity=Quantity+1  WHERE uid=? AND itmid=?");
                $statm->execute(array($uid,$itmid));
                 $mesg= '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>You increast item quantity to one</div>';
                  Redirect($mesg,'back');
          }
          else{
          $stat=$con->prepare("INSERT INTO cart(date,itmid,uid) VALUES(now(),?,?)");
          $stat->execute(array($itmid,$uid));
            $mesg= '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i> Rate inserted</div>';
               Redirect($mesg,'back',5);
        
          }
            
            
        }//end do = manage
        elseif($do=="showcart"){
            $statment=$con->prepare("SELECT cart.*,items.item_name AS name,items.image AS IMG,items.description AS des"
                    . ",items.price AS pri"
                    . " FROM cart"
                    . " INNER JOIN items ON items.item_id=cart.itmid"
                    . " WHERE uid=? AND Quantity!=0");
             $statment->execute(array($_SESSION['uid']));//select all items expect admin
            $itms=$statment->fetchAll();
            if(!empty($itms)){
            ?>
                
    <div class="cartcontent">
  <h1>Shopping Cart</h1>
  <p>Update quantities or remove items from your cart. When you are ready to checkout, click the &ldquo;Checkout Now&rdquo; button.</p>
  
  <p class="removeAlert">
    Item has been removed
  </p>
  
  <table class="items">
    <thead>
      <tr>
        <th>Product</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Total</th>
      </tr>
    </thead>
     <?php
          foreach ($itms as $item){
               $imagarray=explode(",", $item['IMG']);
              ?>
    <tbody>
        
      <tr>
        <td>
          <div class="item">
            <div class="item-front">
                <img src="layout/Img/items/<?php echo $imagarray[0] ?>" />
            </div>
            <div class="item-back">
                <img src="layout/Img/items/<?php echo $imagarray[1] ?>" />
            </div>
          </div>
          <p>
            <?php echo $item['name'] ?><sup>&reg;</sup><br />
            <span class="itemNum">CM-6A</span>
          </p>
          <p class="description1">In Stock</p>
       <p class="description">FREE Shipping</p>
        </td>
        <td class="text-center">$<?php echo $item['pri'] ?></td>
        <td class="text-center">
          <input type="number" class="quantity" value="<?php echo $item['Quantity'] ?>" min="1" />
          <a href="#" class="remove1 text-center">Remove</a>
          <input type="hidden" class="itmid" name="itmid" value="<?php echo $item['itmid']?>"/>
         <input type="hidden" class="userid" name="itmid" value="<?php echo $_SESSION['uid']?>"/>
        
          
        </td>
        <td class="itemTotal text-center">$<?php echo $item['pri'] ?></td>
      </tr>

 
    </tbody>
                                    <?php }
                                    ?>
  </table>
  
  <div class="cost">
    <h2>Order Overview</h2>
    
    <table class="pricing">
      <tbody>
        <tr>
          <td>Subtotal</td>
          <td class="subtotal"></td>
        </tr>
        <tr>
          <td>Tax (5%)</td>
          <td class="tax"></td>
        </tr>
        <tr>
          <td>Shipping</td>
          <td class="shipping">$10.00</td>
        </tr>
        <tr>
          <td><strong>Total:</strong></td>
          <td class="orderTotal"></td>
        </tr>
      </tbody>
    </table>
    
    <a class="cta" href="#">Checkout Now &raquo;</a>
  </div>
</div> <!-- End Content -->
                
                
                
                <?php
            }else{
                echo '<div class="container"><div class="message">there is no items in cart</div></div>';
            }
        }

        elseif($do=="showwishlist"){
            $statment=$con->prepare("SELECT cart.*,items.item_name AS name,items.image AS IMG,items.description AS des"
                    . ",items.price AS pri"
                    . " FROM cart"
                    . " INNER JOIN items ON items.item_id=cart.itmid"
                    . " WHERE uid=? AND wishlist=1");
             $statment->execute(array($_SESSION['uid']));//select all items expect admin
            $itms=$statment->fetchAll();
            if(!empty($itms)){
            ?>
                
    <div class="cartcontent">
  <h1>Wish List</h1>
  <p>Update quantities or remove items from your cart. When you are ready to checkout, click the &ldquo;Checkout Now&rdquo; button.</p>
  
  <p class="removeAlert">
    Item Deleted successfully
  </p>
  
 <p class="AddAlert">
    Item successfully add to cart
  </p>
  
  <table class="items">
    <thead>
      <tr>
        <th>Product</th>
        <th>Price</th>
        <th>options</th>
      </tr>
    </thead>
     <?php
          foreach ($itms as $item){
                             $imagarray=explode(",", $item['IMG']);

              ?>
    <tbody>
        
      <tr>
        <td>
          <div class="item">
            <div class="item-front">
                <img src="layout/Img/items/<?php echo $imagarray[0] ?>" />
            </div>
            <div class="item-back">
                <img src="layout/Img/items/<?php echo $imagarray[1] ?>" />
            </div>
          </div>
          <p>
            <?php echo $item['name'] ?><sup>&reg;</sup><br />
            <span class="itemNum">CM-6A</span>
          </p>
          <p class="description1">In Stock</p>
       <p class="description">FREE Shipping</p>
        </td>
        <td class="text-center">$<?php echo $item['pri'] ?></td>
        <td class="text-center">
          <a href="#" class="btn btn-danger remove">Remove</a>
          <input type="hidden" class="itmid" name="itmid" value="<?php echo $item['itmid']?>"/>
         <input type="hidden" class="userid" name="itmid" value="<?php echo $_SESSION['uid']?>"/>
         <a href="#" class="remove3 btn btn-primary">Cart</a>
         <input type="hidden" class="itmid" name="itmid" value="<?php echo $item['itmid']?>"/>
         <input type="hidden" class="userid" name="itmid" value="<?php echo $_SESSION['uid']?>"/>
         <a href="#" class="btn btn-success">Later</a>
        </td>
       
      </tr>

 
    </tbody>
                                    <?php }
                                    ?>
  </table>
  
</div> <!-- End Content -->
                
                
                
                <?php
            }else{
                echo '<div class="container"><div class="message">there is no items in wish list</div></div>';
            }
        }

      

     
  } else{
     header('Location:login.php');
     exit;
}
       include $tpl.'Footer.php';
       ob_end_flush();
        
