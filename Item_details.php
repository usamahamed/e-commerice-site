<?php 
/*========================================================= 
 *Item Page 
 * ========================================================= 
 */
ob_start();
session_start();
$PageTitel='item details';
include 'inti.php';
$itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
 
        $statm=$con->prepare('SELECT items.* ,categories.Name AS CAT_NM,Users.FullName AS US_NM,Users.userID AS UID FROM items '
                . 'INNER JOIN categories ON categories.ID=items.category_id INNER JOIN Users ON  Users.userID=items.member_id WHERE item_id=? AND Approve_itm=1');
        $statm->execute(array($itemid));
        $count=$statm->rowCount();            
        if($count>0){
        $items=$statm->fetch();
        $imagarray=explode(",", $items['image']);
        //insert Item ID for history log
        
        //end insertion 
        
        
        $statm1=$con->prepare("SELECT * FROM comments WHERE item_id=$itemid");
         $statm1->execute();
         $numreview=$statm1->rowCount();
         if(isset($_SESSION['useruser'])){
   
        $logitems =$itemid.',';
        $uid=$_SESSION['uid'];
        $statm11=$con->prepare("SELECT * FROM itemhistory WHERE uid=$uid");
         $statm11->execute();
         $count=$statm11->rowCount();
         if($count===0){
        $stat=$con->prepare('INSERT INTO itemhistory(itemlogs,uid) VALUES (:zlog,:zuser)');
       $stat->execute(array('zlog'=>$logitems,'zuser'=>$_SESSION['uid']));
      
         }  else {
             $sta1tm=$con->prepare("UPDATE itemhistory SET itemlogs=CONCAT(itemlogs,?) WHERE uid=?");
             $sta1tm->execute(array($logitems,$uid));
             $statmm1=$con->prepare("SELECT * FROM itemhistory WHERE uid=$uid");
             $statmm1->execute();
             $itemsl=$statmm1->fetch();
             $logs2=$itemsl['itemlogs'];
             $logs3 = implode(',',array_unique(explode(',', $logs2)));
             $statm=$con->prepare("UPDATE itemhistory SET itemlogs=? WHERE uid=?");
             $statm->execute(array($logs3,$uid));
               
         }
         
        }         
?>
   <div id="wrap">
       <div class="thumb-show"><img id ="x" src="layout/Img/items/<?php echo $imagarray[0] ; ?>" alt="img"/>
           <div class="preview pull-right"></div>
       </div>
       <div class="zoom-hint"><i class="fa fa-search-plus"></i> Mouse over image to zoom</div>
   
     <div class="sub_images">
         <div id ="x1"><img id ="x" src="layout/Img/items/<?php echo $imagarray[0] ; ?>" alt="img"/></div>
         <div id ="x2"><img id ="x" src="layout/Img/items/<?php echo $imagarray[1] ; ?>" alt="img"/></div>
         <div id ="x3"><img id ="x" src="layout/Img/items/<?php echo $imagarray[2] ; ?>" alt="img"/></div>
         <div id ="x4"><img id ="x" src="layout/Img/items/<?php echo $imagarray[3] ; ?>" alt="img"/></div>
       </div>
      
  <div id="product_layout_3">
    
    <div class="product_desc">
        
      <h1><?php echo $items['item_name'] ?></h1>
      <span class="num-review"><i class="fa fa-registered"></i> <?php echo $numreview ?> review in item  </span>
      <span class="rs rs-fr">
					<i class="fullStar"></i>
					<i class="fullStar"></i>
					<i class="fullStar"></i>
					<i class="fullStar"></i>
					<i class="midStar"></i>
             <a href="rallating.php?do=itemrate&name=<?php echo $items['US_NM']; ?>&itmid=<?php echo $_GET['itemid']; ?>">  Rate Item </a> 
             <div class="freeship pull-right"><img id ="x" src="layout/Img/ship.jpeg" alt="img"/></div>
				</span>
       <hr class="C-hr">
       
       <div class="sharewith  pull-right"><span>share with:</span> 
           <i class="fa fa-facebook-square fa-2x"></i>
           <i class="fa fa-twitter fa-2x"></i>
           <i class="fa fa-google-plus fa-2x"></i>
           <i class="fa fa-pinterest fa-2x"></i>
       </div>
       <span class="condition-lable">Item Condition: </span>
       <span class="condition">
       <?php
       if($items['status']==='1'){
           echo 'New';
       }elseif($items['status']==='2'){
            echo 'Like New';
       }
      elseif($items['status']==='3'){
            echo 'Used';
       }
      elseif($items['status']==='4'){
            echo 'Old';
       }
       ?>
       </span><br>
       <div class="quantity">
 <span class="condition-lable">Quantity: </span>
 <input type="text"><span class="condition-lable-aval">Available <span class="avalible"><?php echo $items['avalible'] ?></span> Items</span>
                 </div>
      <div class="container price-cont">
          <div class="row">
              <div class="col-md-6">
                  <br>
                   <span class="condition-lable">price: <span class="pri">$<?php echo $items['price'] ?></span></span>
                  <p class="shipping"><i class="fa fa-ship" aria-hidden="true"></i> Free shipping</p>
              </div>
          <div class="col-md-6"><br>
              <span class="condition-lable">Country: <span class="pri"><?php echo $items['country_name'] ?></span></span>
              <p class="shipping"><i class="fa fa-archive" aria-hidden="true"></i> In Stock</p>
          </div>
          </div>
      </div>
          <div class="buying">
                 
                 <div class="cart">
                     <a href="cart.php?itemid=<?php echo $_GET['itemid']; ?>" class="add"><i class="fa fa-cart-plus fa-lg"></i> Add to Cart</a>
               <a class=" add1"><i class="fa fa-heart fa-heart1 fa-lg"></i> Wish list</a>
               <?php
               echo '<input type="hidden" name="itemid" class="itemid" value="'.$_GET['itemid'].'"/>';
               echo '<input type="hidden" name="uid" class="uid" value="'. $_SESSION['uid'].'"/>';
               ?>
               <a class="add2"><i class="fa fa-credit-card fa-lg"></i> Check out</a>
                 </div>
          </div>
     
       
          
          <div class="description">
              <span class="condition-lable">Import charges: <span class="avalible1"><span class="bold">No additional import charges at delivery!</span>
                      This item will be shipped through the Global <span class="avalible2">Shipping Program and includes international tracking</span></span></span><br><br>
                      <span class="condition-lable">Delivery:<span class="bold1"><?php $date = strtotime("+7 day");
                      echo date('M d, Y', $date);?></span> Includes international tracking</span><br><br>
                      <span class="condition-lable">Add By:<span class="bold1"><a href="profileg.php?id=<?php echo $items['UID'] ?>"><?php echo $items['US_NM']?></a></span> Send him message</span>
                      
                      <br><br>  <div class="container price-cont">
          <div class="row">
              <div class="col-md-6">
                  <br>
    <p class="shipping1"><i class="fa fa-info-circle" aria-hidden="true"></i>shipping Info</p>
    <p class="shipping1"><i class="fa  fa-user-secret" aria-hidden="true"></i> Secure Shopping</p>             
              </div>
          <div class="col-md-6"><br>
    <p class="shipping1"><i class="fa fa-credit-card" aria-hidden="true"></i> Easy monthly Payment</p>            
    <p class="shipping1"><i class="fa fa-tachometer" aria-hidden="true"></i> 60 Day money back Guarantee</p>
          </div>
          </div>
                        </div><br>
                      
                      
                      <div id="payDet1" class="">
              <span class="condition-lable">Payment: </span>  <img class="pd-img" src="layout/Img/logoPayPal_51x14.png" alt="PayPal" border="0">		
												<span>&nbsp;|&nbsp;<a rel="nofollow"></a>
						<a style="text-decoration: none;">
								<span>
									<img src="layout/Img/CC_icons.png" alt="Visa/MasterCard, Amex, Discover" title="Visa/MasterCard, Amex, Discover" class="pd-pp-cc-container">																			
								</span>
								</a>
                                                 <div class="u-cb spcr">
                                                                           <span class="pd-pp-cc-logo">Processed by PayPal</span>
                                                 </div>
						</span>
			</div>
                        
           
       </div>
         <?php 
             $tag=  explode(",", $items['tags']);
             
             foreach ($tag as $t){
                 $t= str_replace(' ', '', $t);
                 $tlower= strtolower($t);
                    if(!empty($t)){
             echo '<a href="tags.php?name='.$tlower.' "><i class="fa fa-plus" aria-hidden="true"></i>'.$t.'</a>  ';
             }}
             ?>
              <hr>
 <a href="rallating.php?do=insertincorrectitem&itm=<?php echo $_GET['itemid']; ?>" class="pull-right reportincorrect" ><i class="fa fa-bell-o" aria-hidden="true" data-toggle="modal" data-target="#myModal"></i>Report incorrect item info</a>
 <hr>
        </div>
     
          
      </div>
      <div class="tabs_item" ng-app ng-init="tab=1">
	
<ul class="tabs-nav">
  <li><a ng-click="tab=1" ng-class="{active: tab == 1}">Product Details</a></li>
  <li><a ng-click="tab=2" ng-class="{active: tab == 2}">Write your Review</a></li>
  <li><a ng-click="tab=3" ng-class="{active: tab == 3}">FAQ'S</a></li>
</ul>

	<div class="tabs-container">
		<div class="tab-content" ng-show="tab == 1">
        <h3>Add By: <a href="profileg.php?id=<?php echo $items['UID'] ?>"><?php echo $items['US_NM']?></a></h3>
      <?php  $date = new DateTime($items['add_date']);
        echo '<span class="pull-left date data_span">'.$date->format('g:ia l jS F').'</span>';
      ?>
        
          
        <div> <fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full full1" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" checked /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset></div>
			<p class="lead"><?php echo $items['description']?></p>
		</div>

		<div class="tab-content" ng-show="tab == 2">
			<h3>Write your Review</h3>
                           <?php if(isset($_SESSION['useruser'])){ ?>
                    <!Start Comments Show!>
           <hr class="C-hr">
          <div class="row">
              <div class="col-md-offset-3">
                  <div class="add-comment">
                  <h3>Add your Review</h3>
                  <form action="<?php echo $_SERVER['PHP_SELF'].'?itemid='.$items['item_id'] ?>" method="POST" >
                      <textarea name="comment"class="form-control" required="required"></textarea>
                      <input class="btn btn-primary"type="submit" value="Add Review"/>
                  </form>
                  <?php if($_SERVER['REQUEST_METHOD']=='POST'){
                      $comment=  filter_var($_POST['comment'],FILTER_SANITIZE_STRING);
                 $User_id=$_SESSION['uid'];
                 $item_id=$items['item_id'];
                 //check if user rate item first
                 $statment=$con->prepare("SELECT * FROM itemrate WHERE itemid=$item_id AND uid	= $User_id ");
                  $statment->execute();
                   $count1=$statment->rowCount();
                 
                 if(!empty($comment) && $count1!=0){
                     $stat=$con->prepare('INSERT INTO comments(comment,status,date,item_id,User_id) VALUES (:zcom,0,now(),:zitem,:zuser)');
                     $stat->execute(array('zcom'=>$comment,'zitem'=>$item_id,zuser=>$User_id));
                      $itm=$items['item_name'];
                     if($stat){
                         echo '<div class="alert alert-success">Comment Sucess add</div>';
                     $mesg_track="-(User add comment in item $itm)=>";
                    UpMsgLog($mesg_track,$_SESSION['uid']);
                     }
                 }else{
                      echo '<div class="alert alert-danger">you must rating Item before adding review</div>';
                 }
                  }?>
                  </div>
              </div>
          </div>
          <?php }else { echo '<a href="login.php">Login</a> or <a href="login.php">Register</a> to add comment';} ?>
		</div>

		<div class="tab-content" ng-show="tab == 3">
                    <div class="container">
                        <a class="switchlinlkask" href="#">Ask Question</a>
                        <div class="askqus">
                           
                            <div class="switchask">
                               
                                    <label>Ask your Question</label>
                                    <textarea name="question1" class="FAQquestion1"></textarea>
                                    <a class="btn btn-primary btn-lg FAQ_Send_Question" href="#">Send question</a>
                                    <input type="hidden" value="<?php echo $_GET['itemid'] ?>" name="itmid"/>
                                    <input type="hidden" value="<?php echo $_SESSION['uid'] ?>" name="uid"/>
                                    
                                <span class="resultFAQ"><i class="fa fa-spinner" aria-hidden="true"></i> Send Your question........</span>
                            </div>
                        </div>
                        
                            <?php
                            $itemid=$_GET['itemid'];
                          $ques= genral_feth("*","FAQ","WHERE Itmid=$itemid","","ID");
                         
                           $count=0;
                           foreach ($ques as $q){ 
                               $qid=$q['ID'];
       $statm11=$con->prepare("SELECT FAQAnswer.*,Users.FullName AS FUlN FROM FAQAnswer INNER JOIN Users ON Users.userID=FAQAnswer.uid WHERE Qid=?");
            $statm11->execute(array($qid));//select all items expect admin
            $ans=$statm11->fetchAll();
                            ?>
                        <div class="FAQ">
                            <a href="#" class="togglefaq"><i class="fa fa-plus-square"></i> <?php echo $q['Question'] ;?> 
                               
                            </a>
                            
          <div class="faqanswer">
              <?php  foreach ($ans as $q1){ ?>
            <p><?php echo $q1['answer'] ;?>
</p>
<aside class="ANSUSEINFO">By: <?php echo $q1['FUlN'] ?> in <?php  $date = new DateTime($q1['date']);
        echo $date->format('g:ia l jS F');
      ?> </aside>
           <hr class="C-hr">

     <?php }?>
   </div>
                            
                       
                            <span data-toggle="modal" data-target="#myModal<?php echo $count?>">answer</span>
                       
                        </div>   
                         
  

<!-- Modal Answer -->
<div class="modal fade" id="myModal<?php echo $count?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $q['Question'];?></h4>
      </div>
      <div class="modal-body">
          
                                   
                                    <textarea name="question1" class="FAQquestion2"></textarea>
                                    <button class="btn btn-primary btn-lg FAQ_Send_Question1" href="#">Send Answer</button>
                                    <input type="hidden" value="<?php echo $q['ID'] ?>" name="qid"/>
                                    <input type="hidden" value="<?php echo $_SESSION['uid'] ?>" name="qid"/>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    
                                <span class="resultFAQAns1"><i class="fa fa-spinner" aria-hidden="true"></i> Send Your Answer........</span>
                                <span class="resultFAQAns2"><i class="fa fa-check" aria-hidden="true"></i>Your Answer successfully sent</span>
                                
      </div>
    
    </div>
  </div>
</div>
                        
                        
                        
                        
                        
                         <?php $count++;}
                           ?>
                       
                    </div>
         
		</div>

	</div>

</div>
      <hr class="C-hr">
      <div class="item_Statatistics"
      <div class='ui'>
  <div class='ui_box'>
    <div class='ui_box__inner'>
      <h2>
        Review Rate
      </h2>
      <p>how many members reviews item</p>
      <div class='stat'>
        <span>58%</span>
      </div>
      <div class='progress'>
        <div class='progress_bar'></div>
      </div>
      <p>US-shop calculates a product’s star ratings using a machine learned model</p>
    </div>
    <div class='drop'>
      <div class='arrow'></div>
    </div>
  </div>
  <div class='ui_box'>
    <div class='ui_box__inner'>
      <h2>
        seen rate
      </h2>
      <p>how many members see item</p>
      <div class='stat_left'>
              <span>40%</span>

      </div>
      <div class='progress_graph'>
        <div class='progress_graph__bar--1'></div>
        <div class='progress_graph__bar--2'></div>
        <div class='progress_graph__bar--3'></div>
        <div class='progress_graph__bar--4'></div>
      </div>
      <p>seen rate of item according to added date</p>
    </div>
    <div class='drop'>
      <div class='arrow'></div>
    </div>
  </div>
  <div class='ui_box'>
    <div class='ui_box__inner'>
      <h2>
        Total Sales
      </h2>
      <p>total items selling</p>
      <div class='stat'>
        <span>$34,403.93</span>
      </div>
      <div class='progress'>
        <div class='progress_bar--two'></div>
      </div>
      <p>total selles amount based on date added</p>
    </div>
    <div class='drop'>
      <div class='arrow'></div>
    </div>
  </div>
</div>
  </div>
            <hr class="C-hr">

      <h2>Customer Reviews</h2>
                     <?php  
               $statm=$con->prepare("SELECT comments.*,Users.FullName AS UN1,Users.userID AS UID1,Users.img AS IMG FROM comments INNER JOIN Users ON Users.userID=comments.User_id WHERE item_id=? AND status=1 ORDER BY c_id DESC");
               $statm->execute(array($items['item_id']));//select all items expect admin
               $coms=$statm->fetchAll();
              
                  ?>
          <!Start Comments Show!>
          
           <?php   
           foreach ($coms as $c){
               $cid=$c['c_id'];
               try{
                $s1=$con->prepare("SELECT ID FROM rating WHERE Rating='1' AND r_review=$cid");
                $s1->execute();
               $s2=$con->prepare("SELECT ID FROM rating WHERE Rating='2' AND r_review=$cid");
                $s2->execute();
               $s3=$con->prepare("SELECT ID FROM rating WHERE Rating='3' AND r_review=$cid");
                $s3->execute();
               $s4=$con->prepare("SELECT ID FROM rating WHERE Rating='4' AND r_review=$cid");
                $s4->execute();
               }  catch (PDOException $e){ echo $e->getMessage();}
              $poor=$s1->rowCount();
              $Average=$s2->rowCount();
              $Helpful=$s3->rowCount();
              $Buddy=$s4->rowCount();
               //select item rating
              //check if user rate item first
              $item_id=$_GET['itemid'];
              $User_id=$_SESSION['uid'];
                 $statment=$con->prepare("SELECT * FROM itemrate WHERE itemid=$item_id AND uid= $User_id ");
                  $statment->execute();
                   $ratedetails=$statment->fetch();
                   $rate=$ratedetails['rate'];
                   switch ($rate){
                       case 1 :
                           $star1='fullStar'; $star2 ='noStar';$star3='noStar'; $star4 ='noStar';
                           $star5='noStar';
                                  break;
case 2 :
                           $star1='fullStar'; $star2 ='fullStar';$star3='noStar'; $star4 ='noStar';
                           $star5='noStar';
                                  break;
case 3 :
                           $star1='fullStar'; $star2 ='fullStar';$star3='fullStar'; $star4 ='noStar';
                           $star5='noStar';
                                  break;
case 4 :
                           $star1='fullStar'; $star2 ='fullStar';$star3='fullStar'; $star4 ='fullStar';
                           $star5='noStar';
                                  break;
case 5 :
                           $star1='fullStar'; $star2 ='fullStar';$star3='fullStar'; $star4 ='fullStar';$star5='fullStar';
                           
                                  break;
  default:
      

              $star1='fullStar'; $star2 ='fullStar';$star3='fullStar'; $star4 ='fullStar';$star5='fullStar';
                                    
                                                         break;
                                                    
                   }
              
              
               ?>
          <div class="com-box">
               <div class="row">
              <div class="col-sm-2 text-center">
                  <img class="img-responsive img-thumbnail img-circle center-block" src="layout/img/<?php 
                  if($c['IMG']!=''){
                  echo $c['IMG'];}  else {
                     echo 'av.png';
}?>" alt=""/>
                  <a class="UN"href="profileg.php?id=<?php echo $c['UID1'] ?>">By: <?php echo $c['UN1']?></a>
           <?php  $date = new DateTime($c['date']);
        echo '<p class=" date">'.$date->format('g:ia l jS F').'</p>';
      ?>
                  
     <div>  <span class="rs rs-fr">
					<i class="<?php echo $star1 ?>"></i>
					<i class="<?php echo $star2 ?>"></i>
					<i class="<?php echo $star3 ?>"></i>
					<i class="<?php echo $star4 ?>"></i>
					<i class="<?php echo $star5 ?>"></i>
				</span></div>
              </div>
                   <div class="col-sm-7"><p class="lead"><?php echo $c['comment']?></p>
                       <div class="report_abuse">
                           <a class="show_comments"><i class="fa fa-comments"></i>comment</a><span class="delemiter"> |
                           
                           </span> 
                           <span class="content_show">
                           <span><i class="fa fa-question-circle" aria-hidden="true"></i>are this comment helpful?
                               <a href="rallating.php?do=rat&rid=<?php echo $c['c_id'] ?>&itmid=<?php echo $c['item_id'] ?>
                                  &uid=<?php echo $_SESSION['uid']; ?>" class="btn btn-default btn-xs content_show_yes"
                                >
                               yes</a>
                           <a href="rallating.php?do=rat1&rid=<?php echo $c['c_id'] ?>&itmid=<?php echo $c['item_id'] ?>
                                  &uid=<?php echo $_SESSION['uid']; ?>"class="btn btn-default btn-xs content_show_no">no</a>
                       </span>
                           
                           <span class="contnet_hidden">
                               <i class="fa fa-check"></i> send feed back....
                           </span>
                               </span>
                           </span>
      <a href="rallating.php?do=insertinapropratereview&rid=<?php echo $c['c_id'] ?>&itm=<?php echo $_GET['itemid']; ?>" class="pull-right report" ><i class="fa fa-bell-o" aria-hidden="true" data-toggle="modal" data-target="#myModal"></i>Report abuse</a>
       
                       
                       
                       </div>  
                                 <hr class="C-hr">

                   <div class="the_container">
                     <!start view review comments!>
    <?php
    $statm2=$con->prepare("SELECT com_on_review.*,Users.FullName AS UN1,Users.img AS IMG FROM com_on_review "
            . "INNER JOIN Users ON Users.userID=com_on_review.uid WHERE rid=?");
               $statm2->execute(array($c['c_id']));//select all items expect admin
               
               $coms1=$statm2->fetchAll();
               if($coms1){
                   
                 foreach ($coms1 as $c1){
                     if($c1['r_comm']!=null){?>
                     <div class="rev_comm">
                     <div class="container">
                         <div class="row">
                             <div class="col-md-3">
                  <img class="img-responsive img-thumbnail center-block" src="layout/img/<?php 
                  if($c1['IMG']!=''){
                  echo $c1['IMG'];}  else {
                     echo 'av.png';
}?>" alt=""/>
                  <a class="UN1"href="profileg.php?id=<?php echo $c1['uid'] ?>"><?php echo $c['UN1']?></a>
           <?php  $date = new DateTime($c1['date']);
        echo '<p class=" date">'.$date->format('g:ia l jS F').'</p>';
      ?>
                             </div>
                             <div class="col-md-9">
                                 <p class="lead com_con"><?php echo $c1['r_comm']?></p>
                             
                             
                             
                             </div>
              </div>
        
                     </div>
                     </div>
    
    
               <?php 
               
                 }}}else{
    echo 'There is no comments added';
               }
  ?>
    <!END view review comments!>
                   
                   
                   </div>
                   </div>
                   <div class="c_meter">
<div class="col-sm-3">
    <h3>Rating this Review</h3>
    <div class="c_meter">
    <table class="rating-details-histogram ">
  <tbody>
    <tr class="n-5">
      <th scope="row" class="histogram-label nowrap">
        Buddy
      </th>
      <td>
        <div class="histogram-bar" style="width: 100%;">
          <span class="text-inside"><?php echo $Buddy ?></span>
        </div>
      </td>
    </tr>

    <tr class="n-4">
      <th scope="row" class="histogram-label nowrap">
Helpful
      </th>
      <td>
        <div class="histogram-bar" style="width: 78%;">
          <span class="text-inside"><?php echo $Helpful ?></span>
        </div>
      </td>
    </tr>

    <tr class="n-3">
      <th scope="row" class="histogram-label nowrap">
       Average
      </th>
      <td>
        <div class="histogram-bar" style="width: 34%;">
          <span class="text-inside"><?php echo $Average ?></span>
        </div>
      </td>
    </tr>

    <tr class="n-2">
      <th scope="row" class="histogram-label nowrap">
        Poor
      </th>
      <td>
        <div class="histogram-bar" style="width: 15%;">
        <span class="text-inside"><?php echo $poor ?></span></div>
      </td>
    </tr>
  </tbody>
</table>
        <a href="rallating.php?do=manage&name=<?php echo $c['UN1']; ?>&idonwm=<?php echo $c['UID1']; ?>&cid=<?php echo $c['c_id']; ?>" class="btn btn-default">Write Comment</a>
        <a href="rallating.php?do=review&itm=<?php echo $_GET['itemid']; ?>&name=<?php echo $c['UN1']; ?>&idonwm=<?php echo $c['UID1']; ?>&cid=<?php echo $c['c_id']; ?>" class="btn btn-success">review rate</a>
    </div>
  
    
</div>
                   </div>
            
             </div>
          </div>
          <hr class="C-hr">
          
         <?php  }
             ?>
       <?php 
        }//Not count >0 no such item 
        else{
            echo 'no such ID Or comment is not approved';
        }
       include $tpl.'Footer.php';
       ob_end_flush();