<?php ob_start(); ?>    

<?php

/*========================================================= 
 *Comments Page 
 * ========================================================= 
 */
        session_start();
        $PageTitel='FAQ';

    if(isset($_SESSION['user'])){
        include 'inti.php';
        $do=isset($_GET['do'])?$_GET['do']:'manage';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){ //manage memeber page
       
            //fetch data from database to put into table
            try{
          $statm11=$con->prepare("SELECT FAQ.*,Users.FullName AS FUlN,items.item_name AS ITM FROM FAQ INNER JOIN Users ON Users.userID=FAQ.uid "
                  . "INNER JOIN items ON items.item_id=FAQ.Itmid");
            $statm11->execute();//select all items expect admin
            $rows=$statm11->fetchAll();

            }catch(PDOException $e)
                             {
                             echo $e->getMessage();   }     
                              
                                  if(!empty($rows)){
                             
            ?>
        <h1 class="text-center">Mange FAQ Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>
                        <tr>
                               <td>#ID</td>
                               <td>Question</td>
                               <td>item Name</td>
                               <td>user Name</td>
                               <td>Add Date</td>
                               <td>Control</td>
                        </tr>
                            </thead>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['ID'].'</td>';
                echo'<td data-popup="'.$row['Question'].'" class="p_des"><a href="FAQ.php?do=answer&ques='.$row['Question'].'&id='.$row['ID'].'">'.$row['Question'].'</a></td>';
                                             echo'<td data-popup="'.$row['ITM'].'">'.$row['ITM'].'</td>';
                                             echo'<td>'.$row['FUlN'].'</td>';
                                              echo'<td>'.$row['date'].'</td>';
                            echo'<td> '
                                              . '<a href="FAQ.php?do=deleteques&qid='.$row['ID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($row['status']==0){
                                                    echo '<a href="FAQ.php?do=Approveques&qid='.$row['ID'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>

            </div>
       <?php  }else {echo '<div class="container><div class="message">there is no Categories</div></div>';}}//end if do=manage
         /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="answer"){ //manage memeber page
    $qid= isset($_GET['id'])&& is_numeric($_GET['id']) ?intval($_GET['id']):0;
    $ques=$_GET['ques'];

            //fetch data from database to put into table
            try{
   $statm11=$con->prepare("SELECT FAQAnswer.*,Users.FullName AS FUlN FROM FAQAnswer INNER JOIN Users ON Users.userID=FAQAnswer.uid WHERE Qid=?");
            $statm11->execute(array($qid));//select all items expect admin
            $rows=$statm11->fetchAll();
            }catch(PDOException $e)
                             {
                             echo $e->getMessage();   }     
                              
                                  if(!empty($rows)){
                             
            ?>
        <h1 class="text-center"><?php echo $ques; ?></h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>
                        <tr>
                               <td>#ID</td>
                               <td>Answer</td>
                               <td>user Name</td>
                               <td>Add Date</td>
                               <td>Control</td>
                        </tr>
                            </thead>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['ID'].'</td>';
                                            echo'<td data-popup="'.$row['answer'].'" class="p_des" >'.$row['answer'].'</td>';
                                             echo'<td>'.$row['FUlN'].'</td>';
                                              echo'<td>'.$row['date'].'</td>';
                            echo'<td><a href="FAQ.php?do=deleteanswer&qid='.$row['ID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                       if($row['status']==0){
                                                    echo '<a href="FAQ.php?do=Approveanswer&qid='.$row['ID'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }       
                                               
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>

            </div>
       <?php  }else {echo '<div class="container><div class="message">there is no comments</div></div>';}}//end if do=manage

 
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
        elseif ($do==='deleteques') {
        //delete Member
            echo '<h1 class="text-center"> Delete Question</h1>';
            echo '<div class="container">';
                    $qid= isset($_GET['qid'])&& is_numeric($_GET['qid']) ?intval($_GET['qid']):0;
                    
                   $check=  CheckItems("ID", "FAQ", $qid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM FAQ WHERE ID=:zqid');
                        $statm->bindParam(":zqid",$qid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
             }
               /*========================================================= 
            *
            * if do==deletecomment
            * 
    * ========================================================= 
    */
        elseif ($do==='deleteanswer') {
        //delete Member
            echo '<h1 class="text-center"> Delete answer</h1>';
            echo '<div class="container">';
                    $qid= isset($_GET['qid'])&& is_numeric($_GET['qid']) ?intval($_GET['qid']):0;
                    
                   $check=  CheckItems("ID", "FAQAnswer", $qid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM FAQAnswer WHERE ID=:zqid');
                        $statm->bindParam(":zqid",$qid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
             }
    /*========================================================= 
            *
            * if do==activate Question
            * 
    * ========================================================= 
    */
             elseif($do==='Approveques'){
                 //Aprove users in database
        echo '<h1 class="text-center"> Approve Question</h1>';
            echo '<div class="container">';
                    $qid= isset($_GET['qid'])&& is_numeric($_GET['qid']) ?intval($_GET['qid']):0;
                    
                   $check=  CheckItems("ID", "FAQ", $qid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE FAQ SET status=1 WHERE ID=?');
                        $statm->execute(array($qid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Activated</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';             
            
                 }
                   /*========================================================= 
            *
            * if do==activate Answer
            * 
    * ========================================================= 
    */
             elseif($do==='Approveanswer'){
                 //Aprove users in database
        echo '<h1 class="text-center"> Approve Question</h1>';
            echo '<div class="container">';
                    $qid= isset($_GET['qid'])&& is_numeric($_GET['qid']) ?intval($_GET['qid']):0;
                    
                   $check=  CheckItems("ID", "FAQAnswer", $qid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE FAQAnswer SET status=1 WHERE ID=?');
                        $statm->execute(array($qid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Activated</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';             
            
                 }
         include $tpl.'Footer.php';
        }
        else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 