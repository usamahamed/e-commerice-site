<?php
include 'connectdb.php';
include 'stat_count.php';

    
    
    
  //start plot barchart for users   
  
require_once ('Includes/libs/src/jpgraph.php');
require_once ('Includes/libs/src/jpgraph_bar.php');
$datay=array($users,$trust_user,$admin_user,$pending_users);


// Create the graph. These two calls are always required
$graph = new Graph(350,220,'auto');
$graph->SetScale("textlin");

// set major and minor tick positions manually
$graph->yaxis->SetTickPositions(array(0,5,10,15,20,25), array(15,45,75,105,135));
$graph->SetBox(false);

//$graph->ygrid->SetColor('gray');
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array("Numbers",'Trust','Admin','Approve'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

// Create the bar plots
$b1plot = new BarPlot($datay);

// ...and add it to the graPH
$graph->Add($b1plot);


$b1plot->SetColor("white");
$b1plot->SetFillGradient("#4B0082","white",GRAD_LEFT_REFLECTION);
$b1plot->SetWidth(45);
$graph->title->Set("Users Statistics");

// Display the graph
$graph->Stroke();
//End plot chart for users
