
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery','datatables'], factory);
    }
    else {
        factory(jQuery);
    }
}(function ($) {
    /* Set the defaults for DataTables initialisation */
	$.extend( true, $.fn.dataTable.defaults, {
		"sDom": "<'row'<'col-sm-12'<'pull-right'f><'pull-left'l>r<'clearfix'>>>t<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>",
	    "sPaginationType": "bs_normal",
	    "oLanguage": {
	        "sLengthMenu": "Show _MENU_ Rows",
	        "sSearch": ""
	    }
	} );

	/* Default class modification */
	$.extend( $.fn.dataTableExt.oStdClasses, {
		"sWrapper": "dataTables_wrapper form-inline"
	} );

	/* API method to get paging information */
	$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
	{
		return {
			"iStart":         oSettings._iDisplayStart,
			"iEnd":           oSettings.fnDisplayEnd(),
			"iLength":        oSettings._iDisplayLength,
			"iTotal":         oSettings.fnRecordsTotal(),
			"iFilteredTotal": oSettings.fnRecordsDisplay(),
			"iPage":          oSettings._iDisplayLength === -1 ?
				0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
			"iTotalPages":    oSettings._iDisplayLength === -1 ?
				0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
		};
	};

	/* Bootstrap style pagination control */
	$.extend( $.fn.dataTableExt.oPagination, {
		"bs_normal": {
			"fnInit": function( oSettings, nPaging, fnDraw ) {
				var oLang = oSettings.oLanguage.oPaginate;
				var fnClickHandler = function ( e ) {
					e.preventDefault();
					if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
						fnDraw( oSettings );
					}
				};
				$(nPaging).append(
					'<ul class="pagination">'+
						'<li class="prev disabled"><a href="#"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;'+oLang.sPrevious+'</a></li>'+
						'<li class="next disabled"><a href="#">'+oLang.sNext+'&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></li>'+
					'</ul>'
				);
				var els = $('a', nPaging);
				$(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
				$(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
			},
			"fnUpdate": function ( oSettings, fnDraw ) {
				var iListLength = 5;
				var oPaging = oSettings.oInstance.fnPagingInfo();
				var an = oSettings.aanFeatures.p;
				var i, ien, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
				if ( oPaging.iTotalPages < iListLength) {
					iStart = 1;
					iEnd = oPaging.iTotalPages;
				}
				else if ( oPaging.iPage <= iHalf ) {
					iStart = 1;
					iEnd = iListLength;
				} else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
					iStart = oPaging.iTotalPages - iListLength + 1;
					iEnd = oPaging.iTotalPages;
				} else {
					iStart = oPaging.iPage - iHalf + 1;
					iEnd = iStart + iListLength - 1;
				}
				for ( i=0, ien=an.length ; i<ien ; i++ ) {
					$('li:gt(0)', an[i]).filter(':not(:last)').remove();
					for ( j=iStart ; j<=iEnd ; j++ ) {
						sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
						$('<li '+sClass+'><a href="#">'+j+'</a></li>')
							.insertBefore( $('li:last', an[i])[0] )
							.bind('click', function (e) {
								e.preventDefault();
								if ( oSettings.oApi._fnPageChange(oSettings, parseInt($('a', this).text(),10)-1) ) {
									fnDraw( oSettings );
								}
							} );
					}
					if ( oPaging.iPage === 0 ) {
						$('li:first', an[i]).addClass('disabled');
					} else {
						$('li:first', an[i]).removeClass('disabled');
					}

					if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
						$('li:last', an[i]).addClass('disabled');
					} else {
						$('li:last', an[i]).removeClass('disabled');
					}
				}
			}
		},	
		"bs_two_button": {
			"fnInit": function ( oSettings, nPaging, fnCallbackDraw )
			{
				var oLang = oSettings.oLanguage.oPaginate;
				var oClasses = oSettings.oClasses;
				var fnClickHandler = function ( e ) {
					if ( oSettings.oApi._fnPageChange( oSettings, e.data.action ) )
					{
						fnCallbackDraw( oSettings );
					}
				};
				var sAppend = '<ul class="pagination">'+
					'<li class="prev"><a class="'+oSettings.oClasses.sPagePrevDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;'+oLang.sPrevious+'</a></li>'+
					'<li class="next"><a class="'+oSettings.oClasses.sPageNextDisabled+'" tabindex="'+oSettings.iTabIndex+'" role="button">'+oLang.sNext+'&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></li>'+
					'</ul>';
				$(nPaging).append( sAppend );
				var els = $('a', nPaging);
				var nPrevious = els[0],
					nNext = els[1];
				oSettings.oApi._fnBindAction( nPrevious, {action: "previous"}, fnClickHandler );
				oSettings.oApi._fnBindAction( nNext,     {action: "next"},     fnClickHandler );
				if ( !oSettings.aanFeatures.p )
				{
					nPaging.id = oSettings.sTableId+'_paginate';
					nPrevious.id = oSettings.sTableId+'_previous';
					nNext.id = oSettings.sTableId+'_next';
					nPrevious.setAttribute('aria-controls', oSettings.sTableId);
					nNext.setAttribute('aria-controls', oSettings.sTableId);
				}
			},
			"fnUpdate": function ( oSettings, fnCallbackDraw )
			{
				if ( !oSettings.aanFeatures.p )
				{
					return;
				}
				var oPaging = oSettings.oInstance.fnPagingInfo();
				var oClasses = oSettings.oClasses;
				var an = oSettings.aanFeatures.p;
				var nNode;
				for ( var i=0, iLen=an.length ; i<iLen ; i++ )
				{
					if ( oPaging.iPage === 0 ) {
						$('li:first', an[i]).addClass('disabled');
					} else {
						$('li:first', an[i]).removeClass('disabled');
					}

					if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
						$('li:last', an[i]).addClass('disabled');
					} else {
						$('li:last', an[i]).removeClass('disabled');
					}
				}
			}
		},
		"bs_four_button": {
			"fnInit": function ( oSettings, nPaging, fnCallbackDraw )
				{
					var oLang = oSettings.oLanguage.oPaginate;
					var oClasses = oSettings.oClasses;
					var fnClickHandler = function ( e ) {
						e.preventDefault()
						if ( oSettings.oApi._fnPageChange( oSettings, e.data.action ) )
						{
							fnCallbackDraw( oSettings );
						}
					};
					$(nPaging).append(
						'<ul class="pagination">'+
						'<li class="disabled"><a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageFirst+'"><span class="glyphicon glyphicon-backward"></span>&nbsp;'+oLang.sFirst+'</a></li>'+
						'<li class="disabled"><a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPagePrevious+'"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;'+oLang.sPrevious+'</a></li>'+
						'<li><a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageNext+'">'+oLang.sNext+'&nbsp;<span class="glyphicon glyphicon-chevron-right"></span></a></li>'+
						'<li><a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageLast+'">'+oLang.sLast+'&nbsp;<span class="glyphicon glyphicon-forward"></span></a></li>'+
						'</ul>'
					);
					var els = $('a', nPaging);
					var nFirst = els[0],
						nPrev = els[1],
						nNext = els[2],
						nLast = els[3];
					oSettings.oApi._fnBindAction( nFirst, {action: "first"},    fnClickHandler );
					oSettings.oApi._fnBindAction( nPrev,  {action: "previous"}, fnClickHandler );
					oSettings.oApi._fnBindAction( nNext,  {action: "next"},     fnClickHandler );
					oSettings.oApi._fnBindAction( nLast,  {action: "last"},     fnClickHandler );
					if ( !oSettings.aanFeatures.p )
					{
						nPaging.id = oSettings.sTableId+'_paginate';
						nFirst.id =oSettings.sTableId+'_first';
						nPrev.id =oSettings.sTableId+'_previous';
						nNext.id =oSettings.sTableId+'_next';
						nLast.id =oSettings.sTableId+'_last';
					}
				},
			"fnUpdate": function ( oSettings, fnCallbackDraw )
				{
					if ( !oSettings.aanFeatures.p )
					{
						return;
					}
					var oPaging = oSettings.oInstance.fnPagingInfo();
					var oClasses = oSettings.oClasses;
					var an = oSettings.aanFeatures.p;
					var nNode;
					for ( var i=0, iLen=an.length ; i<iLen ; i++ )
					{
						if ( oPaging.iPage === 0 ) {
							$('li:eq(0)', an[i]).addClass('disabled');
							$('li:eq(1)', an[i]).addClass('disabled');
						} else {
							$('li:eq(0)', an[i]).removeClass('disabled');
							$('li:eq(1)', an[i]).removeClass('disabled');
						}

						if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
							$('li:eq(2)', an[i]).addClass('disabled');
							$('li:eq(3)', an[i]).addClass('disabled');
						} else {
							$('li:eq(2)', an[i]).removeClass('disabled');
							$('li:eq(3)', an[i]).removeClass('disabled');
						}
					}
				}
		},
		"bs_full": {
			"fnInit": function ( oSettings, nPaging, fnCallbackDraw )
				{
					var oLang = oSettings.oLanguage.oPaginate;
					var oClasses = oSettings.oClasses;
					var fnClickHandler = function ( e ) {
						if ( oSettings.oApi._fnPageChange( oSettings, e.data.action ) )
						{
							fnCallbackDraw( oSettings );
						}
					};
					$(nPaging).append(
						'<ul class="pagination">'+
						'<li class="disabled"><a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageFirst+'">'+oLang.sFirst+'</a></li>'+
						'<li class="disabled"><a  tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPagePrevious+'">'+oLang.sPrevious+'</a></li>'+
						'<li><a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageNext+'">'+oLang.sNext+'</a></li>'+
						'<li><a tabindex="'+oSettings.iTabIndex+'" class="'+oClasses.sPageButton+" "+oClasses.sPageLast+'">'+oLang.sLast+'</a></li>'+
						'</ul>'
					);
					var els = $('a', nPaging);
					var nFirst = els[0],
						nPrev = els[1],
						nNext = els[2],
						nLast = els[3];
					oSettings.oApi._fnBindAction( nFirst, {action: "first"},    fnClickHandler );
					oSettings.oApi._fnBindAction( nPrev,  {action: "previous"}, fnClickHandler );
					oSettings.oApi._fnBindAction( nNext,  {action: "next"},     fnClickHandler );
					oSettings.oApi._fnBindAction( nLast,  {action: "last"},     fnClickHandler );
					if ( !oSettings.aanFeatures.p )
					{
						nPaging.id = oSettings.sTableId+'_paginate';
						nFirst.id =oSettings.sTableId+'_first';
						nPrev.id =oSettings.sTableId+'_previous';
						nNext.id =oSettings.sTableId+'_next';
						nLast.id =oSettings.sTableId+'_last';
					}
				},
			"fnUpdate": function ( oSettings, fnCallbackDraw )
				{
					if ( !oSettings.aanFeatures.p )
					{
						return;
					}
					var oPaging = oSettings.oInstance.fnPagingInfo();
					var iPageCount = $.fn.dataTableExt.oPagination.iFullNumbersShowPages;
					var iPageCountHalf = Math.floor(iPageCount / 2);
					var iPages = Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength);
					var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
					var sList = "";
					var iStartButton, iEndButton, i, iLen;
					var oClasses = oSettings.oClasses;
					var anButtons, anStatic, nPaginateList, nNode;
					var an = oSettings.aanFeatures.p;
					var fnBind = function (j) {
						oSettings.oApi._fnBindAction( this, {"page": j+iStartButton-1}, function(e) {
							if( oSettings.oApi._fnPageChange( oSettings, e.data.page ) ){
								fnCallbackDraw( oSettings );
							}
							e.preventDefault();
						} );
					};
					if ( oSettings._iDisplayLength === -1 )
					{
						iStartButton = 1;
						iEndButton = 1;
						iCurrentPage = 1;
					}
					else if (iPages < iPageCount)
					{
						iStartButton = 1;
						iEndButton = iPages;
					}
					else if (iCurrentPage <= iPageCountHalf)
					{
						iStartButton = 1;
						iEndButton = iPageCount;
					}
					else if (iCurrentPage >= (iPages - iPageCountHalf))
					{
						iStartButton = iPages - iPageCount + 1;
						iEndButton = iPages;
					}
					else
					{
						iStartButton = iCurrentPage - Math.ceil(iPageCount / 2) + 1;
						iEndButton = iStartButton + iPageCount - 1;
					}
					for ( i=iStartButton ; i<=iEndButton ; i++ )
					{
						sList += (iCurrentPage !== i) ?
							'<li><a tabindex="'+oSettings.iTabIndex+'">'+oSettings.fnFormatNumber(i)+'</a></li>' :
							'<li class="active"><a tabindex="'+oSettings.iTabIndex+'">'+oSettings.fnFormatNumber(i)+'</a></li>';
					}
					for ( i=0, iLen=an.length ; i<iLen ; i++ )
					{
						nNode = an[i];
						if ( !nNode.hasChildNodes() )
						{
							continue;
						}
						$('li:gt(1)', an[i]).filter(':not(li:eq(-2))').filter(':not(li:eq(-1))').remove();
						if ( oPaging.iPage === 0 ) {
							$('li:eq(0)', an[i]).addClass('disabled');
							$('li:eq(1)', an[i]).addClass('disabled');
						} else {
							$('li:eq(0)', an[i]).removeClass('disabled');
							$('li:eq(1)', an[i]).removeClass('disabled');
						}
						if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
							$('li:eq(-1)', an[i]).addClass('disabled');
							$('li:eq(-2)', an[i]).addClass('disabled');
						} else {
							$('li:eq(-1)', an[i]).removeClass('disabled');
							$('li:eq(-2)', an[i]).removeClass('disabled');
						}
						$(sList)
							.insertBefore($('li:eq(-2)', an[i]))
							.bind('click', function (e) {
								e.preventDefault();
								if ( oSettings.oApi._fnPageChange(oSettings, parseInt($('a', this).text(),10)-1) ) {
									fnCallbackDraw( oSettings );
								}
							});
					}
				}
		}	
	} );


	/*
	 * TableTools Bootstrap compatibility
	 * Required TableTools 2.1+
	 */
	if ( $.fn.DataTable.TableTools ) {
		// Set the classes that TableTools uses to something suitable for Bootstrap
		$.extend( true, $.fn.DataTable.TableTools.classes, {
			"container": "DTTT btn-group",
			"buttons": {
				"normal": "btn",
				"disabled": "disabled"
			},
			"collection": {
				"container": "DTTT_dropdown dropdown-menu",
				"buttons": {
					"normal": "",
					"disabled": "disabled"
				}
			},
			"print": {
				"info": "DTTT_print_info modal"
			},
			"select": {
				"row": "active"
			}
		} );

		// Have the collection use a bootstrap compatible dropdown
		$.extend( true, $.fn.DataTable.TableTools.DEFAULTS.oTags, {
			"collection": {
				"container": "ul",
				"button": "li",
				"liner": "a"
			}
		} );
	}
}));


$(function(){
   'use strict';
   //fire popup function
   popup();
     //fire datatable plugin
	$('.datatable').dataTable({
				"sPaginationType": "bs_normal",
                            
                                 responsive: true
                                
			});	
			$('.datatable').each(function(){
				var datatable = $(this);
				// SEARCH - Add the placeholder for Search and Turn this into in-line form control
				var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
				search_input.attr('placeholder', 'Search');
				search_input.addClass('form-control input-sm');
				// LENGTH - Inline-Form control
				var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
				length_sel.addClass('form-control input-sm');
                datatable.bind('page', function(e){
                    window.console && console.log('pagination event:', e) //this event must be fired whenever you paginate
                });
			});
                        

       
 
$("td:first-child,td:nth-child(2),td:nth-child(3),td:nth-child(4),td:nth-child(5)")
  .css({
    /* required to allow resizer embedding */
    position: "relative"
  })
  /* check .resizer CSS */
  .prepend("<div class='resizer'></div>")
  .resizable({
    resizeHeight: false,
    // we use the column as handle and filter
    // by the contained .resizer element
    handleSelector: "",
    onDragStart: function(e, $el, opt) {
      // only drag resizer
      if (!$(e.target).hasClass("resizer"))
        return false;
      return true;
    }
  });
                        
                        
      //fire selectbox plugin
    $("select").selectBoxIt({
    showEffect: "fadeIn",
    showEffectSpeed: 400,
    hideEffect: "fadeOut",
    hideEffectSpeed: 400,
    autoWidth:false
        });
             $('.inlinebar').sparkline('html', {
    type: 'bar',
    height: '36px',
    width: '100%', 
    barSpacing: 2, 
    negBarColor: '#8A3E0B', 
    barColor: '#FFF', 
    barWidth: 18
  });

                        
   //Dashboard function
   $('.toggle-info').click(function(){
       $(this).toggleClass('selected').parent().next('.panel-body').fadeToggle(100);
       if($(this).hasClass('selected')){
           $(this).html('<i class="fa fa-minus fa-lg"></i>');
       }else{
        $(this).html('<i class="fa fa-plus fa-lg"></i>');

       }
   });
   
 //header panel
 $(".rad-toggle-btn").on('click', function() {
		$(".rad-logo-container").toggleClass("rad-nav-min");
		$(".rad-sidebar").toggleClass("rad-nav-min");
		$(".rad-body-wrapper").toggleClass("rad-nav-min");
		setTimeout(function() {
			initializeCharts();
		}, 200);
	});
        $("li.rad-dropdown > a.rad-menu-item").on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		$(".rad-dropmenu-item").removeClass("active");
		$(this).next(".rad-dropmenu-item").toggleClass("active");
	});
 
 //end header panel
   //placholder handel
   $('[placeholder]').focus(function(){
       $(this).attr('data-cash', $(this).attr('placeholder'));
       $(this).attr('placeholder','');
   }).blur(function(){
       
      $(this).attr('placeholder',$(this).attr('data-cash')); 
   });
    //add astric to required feild
    $("input").each(function(){
       if($(this).attr('required')==='required'){
           $(this).after('<span class="asterisk">*</span>');
       }
       
    });
    //convert password to text in hover
    var pass=$('.password');
    $('.show-pass').hover(function(){
        pass.attr('type','text');
    },function(){
        pass.attr('type','password');

    });
    //exit function
    
    //confirmatin mess before delete member
    $('.confirm').click(function(){
        
    });
    //View The category body
    $('.categ h3').click(function(){
        $(this).next('.full-v').fadeToggle(250);
    });
    //add class active 
    $('.option span').click(function(){
        $(this).addClass('active1').siblings('span').removeClass('active1');
        if($(this).data('view')==='Full'){
            $('.categ .full-v').fadeIn(200);
        }else{
        $('.categ .full-v').fadeOut(200);

        }
    });
//show delete button in categories
$('.child-delete').hover(function(){
    
        $(this).find('.show-delete').fadeIn();
    
},function(){
        $(this).find('.show-delete').fadeOut();

});

});



function popup(){
	$("*[data-popup]").hover(function(){
			var data = $(this).attr("data-popup"),
							offsetDataTop = $(this).offset().top,
							offsetDataLeft = $(this).offset().left;

		if(data != ""){
			// .popup creation
			$("body").prepend("<div class='popup'>"+data+"</div>");
			// .popup properties
			var popupWidth = $(".popup").innerWidth(),
							thisWith = $(this).innerWidth(),
							marginLeft = (thisWith - popupWidth)/2;
			// .popup init
			$(".popup").css({
					opacity : 0,
					top : offsetDataTop - 40,
					left : offsetDataLeft + marginLeft
			});
			// .popup animation
			$(".popup").animate({
					opacity : 1,
					marginTop : 20
			}, "fast");
		}
	}, function(){ $(".popup").remove(); }); // .popup removed
}


/*(function() {
  var mapLoaded, showMap, showPage;

  mapLoaded = false;

  showPage = function(pageName, cb) {
    var $page, $prevPage, tl;
    $page = $(".page.page-" + pageName);
    $prevPage = $(".page:visible");
    if ($prevPage.attr("class") === $page.attr("class")) {
      return $page;
    }
    tl = new TimelineLite({
      paused: true,
      onComplete: function() {
        if (!mapLoaded && pageName === "map") {
          showMap();
          mapLoaded = true;
        }
        if (cb) {
          return cb();
        }
      }
    });
    if ($prevPage.length > 0) {
      tl.to($prevPage, 0.5, {
        x: 800,
        ease: Power3.easeIn,
        clearProps: "scale",
        onComplete: function() {
          return $prevPage.hide();
        }
      });
    }
    tl.from($page, 0.5, {
      scale: 0.6,
      delay: 0.2,
      ease: Power3.easeOut,
      onStart: function() {
        return $page.show();
      }
    });
    if (pageName === "home") {
      tl.from(".page-home .panel-time", 0.6, {
        x: -400,
        ease: Power3.easeOut
      });
      tl.from(".page-home .panel-weather", 0.6, {
        x: "+=400",
        ease: Power3.easeOut
      }, '-=0.3');
      tl.staggerFrom(".page-home .panel-functions .icon", 1.5, {
        y: 150,
        clearProps: "opacity, scale",
        ease: Elastic.easeOut
      }, 0.2, "-=0.4");
      tl.staggerFrom(".page-home .panel-calendar li", 1.5, {
        x: -400,
        ease: Power3.easeOut
      }, 0.2, "-=2");
      tl.staggerFrom(".page-home .panel-tasks li", 1.5, {
        x: 400,
        ease: Power3.easeOut
      }, 0.2, "-=1.8");
    }
    if (pageName === "weather") {
      tl.from(".page-weather .panel-now", 0.6, {
        x: -500,
        ease: Power3.easeOut
      });
      tl.from(".page-weather .panel-today", 0.6, {
        x: -500,
        ease: Power3.easeOut
      }, '-=0.2');
      tl.from(".page-weather .panel-location", 0.4, {
        x: "+=400",
        ease: Power3.easeOut
      }, '-=0.5');
      tl.staggerFrom(".page-weather .panel-forecast .box", 1.2, {
        y: 150,
        delay: 0.5,
        ease: Elastic.easeOut
      }, 0.1, "-=0.5");
    }
    if (pageName === "calendar") {
      tl.staggerFrom(".page-calendar .panel-calendar", 1.0, {
        y: -150,
        autoAlpha: 0,
        ease: Power3.easeOut
      }, 0.3);
      tl.staggerFrom(".page-calendar .panel-calendar li", 1.0, {
        y: 150,
        autoAlpha: 0,
        ease: Power3.easeOut
      }, 0.1, "-=0.6");
    }
    if (pageName === "tasks") {
      tl.staggerFrom(".page-tasks .panel-tasklist", 1.0, {
        y: -150,
        autoAlpha: 0,
        ease: Power3.easeOut
      }, 0.3, "-=0.2");
      tl.staggerFrom(".page-tasks .panel-tasklist li", 1.0, {
        y: 150,
        autoAlpha: 0,
        ease: Power3.easeOut
      }, 0.1, "-=0.8");
    }
    tl.play();
    return $page;
  };

  $(function() {
    $('.page').each(function(i, page) {
      var mc, type;
      if ($(page).hasClass("page-home")) {
        return;
      }
      mc = new Hammer(page, {
        preventDefault: true
      });
      type = "pan";
      mc.get(type).set({
        direction: Hammer.DIRECTION_HORIZONTAL,
        threshold: 10
      });
      return mc.on(type + 'right', function(ev) {
        mc.get(type).set({
          enable: false
        });
        console.log(ev);
        return showPage("home", function() {
          return mc.get(type).set({
            enable: true
          });
        });
      });
    });
    $(".page-home .panel-functions .icon").on("click", function() {
      TweenLite.to($(this), 0.5, {
        scale: 2.0,
        clearProps: "opacity, scale",
        opacity: 0
      });
      return showPage($(this).attr('data-page'));
    });
   
$('.add-catrgo').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    showPage('insertcat');
});

$('.cat_edit').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    showPage('editcat');
});

   

  });



}).call(this);*/


