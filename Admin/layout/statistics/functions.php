<?php

/* 
 * function name-------->getTitel()
 * function Version----->.01
 * function role----->Print the titel of each page depend on specfic variable 
 * Start function
 */
function getTitel(){
    global $PageTitel;
    if(isset($PageTitel)){
        echo $PageTitel;
    }
    else {
        echo "Default";
    }
}
/*End getTitel Function*/
/* 
 * function name-------->Redirect()
 * function Version----->.02
 * function role----->Redirest to home page after specific time and produce error message
 * Start function
 */
function Redirect($mesg,$url=null,$second=3){
     if($url===null){
         $url='index.php';
         $link='Home Page';
     }else{
         if(isset($_SERVER['HTTP_REFERER'])&&$_SERVER['HTTP_REFERER']!==''){
                  $url=$_SERVER['HTTP_REFERER'];
                  $link='Prevouis Page';

         }else{
                      $url='index.php';
                      $link='Home Page';


         }
     }
   echo $mesg;
  echo "<div class='alert alert-info'>you will be redirectd to $link page after$second</div>";
  //echo '<script>window.location=index.php</script>'; 
 // header("Location:index.php",TRUE);
  header("refresh:$second;url=$url",true);
  
  exit;
  
}
/*End RedirectFunction Function*/
/* 
 * function name-------->CheckItems()
 * function Version----->.01
 * function role----->check existance in database
 * Start function
 */
function CheckItems($select,$from,$value){
    global $con;
    $statment=$con->prepare("SELECT $select FROM $from WHERE $select=?");
    $statment->execute(array($value));
    $count=$statment->rowCount();
    return $count;
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitems()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitems($item,$table){
    global $con;
    $statm2=$con->prepare("SELECT COUNT($item) FROM $table");
        $statm2->execute();
        return  $statm2->fetchColumn();
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitemsAll()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitemsAll($item,$table,$select){
    global $con;
    $statm2=$con->prepare("SELECT COUNT($item) FROM $table WHERE $select");
        $statm2->execute();
        return  $statm2->fetchColumn();
}
/*End RedirectFunction Function*/
/* 
 * function name-------->getLatest()
 * function Version----->.01
 * function role----->get latest items from database[users,items,comments]
 * Start function
 */
function getlatest($select,$table,$order,$limt=5){
        global $con;
$statm3=$con->prepare("SELECT $select FROM $table ORDER BY $order DESC LIMIT $limt");
        $statm3->execute();
$rows=$statm3->fetchAll();
return $rows;

}
/*End getLatest Function*/
