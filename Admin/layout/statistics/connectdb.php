<?php

$dbm='mysql:host=localhost;dbname=shop';
$user='root';
$pass='root';
$option=array(
    
    PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8'
);
try{
    $con=new PDO($dbm,$user,$pass,$option);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
 catch (PDOException $e){
     echo "fail to connect".$e->getMessage();
 }