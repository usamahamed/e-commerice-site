<?php ob_start(); ?>    
<?php 
/*========================================================= 
 *Items Page 
 * ========================================================= 
 */
    session_start();
        $PageTitel='Logs Activity';

    if(isset($_SESSION['user'])){
        include 'inti.php';
            $do=isset($_GET['do'])?$_GET['do']:'manage';

    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){
 
            //fetch data from database to put into table
            $statm=$con->prepare("SELECT logs.*,Users.UserName AS US_NM "
                               . "FROM logs INNER JOIN Users ON  Users.userID=logs.User_ID ");
            $statm->execute();//select all items expect admin
            $itms=$statm->fetchAll();
            if(!empty($itms)){
            ?>
        <h1 class="text-center">Mange Items Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>    <tr>
                               <td>#ID</td>
                               <td>Server_name</td>
                               <td>Server_protocol</td>
                               <td>Request_method</td>
                               <td>log_time</td>
                               <td>activity</td>
                               <td>User Name</td>
                               <td>Control</td>
                                </tr></thead>
                        <?php
                                    foreach ($itms as $item){
                                        echo '<tr>';
                                            echo'<td>'.$item['ID'].'</td>';
                                            echo'<td>'.$item['Server_name'].'</td>';
                                             echo'<td>'.$item['Server_protocol'].'</td>';
                                             echo'<td>'.$item['Request_method'].'</td>';
                                              echo'<td>'.$item['log_time'].'</td>';
                                              echo'<td class="p_des">'.$item['activity'].'</td>';
                                              echo'<td>'.$item['US_NM'].'</td>';
                                              
                            echo'<td> <a href="logs.php?do=detail&userid='.$item['User_ID'].'" class="btn btn-success"><i class="fa fa-edit"></i>Detail</a>'
                                         . '<a href="items.php?do=delete&itemid='.$item['User_ID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                      
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>
                <a href="items.php?do=Add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>New Item</a>

            </div><?php    
            
             }else {echo '<div class="container"><div class="message">there is no Logs</div></div>';}   }
     /*========================================================= 
            *
            * if do==show detail
            * 
    * ========================================================= 
    */
        if($do=="detail"){
     $userID= isset($_GET['userid'])&& is_numeric($_GET['userid']) ?intval($_GET['userid']):0;
            ?>
        <h1 class="text-center">Detail logs for User <?php echo getUserName($userID) ?></h1>  
        <div class="container info">
            <ul class="list-unstyled">
                <?php
                            foreach(DisplayLog($userID) as $log){
                   echo '<li><i class="fa fa-server" aria-hidden="true"></i>Server Name is:'.$log['Server_name'].''.'</li>';
                   echo '<li><i class="fa fa-server" aria-hidden="true"></i>Server protocol is:'.$log['Server_protocol'].''.'</li>';
                   echo '<li><i class="fa fa-server" aria-hidden="true"></i>Request method is:'.$log['Request_method'].''.'</li>';
                   echo '<li><i class="fa fa-server" aria-hidden="true"></i>Last login time is:'.$log['log_time'].''.'</li>';
                   echo '<li><i class="fa fa-server" aria-hidden="true"></i>Activity is:'.$log['activity'].''.'</li>';
                            }
                ?> 
            </ul>
            
        </div>
            <?php
            
        }
             
      
       include $tpl.'Footer.php';
    }
else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 


