<?php ob_start(); ?>    

<?php

/*========================================================= 
 *Members Page 
 * ========================================================= 
 */
        session_start();
        $PageTitel='Members';

    if(isset($_SESSION['user'])){
        include 'inti.php';
        $do=isset($_GET['do'])?$_GET['do']:'manage';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){ //manage memeber page
        $quary='';
        if(isset($_GET['page'])&&$_GET['page']='pending'){
            $quary='AND ApproveStatus=0';
        }
            //fetch data from database to put into table
            $statm=$con->prepare("SELECT * FROM users WHERE groupID!=1 $quary");
            $statm->execute();//select all user expect admin
            $rows=$statm->fetchAll();
            if(!empty($rows)){
            ?>

        <h1 class="text-center">Mange Member Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table id="datatable"  class="datatable table table-bordered text-center">
                            <thead>

                        <tr>
                               <td>#ID</td>
                               <td>User Name</td>
                               <td>Email</td>
                               <td>Full Name</td>
                               <td>Register Date</td>
                               <td>Control</td>
                        </tr>
                            </thead>

                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['userID'].'</td>';
                                            echo'<td>'.'<a href="message.php?user='.$row['userID'].'&full='.$row['FullName'].'&mail='.$row['Email'].'">'.$row['UserName'].'</a>'.'</td>';
                                             echo'<td>'.$row['Email'].'</td>';
                                             echo'<td>'.$row['FullName'].'</td>';
                                              echo'<td>'.$row['Date'].'</td>';
                            echo'<td> <a href="members.php?do=edit&userid='.$row['userID'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                              . '<a href="members.php?do=delete&userid='.$row['userID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($row['ApproveStatus']==0){
                                                    echo '<a href="members.php?do=activate&userid='.$row['userID'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
            }
                        ?>
                      
                      
                    </table>
                    
                </div>
                        <a href="members.php?do=Add" class="btn btn-primary"><i class="fa fa-plus"></i>New Members</a>
            </div>
       <?php }else {echo '<div class="container><div class="message">there is no users</div></div>';}}//end if do=manage
    /*========================================================= 
            *
            * if do==Add
            * 
    * ========================================================= 
    */
        elseif($do==='Add'){
            //Add new Member
            //Add members Form
            ?>
            <h1 class="text-center"><?php echo lang("Mem_add")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=insert" method="POST">
                             <!Start User name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="username"  class="form-control" autocomplete="off" required="required" placeholder="UserName to login into shop"/>
                                 </div>
                             </div>
                          <!Start User name password!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Pass")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="password" name="password" class="password form-control"  autocomplete="new-password" placeholder="Password must be hard complex" required="required"/>
                                     <i class="show-pass fa fa-eye fa-2x"></i>
                                 </div>
                             </div>
                <!Start User name E-Mail!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Email")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="email" name="email"  class="form-control" required="required" placeholder="E-mail must be value"/>
                                 </div>
                             </div>
                <!Start User name Full Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_FullN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="full"  class="form-control" required="required" placeholder="Full name appear in profile page"/>
                                 </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("ADD_BUtton")?>" class="btn btn-primary btn-lg"/>
                                 </div>
                             </div>
                         </form>
                      </div>
            
      <?php 
      
      
        }//end if do=add
    /*========================================================= 
            *
            * if do==insert
            * 
    * ========================================================= 
    */
        elseif ($do==='insert') {
              //in insert form
            
                if($_SERVER['REQUEST_METHOD']=='POST'){
                echo '<h1 class="text-center"> Insert Member</h1>';
                echo '<div class="container">';
                $user=filter_input(INPUT_POST, 'username');
                $pass=filter_input(INPUT_POST, 'password');
                $email=filter_input(INPUT_POST, 'email');
                $full=filter_input(INPUT_POST, 'full');
                $hashpass=sha1(filter_input(INPUT_POST, 'password'));

                //validate the form
                $errorarray=array();
                if(strlen($user)<4&&strlen($user)!=0){$errorarray[]="UserName can't be <strong>less than 4 character</strong>"; }
                if(strlen($user)>20){$errorarray[]="UserName can\'t be <strong>more than 20 character</strong>"; }
                if(empty($user)){$errorarray[]="UserName must be <strong>not Empty</strong>";}
                if(empty($pass)){$errorarray[]="Password must be <strong>not Empty</strong>";}
                if(empty($email)){$errorarray[]="Email must be <strong>not Empty</strong>";}
                if(empty($full)){$errorarray[]="Full Name must be <strong>not Empty</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\"'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                       //Insert member
                       //check if user exist
                       $check=  CheckItems("UserName", "users", $user);
                       if($check==1){
                           $msg= '<div class="alert alert-danger>"sorry this user is exist</div>';
                            Redirect($msg,"back");

                       }
                       else{
                       $statm=$con->prepare('INSERT INTO users (UserName,password,Email,FullName,ApproveStatus,Date)VALUES(:zuser,:zpass,:zmail,:zname,1,now())' );
                       $statm->execute(array("zuser"=>$user,"zpass"=>$hashpass,"zmail"=>$email,"zname"=>$full));
                     //echo success message
                       echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect($msg,"back");
                     echo '</div>';
                       }
                       }
            }//end is not post request 
            else{
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null,5);
            }//end show error
            echo '</div>';
            }//end if do=insert
    /*========================================================= 
            *
            * if do==edit
            * 
    * ========================================================= 
    */
        elseif($do=='edit'){            //Edit page
            
           $userid= isset($_GET['userid'])&& is_numeric($_GET['userid']) ?intval($_GET['userid']):0;
        $statm=$con->prepare('SELECT * FROM users WHERE userID=?');
        $statm->execute(array($userid));
        $row=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {      
              ?>
        
                <h1 class="text-center"><?php echo lang("mem_edit")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=update" method="POST">
                             <input type="hidden" name="userid"value="<?php echo $userid?>"/>
                             <!Start User name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="username" value="<?php echo $row['UserName']?>" class="form-control" autocomplete="off" required="required"/>
                                 </div>
                             </div>
                          <!Start User name password!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Pass")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="hidden" name="oldpassword" value="<?php echo $row['password'];?>"/>
                                     <input type="text" name="newpassword" class="form-control"  autocomplete="new-password" placeholder="Leave it blank if you don't change"/>
                                 </div>
                             </div>
                <!Start User name E-Mail!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Email")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="email" name="email" value="<?php echo $row['Email']?>" class="form-control" required="required"/>
                                 </div>
                             </div>
                <!Start User name Full Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_FullN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="full" value="<?php echo $row['FullName']?>" class="form-control" required="required"/>
                                 </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("Save")?>" class="btn btn-primary btn-lg"/>
                                 </div>
                             </div>
                         </form>
                      </div>
            
        <?php   }
        //$count =0 in other word no such userid in database
        else{
                echo '<div class="container">';
        $msg= '<div class="alert alert-danger">There is no such ID</div>';
                     Redirect($msg);    
                  echo '</div>';

        }
            
        }//end if do=edit
    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='update'){//update page
            
              if($_SERVER['REQUEST_METHOD']=='POST'){
                  echo '<h1 class="text-center"> update Member</h1>';
            echo '<div class="container">';
                $id=filter_input(INPUT_POST, 'userid');
                $user=filter_input(INPUT_POST, 'username');
                $email=filter_input(INPUT_POST, 'email');
                $full=filter_input(INPUT_POST, 'full');
               // password updata
                $pass=empty(filter_input(INPUT_POST, 'newpassword'))?filter_input(INPUT_POST, 'oldpassword'):sha1(filter_input(INPUT_POST, 'newpassword'));
                //validate the form
                $errorarray=array();
                if(strlen($user)<4&&strlen($user)!=0){$errorarray[]="UserName can't be <strong>less than 4 character</strong>"; }
                if(strlen($user)>20){$errorarray[]="UserName can\'t be <strong>more than 20 character</strong>"; }
                if(empty($user)){$errorarray[]="UserName must be <strong>not Empty</strong>";}
                if(empty($email)){$errorarray[]="Email must be <strong>not Empty</strong>";}
                if(empty($full)){$errorarray[]="Full Name must be <strong>not Empty</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\"'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                        $statm2=$con->prepare("SELECT * FROM users WHERE UserName=? AND userID!=?");
                                        $statm2->execute(array($user,$id));
                                        $count=$statm2->rowCount();
                                        if($count==1){
                                            echo '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>this user exist</div>';
                                            Redirect($mesg,'back');
                                            }  else {
                                            
    


                $statm=$con->prepare("UPDATE users SET UserName=?,Email=?,FullName=?,password=? WHERE userID=?");
                $statm->execute(array($user,$email,$full,$pass,$id));
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect($msg,"back");
                     echo '</div>';
                       }}
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
        elseif ($do==='delete') {
        //delete Member
            echo '<h1 class="text-center"> Delete Member</h1>';
            echo '<div class="container">';
                    $userid= isset($_GET['userid'])&& is_numeric($_GET['userid']) ?intval($_GET['userid']):0;
                    
                   $check=  CheckItems("userID", "users", $userid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM users WHERE userID=:zuser');
                        $statm->bindParam(":zuser",$userid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
             }
    /*========================================================= 
            *
            * if do==activate
            * 
    * ========================================================= 
    */
             elseif($do==='activate'){
                 //Aprove users in database
        echo '<h1 class="text-center"> Activate Member</h1>';
            echo '<div class="container">';
                    $userid= isset($_GET['userid'])&& is_numeric($_GET['userid']) ?intval($_GET['userid']):0;
                    
                   $check=  CheckItems("userID", "users", $userid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE users SET ApproveStatus=1 WHERE userID=?');
                        $statm->execute(array($userid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Activated</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg);

                 }
            echo '</div>';             
            
                 }
         include $tpl.'Footer.php';
        }
        else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 