<?php ob_start(); ?>    

<?php

/*========================================================= 
 *Comments Page 
 * ========================================================= 
 */
        session_start();
        $PageTitel='Comments';

    if(isset($_SESSION['user'])){
        include 'inti.php';
        $do=isset($_GET['do'])?$_GET['do']:'manage';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){ //manage memeber page
       
            //fetch data from database to put into table
            try{
            $statm=$con->prepare("SELECT comments.*,Users.UserName AS UN1, items.item_name AS ITN1 FROM comments INNER JOIN Users ON Users.userID=comments.User_id INNER JOIN items ON items.item_id=comments.item_id");
            
            $statm->execute();//select all items expect admin
            $rows=$statm->fetchAll();
            }catch(PDOException $e)
                             {
                             echo $e->getMessage();   }     
                              
                                  if(!empty($rows)){
                             
            ?>
        <h1 class="text-center">Mange comment Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>    <tr>
                               <td>#ID</td>
                               <td>comment</td>
                               <td>item Name</td>
                               <td>user Name</td>
                               <td>Add Date</td>
                               <td>Control</td>
                                </tr></thead>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['c_id'].'</td>';
                echo'<td class="p_des"><a href="comments.php?do=commentonreview&id='.$row['c_id'].'">'.$row['comment'].'</a></td>';
                                             echo'<td>'.$row['ITN1'].'</td>';
                                             echo'<td>'.$row['UN1'].'</td>';
                                              echo'<td>'.$row['date'].'</td>';
                            echo'<td> <a href="comments.php?do=edit&comid='.$row['c_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                              . '<a href="comments.php?do=delete&comid='.$row['c_id'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($row['status']==0){
                                                    echo '<a href="comments.php?do=Approve&comid='.$row['c_id'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>

            </div>
       <?php  }else {echo '<div class="container><div class="message">there is no Categories</div></div>';}}//end if do=manage
         /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="commentonreview"){ //manage memeber page
    $comid= isset($_GET['id'])&& is_numeric($_GET['id']) ?intval($_GET['id']):0;

            //fetch data from database to put into table
            try{
   $statm=$con->prepare("SELECT com_on_review.*,Users.UserName AS UN1 FROM com_on_review INNER JOIN Users ON Users.userID=com_on_review.uid WHERE rid=?");
            
            $statm->execute(array($comid));//select all items expect admin
            $rows=$statm->fetchAll();
            }catch(PDOException $e)
                             {
                             echo $e->getMessage();   }     
                              
                                  if(!empty($rows)){
                             
            ?>
        <h1 class="text-center">Mange comment Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>    <tr>
                               <td>#ID</td>
                               <td>comment</td>
                               <td>user Name</td>
                               <td>Add Date</td>
                               <td>Control</td>
                                </tr></thead>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['ID'].'</td>';
                                            echo'<td class="p_des">'.$row['r_comm'].'</td>';
                                             echo'<td>'.$row['UN1'].'</td>';
                                              echo'<td>'.$row['date'].'</td>';
                            echo'<td><a href="comments.php?do=deletecomment&comid='.$row['ID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                               
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>

            </div>
       <?php  }else {echo '<div class="container><div class="message">there is no comments</div></div>';}}//end if do=manage
    /*========================================================= 
            *
            * if do==edit
            * 
    * ========================================================= 
    */
       
        elseif($do=='edit'){            //Edit page
            
           $comid= isset($_GET['comid'])&& is_numeric($_GET['comid']) ?intval($_GET['comid']):0;
        $statm=$con->prepare('SELECT * FROM comments WHERE c_id=?');
        $statm->execute(array($comid));
        $row=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {      
              ?>
        
                <h1 class="text-center"><?php echo lang("com_edit")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=update" method="POST">
                             <input type="hidden" name="comid"value="<?php echo $comid?>"/>
                             <!Start User name!>
                             <div class="form-group form-group-lg com-div">
                                 <label class="col-sm-2 control-label"><?php echo lang("comm")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="comment" value="<?php echo $row['comment']?>" class="form-control com-in" autocomplete="off" required="required"/>
                                 </div>
                             </div>
                    
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("Save")?>" class="btn btn-primary btn-lg"/>
                                 </div>
                             </div>
                         </form>
                      </div>
            
        <?php   }
        //$count =0 in other word no such userid in database
        else{
                echo '<div class="container">';
        $msg= '<div class="alert alert-danger">There is no such ID</div>';
                     Redirect($msg);    
                  echo '</div>';

        }
            
        }//end if do=edit
    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='update'){//update page
            
              if($_SERVER['REQUEST_METHOD']=='POST'){
                  echo '<h1 class="text-center"> update Comment</h1>';
            echo '<div class="container">';
                $id=filter_input(INPUT_POST, 'comid');
                $comm=filter_input(INPUT_POST, 'comment');
               
              
                //updata database
                  
                $statm=$con->prepare("UPDATE comments SET comment=? WHERE c_id=?");
                $statm->execute(array($comm,$id));
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect($msg,"back");
                     echo '</div>';
                       
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
        elseif ($do==='delete') {
        //delete Member
            echo '<h1 class="text-center"> Delete Comment</h1>';
            echo '<div class="container">';
                    $comid= isset($_GET['comid'])&& is_numeric($_GET['comid']) ?intval($_GET['comid']):0;
                    
                   $check=  CheckItems("c_id", "comments", $comid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM comments WHERE c_id=:zcom');
                        $statm->bindParam(":zcom",$comid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
             }
               /*========================================================= 
            *
            * if do==deletecomment
            * 
    * ========================================================= 
    */
        elseif ($do==='deletecomment') {
        //delete Member
            echo '<h1 class="text-center"> Delete Comment</h1>';
            echo '<div class="container">';
                    $comid= isset($_GET['comid'])&& is_numeric($_GET['comid']) ?intval($_GET['comid']):0;
                    
                   $check=  CheckItems("ID", "com_on_review", $comid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM com_on_review WHERE ID=:zcom');
                        $statm->bindParam(":zcom",$comid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
             }
    /*========================================================= 
            *
            * if do==activate
            * 
    * ========================================================= 
    */
             elseif($do==='Approve'){
                 //Aprove users in database
        echo '<h1 class="text-center"> Approve comment</h1>';
            echo '<div class="container">';
                    $comid= isset($_GET['comid'])&& is_numeric($_GET['comid']) ?intval($_GET['comid']):0;
                    
                   $check=  CheckItems("c_id", "comments", $comid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE comments SET status=1 WHERE c_id=?');
                        $statm->execute(array($comid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Activated</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';             
            
                 }
         include $tpl.'Footer.php';
        }
        else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 