<?php

session_start();    //start session
session_unset();    //destroy session variables
session_destroy();  //destroy session
header("Location:index.php");
exit;

