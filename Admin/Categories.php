<?php ob_start(); ?>    
<?php 
/*========================================================= 
 *Categories Page 
 * ========================================================= 
 */
    session_start();
        $PageTitel='Categories';

    if(isset($_SESSION['user'])){
        include 'inti.php';
    $do=isset($_GET['do'])?$_GET['do']:'manage';
    /*========================================================= 
        *
        * if do==manage
        * 
    * ========================================================= 
    */
        if($do=="manage"){
           $sort='ASC';
           $sortArr=array('ASC','DESC');
           if(isset($_GET['sort'])&&  in_array($_GET['sort'], $sortArr)){
               $sort=$_GET['sort'];
           }
            $statmn=$con->prepare("SELECT * FROM categories WHERE parent=0 ORDER BY Ordering $sort");
            $statmn->execute();
            $cats=$statmn->fetchAll();
                        if(!empty($cats)){

            ?>
         <h1 class='text-center'><?php echo lang("Cat_man")?></h1>
         <div class="container categories1">
             <div class="panel panel-default">
                 <div class="panel panel-heading"><i class="fa fa-edit"></i><?php echo lang("Cat_man")?>
                     <div class="option pull-right">
                         <i class="fa fa-sort"></i>Oerdring:[
                         <a class="<?php if($sort==='ASC'){echo 'active1';}?>" href="?sort=ASC">ASC</a>|
                         <a class="<?php if($sort==='DESC'){echo 'active1';}?>" href="?sort=DESC">DESC</a>]
                          <i class="fa fa-eye"></i>View:] <span class="active1" data-view="Classic"> Classic</span>|
                               <span data-view="Full">Full</span>]
                     </div>
                 </div>
                 <div class="panel-body">
                     
                 <?php
                 foreach ($cats as $cat){
                     echo '<div class="categ">';
                     echo '<div class="hidden-buttons">';
                          echo "<a href='Categories.php?do=edit&catid= ".$cat['ID']."' class='btn btn-xs btn-primary'><i class='fa fa-edit'></i>Edit</a>";
                       echo '<a href="Categories.php?do=delete&catid=  '.$cat['ID'].'   " class="confirm btn btn-xs btn-danger"><i class="fa fa-close"></i>Delete</a>';
                     
                     echo '</div>';
                            echo '<h3>'.$cat['Name'].'</h3>';
                                     echo '<div class="full-v"';
                                        echo '<p>';if($cat['Discription']===''){echo'This category has no description';}else {echo $cat['Discription']; } echo'</p> ';
                                        if($cat['Visability']==="1"){ echo '<span class="Visbility"><i class="fa fa-eye"></i>Hidden</span>';}
                                        if($cat['Allow_comment']==="1"){ echo '<span class="comment"><i class="fa fa-close"></i>Comment disable</span>';}
                                        if($cat['Allow_Ads']==="1"){ echo '<span class="advertise"><i class="fa fa-close"></i>Advertise disable</span>';}
                                  echo '</div>';
                                                   //GET CHILD CATEGORIES
                                 
                                   $CHILDcats= genral_feth("*","categories","WHERE parent=".$cat['ID']."","","ID","ASC");
                                   if(!empty($CHILDcats)){
                                       echo '<h4 class="child-head"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i>Subcategoriy</h4>';
                                    echo '<ul class="list-unstyled child-cats">';
                                         foreach ($CHILDcats as $cat){
                                         echo " <li class='child-delete'><a class='child-delete1' href='Categories.php?do=edit&catid= ".$cat['ID']."' >".$cat['Name']."</a>  ";
                                       echo "<a href='Categories.php?do=delete&catid= ".$cat['ID']."' class='show-delete'>Delete</a></li> ";

                                         }
                                         echo '</ul>';

                 }
                                echo '</div>';
                
                          echo '<hr>';

                     }
                 ?>
            </div>
         </div>
             <a href="Categories.php?do=Add" class=" add-catrgo btn btn-primary"><i class="fa fa-plus"></i>Add New Categories</a>
         </div>
          
            
     <?php  }else {echo '<div class="container"><div class="message">there is no Categories</div><a href="Categories.php?do=Add" class="add-catrgo btn btn-primary"><i class="fa fa-plus"></i>Add New Categories</a></div>';} }
     /*========================================================= 
        *
        * if do==add
        * 
    * ========================================================= 
    */
        elseif($do==='Add'){?>
             <h1 class="text-center"><?php echo lang("Cat_add")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="Categories.php?do=insert" method="POST">
                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name"  class="form-control" autocomplete="off" required="required" placeholder="Name Of The Category"/>
                                 </div>
                             </div>
                          <!Start Discription!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_Des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="discription" class="form-control"  autocomplete="none" placeholder="Describe the Category" />
                                 </div>
                             </div>
                           <!Start Supcategory selection!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">parent?</label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="sub">
                                         <option value="0">None</option>
                                         <?php
                                         $cats= genral_feth("*","categories","WHERE parent=0","","ID","ASC");
                                         foreach ($cats as $c){
                                         echo " <option value='".$c['ID']."'";      
                                         echo ">".$c['Name']."</option> ";
                                         }
                                         ?>
                                     </select>
                                 </div>
                             </div>
                <!Start ordering!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_or")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="order"  class="form-control" placeholder="Number to arrange the categories"/>
                                 </div>
                             </div>
                <!Start Visability feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_vis")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="v-y"type="radio" name="visable" value="0" checked=""/>
                                     <label for="v-y">Yes</label></div>
                                   <div>
                                       <input id="v-n" type="radio" name="visable" value="1" checked=""/>
                                     <label for="v-n">No</label></div>
                                     </div>
                             </div>
                 <!Start Comment feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_c")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="c-y"type="radio" name="Comment" value="0" checked=""/>
                                     <label for="c-y">Yes</label></div>
                                   <div>
                                       <input id="c-n" type="radio" name="Comment" value="1" checked=""/>
                                     <label for="c-n">No</label></div>
                                     </div>
                             </div>
             <!Start Advistise feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_d")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="d-y"type="radio" name="Advert" value="0" checked=""/>
                                     <label for="d-y">Yes</label></div>
                                   <div>
                                       <input id="d-n" type="radio" name="Advert" value="1" checked=""/>
                                     <label for="d-n">No</label></div>
                                     </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("ADD_cat")?>" class="btn btn-primary btn-lg" />
                                 </div>
                             </div>
                         </form>
                      </div>
    
       
            
        <?php }
     /*========================================================= 
        *
        * if do==Insert
        * 
    * ========================================================= 
    */
        elseif ($do==='insert') {
                 //in insert form
           
                if($_SERVER['REQUEST_METHOD']=='POST'){
                echo '<h1 class="text-center"> Insert Category</h1>';
                echo '<div class="container">';
                $name=$_POST['name'];
                $des=$_POST['discription'];
                 $parent=$_POST['sub'];
                $order=$_POST['order'];
                $vis=$_POST['visable'];
                $vcom=$_POST['Comment'];
                $adv=$_POST['Advert'];
                //updata database
                
                       //Insert Category
                       //check if Category exist
                       $check=  CheckItems("Name", "categories", $name);
                       if($check==1){
                           $msg= '<div class="alert alert-danger>"sorry this Category is exist</div>';
                            Redirect($msg,"back");
                       }
                       else{
                           try{
                     $statm=$con->prepare('INSERT INTO categories (Name,Discription,parent,Ordering,Visability,Allow_comment,Allow_Ads)VALUES(:zname,:zdes,:zpar,:zord,:zvis,:zcom,:zads)' );
                       $statm->execute(array("zname"=>$name,"zdes"=>$des,"zpar"=>$parent,"zord"=>$order,"zvis"=>$vis,"zcom"=>$vcom,"zads"=>$adv));
                            }catch(PDOException $e)
                             {
                                  echo $e->getMessage();        
                              }
                //echo success message
                       echo '<div class="container">';
                   $msg1= '<div class="alert alert-success">'.$statm->rowCount().'record Inserted</div>';
                     Redirect($msg1,"back",5);
                     echo '</div>';
                       }
                       
            }//end is not post request 
            else{
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null,5);
            }//end show error
            echo '</div>';
        }
    /*========================================================= 
        *
        * if do==edit
        * 
    * ========================================================= 
    */
        elseif($do=='edit'){
               $catid= isset($_GET['catid'])&& is_numeric($_GET['catid']) ?intval($_GET['catid']):0;
        $statm=$con->prepare('SELECT * FROM categories WHERE ID=?');
        $statm->execute(array($catid));
        $cat=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {?>      
                <h1 class="text-center"><?php echo lang("Cat_ed")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="Categories.php?do=update" method="POST">
                        <input type="hidden" name="catid"value="<?php echo $catid?>"/>

                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name"  class="form-control"  required="required" placeholder="Name Of The Category" value="<?php echo $cat['Name']?>"/>
                                 </div>
                             </div>
                          <!Start Discription!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_Des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="discription" class="form-control"   placeholder="Describe the Category" value="<?php echo $cat['Discription']?>"/>
                                 </div>
                             </div>
                            <!Start Supcategory selection!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">parent?</label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="sub">
                                         <option value="0">None</option>
                                         <?php
                                         $cats= genral_feth("*","categories","WHERE parent=0","","ID","ASC");
                                         foreach ($cats as $c){
                                         echo " <option value='".$c['ID']."'";
                                         if($c['ID']===$cat['parent']){
                                             echo 'selected="selected"';
                                         }
                                         echo ">".$c['Name']."</option> ";
                                         }
                                         ?>
                                     </select>
                                 </div>
                             </div>
                <!Start ordering!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_or")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="order"  class="form-control" placeholder="Number to arrange the categories" value="<?php echo $cat['Ordering']?>"/>
                                 </div>
                             </div>
                <!Start Visability feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_vis")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="v-y"type="radio" name="visable" value="0" <?php if($cat['Visability']==0) {echo "checked";}?>/>
                                     <label for="v-y">Yes</label></div>
                                   <div>
                                       <input id="v-n" type="radio" name="visable" value="1" <?php if($cat['Visability']==1) {echo "checked";}?> />
                                     <label for="v-n">No</label></div>
                                     </div>
                             </div>
                 <!Start Comment feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_c")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="c-y"type="radio" name="Comment" value="0"<?php if($cat['Allow_comment']==0) {echo "checked";}?> />
                                     <label for="c-y">Yes</label></div>
                                   <div>
                                       <input id="c-n" type="radio" name="Comment" value="1" <?php if($cat['Allow_comment']==1) {echo "checked";}?>/>
                                     <label for="c-n">No</label></div>
                                     </div>
                             </div>
             <!Start Advistise feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_d")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="d-y"type="radio" name="Advert" value="0" <?php if($cat['Allow_Ads']==0) {echo "checked";}?>/>
                                     <label for="d-y">Yes</label></div>
                                   <div>
                                       <input id="d-n" type="radio" name="Advert" value="1" <?php if($cat['Allow_Ads']==1) {echo "checked";}?> />
                                     <label for="d-n">No</label></div>
                                     </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("save_cat")?>" class="btn btn-primary btn-lg" />
                                 </div>
                             </div>
                         </form>
                      </div>
                
                
                
        
            
        <?php   }
        //$count =0 in other word no such userid in database
        else{
                echo '<div class="container">';
        $msg= '<div class="alert alert-danger">There is no such ID</div>';
                     Redirect($msg);    
                  echo '</div>';

        }
        }
    /*========================================================= 
        *
        * if do==update
        * 
    * ========================================================= 
    */
        elseif($do=='update'){
                     if($_SERVER['REQUEST_METHOD']=='POST'){
                  echo '<h1 class="text-center"> update Category</h1>';
            echo '<div class="container">';
                $id=filter_input(INPUT_POST, 'catid');
                $nam=filter_input(INPUT_POST, 'name');
                $des=filter_input(INPUT_POST, 'discription');
                $parent=filter_input(INPUT_POST, 'sub');
                $or=filter_input(INPUT_POST, 'order');
                $vi=filter_input(INPUT_POST, 'visable');
                $co=filter_input(INPUT_POST, 'Comment');
                $ad=filter_input(INPUT_POST, 'Advert');
               
                //updata database
                  
                $statm=$con->prepare("UPDATE categories SET Name=?,Discription=?,parent=?,Ordering=?,Visability=?,Allow_comment=?,Allow_Ads=? WHERE ID=?");
                $statm->execute(array($nam,$des,$parent,$or,$vi,$co,$ad,$id));
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect($msg,"back");
                     echo '</div>';
                     
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }
    /*========================================================= 
        *
        * if do==delete
        * 
    * ========================================================= 
    */
         elseif ($do==='delete') {
             //delete Member
            echo '<h1 class="text-center"> Delete Category</h1>';
            echo '<div class="container">';
                    $catid= isset($_GET['catid'])&& is_numeric($_GET['catid']) ?intval($_GET['catid']):0;
                    
                   $check=  CheckItems("ID", "categories", $catid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM categories WHERE ID=:zID');
                        $statm->bindParam(":zID",$catid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
         }
      
       include $tpl.'Footer.php';
    }
else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 

