<?php ob_start(); ?>    
<?php 
/*========================================================= 
 *Items Page 
 * ========================================================= 
 */
    session_start();
        $PageTitel='items';

    if(isset($_SESSION['user'])){
        include 'inti.php';
    $do=isset($_GET['do'])?$_GET['do']:'manage';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){
 
            //fetch data from database to put into table
            $statm=$con->prepare("SELECT items.*,categories.Name AS Category_name,Users.UserName AS US_NM "
                               . "FROM items INNER JOIN categories ON categories.ID=items.category_id "
                               . "INNER JOIN Users ON  Users.userID=items.member_id");
            $statm->execute();//select all items expect admin
            $itms=$statm->fetchAll();
            if(!empty($itms)){
            ?>
        <h1 class="text-center">Mange Items Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>    <tr>
                               <td>#ID</td>
                               <td>User Name</td>
                               <td>Description</td>
                               <td>Price</td>
                               <td>Adding Date</td>
                               <td>Category</td>
                               <td>User Name</td>
                               <td>Control</td>
                                </tr></thead>
                        <?php
                                    foreach ($itms as $item){
                                        echo '<tr>';
                                            echo'<td>'.$item['item_id'].'</td>';
                                            echo'<td data-popup="'.$item['item_name'].'">'.$item['item_name'].'</td>';
                                             echo'<td data-popup="'.$item['description'].'" class="p_des">'.$item['description'].'</td>';
                                             echo'<td>'.$item['price'].'</td>';
                                              echo'<td>'.$item['add_date'].'</td>';
                                              echo'<td>'.$item['Category_name'].'</td>';
                                              echo'<td>'.$item['US_NM'].'</td>';
                                              
                            echo'<td> <a href="items.php?do=edit&itemid='.$item['item_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                         . '<a href="items.php?do=delete&itemid='.$item['item_id'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                      if($item['Approve_itm']==0){
                         echo '<a href="items.php?do=Approve&itemid='.$item['item_id'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>
                <a href="items.php?do=Add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>New Item</a>

            </div><?php     }else {echo '<div class="container"><div class="message">there is no Categories</div><a href="items.php?do=Add" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>New Item</a></div>';}   }
    /*========================================================= 
            *
            * if do==add
            * 
    * ========================================================= 
    */
        elseif($do==='Add'){
     ?>
        <h1 class="text-center"><?php echo lang("itm_ad")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="items.php?do=insert" method="POST">
                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_nm")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name" 
                                            class="form-control" 
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Name Of The Item"/>
                                 </div>
                             </div>
                          
               <!Start Description!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="des"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Description Of The Item"/>
                                 </div>
                             </div>
            <!Start Price!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_price")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="pri"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Price Of The Item"/>
                                 </div>
                             </div>
          <!Start country made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_country")?></label>
                                 <div class="col-sm-10 col-md-4">
                             <!Start country made!>
                                      <select name="cont"><option value="Pick a Country">Choose Country</option><option value="Afghanistan">Afghanistan</option><option value="Albania">Albania</option>
                                          <option value="Algeria">Algeria</option>
                                                  <option value="American Samoa">American Samoa</option>
                                                  <option value="Andorra">Andorra</option>
                                                  <option value="Angola">Angola</option>
                                                  <option value="Anguilla">Anguilla</option>
                                                  <option value="Antarctica">Antarctica</option>
                                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                  <option value="Argentina">Argentina</option>
                                                  <option value="Armenia">Armenia</option>
                                                  <option value="Aruba">Aruba</option>
                                                  <option value="Australia">Australia</option>
                                                  <option value="Austria">Austria</option>
                                                  <option value="Azerbaijan">Azerbaijan</option>
                                                  <option value="Bahamas">Bahamas</option>
                                                  <option value="Bahrain">Bahrain</option>
                                                  <option value="Bangladesh">Bangladesh</option>
                                                  <option value="Barbados">Barbados</option>
                                                  <option value="Belarus">Belarus</option>
                                                  <option value="Belgium">Belgium</option>
                                                  <option value="Belize">Belize</option>
                                                  <option value="Benin">Benin</option>
                                                  <option value="Bermuda">Bermuda</option>
                                                  <option value="Bhutan">Bhutan</option>
                                                  <option value="Bolivia">Bolivia</option>
                                                  <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                  <option value="Botswana">Botswana</option>
                                                  <option value="Bouvet Island">Bouvet Island</option>
                                                  <option value="Brazil">Brazil</option>
                                                  <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                  <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                  <option value="Bulgaria">Bulgaria</option>
                                                  <option value="Burkina Faso">Burkina Faso</option>
                                                  <option value="Burundi">Burundi</option>
                                                  <option value="Cambodia">Cambodia</option>
                                                  <option value="Cameroon">Cameroon</option>
                                                  <option value="Canada">Canada</option>
                                                  <option value="Cape Verde">Cape Verde</option>
                                                  <option value="Cayman Islands">Cayman Islands</option>
                                                  <option value="Central African Republic">Central African Republic</option>
                                                  <option value="Chad">Chad</option>
                                                  <option value="Chile">Chile</option>
                                                  <option value="China">China</option>
                                                  <option value="Christmas Island">Christmas Island</option>
                                                  <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                  <option value="Colombia">Colombia</option>
                                                  <option value="Comoros">Comoros</option>
                                                  <option value="Congo">Congo</option>
                                                  <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                  <option value="Cook Islands">Cook Islands</option>
                                                  <option value="Costa Rica">Costa Rica</option>
                                                  <option value="Cote D&#39;ivoire">Cote D'ivoire</option>
                                                  <option value="Croatia">Croatia</option>
                                                  <option value="Cuba">Cuba</option>
                                                  <option value="Cyprus">Cyprus</option>
                                                  <option value="Czech Republic">Czech Republic</option>
                                                  <option value="Denmark">Denmark</option>
                                                  <option value="Djibouti">Djibouti</option>
                                                  <option value="Dominica">Dominica</option>
                                                  <option value="Dominican Republic">Dominican Republic</option>
                                                  <option value="Ecuador">Ecuador</option>
                                                  <option value="Egypt">Egypt</option>
                                                  <option value="El Salvador">El Salvador</option>
                                                  <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                  <option value="Eritrea">Eritrea</option>
                                                  <option value="Estonia">Estonia</option>
                                                  <option value="Ethiopia">Ethiopia</option>
                                                  <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                  <option value="Faroe Islands">Faroe Islands</option>
                                                  <option value="Fiji">Fiji</option>
                                                  <option value="Finland">Finland</option>
                                                  <option value="France">France</option>
                                                  <option value="French Guiana">French Guiana</option>
                                                  <option value="French Polynesia">French Polynesia</option>
                                                  <option value="French Southern Territories">French Southern Territories</option>
                                                  <option value="Gabon">Gabon</option>
                                                  <option value="Gambia">Gambia</option>
                                                  <option value="Georgia">Georgia</option>
                                                  <option value="Germany">Germany</option>
                                                  <option value="Ghana">Ghana</option>
                                                  <option value="Gibraltar">Gibraltar</option>
                                                  <option value="Greece">Greece</option>
                                                  <option value="Greenland">Greenland</option>
                                                  <option value="Grenada">Grenada</option>
                                                  <option value="Guadeloupe">Guadeloupe</option>
                                                  <option value="Guam">Guam</option>
                                                  <option value="Guatemala">Guatemala</option>
                                                  <option value="Guinea">Guinea</option>
                                                  <option value="Guinea-bissau">Guinea-bissau</option>
                                                  <option value="Guyana">Guyana</option>
                                                  <option value="Haiti">Haiti</option>
                                                  <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                  <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                  <option value="Honduras">Honduras</option>
                                                  <option value="Hong Kong">Hong Kong</option>
                                                  <option value="Hungary">Hungary</option>
                                                  <option value="Iceland">Iceland</option>
                                                  <option value="India">India</option>
                                                  <option value="Indonesia">Indonesia</option>
                                                  <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                  <option value="Iraq">Iraq</option>
                                                  <option value="Ireland">Ireland</option>
                                                  <option value="Israel">Israel</option>
                                                  <option value="Italy">Italy</option>
                                                  <option value="Jamaica">Jamaica</option>
                                                  <option value="Japan">Japan</option>
                                                  <option value="Jordan">Jordan</option>
                                                  <option value="Kazakhstan">Kazakhstan</option>
                                                  <option value="Kenya">Kenya</option>
                                                  <option value="Kiribati">Kiribati</option>
                                                  <option value="Korea, Democratic People&#39;s Republic of">Korea, Democratic People's Republic of</option>
                                                  <option value="Korea, Republic of">Korea, Republic of</option>
                                                  <option value="Kuwait">Kuwait</option>
                                                  <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                  <option value="Lao People&#39;s Democratic Republic">Lao People's Democratic Republic</option>
                                                  <option value="Latvia">Latvia</option>
                                                  <option value="Lebanon">Lebanon</option>
                                                  <option value="Lesotho">Lesotho</option>
                                                  <option value="Liberia">Liberia</option>
                                                  <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                  <option value="Liechtenstein">Liechtenstein</option>
                                                  <option value="Lithuania">Lithuania</option>
                                                  <option value="Luxembourg">Luxembourg</option>
                                                  <option value="Macao">Macao</option>
                                                  <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                  <option value="Madagascar">Madagascar</option>
                                                  <option value="Malawi">Malawi</option>
                                                  <option value="Malaysia">Malaysia</option>
                                                  <option value="Maldives">Maldives</option>
                                                  <option value="Mali">Mali</option>
                                                  <option value="Malta">Malta</option>
                                                  <option value="Marshall Islands">Marshall Islands</option>
                                                  <option value="Martinique">Martinique</option>
                                                  <option value="Mauritania">Mauritania</option>
                                                  <option value="Mauritius">Mauritius</option>
                                                  <option value="Mayotte">Mayotte</option>
                                                  <option value="Mexico">Mexico</option>
                                                  <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                  <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                  <option value="Monaco">Monaco</option>
                                                  <option value="Mongolia">Mongolia</option>
                                                  <option value="Montenegro">Montenegro</option>
                                                  <option value="Montserrat">Montserrat</option>
                                                  <option value="Morocco">Morocco</option>
                                                  <option value="Mozambique">Mozambique</option>
                                                  <option value="Myanmar">Myanmar</option>
                                                  <option value="Namibia">Namibia</option>
                                                  <option value="Nauru">Nauru</option>
                                                  <option value="Nepal">Nepal</option>
                                                  <option value="Netherlands">Netherlands</option>
                                                  <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                  <option value="New Caledonia">New Caledonia</option>
                                                  <option value="New Zealand">New Zealand</option>
                                                  <option value="Nicaragua">Nicaragua</option>
                                                  <option value="Niger">Niger</option>
                                                  <option value="Nigeria">Nigeria</option>
                                                  <option value="Niue">Niue</option>
                                                  <option value="Norfolk Island">Norfolk Island</option>
                                                  <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                  <option value="Norway">Norway</option>
                                                  <option value="Oman">Oman</option>
                                                  <option value="Pakistan">Pakistan</option>
                                                  <option value="Palau">Palau</option>
                                                  <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                  <option value="Panama">Panama</option>
                                                  <option value="Papua New Guinea">Papua New Guinea</option>
                                                  <option value="Paraguay">Paraguay</option>
                                                  <option value="Peru">Peru</option>
                                                  <option value="Philippines">Philippines</option>
                                                  <option value="Pitcairn">Pitcairn</option>
                                                  <option value="Poland">Poland</option>
                                                  <option value="Portugal">Portugal</option>
                                                  <option value="Puerto Rico">Puerto Rico</option>
                                                  <option value="Qatar">Qatar</option>
                                                  <option value="Reunion">Reunion</option>
                                                  <option value="Romania">Romania</option>
                                                  <option value="Russian Federation">Russian Federation</option>
                                                  <option value="Rwanda">Rwanda</option>
                                                  <option value="Saint Helena">Saint Helena</option>
                                                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                  <option value="Saint Lucia">Saint Lucia</option>
                                                  <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                  <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                  <option value="Samoa">Samoa</option>
                                                  <option value="San Marino">San Marino</option>
                                                  <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                  <option value="Saudi Arabia">Saudi Arabia</option>
                                                  <option value="Senegal">Senegal</option>
                                                  <option value="Serbia">Serbia</option>
                                                  <option value="Seychelles">Seychelles</option>
                                                  <option value="Sierra Leone">Sierra Leone</option>
                                                  <option value="Singapore">Singapore</option>
                                                  <option value="Slovakia">Slovakia</option>
                                                  <option value="Slovenia">Slovenia</option>
                                                  <option value="Solomon Islands">Solomon Islands</option>
                                                  <option value="Somalia">Somalia</option>
                                                  <option value="South Africa">South Africa</option>
                                                  <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                  <option value="South Sudan">South Sudan</option>
                                                  <option value="Spain">Spain</option>
                                                  <option value="Sri Lanka">Sri Lanka</option>
                                                  <option value="Sudan">Sudan</option>
                                                  <option value="Suriname">Suriname</option>
                                                  <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                  <option value="Swaziland">Swaziland</option>
                                                  <option value="Sweden">Sweden</option>
                                                  <option value="Switzerland">Switzerland</option>
                                                  <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                  <option value="Taiwan, Republic of China">Taiwan, Republic of China</option>
                                                  <option value="Tajikistan">Tajikistan</option>
                                                  <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                  <option value="Thailand">Thailand</option>
                                                  <option value="Timor-leste">Timor-leste</option>
                                                  <option value="Togo">Togo</option>
                                                  <option value="Tokelau">Tokelau</option>
                                                  <option value="Tonga">Tonga</option>
                                                  <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                  <option value="Tunisia">Tunisia</option>
                                                  <option value="Turkey">Turkey</option>
                                                  <option value="Turkmenistan">Turkmenistan</option>
                                                  <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                  <option value="Tuvalu">Tuvalu</option>
                                                  <option value="Uganda">Uganda</option>
                                                  <option value="Ukraine">Ukraine</option>
                                                  <option value="United Arab Emirates">United Arab Emirates</option>
                                                  <option value="United Kingdom">United Kingdom</option>
                                                  <option value="United States">United States</option>
                                                  <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                  <option value="Uruguay">Uruguay</option>
                                                  <option value="Uzbekistan">Uzbekistan</option>
                                                  <option value="Vanuatu">Vanuatu</option>
                                                  <option value="Venezuela">Venezuela</option>
                                                  <option value="Viet Nam">Viet Nam</option>
                                                  <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                  <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                  <option value="Western Sahara">Western Sahara</option>
                                                  <option value="Yemen">Yemen</option>
                                                  <option value="Zambia">Zambia</option>
                                                  <option value="Zimbabwe">Zimbabwe</option>
                                          </select>
                                            <!End Start country made!>

                                 </div>
                             </div>
                          
          <!Start status made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_stat")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="status" required="required">
                                         <option value="0">..</option>
                                         <option value="1">New</option>
                                         <option value="2">Like New</option>
                                         <option value="3">Used</option>
                                         <option value="4">Old</option>
                                     </select>
                                            
                                 </div>
                             </div>
       
       <!Start Members Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_fld")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="member" required="required">
                                         <option value="0">..</option>
                                        <?php
                                       // $statm=$con->prepare('SELECT * FROM users');
                                     //   $statm->execute();
                                        $r= genral_feth("*","users","","","userID");
                                        foreach ($r as $usr){
                                            echo "<option value='".$usr['userID']."'>".$usr['UserName']."</option>";
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
       
      <!Start Category Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Admin_CAT")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="categ" required="required">
                                         <option value="0">..</option>
                                        <?php
                                       
                                        $c= genral_feth("*","categories","WHERE parent=0","","ID","ASC");

                                        foreach ($c as $cats){
                                            echo "<option value='".$cats['ID']."'>".$cats['Name']."</option>";
                             $child_cats= genral_feth("*","categories","WHERE parent={$cats['ID']}","","ID","ASC");
                                            foreach ($child_cats as $child){
                             echo "<option value='".$child['ID']."'>---".$child['Name']."</option>";

                                            }
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
        <!Start Tags!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_price")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="tags"
                                            class="form-control"
                                            autocomplete="off" 
                                            placeholder="Seprate Tags with comma(..,..)"/>
                                 </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" 
                                            value="<?php echo lang("itm_add")?>" 
                                            class="btn btn-primary btn-sm" />
                                 </div>
                             </div>
                         </form>
                      </div>
    
       
            
    <?php
            
        }
    /*========================================================= 
            *
            * if do==Insert
            * 
    * ========================================================= 
    */
        elseif ($do==='insert') {
             //in insert form
           
                if($_SERVER['REQUEST_METHOD']=='POST'){
                echo '<h1 class="text-center"> Insert Items</h1>';
                echo '<div class="container">';
                $name=filter_input(INPUT_POST, 'name');
                $des=filter_input(INPUT_POST, 'des');
                $pri=filter_input(INPUT_POST, 'pri');
                $cont=filter_input(INPUT_POST, 'cont');
                $status=filter_input(INPUT_POST, 'status');
                $memb=filter_input(INPUT_POST, 'member');
                $categ=filter_input(INPUT_POST, 'categ');
                $tags=filter_input(INPUT_POST, 'tags');

                
                //validate the form
                $errorarray=array();
                if(empty($name)){$errorarray[]="Name must be <strong>not Empty</strong>";}
                if(empty($des)){$errorarray[]="Description must be <strong>not Empty</strong>";}
                if(empty($pri)){$errorarray[]="Price must be <strong>not Empty</strong>";}
                if(empty($cont)){$errorarray[]="country must be <strong>not Empty</strong>";}
                if($status==0){$errorarray[]="you must choose <strong>status</strong>";}
                if($memb==0){$errorarray[]="you must choose <strong>The member</strong>";}
                if($categ==0){$errorarray[]="you must choose <strong>The category</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\">'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                       //Insert Items
                       try{
           $statm=$con->prepare('INSERT INTO items (item_name,description,price,add_date,country_name,status,category_id,member_id,tags)VALUES(:zname,:zdes,:zpri,now(),:zcon,:zst,:zcat,:zmem,:ztags)' );
           $statm->execute(array("zname"=>$name,"zdes"=>$des,"zpri"=>$pri,"zcon"=>$cont,"zst"=>$status,"zcat"=>$categ,"zmem"=>$memb,"ztags"=>$tags));
                     
                      //echo success message
                       echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect($msg,"back");
                     echo '</div>';
                       }catch(PDOException $e)
                             {
                                  echo $e->getMessage();    
                              }
                       
                       }
            }//end is not post request 
            else{
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg);
            }//end show error
            echo '</div>';
        }
    /*========================================================= 
            *
            * if do==edit
            * 
    * ========================================================= 
    */
        elseif($do=='edit'){
            
        
        $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
        $statm=$con->prepare('SELECT * FROM items WHERE item_id=?');
        $statm->execute(array($itemid));
        $items=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {   
              ?>
         <h1 class="text-center"><?php echo lang("itm_ed")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="items.php?do=update" method="POST">
                <input type="hidden" name="itmid"value="<?php echo $itemid?>"/>

                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_nm")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name" 
                                            class="form-control" 
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Name Of The Item"
                                            value="<?php echo $items['item_name'];?>"/>
                                 </div>
                             </div>
                          
               <!Start Description!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="des"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Description Of The Item"
                                             value="<?php echo $items['description'];?>"/>
                                 </div>
                             </div>
            <!Start Price!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_price")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="pri"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Price Of The Item"
                                            value="<?php echo $items['price'];?>"/>
                                 </div>
                             </div>
          <!Start country made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_country")?></label>
                                 <div class="col-sm-10 col-md-4">
                             <!Start country made!>
                             <select name="cont"><option value="<?php echo $items['country_name'];?>" selected="selected">
                                     <?php echo $items['country_name'];?></option>
                                                 <option value="Afghanistan">Afghanistan</option>
                                                 <option value="Albania">Albania</option>
                                                  <option value="Algeria">Algeria</option>
                                                  <option value="American Samoa">American Samoa</option>
                                                  <option value="Andorra">Andorra</option>
                                                  <option value="Angola">Angola</option>
                                                  <option value="Anguilla">Anguilla</option>
                                                  <option value="Antarctica">Antarctica</option>
                                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                  <option value="Argentina">Argentina</option>
                                                  <option value="Armenia">Armenia</option>
                                                  <option value="Aruba">Aruba</option>
                                                  <option value="Australia">Australia</option>
                                                  <option value="Austria">Austria</option>
                                                  <option value="Azerbaijan">Azerbaijan</option>
                                                  <option value="Bahamas">Bahamas</option>
                                                  <option value="Bahrain">Bahrain</option>
                                                  <option value="Bangladesh">Bangladesh</option>
                                                  <option value="Barbados">Barbados</option>
                                                  <option value="Belarus">Belarus</option>
                                                  <option value="Belgium">Belgium</option>
                                                  <option value="Belize">Belize</option>
                                                  <option value="Benin">Benin</option>
                                                  <option value="Bermuda">Bermuda</option>
                                                  <option value="Bhutan">Bhutan</option>
                                                  <option value="Bolivia">Bolivia</option>
                                                  <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                  <option value="Botswana">Botswana</option>
                                                  <option value="Bouvet Island">Bouvet Island</option>
                                                  <option value="Brazil">Brazil</option>
                                                  <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                  <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                  <option value="Bulgaria">Bulgaria</option>
                                                  <option value="Burkina Faso">Burkina Faso</option>
                                                  <option value="Burundi">Burundi</option>
                                                  <option value="Cambodia">Cambodia</option>
                                                  <option value="Cameroon">Cameroon</option>
                                                  <option value="Canada">Canada</option>
                                                  <option value="Cape Verde">Cape Verde</option>
                                                  <option value="Cayman Islands">Cayman Islands</option>
                                                  <option value="Central African Republic">Central African Republic</option>
                                                  <option value="Chad">Chad</option>
                                                  <option value="Chile">Chile</option>
                                                  <option value="China">China</option>
                                                  <option value="Christmas Island">Christmas Island</option>
                                                  <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                  <option value="Colombia">Colombia</option>
                                                  <option value="Comoros">Comoros</option>
                                                  <option value="Congo">Congo</option>
                                                  <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                  <option value="Cook Islands">Cook Islands</option>
                                                  <option value="Costa Rica">Costa Rica</option>
                                                  <option value="Cote D&#39;ivoire">Cote D'ivoire</option>
                                                  <option value="Croatia">Croatia</option>
                                                  <option value="Cuba">Cuba</option>
                                                  <option value="Cyprus">Cyprus</option>
                                                  <option value="Czech Republic">Czech Republic</option>
                                                  <option value="Denmark">Denmark</option>
                                                  <option value="Djibouti">Djibouti</option>
                                                  <option value="Dominica">Dominica</option>
                                                  <option value="Dominican Republic">Dominican Republic</option>
                                                  <option value="Ecuador">Ecuador</option>
                                                  <option value="Egypt">Egypt</option>
                                                  <option value="El Salvador">El Salvador</option>
                                                  <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                  <option value="Eritrea">Eritrea</option>
                                                  <option value="Estonia">Estonia</option>
                                                  <option value="Ethiopia">Ethiopia</option>
                                                  <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                  <option value="Faroe Islands">Faroe Islands</option>
                                                  <option value="Fiji">Fiji</option>
                                                  <option value="Finland">Finland</option>
                                                  <option value="France">France</option>
                                                  <option value="French Guiana">French Guiana</option>
                                                  <option value="French Polynesia">French Polynesia</option>
                                                  <option value="French Southern Territories">French Southern Territories</option>
                                                  <option value="Gabon">Gabon</option>
                                                  <option value="Gambia">Gambia</option>
                                                  <option value="Georgia">Georgia</option>
                                                  <option value="Germany">Germany</option>
                                                  <option value="Ghana">Ghana</option>
                                                  <option value="Gibraltar">Gibraltar</option>
                                                  <option value="Greece">Greece</option>
                                                  <option value="Greenland">Greenland</option>
                                                  <option value="Grenada">Grenada</option>
                                                  <option value="Guadeloupe">Guadeloupe</option>
                                                  <option value="Guam">Guam</option>
                                                  <option value="Guatemala">Guatemala</option>
                                                  <option value="Guinea">Guinea</option>
                                                  <option value="Guinea-bissau">Guinea-bissau</option>
                                                  <option value="Guyana">Guyana</option>
                                                  <option value="Haiti">Haiti</option>
                                                  <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                  <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                  <option value="Honduras">Honduras</option>
                                                  <option value="Hong Kong">Hong Kong</option>
                                                  <option value="Hungary">Hungary</option>
                                                  <option value="Iceland">Iceland</option>
                                                  <option value="India">India</option>
                                                  <option value="Indonesia">Indonesia</option>
                                                  <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                  <option value="Iraq">Iraq</option>
                                                  <option value="Ireland">Ireland</option>
                                                  <option value="Israel">Israel</option>
                                                  <option value="Italy">Italy</option>
                                                  <option value="Jamaica">Jamaica</option>
                                                  <option value="Japan">Japan</option>
                                                  <option value="Jordan">Jordan</option>
                                                  <option value="Kazakhstan">Kazakhstan</option>
                                                  <option value="Kenya">Kenya</option>
                                                  <option value="Kiribati">Kiribati</option>
                                                  <option value="Korea, Democratic People&#39;s Republic of">Korea, Democratic People's Republic of</option>
                                                  <option value="Korea, Republic of">Korea, Republic of</option>
                                                  <option value="Kuwait">Kuwait</option>
                                                  <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                  <option value="Lao People&#39;s Democratic Republic">Lao People's Democratic Republic</option>
                                                  <option value="Latvia">Latvia</option>
                                                  <option value="Lebanon">Lebanon</option>
                                                  <option value="Lesotho">Lesotho</option>
                                                  <option value="Liberia">Liberia</option>
                                                  <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                  <option value="Liechtenstein">Liechtenstein</option>
                                                  <option value="Lithuania">Lithuania</option>
                                                  <option value="Luxembourg">Luxembourg</option>
                                                  <option value="Macao">Macao</option>
                                                  <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                  <option value="Madagascar">Madagascar</option>
                                                  <option value="Malawi">Malawi</option>
                                                  <option value="Malaysia">Malaysia</option>
                                                  <option value="Maldives">Maldives</option>
                                                  <option value="Mali">Mali</option>
                                                  <option value="Malta">Malta</option>
                                                  <option value="Marshall Islands">Marshall Islands</option>
                                                  <option value="Martinique">Martinique</option>
                                                  <option value="Mauritania">Mauritania</option>
                                                  <option value="Mauritius">Mauritius</option>
                                                  <option value="Mayotte">Mayotte</option>
                                                  <option value="Mexico">Mexico</option>
                                                  <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                  <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                  <option value="Monaco">Monaco</option>
                                                  <option value="Mongolia">Mongolia</option>
                                                  <option value="Montenegro">Montenegro</option>
                                                  <option value="Montserrat">Montserrat</option>
                                                  <option value="Morocco">Morocco</option>
                                                  <option value="Mozambique">Mozambique</option>
                                                  <option value="Myanmar">Myanmar</option>
                                                  <option value="Namibia">Namibia</option>
                                                  <option value="Nauru">Nauru</option>
                                                  <option value="Nepal">Nepal</option>
                                                  <option value="Netherlands">Netherlands</option>
                                                  <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                  <option value="New Caledonia">New Caledonia</option>
                                                  <option value="New Zealand">New Zealand</option>
                                                  <option value="Nicaragua">Nicaragua</option>
                                                  <option value="Niger">Niger</option>
                                                  <option value="Nigeria">Nigeria</option>
                                                  <option value="Niue">Niue</option>
                                                  <option value="Norfolk Island">Norfolk Island</option>
                                                  <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                  <option value="Norway">Norway</option>
                                                  <option value="Oman">Oman</option>
                                                  <option value="Pakistan">Pakistan</option>
                                                  <option value="Palau">Palau</option>
                                                  <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                  <option value="Panama">Panama</option>
                                                  <option value="Papua New Guinea">Papua New Guinea</option>
                                                  <option value="Paraguay">Paraguay</option>
                                                  <option value="Peru">Peru</option>
                                                  <option value="Philippines">Philippines</option>
                                                  <option value="Pitcairn">Pitcairn</option>
                                                  <option value="Poland">Poland</option>
                                                  <option value="Portugal">Portugal</option>
                                                  <option value="Puerto Rico">Puerto Rico</option>
                                                  <option value="Qatar">Qatar</option>
                                                  <option value="Reunion">Reunion</option>
                                                  <option value="Romania">Romania</option>
                                                  <option value="Russian Federation">Russian Federation</option>
                                                  <option value="Rwanda">Rwanda</option>
                                                  <option value="Saint Helena">Saint Helena</option>
                                                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                  <option value="Saint Lucia">Saint Lucia</option>
                                                  <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                  <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                  <option value="Samoa">Samoa</option>
                                                  <option value="San Marino">San Marino</option>
                                                  <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                  <option value="Saudi Arabia">Saudi Arabia</option>
                                                  <option value="Senegal">Senegal</option>
                                                  <option value="Serbia">Serbia</option>
                                                  <option value="Seychelles">Seychelles</option>
                                                  <option value="Sierra Leone">Sierra Leone</option>
                                                  <option value="Singapore">Singapore</option>
                                                  <option value="Slovakia">Slovakia</option>
                                                  <option value="Slovenia">Slovenia</option>
                                                  <option value="Solomon Islands">Solomon Islands</option>
                                                  <option value="Somalia">Somalia</option>
                                                  <option value="South Africa">South Africa</option>
                                                  <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                  <option value="South Sudan">South Sudan</option>
                                                  <option value="Spain">Spain</option>
                                                  <option value="Sri Lanka">Sri Lanka</option>
                                                  <option value="Sudan">Sudan</option>
                                                  <option value="Suriname">Suriname</option>
                                                  <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                  <option value="Swaziland">Swaziland</option>
                                                  <option value="Sweden">Sweden</option>
                                                  <option value="Switzerland">Switzerland</option>
                                                  <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                  <option value="Taiwan, Republic of China">Taiwan, Republic of China</option>
                                                  <option value="Tajikistan">Tajikistan</option>
                                                  <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                  <option value="Thailand">Thailand</option>
                                                  <option value="Timor-leste">Timor-leste</option>
                                                  <option value="Togo">Togo</option>
                                                  <option value="Tokelau">Tokelau</option>
                                                  <option value="Tonga">Tonga</option>
                                                  <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                  <option value="Tunisia">Tunisia</option>
                                                  <option value="Turkey">Turkey</option>
                                                  <option value="Turkmenistan">Turkmenistan</option>
                                                  <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                  <option value="Tuvalu">Tuvalu</option>
                                                  <option value="Uganda">Uganda</option>
                                                  <option value="Ukraine">Ukraine</option>
                                                  <option value="United Arab Emirates">United Arab Emirates</option>
                                                  <option value="United Kingdom">United Kingdom</option>
                                                  <option value="United States">United States</option>
                                                  <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                  <option value="Uruguay">Uruguay</option>
                                                  <option value="Uzbekistan">Uzbekistan</option>
                                                  <option value="Vanuatu">Vanuatu</option>
                                                  <option value="Venezuela">Venezuela</option>
                                                  <option value="Viet Nam">Viet Nam</option>
                                                  <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                  <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                  <option value="Western Sahara">Western Sahara</option>
                                                  <option value="Yemen">Yemen</option>
                                                  <option value="Zambia">Zambia</option>
                                                  <option value="Zimbabwe">Zimbabwe</option>
                                          </select>
                                            <!End Start country made!>

                                 </div>
                             </div>
                          
          <!Start status made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_stat")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="status"  required="required">
                                         <option value="0" >..</option>
                                         <option value="1" <?php if($items['status']==1){echo "selected";}?>>New</option>
                                         <option value="2"<?php if($items['status']==2){echo "selected";}?>>Like New</option>
                                         <option value="3"<?php if($items['status']==3){echo "selected";}?>>Used</option>
                                         <option value="4"<?php if($items['status']==4){echo "selected";}?>>Old</option>
                                     </select>
                                            
                                 </div>
                             </div>
       
       <!Start Members Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_fld")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="member" required="required">
                                        <?php
                                        $statm=$con->prepare('SELECT * FROM users');
                                        $statm->execute();
                                        $r= $statm->fetchAll();
                                        foreach ($r as $usr){
                                            echo "<option value='".$usr['userID']."'";
                                            if($items['member_id']==$usr['userID'])
                                                {
                                                echo "selected";
                                                
                                                }
                                            echo ">"; echo $usr['UserName']."</option>";
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
       
      <!Start Category Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Admin_CAT")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="categ" required="required">
                                        <?php
                                        $statm1=$con->prepare('SELECT * FROM categories');
                                        $statm1->execute();
                                        $c= $statm1->fetchAll();
                                        foreach ($c as $cats){
                                           // echo "<option value='".$cats['ID']."'>".$cats['Name']."</option>";
                                            
                                             echo "<option value='".$cats['ID']."'";
                                            if($items['item_id']==$cats['ID'])
                                                {
                                                echo "selected";
                                                
                                                }
                                            echo ">"; echo $cats['Name']."</option>";
                                            
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
                      <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">tags</label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="tags"
                                            class="form-control"
                                            autocomplete="off" 
                                  value="<?php echo $items['tags'];?>"/>

                                 </div>
                             </div>
       
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" 
                                            value="<?php echo lang("itm_up")?>" 
                                            class="btn btn-primary btn-sm" />
                                 </div>
                             </div>
                         </form>
                         <?php
 //fetch data from database to put into table
            try{
            $statm11=$con->prepare("SELECT comments.*,Users.UserName AS UN1 FROM comments INNER JOIN Users ON Users.userID=comments.User_id where comments.item_id=?");
            
            $statm11->execute(array($itemid));//select all items expect admin
            $rows=$statm11->fetchAll();
            }catch(PDOException $e)
                             {
                                  echo $e->getMessage();        
                              }
                      if(!empty($rows))  {      
            ?>
        <h1 class="text-center">Mange [<?php echo $items['item_name'];?>] comment</h1><br>
                <div class="table-responsive">
                    <table class="main-table text-center table table-bordered">
                        <tr>
                               <td>comment</td>
                               <td>user Name</td>
                               <td>Add Date</td>
                               <td>Control</td>
                        </tr>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['comment'].'</td>';
                                             echo'<td>'.$row['UN1'].'</td>';
                                              echo'<td>'.$row['date'].'</td>';
                            echo'<td> <a href="comments.php?do=edit&comid='.$row['c_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                              . '<a href="comments.php?do=delete&comid='.$row['c_id'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($row['status']==0){
                                                    echo '<a href="comments.php?do=Approve&comid='.$row['c_id'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>
                      <?php } ?>
                      </div>  
                       
        <?php   }
        //$count =0 in other word no such userid in database
        else{
                echo '<div class="container">';
        $msg= '<div class="alert alert-danger">There is no such ID</div>';
                     Redirect($msg);    
                  echo '</div>';

       
        } 
        }
    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='update'){
             if($_SERVER['REQUEST_METHOD']=='POST'){
                  echo '<h1 class="text-center"> update Iteme</h1>';
            echo '<div class="container">';
                $id=filter_input(INPUT_POST, 'itmid');
                $itm_nam=filter_input(INPUT_POST, 'name');
                $itm_des=filter_input(INPUT_POST, 'des');
                $itm_pri=filter_input(INPUT_POST, 'pri');
                $itm_cont=filter_input(INPUT_POST, 'cont');
                $itm_stat=filter_input(INPUT_POST, 'status');
                $itm_mem=filter_input(INPUT_POST, 'member');
                $itm_cat=filter_input(INPUT_POST, 'categ');
                $itm_tags=filter_input(INPUT_POST, 'tags');
                
                //validate the form
                $errorarray=array();
                if(empty($itm_nam)){$errorarray[]="Name must be <strong>not Empty</strong>";}
                if(empty($itm_des)){$errorarray[]="Description must be <strong>not Empty</strong>";}
                if(empty($itm_pri)){$errorarray[]="Price must be <strong>not Empty</strong>";}
                if(empty($itm_cont)){$errorarray[]="country must be <strong>not Empty</strong>";}
                if($itm_stat==0){$errorarray[]="you must choose <strong>status</strong>";}
                if($itm_mem==0){$errorarray[]="you must choose <strong>The member</strong>";}
                if($itm_cat==0){$errorarray[]="you must choose <strong>The category</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\">'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                       try{
                $statm=$con->prepare("UPDATE items SET item_name=?,description=?,price=?,country_name=?,status=?,category_id=?,member_id=?,tags=? WHERE item_id=?");
                $statm->execute(array($itm_nam,$itm_des,$itm_pri,$itm_cont,$itm_stat,$itm_cat,$itm_mem,$itm_tags,$id));
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect($msg,"back");
                     echo '</div>';
                       }catch(PDOException $e)
                             {
                                  echo $e->getMessage();    
                              }
                       }
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
         elseif ($do==='delete') {
              //delete Member
            echo '<h1 class="text-center"> Delete Item</h1>';
            echo '<div class="container">';
                    $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
                    
                   $check=  CheckItems("item_id", "items", $itemid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM items WHERE item_id=:zitem');
                        $statm->bindParam(":zitem",$itemid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
         }
    /*========================================================= 
            *
            * if do==approve
            * 
    * ========================================================= 
    */
          elseif ($do==='Approve') {
                       //Aprove users in database
        echo '<h1 class="text-center"> Approve Item</h1>';
            echo '<div class="container">';
                    $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
                    
                   $check=  CheckItems("item_id", "items", $itemid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE items SET Approve_itm=1 WHERE item_id=?');
                        $statm->execute(array($itemid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'Item Approved</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such item';
                   Redirect($msg);

                 }
            echo '</div>';  
          }
      
       include $tpl.'Footer.php';
    }
else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 


