 <?php 
 /*========================================================= 
 *Dashboard Page 
 * ========================================================= 
 */
 ob_start();
    session_start();
        $PageTitel='Dashboard';

    if(isset($_SESSION['user'])){
        include 'inti.php';
        //count latest user and display in dashboard
        $num_user=3;
        $latest_user= getlatest("*","users","userID",$num_user);
       //count latest Items and display in dashboard
        $num_items=3;
        $latest_items=getlatest("*","items","item_id",$num_items);
        //count latest comments
        $num_comm=6;
        $latest_comments=getlatest("*","comments","c_id",$num_comm);
/*Start Dashbooard*/
        ?>
<div style="display:block" class="page page-home">
  <div class="bg"></div>
  <div class="overlay"></div>
  <div class="panel panel-time">
    <div class="span time"><i class="fa fa-users"></i><a href="members.php"><?php echo countitems("userID", "users")-1?></a></div>
    <div class="span date">Total Members</div>
  </div>
   <div class="panel panel-time panel-time1">
    <div class="span time"> <i class="fa fa-user-plus"></i><a href="members.php?do=manage&page=pending">
                                     <?php echo CheckItems('ApproveStatus','users',0)?></a></div>
    <div class="span date">Pending Members</div>
  </div>
   <div class="panel panel-time panel-time2">
    <div class="span time"><i class="fa fa-tag"></i><a href="items.php"><?php echo countitems('item_id','items')?></a></div>
    <div class="span date">Total Items</div>
  </div>
   <div class="panel panel-time panel-time3">
    <div class="span time"> <i class="fa fa-comments"></i><a href="comments.php"><?php echo countitems('c_id','comments')?></a></div>
    <div class="span date">Total Comments</div>
  </div>

  <div class="panel panel-calendar">
      <div class="HeaderUserLatest">Latest Register members</div>
      <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-user"></i> Latest <?php echo $num_user?> registered user
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                          <ul class="list-unstyled latest-user">
                          <?php      
                                    if(!empty($latest_user)){

                         foreach ($latest_user as $latus){
                             echo '<li>'.$latus['UserName'].'<span class="btn btn-success edit1 pull-right"><i class="fa fa-edit">';
                                     echo '</i><a href="members.php?do=edit&userid='.$latus['userID'].'"">Edit</a></span>';
                                       if($latus['ApproveStatus']==0){
                                                    echo '<a href="members.php?do=activate&userid='.$latus['userID'].'" class="btn btn-info activate1 pull-right"><i class="fa fa-edit"></i>Approve</a>';
                                                }
                                    echo ''.'</li>';
                                    }
                                    
                                       }  else {
                           echo 'There is no recently register user';    
                            }
                          ?>
                          </ul>
                      </div>
                  </div>
  </div>
  
    <div class="panel panel-calendar panel-calendar1">
      <div class="HeaderUserLatest">Latest recently items</div>
    <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-tag"></i> Latest <?php echo $num_items?> recently items 
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                            <ul class="list-unstyled latest-user">
                          <?php   
                          if(!empty($latest_items)){
                         foreach ($latest_items as $latusitm){
                             echo '<li>'.$latusitm['item_name'].'<span class="btn btn-success edit1 pull-right"><i class="fa fa-edit">';
                                     echo '</i><a href="items.php?do=edit&itemid='.$latusitm['item_id'].'"">Edit</a></span>';
                                       if($latusitm['Approve_itm']==0){
                                                    echo '<a href="items.php?do=Approve&itemid='.$latusitm['item_id'].'" class="btn btn-info activate1 pull-right"><i class="fa fa-check"></i>Approve</a>';
                                                }
                                    echo ''.'</li>';
                          }}  else {
                     echo 'There is no recent items added';    
                                }
                          ?>
                          </ul>
                      </div>
                  </div>
  </div>
  
    <div class="panel panel-calendar panel-calendar2">
      <div class="HeaderUserLatest">Latest recently Comment</div>
        <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-tag"></i> Latest <?php echo $num_items?> recently items 
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                            <ul class="list-unstyled latest-user">
                          <?php
                          $statm112=$con->prepare("SELECT comments.*,Users.UserName AS UN1,items.item_name AS ITM FROM comments INNER JOIN Users ON Users.userID=comments.User_id INNER JOIN items ON items.item_id=comments.item_id LIMIT 3");
                            $statm112->execute();//select all comments expect admin
                            $comments1=$statm112->fetchAll();
                         
                          
                         if(!empty($comments1)){
                         foreach ($comments1 as $latcotm){
                             echo '<li>'.$latcotm['UN1'].' add review<span class="btn btn-success edit1 pull-right"><i class="fa fa-edit">';
                                     echo '</i><a href="comments.php?do=edit&comid='.$latcotm['c_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a></span>';
                                       if($latusitm['Approve_itm']==0){
 echo '<a href="comments.php?do=Approve&comid='.$latcotm['c_id'].'" class="btn btn-info"><i class="fa fa-check"></i>Approve</a></div>';                                                }
                                    echo ''.'</li>';
                          }}  else {
                     echo 'There is no recent items added';    
                                }
                          ?>
                          </ul>
                      </div>
                  </div>
  </div>
  
  
 
  <div class="panel panel-functions">
    <div data-page="weather" class="icon icon-weather"><i class="fa fa-sun-o"></i><span class="title">Weather</span></div>
    <div data-page="calendar" class="icon icon-calendar"><i class="fa fa-calendar"></i><span class="title">Calendar</span></div>
    <div data-page="map" class="icon icon-map"><i class="fa fa-globe"></i><span class="title">Map</span></div>
    <div data-page="tasks" class="icon icon-tasks"><i class="fa fa-tasks"></i><span class="title">Tasks</span></div>
    <div data-page="news" class="icon icon-news"><i class="fa fa-rss"></i><span class="title">News</span></div>
  </div>
</div>

            <div class="page page-insertcat">

             <h1 class="text-center"><?php echo lang("Cat_add")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=insertcat" method="POST">
                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name"  class="form-control" autocomplete="off" required="required" placeholder="Name Of The Category"/>
                                 </div>
                             </div>
                          <!Start Discription!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_Des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="discription" class="form-control"  autocomplete="none" placeholder="Describe the Category" />
                                 </div>
                             </div>
                           <!Start Supcategory selection!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">parent?</label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="sub">
                                         <option value="0">None</option>
                                         <?php
                                         $cats= genral_feth("*","categories","WHERE parent=0","","ID","ASC");
                                         foreach ($cats as $c){
                                         echo " <option value='".$c['ID']."'";      
                                         echo ">".$c['Name']."</option> ";
                                         }
                                         ?>
                                     </select>
                                 </div>
                             </div>
                <!Start ordering!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_or")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="order"  class="form-control" placeholder="Number to arrange the categories"/>
                                 </div>
                             </div>
                <!Start Visability feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_vis")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="v-y"type="radio" name="visable" value="0" checked=""/>
                                     <label for="v-y">Yes</label></div>
                                   <div>
                                       <input id="v-n" type="radio" name="visable" value="1" checked=""/>
                                     <label for="v-n">No</label></div>
                                     </div>
                             </div>
                 <!Start Comment feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_c")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="c-y"type="radio" name="Comment" value="0" checked=""/>
                                     <label for="c-y">Yes</label></div>
                                   <div>
                                       <input id="c-n" type="radio" name="Comment" value="1" checked=""/>
                                     <label for="c-n">No</label></div>
                                     </div>
                             </div>
             <!Start Advistise feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_d")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="d-y"type="radio" name="Advert" value="0" checked=""/>
                                     <label for="d-y">Yes</label></div>
                                   <div>
                                       <input id="d-n" type="radio" name="Advert" value="1" checked=""/>
                                     <label for="d-n">No</label></div>
                                     </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("ADD_cat")?>" class="btn btn-primary btn-lg" />
                                 </div>
                             </div>
                         </form>
                      </div>
         </div>




<div class="page page-editcat">
    <?php
     $catid= isset($_GET['catid'])&& is_numeric($_GET['catid']) ?intval($_GET['catid']):0;
        $statm=$con->prepare('SELECT * FROM categories WHERE ID=?');
        $statm->execute(array($catid));
        $cat=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {?>      
                <h1 class="text-center"><?php echo lang("Cat_ed")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=updatecat" method="POST">
                        <input type="hidden" name="catid"value="<?php echo $catid?>"/>

                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name"  class="form-control"  required="required" placeholder="Name Of The Category" value="<?php echo $cat['Name']?>"/>
                                 </div>
                             </div>
                          <!Start Discription!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_Des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="discription" class="form-control"   placeholder="Describe the Category" value="<?php echo $cat['Discription']?>"/>
                                 </div>
                             </div>
                            <!Start Supcategory selection!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">parent?</label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="sub">
                                         <option value="0">None</option>
                                         <?php
                                         $cats= genral_feth("*","categories","WHERE parent=0","","ID","ASC");
                                         foreach ($cats as $c){
                                         echo " <option value='".$c['ID']."'";
                                         if($c['ID']===$cat['parent']){
                                             echo 'selected="selected"';
                                         }
                                         echo ">".$c['Name']."</option> ";
                                         }
                                         ?>
                                     </select>
                                 </div>
                             </div>
                <!Start ordering!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_or")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="order"  class="form-control" placeholder="Number to arrange the categories" value="<?php echo $cat['Ordering']?>"/>
                                 </div>
                             </div>
                <!Start Visability feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_vis")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="v-y"type="radio" name="visable" value="0" <?php if($cat['Visability']==0) {echo "checked";}?>/>
                                     <label for="v-y">Yes</label></div>
                                   <div>
                                       <input id="v-n" type="radio" name="visable" value="1" <?php if($cat['Visability']==1) {echo "checked";}?> />
                                     <label for="v-n">No</label></div>
                                     </div>
                             </div>
                 <!Start Comment feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_c")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="c-y"type="radio" name="Comment" value="0"<?php if($cat['Allow_comment']==0) {echo "checked";}?> />
                                     <label for="c-y">Yes</label></div>
                                   <div>
                                       <input id="c-n" type="radio" name="Comment" value="1" <?php if($cat['Allow_comment']==1) {echo "checked";}?>/>
                                     <label for="c-n">No</label></div>
                                     </div>
                             </div>
             <!Start Advistise feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Cat_d")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <div>
                                         <input id="d-y"type="radio" name="Advert" value="0" <?php if($cat['Allow_Ads']==0) {echo "checked";}?>/>
                                     <label for="d-y">Yes</label></div>
                                   <div>
                                       <input id="d-n" type="radio" name="Advert" value="1" <?php if($cat['Allow_Ads']==1) {echo "checked";}?> />
                                     <label for="d-n">No</label></div>
                                     </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("save_cat")?>" class="btn btn-primary btn-lg" />
                                 </div>
                             </div>
                         </form>
                      </div>
            <?php }?>
    
    
    
</div>


<div class="page page-weather">
 
<?php 
/*========================================================= 
 *Categories Page 
 * ========================================================= 
 */

    $do=isset($_GET['do'])?$_GET['do']:'managecat';
    /*========================================================= 
        *
        * if do==manage
        * 
    * ========================================================= 
    */
        if($do=="managecat"){
           $sort='ASC';
           $sortArr=array('ASC','DESC');
           if(isset($_GET['sort'])&&  in_array($_GET['sort'], $sortArr)){
               $sort=$_GET['sort'];
           }
            $statmn=$con->prepare("SELECT * FROM categories WHERE parent=0 ORDER BY Ordering $sort");
            $statmn->execute();
            $cats=$statmn->fetchAll();
                        if(!empty($cats)){

            ?>
         <h1 class='text-center'><?php echo lang("Cat_man")?></h1>
         <div class="container categories1">
             <div class="panel panel-default">
                 <div class="panel panel-heading"><i class="fa fa-edit"></i><?php echo lang("Cat_man")?>
                     <div class="option pull-right">
                         <i class="fa fa-sort"></i>Oerdring:[
                         <a class="<?php if($sort==='ASC'){echo 'active1';}?>" href="?sort=ASC">ASC</a>|
                         <a class="<?php if($sort==='DESC'){echo 'active1';}?>" href="?sort=DESC">DESC</a>]
                          <i class="fa fa-eye"></i>View:] <span class="active1" data-view="Classic"> Classic</span>|
                               <span data-view="Full">Full</span>]
                     </div>
                 </div>
                 <div class="panel-body">
                     
                 <?php
                 foreach ($cats as $cat){
                     echo '<div class="categ">';
                     echo '<div class="hidden-buttons">';
                          echo "<a href='?do=editcat&catid= ".$cat['ID']."' class='cat_edit btn btn-xs btn-primary'><i class='fa fa-edit'></i>Edit</a>";
                       echo '<a href="?do=deletecat&catid=  '.$cat['ID'].'   " class="confirm btn btn-xs btn-danger"><i class="fa fa-close"></i>Delete</a>';
                     
                     echo '</div>';
                            echo '<h3>'.$cat['Name'].'</h3>';
                                     echo '<div class="full-v"';
                                        echo '<p>';if($cat['Discription']===''){echo'This category has no description';}else {echo $cat['Discription']; } echo'</p> ';
                                        if($cat['Visability']==="1"){ echo '<span class="Visbility"><i class="fa fa-eye"></i>Hidden</span>';}
                                        if($cat['Allow_comment']==="1"){ echo '<span class="comment"><i class="fa fa-close"></i>Comment disable</span>';}
                                        if($cat['Allow_Ads']==="1"){ echo '<span class="advertise"><i class="fa fa-close"></i>Advertise disable</span>';}
                                  echo '</div>';
                                                   //GET CHILD CATEGORIES
                                 
                                   $CHILDcats= genral_feth("*","categories","WHERE parent=".$cat['ID']."","","ID","ASC");
                                   if(!empty($CHILDcats)){
                                       echo '<h4 class="child-head"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i>Subcategoriy</h4>';
                                    echo '<ul class="list-unstyled child-cats">';
                                         foreach ($CHILDcats as $cat){
                                         echo " <li class='child-delete'><a class='child-delete1' href='Categories.php?do=edit&catid= ".$cat['ID']."' >".$cat['Name']."</a>  ";
                                       echo "<a href='Categories.php?do=delete&catid= ".$cat['ID']."' class='show-delete'>Delete</a></li> ";

                                         }
                                         echo '</ul>';

                 }
                                echo '</div>';
                
                          echo '<hr>';

                     }
                 ?>
            </div>
         </div>
             <a href="#" class="add-catrgo btn btn-primary"><i class="fa fa-plus"></i>Add New Categories</a>
  
         </div>
          
            
     <?php  }else {echo '<div class="container"><div class="message">there is no Categories</div><a href="?do=Addcat" class="add-catrgo btn btn-primary"><i class="fa fa-plus"></i>Add New Categories</a></div>';} }
     /*========================================================= 
        *
        * if do==add
        * 
    * ========================================================= 
    */
        elseif($do==='Addcat'){?>
            
        <?php }
     /*========================================================= 
        *
        * if do==Insert
        * 
    * ========================================================= 
    */
        elseif ($do==='insertcat') {
                 //in insert form
           
                if($_SERVER['REQUEST_METHOD']=='POST'){
                echo '<h1 class="text-center"> Insert Category</h1>';
                echo '<div class="container">';
                 $name=$_POST['name'];
                 $des=$_POST['discription'];
                  $parent=$_POST['sub'];
                  $order=$_POST['order'];
                  $vis=$_POST['visable'];
                  $vcom=$_POST['Comment'];
                  $adv=$_POST['Advert'];
                //updata database
                
                       //Insert Category
                       //check if Category exist
                       $check=  CheckItems("Name", "categories", $name);
                       if($check==1){
                           $msg= '<div class="alert alert-danger>"sorry this Category is exist</div>';
                            Redirect1($msg,"back");
                       }
                       else{
                           try{
                     $statm=$con->prepare('INSERT INTO categories (Name,Discription,parent,Ordering,Visability,Allow_comment,Allow_Ads)VALUES(:zname,:zdes,:zpar,:zord,:zvis,:zcom,:zads)' );
                       $statm->execute(array("zname"=>$name,"zdes"=>$des,"zpar"=>$parent,"zord"=>$order,"zvis"=>$vis,"zcom"=>$vcom,"zads"=>$adv));
                            }catch(PDOException $e)
                             {
                                  echo $e->getMessage();        
                              }
                //echo success message
                       echo '<div class="container">';
                   $msg1= '<div class="alert alert-success">'.$statm->rowCount().'record Inserted</div>';
                     Redirect1($msg1,"back",5);
                     echo '</div>';
                       }
                       
            }//end is not post request 
            else{
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect1($msg,null,5);
            }//end show error
            echo '</div>';
        }
    /*========================================================= 
        *
        * if do==edit
        * 
    * ========================================================= 
    */
        elseif($do=='editcat'){
              ?>
                
                
        
            
        <?php   
        }
    /*========================================================= 
        *
        * if do==update
        * 
    * ========================================================= 
    */
        elseif($do=='updatecat'){
                     if($_SERVER['REQUEST_METHOD']=='POST'){
                  echo '<h1 class="text-center"> update Category</h1>';
            echo '<div class="container">';
                $id=filter_input(INPUT_POST, 'catid');
                $nam=filter_input(INPUT_POST, 'name');
                $des=filter_input(INPUT_POST, 'discription');
                $parent=filter_input(INPUT_POST, 'sub');
                $or=filter_input(INPUT_POST, 'order');
                $vi=filter_input(INPUT_POST, 'visable');
                $co=filter_input(INPUT_POST, 'Comment');
                $ad=filter_input(INPUT_POST, 'Advert');
               
                //updata database
                  
                $statm=$con->prepare("UPDATE categories SET Name=?,Discription=?,parent=?,Ordering=?,Visability=?,Allow_comment=?,Allow_Ads=? WHERE ID=?");
                $statm->execute(array($nam,$des,$parent,$or,$vi,$co,$ad,$id));
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect1($msg,"back");
                     echo '</div>';
                     
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect1($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }
    /*========================================================= 
        *
        * if do==delete
        * 
    * ========================================================= 
    */
         elseif ($do==='deletecat') {
             //delete Member
            echo '<h1 class="text-center"> Delete Category</h1>';
            echo '<div class="container">';
                    $catid= isset($_GET['catid'])&& is_numeric($_GET['catid']) ?intval($_GET['catid']):0;
                    
                   $check=  CheckItems("ID", "categories", $catid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM categories WHERE ID=:zID');
                        $statm->bindParam(":zID",$catid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect1($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect1($msg,'back');

                 }
            echo '</div>';
         }
      
    
          
    
        ?>
<?php ob_end_flush(); ?>


  
  
  
  
  
  
  

</div>
<div class="page page-calendar">

  <?php ob_start(); ?>    
<?php 
/*========================================================= 
 *Items Page 
 * ========================================================= 
 */

    $do=isset($_GET['do'])?$_GET['do']:'manageitm';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manageitm"){
 
            //fetch data from database to put into table
            $statm=$con->prepare("SELECT items.*,categories.Name AS Category_name,Users.UserName AS US_NM "
                               . "FROM items INNER JOIN categories ON categories.ID=items.category_id "
                               . "INNER JOIN Users ON  Users.userID=items.member_id");
            $statm->execute();//select all items expect admin
            $itms=$statm->fetchAll();
            if(!empty($itms)){
            ?>
        <h1 class="text-center">Mange Items Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="main-table text-center table table-bordered">
                        <tr>
                               <td>#ID</td>
                               <td>User Name</td>
                               <td>Description</td>
                               <td>Price</td>
                               <td>Adding Date</td>
                               <td>Category</td>
                               <td>User Name</td>
                               <td>Control</td>
                        </tr>
                        <?php
                                    foreach ($itms as $item){
                                        echo '<tr>';
                                            echo'<td>'.$item['item_id'].'</td>';
                                            echo'<td>'.$item['item_name'].'</td>';
                                             echo'<td class="p_des">'.$item['description'].'</td>';
                                             echo'<td>'.$item['price'].'</td>';
                                              echo'<td>'.$item['add_date'].'</td>';
                                              echo'<td>'.$item['Category_name'].'</td>';
                                              echo'<td>'.$item['US_NM'].'</td>';
                                              
                            echo'<td> <a href="?do=edititm&itemid='.$item['item_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                         . '<a href="?do=deleteitm&itemid='.$item['item_id'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                      if($item['Approve_itm']==0){
                         echo '<a href="?do=Approveitm&itemid='.$item['item_id'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>
                <a href="?do=Additm" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>New Item</a>

            </div><?php     }else {echo '<div class="container"><div class="message">there is no Categories</div><a href="?do=Additm" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i>New Item</a></div>';}   }
    /*========================================================= 
            *
            * if do==add
            * 
    * ========================================================= 
    */
        elseif($do==='Additm'){
     ?>
        <h1 class="text-center"><?php echo lang("itm_ad")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=insertitm" method="POST">
                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_nm")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name" 
                                            class="form-control" 
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Name Of The Item"/>
                                 </div>
                             </div>
                          
               <!Start Description!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="des"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Description Of The Item"/>
                                 </div>
                             </div>
            <!Start Price!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_price")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="pri"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Price Of The Item"/>
                                 </div>
                             </div>
          <!Start country made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_country")?></label>
                                 <div class="col-sm-10 col-md-4">
                             <!Start country made!>
                                      <select name="cont"><option value="Pick a Country">Choose Country</option><option value="Afghanistan">Afghanistan</option><option value="Albania">Albania</option>
                                          <option value="Algeria">Algeria</option>
                                                  <option value="American Samoa">American Samoa</option>
                                                  <option value="Andorra">Andorra</option>
                                                  <option value="Angola">Angola</option>
                                                  <option value="Anguilla">Anguilla</option>
                                                  <option value="Antarctica">Antarctica</option>
                                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                  <option value="Argentina">Argentina</option>
                                                  <option value="Armenia">Armenia</option>
                                                  <option value="Aruba">Aruba</option>
                                                  <option value="Australia">Australia</option>
                                                  <option value="Austria">Austria</option>
                                                  <option value="Azerbaijan">Azerbaijan</option>
                                                  <option value="Bahamas">Bahamas</option>
                                                  <option value="Bahrain">Bahrain</option>
                                                  <option value="Bangladesh">Bangladesh</option>
                                                  <option value="Barbados">Barbados</option>
                                                  <option value="Belarus">Belarus</option>
                                                  <option value="Belgium">Belgium</option>
                                                  <option value="Belize">Belize</option>
                                                  <option value="Benin">Benin</option>
                                                  <option value="Bermuda">Bermuda</option>
                                                  <option value="Bhutan">Bhutan</option>
                                                  <option value="Bolivia">Bolivia</option>
                                                  <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                  <option value="Botswana">Botswana</option>
                                                  <option value="Bouvet Island">Bouvet Island</option>
                                                  <option value="Brazil">Brazil</option>
                                                  <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                  <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                  <option value="Bulgaria">Bulgaria</option>
                                                  <option value="Burkina Faso">Burkina Faso</option>
                                                  <option value="Burundi">Burundi</option>
                                                  <option value="Cambodia">Cambodia</option>
                                                  <option value="Cameroon">Cameroon</option>
                                                  <option value="Canada">Canada</option>
                                                  <option value="Cape Verde">Cape Verde</option>
                                                  <option value="Cayman Islands">Cayman Islands</option>
                                                  <option value="Central African Republic">Central African Republic</option>
                                                  <option value="Chad">Chad</option>
                                                  <option value="Chile">Chile</option>
                                                  <option value="China">China</option>
                                                  <option value="Christmas Island">Christmas Island</option>
                                                  <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                  <option value="Colombia">Colombia</option>
                                                  <option value="Comoros">Comoros</option>
                                                  <option value="Congo">Congo</option>
                                                  <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                  <option value="Cook Islands">Cook Islands</option>
                                                  <option value="Costa Rica">Costa Rica</option>
                                                  <option value="Cote D&#39;ivoire">Cote D'ivoire</option>
                                                  <option value="Croatia">Croatia</option>
                                                  <option value="Cuba">Cuba</option>
                                                  <option value="Cyprus">Cyprus</option>
                                                  <option value="Czech Republic">Czech Republic</option>
                                                  <option value="Denmark">Denmark</option>
                                                  <option value="Djibouti">Djibouti</option>
                                                  <option value="Dominica">Dominica</option>
                                                  <option value="Dominican Republic">Dominican Republic</option>
                                                  <option value="Ecuador">Ecuador</option>
                                                  <option value="Egypt">Egypt</option>
                                                  <option value="El Salvador">El Salvador</option>
                                                  <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                  <option value="Eritrea">Eritrea</option>
                                                  <option value="Estonia">Estonia</option>
                                                  <option value="Ethiopia">Ethiopia</option>
                                                  <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                  <option value="Faroe Islands">Faroe Islands</option>
                                                  <option value="Fiji">Fiji</option>
                                                  <option value="Finland">Finland</option>
                                                  <option value="France">France</option>
                                                  <option value="French Guiana">French Guiana</option>
                                                  <option value="French Polynesia">French Polynesia</option>
                                                  <option value="French Southern Territories">French Southern Territories</option>
                                                  <option value="Gabon">Gabon</option>
                                                  <option value="Gambia">Gambia</option>
                                                  <option value="Georgia">Georgia</option>
                                                  <option value="Germany">Germany</option>
                                                  <option value="Ghana">Ghana</option>
                                                  <option value="Gibraltar">Gibraltar</option>
                                                  <option value="Greece">Greece</option>
                                                  <option value="Greenland">Greenland</option>
                                                  <option value="Grenada">Grenada</option>
                                                  <option value="Guadeloupe">Guadeloupe</option>
                                                  <option value="Guam">Guam</option>
                                                  <option value="Guatemala">Guatemala</option>
                                                  <option value="Guinea">Guinea</option>
                                                  <option value="Guinea-bissau">Guinea-bissau</option>
                                                  <option value="Guyana">Guyana</option>
                                                  <option value="Haiti">Haiti</option>
                                                  <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                  <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                  <option value="Honduras">Honduras</option>
                                                  <option value="Hong Kong">Hong Kong</option>
                                                  <option value="Hungary">Hungary</option>
                                                  <option value="Iceland">Iceland</option>
                                                  <option value="India">India</option>
                                                  <option value="Indonesia">Indonesia</option>
                                                  <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                  <option value="Iraq">Iraq</option>
                                                  <option value="Ireland">Ireland</option>
                                                  <option value="Israel">Israel</option>
                                                  <option value="Italy">Italy</option>
                                                  <option value="Jamaica">Jamaica</option>
                                                  <option value="Japan">Japan</option>
                                                  <option value="Jordan">Jordan</option>
                                                  <option value="Kazakhstan">Kazakhstan</option>
                                                  <option value="Kenya">Kenya</option>
                                                  <option value="Kiribati">Kiribati</option>
                                                  <option value="Korea, Democratic People&#39;s Republic of">Korea, Democratic People's Republic of</option>
                                                  <option value="Korea, Republic of">Korea, Republic of</option>
                                                  <option value="Kuwait">Kuwait</option>
                                                  <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                  <option value="Lao People&#39;s Democratic Republic">Lao People's Democratic Republic</option>
                                                  <option value="Latvia">Latvia</option>
                                                  <option value="Lebanon">Lebanon</option>
                                                  <option value="Lesotho">Lesotho</option>
                                                  <option value="Liberia">Liberia</option>
                                                  <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                  <option value="Liechtenstein">Liechtenstein</option>
                                                  <option value="Lithuania">Lithuania</option>
                                                  <option value="Luxembourg">Luxembourg</option>
                                                  <option value="Macao">Macao</option>
                                                  <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                  <option value="Madagascar">Madagascar</option>
                                                  <option value="Malawi">Malawi</option>
                                                  <option value="Malaysia">Malaysia</option>
                                                  <option value="Maldives">Maldives</option>
                                                  <option value="Mali">Mali</option>
                                                  <option value="Malta">Malta</option>
                                                  <option value="Marshall Islands">Marshall Islands</option>
                                                  <option value="Martinique">Martinique</option>
                                                  <option value="Mauritania">Mauritania</option>
                                                  <option value="Mauritius">Mauritius</option>
                                                  <option value="Mayotte">Mayotte</option>
                                                  <option value="Mexico">Mexico</option>
                                                  <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                  <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                  <option value="Monaco">Monaco</option>
                                                  <option value="Mongolia">Mongolia</option>
                                                  <option value="Montenegro">Montenegro</option>
                                                  <option value="Montserrat">Montserrat</option>
                                                  <option value="Morocco">Morocco</option>
                                                  <option value="Mozambique">Mozambique</option>
                                                  <option value="Myanmar">Myanmar</option>
                                                  <option value="Namibia">Namibia</option>
                                                  <option value="Nauru">Nauru</option>
                                                  <option value="Nepal">Nepal</option>
                                                  <option value="Netherlands">Netherlands</option>
                                                  <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                  <option value="New Caledonia">New Caledonia</option>
                                                  <option value="New Zealand">New Zealand</option>
                                                  <option value="Nicaragua">Nicaragua</option>
                                                  <option value="Niger">Niger</option>
                                                  <option value="Nigeria">Nigeria</option>
                                                  <option value="Niue">Niue</option>
                                                  <option value="Norfolk Island">Norfolk Island</option>
                                                  <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                  <option value="Norway">Norway</option>
                                                  <option value="Oman">Oman</option>
                                                  <option value="Pakistan">Pakistan</option>
                                                  <option value="Palau">Palau</option>
                                                  <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                  <option value="Panama">Panama</option>
                                                  <option value="Papua New Guinea">Papua New Guinea</option>
                                                  <option value="Paraguay">Paraguay</option>
                                                  <option value="Peru">Peru</option>
                                                  <option value="Philippines">Philippines</option>
                                                  <option value="Pitcairn">Pitcairn</option>
                                                  <option value="Poland">Poland</option>
                                                  <option value="Portugal">Portugal</option>
                                                  <option value="Puerto Rico">Puerto Rico</option>
                                                  <option value="Qatar">Qatar</option>
                                                  <option value="Reunion">Reunion</option>
                                                  <option value="Romania">Romania</option>
                                                  <option value="Russian Federation">Russian Federation</option>
                                                  <option value="Rwanda">Rwanda</option>
                                                  <option value="Saint Helena">Saint Helena</option>
                                                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                  <option value="Saint Lucia">Saint Lucia</option>
                                                  <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                  <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                  <option value="Samoa">Samoa</option>
                                                  <option value="San Marino">San Marino</option>
                                                  <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                  <option value="Saudi Arabia">Saudi Arabia</option>
                                                  <option value="Senegal">Senegal</option>
                                                  <option value="Serbia">Serbia</option>
                                                  <option value="Seychelles">Seychelles</option>
                                                  <option value="Sierra Leone">Sierra Leone</option>
                                                  <option value="Singapore">Singapore</option>
                                                  <option value="Slovakia">Slovakia</option>
                                                  <option value="Slovenia">Slovenia</option>
                                                  <option value="Solomon Islands">Solomon Islands</option>
                                                  <option value="Somalia">Somalia</option>
                                                  <option value="South Africa">South Africa</option>
                                                  <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                  <option value="South Sudan">South Sudan</option>
                                                  <option value="Spain">Spain</option>
                                                  <option value="Sri Lanka">Sri Lanka</option>
                                                  <option value="Sudan">Sudan</option>
                                                  <option value="Suriname">Suriname</option>
                                                  <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                  <option value="Swaziland">Swaziland</option>
                                                  <option value="Sweden">Sweden</option>
                                                  <option value="Switzerland">Switzerland</option>
                                                  <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                  <option value="Taiwan, Republic of China">Taiwan, Republic of China</option>
                                                  <option value="Tajikistan">Tajikistan</option>
                                                  <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                  <option value="Thailand">Thailand</option>
                                                  <option value="Timor-leste">Timor-leste</option>
                                                  <option value="Togo">Togo</option>
                                                  <option value="Tokelau">Tokelau</option>
                                                  <option value="Tonga">Tonga</option>
                                                  <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                  <option value="Tunisia">Tunisia</option>
                                                  <option value="Turkey">Turkey</option>
                                                  <option value="Turkmenistan">Turkmenistan</option>
                                                  <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                  <option value="Tuvalu">Tuvalu</option>
                                                  <option value="Uganda">Uganda</option>
                                                  <option value="Ukraine">Ukraine</option>
                                                  <option value="United Arab Emirates">United Arab Emirates</option>
                                                  <option value="United Kingdom">United Kingdom</option>
                                                  <option value="United States">United States</option>
                                                  <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                  <option value="Uruguay">Uruguay</option>
                                                  <option value="Uzbekistan">Uzbekistan</option>
                                                  <option value="Vanuatu">Vanuatu</option>
                                                  <option value="Venezuela">Venezuela</option>
                                                  <option value="Viet Nam">Viet Nam</option>
                                                  <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                  <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                  <option value="Western Sahara">Western Sahara</option>
                                                  <option value="Yemen">Yemen</option>
                                                  <option value="Zambia">Zambia</option>
                                                  <option value="Zimbabwe">Zimbabwe</option>
                                          </select>
                                            <!End Start country made!>

                                 </div>
                             </div>
                          
          <!Start status made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_stat")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="status" required="required">
                                         <option value="0">..</option>
                                         <option value="1">New</option>
                                         <option value="2">Like New</option>
                                         <option value="3">Used</option>
                                         <option value="4">Old</option>
                                     </select>
                                            
                                 </div>
                             </div>
       
       <!Start Members Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_fld")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="member" required="required">
                                         <option value="0">..</option>
                                        <?php
                                       // $statm=$con->prepare('SELECT * FROM users');
                                     //   $statm->execute();
                                        $r= genral_feth("*","users","","","userID");
                                        foreach ($r as $usr){
                                            echo "<option value='".$usr['userID']."'>".$usr['UserName']."</option>";
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
       
      <!Start Category Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Admin_CAT")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="categ" required="required">
                                         <option value="0">..</option>
                                        <?php
                                       
                                        $c= genral_feth("*","categories","WHERE parent=0","","ID","ASC");

                                        foreach ($c as $cats){
                                            echo "<option value='".$cats['ID']."'>".$cats['Name']."</option>";
                             $child_cats= genral_feth("*","categories","WHERE parent={$cats['ID']}","","ID","ASC");
                                            foreach ($child_cats as $child){
                             echo "<option value='".$child['ID']."'>---".$child['Name']."</option>";

                                            }
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
        <!Start Tags!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_price")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="tags"
                                            class="form-control"
                                            autocomplete="off" 
                                            placeholder="Seprate Tags with comma(..,..)"/>
                                 </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" 
                                            value="<?php echo lang("itm_add")?>" 
                                            class="btn btn-primary btn-sm" />
                                 </div>
                             </div>
                         </form>
                      </div>
    
       
            
    <?php
            
        }
    /*========================================================= 
            *
            * if do==Insert
            * 
    * ========================================================= 
    */
        elseif ($do==='insertitm') {
             //in insert form
           
                if($_SERVER['REQUEST_METHOD']=='POST'){
                echo '<h1 class="text-center"> Insert Items</h1>';
                echo '<div class="container">';
                $name=filter_input(INPUT_POST, 'name');
                $des=filter_input(INPUT_POST, 'des');
                $pri=filter_input(INPUT_POST, 'pri');
                $cont=filter_input(INPUT_POST, 'cont');
                $status=filter_input(INPUT_POST, 'status');
                $memb=filter_input(INPUT_POST, 'member');
                $categ=filter_input(INPUT_POST, 'categ');
                $tags=filter_input(INPUT_POST, 'tags');

                
                //validate the form
                $errorarray=array();
                if(empty($name)){$errorarray[]="Name must be <strong>not Empty</strong>";}
                if(empty($des)){$errorarray[]="Description must be <strong>not Empty</strong>";}
                if(empty($pri)){$errorarray[]="Price must be <strong>not Empty</strong>";}
                if(empty($cont)){$errorarray[]="country must be <strong>not Empty</strong>";}
                if($status==0){$errorarray[]="you must choose <strong>status</strong>";}
                if($memb==0){$errorarray[]="you must choose <strong>The member</strong>";}
                if($categ==0){$errorarray[]="you must choose <strong>The category</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\">'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                       //Insert Items
                       try{
           $statm=$con->prepare('INSERT INTO items (item_name,description,price,add_date,country_name,status,category_id,member_id,tags)VALUES(:zname,:zdes,:zpri,now(),:zcon,:zst,:zcat,:zmem,:ztags)' );
           $statm->execute(array("zname"=>$name,"zdes"=>$des,"zpri"=>$pri,"zcon"=>$cont,"zst"=>$status,"zcat"=>$categ,"zmem"=>$memb,"ztags"=>$tags));
                     
                      //echo success message
                       echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect1($msg,"back");
                     echo '</div>';
                       }catch(PDOException $e)
                             {
                                  echo $e->getMessage();    
                              }
                       
                       }
            }//end is not post request 
            else{
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect1($msg);
            }//end show error
            echo '</div>';
        }
    /*========================================================= 
            *
            * if do==edit
            * 
    * ========================================================= 
    */
        elseif($do=='edititm'){
            
        
                 $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
        $statm=$con->prepare('SELECT * FROM items WHERE item_id=?');
        $statm->execute(array($itemid));
        $items=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {   
              ?>
         <h1 class="text-center"><?php echo lang("itm_ed")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=updateitm" method="POST">
                <input type="hidden" name="itmid"value="<?php echo $itemid?>"/>

                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_nm")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name" 
                                            class="form-control" 
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Name Of The Item"
                                            value="<?php echo $items['item_name'];?>"/>
                                 </div>
                             </div>
                          
               <!Start Description!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_des")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="des"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Description Of The Item"
                                             value="<?php echo $items['description'];?>"/>
                                 </div>
                             </div>
            <!Start Price!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_price")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="pri"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Price Of The Item"
                                            value="<?php echo $items['price'];?>"/>
                                 </div>
                             </div>
          <!Start country made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_country")?></label>
                                 <div class="col-sm-10 col-md-4">
                             <!Start country made!>
                             <select name="cont"><option value="<?php echo $items['country_name'];?>" selected="selected">
                                     <?php echo $items['country_name'];?></option>
                                                 <option value="Afghanistan">Afghanistan</option>
                                                 <option value="Albania">Albania</option>
                                                  <option value="Algeria">Algeria</option>
                                                  <option value="American Samoa">American Samoa</option>
                                                  <option value="Andorra">Andorra</option>
                                                  <option value="Angola">Angola</option>
                                                  <option value="Anguilla">Anguilla</option>
                                                  <option value="Antarctica">Antarctica</option>
                                                  <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                  <option value="Argentina">Argentina</option>
                                                  <option value="Armenia">Armenia</option>
                                                  <option value="Aruba">Aruba</option>
                                                  <option value="Australia">Australia</option>
                                                  <option value="Austria">Austria</option>
                                                  <option value="Azerbaijan">Azerbaijan</option>
                                                  <option value="Bahamas">Bahamas</option>
                                                  <option value="Bahrain">Bahrain</option>
                                                  <option value="Bangladesh">Bangladesh</option>
                                                  <option value="Barbados">Barbados</option>
                                                  <option value="Belarus">Belarus</option>
                                                  <option value="Belgium">Belgium</option>
                                                  <option value="Belize">Belize</option>
                                                  <option value="Benin">Benin</option>
                                                  <option value="Bermuda">Bermuda</option>
                                                  <option value="Bhutan">Bhutan</option>
                                                  <option value="Bolivia">Bolivia</option>
                                                  <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                  <option value="Botswana">Botswana</option>
                                                  <option value="Bouvet Island">Bouvet Island</option>
                                                  <option value="Brazil">Brazil</option>
                                                  <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                  <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                  <option value="Bulgaria">Bulgaria</option>
                                                  <option value="Burkina Faso">Burkina Faso</option>
                                                  <option value="Burundi">Burundi</option>
                                                  <option value="Cambodia">Cambodia</option>
                                                  <option value="Cameroon">Cameroon</option>
                                                  <option value="Canada">Canada</option>
                                                  <option value="Cape Verde">Cape Verde</option>
                                                  <option value="Cayman Islands">Cayman Islands</option>
                                                  <option value="Central African Republic">Central African Republic</option>
                                                  <option value="Chad">Chad</option>
                                                  <option value="Chile">Chile</option>
                                                  <option value="China">China</option>
                                                  <option value="Christmas Island">Christmas Island</option>
                                                  <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                  <option value="Colombia">Colombia</option>
                                                  <option value="Comoros">Comoros</option>
                                                  <option value="Congo">Congo</option>
                                                  <option value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option>
                                                  <option value="Cook Islands">Cook Islands</option>
                                                  <option value="Costa Rica">Costa Rica</option>
                                                  <option value="Cote D&#39;ivoire">Cote D'ivoire</option>
                                                  <option value="Croatia">Croatia</option>
                                                  <option value="Cuba">Cuba</option>
                                                  <option value="Cyprus">Cyprus</option>
                                                  <option value="Czech Republic">Czech Republic</option>
                                                  <option value="Denmark">Denmark</option>
                                                  <option value="Djibouti">Djibouti</option>
                                                  <option value="Dominica">Dominica</option>
                                                  <option value="Dominican Republic">Dominican Republic</option>
                                                  <option value="Ecuador">Ecuador</option>
                                                  <option value="Egypt">Egypt</option>
                                                  <option value="El Salvador">El Salvador</option>
                                                  <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                  <option value="Eritrea">Eritrea</option>
                                                  <option value="Estonia">Estonia</option>
                                                  <option value="Ethiopia">Ethiopia</option>
                                                  <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                  <option value="Faroe Islands">Faroe Islands</option>
                                                  <option value="Fiji">Fiji</option>
                                                  <option value="Finland">Finland</option>
                                                  <option value="France">France</option>
                                                  <option value="French Guiana">French Guiana</option>
                                                  <option value="French Polynesia">French Polynesia</option>
                                                  <option value="French Southern Territories">French Southern Territories</option>
                                                  <option value="Gabon">Gabon</option>
                                                  <option value="Gambia">Gambia</option>
                                                  <option value="Georgia">Georgia</option>
                                                  <option value="Germany">Germany</option>
                                                  <option value="Ghana">Ghana</option>
                                                  <option value="Gibraltar">Gibraltar</option>
                                                  <option value="Greece">Greece</option>
                                                  <option value="Greenland">Greenland</option>
                                                  <option value="Grenada">Grenada</option>
                                                  <option value="Guadeloupe">Guadeloupe</option>
                                                  <option value="Guam">Guam</option>
                                                  <option value="Guatemala">Guatemala</option>
                                                  <option value="Guinea">Guinea</option>
                                                  <option value="Guinea-bissau">Guinea-bissau</option>
                                                  <option value="Guyana">Guyana</option>
                                                  <option value="Haiti">Haiti</option>
                                                  <option value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option>
                                                  <option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
                                                  <option value="Honduras">Honduras</option>
                                                  <option value="Hong Kong">Hong Kong</option>
                                                  <option value="Hungary">Hungary</option>
                                                  <option value="Iceland">Iceland</option>
                                                  <option value="India">India</option>
                                                  <option value="Indonesia">Indonesia</option>
                                                  <option value="Iran, Islamic Republic of">Iran, Islamic Republic of</option>
                                                  <option value="Iraq">Iraq</option>
                                                  <option value="Ireland">Ireland</option>
                                                  <option value="Israel">Israel</option>
                                                  <option value="Italy">Italy</option>
                                                  <option value="Jamaica">Jamaica</option>
                                                  <option value="Japan">Japan</option>
                                                  <option value="Jordan">Jordan</option>
                                                  <option value="Kazakhstan">Kazakhstan</option>
                                                  <option value="Kenya">Kenya</option>
                                                  <option value="Kiribati">Kiribati</option>
                                                  <option value="Korea, Democratic People&#39;s Republic of">Korea, Democratic People's Republic of</option>
                                                  <option value="Korea, Republic of">Korea, Republic of</option>
                                                  <option value="Kuwait">Kuwait</option>
                                                  <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                  <option value="Lao People&#39;s Democratic Republic">Lao People's Democratic Republic</option>
                                                  <option value="Latvia">Latvia</option>
                                                  <option value="Lebanon">Lebanon</option>
                                                  <option value="Lesotho">Lesotho</option>
                                                  <option value="Liberia">Liberia</option>
                                                  <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                  <option value="Liechtenstein">Liechtenstein</option>
                                                  <option value="Lithuania">Lithuania</option>
                                                  <option value="Luxembourg">Luxembourg</option>
                                                  <option value="Macao">Macao</option>
                                                  <option value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option>
                                                  <option value="Madagascar">Madagascar</option>
                                                  <option value="Malawi">Malawi</option>
                                                  <option value="Malaysia">Malaysia</option>
                                                  <option value="Maldives">Maldives</option>
                                                  <option value="Mali">Mali</option>
                                                  <option value="Malta">Malta</option>
                                                  <option value="Marshall Islands">Marshall Islands</option>
                                                  <option value="Martinique">Martinique</option>
                                                  <option value="Mauritania">Mauritania</option>
                                                  <option value="Mauritius">Mauritius</option>
                                                  <option value="Mayotte">Mayotte</option>
                                                  <option value="Mexico">Mexico</option>
                                                  <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                  <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                  <option value="Monaco">Monaco</option>
                                                  <option value="Mongolia">Mongolia</option>
                                                  <option value="Montenegro">Montenegro</option>
                                                  <option value="Montserrat">Montserrat</option>
                                                  <option value="Morocco">Morocco</option>
                                                  <option value="Mozambique">Mozambique</option>
                                                  <option value="Myanmar">Myanmar</option>
                                                  <option value="Namibia">Namibia</option>
                                                  <option value="Nauru">Nauru</option>
                                                  <option value="Nepal">Nepal</option>
                                                  <option value="Netherlands">Netherlands</option>
                                                  <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                  <option value="New Caledonia">New Caledonia</option>
                                                  <option value="New Zealand">New Zealand</option>
                                                  <option value="Nicaragua">Nicaragua</option>
                                                  <option value="Niger">Niger</option>
                                                  <option value="Nigeria">Nigeria</option>
                                                  <option value="Niue">Niue</option>
                                                  <option value="Norfolk Island">Norfolk Island</option>
                                                  <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                  <option value="Norway">Norway</option>
                                                  <option value="Oman">Oman</option>
                                                  <option value="Pakistan">Pakistan</option>
                                                  <option value="Palau">Palau</option>
                                                  <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                  <option value="Panama">Panama</option>
                                                  <option value="Papua New Guinea">Papua New Guinea</option>
                                                  <option value="Paraguay">Paraguay</option>
                                                  <option value="Peru">Peru</option>
                                                  <option value="Philippines">Philippines</option>
                                                  <option value="Pitcairn">Pitcairn</option>
                                                  <option value="Poland">Poland</option>
                                                  <option value="Portugal">Portugal</option>
                                                  <option value="Puerto Rico">Puerto Rico</option>
                                                  <option value="Qatar">Qatar</option>
                                                  <option value="Reunion">Reunion</option>
                                                  <option value="Romania">Romania</option>
                                                  <option value="Russian Federation">Russian Federation</option>
                                                  <option value="Rwanda">Rwanda</option>
                                                  <option value="Saint Helena">Saint Helena</option>
                                                  <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                  <option value="Saint Lucia">Saint Lucia</option>
                                                  <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                                                  <option value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option>
                                                  <option value="Samoa">Samoa</option>
                                                  <option value="San Marino">San Marino</option>
                                                  <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                  <option value="Saudi Arabia">Saudi Arabia</option>
                                                  <option value="Senegal">Senegal</option>
                                                  <option value="Serbia">Serbia</option>
                                                  <option value="Seychelles">Seychelles</option>
                                                  <option value="Sierra Leone">Sierra Leone</option>
                                                  <option value="Singapore">Singapore</option>
                                                  <option value="Slovakia">Slovakia</option>
                                                  <option value="Slovenia">Slovenia</option>
                                                  <option value="Solomon Islands">Solomon Islands</option>
                                                  <option value="Somalia">Somalia</option>
                                                  <option value="South Africa">South Africa</option>
                                                  <option value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option>
                                                  <option value="South Sudan">South Sudan</option>
                                                  <option value="Spain">Spain</option>
                                                  <option value="Sri Lanka">Sri Lanka</option>
                                                  <option value="Sudan">Sudan</option>
                                                  <option value="Suriname">Suriname</option>
                                                  <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                                                  <option value="Swaziland">Swaziland</option>
                                                  <option value="Sweden">Sweden</option>
                                                  <option value="Switzerland">Switzerland</option>
                                                  <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                  <option value="Taiwan, Republic of China">Taiwan, Republic of China</option>
                                                  <option value="Tajikistan">Tajikistan</option>
                                                  <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                  <option value="Thailand">Thailand</option>
                                                  <option value="Timor-leste">Timor-leste</option>
                                                  <option value="Togo">Togo</option>
                                                  <option value="Tokelau">Tokelau</option>
                                                  <option value="Tonga">Tonga</option>
                                                  <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                  <option value="Tunisia">Tunisia</option>
                                                  <option value="Turkey">Turkey</option>
                                                  <option value="Turkmenistan">Turkmenistan</option>
                                                  <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                  <option value="Tuvalu">Tuvalu</option>
                                                  <option value="Uganda">Uganda</option>
                                                  <option value="Ukraine">Ukraine</option>
                                                  <option value="United Arab Emirates">United Arab Emirates</option>
                                                  <option value="United Kingdom">United Kingdom</option>
                                                  <option value="United States">United States</option>
                                                  <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                  <option value="Uruguay">Uruguay</option>
                                                  <option value="Uzbekistan">Uzbekistan</option>
                                                  <option value="Vanuatu">Vanuatu</option>
                                                  <option value="Venezuela">Venezuela</option>
                                                  <option value="Viet Nam">Viet Nam</option>
                                                  <option value="Virgin Islands, British">Virgin Islands, British</option>
                                                  <option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
                                                  <option value="Wallis and Futuna">Wallis and Futuna</option>
                                                  <option value="Western Sahara">Western Sahara</option>
                                                  <option value="Yemen">Yemen</option>
                                                  <option value="Zambia">Zambia</option>
                                                  <option value="Zimbabwe">Zimbabwe</option>
                                          </select>
                                            <!End Start country made!>

                                 </div>
                             </div>
                          
          <!Start status made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("itm_stat")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="status"  required="required">
                                         <option value="0" >..</option>
                                         <option value="1" <?php if($items['status']==1){echo "selected";}?>>New</option>
                                         <option value="2"<?php if($items['status']==2){echo "selected";}?>>Like New</option>
                                         <option value="3"<?php if($items['status']==3){echo "selected";}?>>Used</option>
                                         <option value="4"<?php if($items['status']==4){echo "selected";}?>>Old</option>
                                     </select>
                                            
                                 </div>
                             </div>
       
       <!Start Members Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_fld")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="member" required="required">
                                        <?php
                                        $statm=$con->prepare('SELECT * FROM users');
                                        $statm->execute();
                                        $r= $statm->fetchAll();
                                        foreach ($r as $usr){
                                            echo "<option value='".$usr['userID']."'";
                                            if($items['member_id']==$usr['userID'])
                                                {
                                                echo "selected";
                                                
                                                }
                                            echo ">"; echo $usr['UserName']."</option>";
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
       
      <!Start Category Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("Admin_CAT")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="categ" required="required">
                                        <?php
                                        $statm1=$con->prepare('SELECT * FROM categories');
                                        $statm1->execute();
                                        $c= $statm1->fetchAll();
                                        foreach ($c as $cats){
                                           // echo "<option value='".$cats['ID']."'>".$cats['Name']."</option>";
                                            
                                             echo "<option value='".$cats['ID']."'";
                                            if($items['item_id']==$cats['ID'])
                                                {
                                                echo "selected";
                                                
                                                }
                                            echo ">"; echo $cats['Name']."</option>";
                                            
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
                      <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">tags</label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="tags"
                                            class="form-control"
                                            autocomplete="off" 
                                  value="<?php echo $items['tags'];?>"/>

                                 </div>
                             </div>
       
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" 
                                            value="<?php echo lang("itm_up")?>" 
                                            class="btn btn-primary btn-sm" />
                                 </div>
                             </div>
                         </form>
                         <?php
 //fetch data from database to put into table
            try{
            $statm11=$con->prepare("SELECT comments.*,Users.UserName AS UN1 FROM comments INNER JOIN Users ON Users.userID=comments.User_id where comments.item_id=?");
            
            $statm11->execute(array($itemid));//select all items expect admin
            $rows=$statm11->fetchAll();
            }catch(PDOException $e)
                             {
                                  echo $e->getMessage();        
                              }
                      if(!empty($rows))  {      
            ?>
        <h1 class="text-center">Mange [<?php echo $items['item_name'];?>] comment</h1><br>
                <div class="table-responsive">
                    <table class="main-table text-center table table-bordered">
                        <tr>
                               <td>comment</td>
                               <td>user Name</td>
                               <td>Add Date</td>
                               <td>Control</td>
                        </tr>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['comment'].'</td>';
                                             echo'<td>'.$row['UN1'].'</td>';
                                              echo'<td>'.$row['date'].'</td>';
                            echo'<td> <a href="?do=editcom&comid='.$row['c_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                              . '<a href="?do=deletecom&comid='.$row['c_id'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($row['status']==0){
                                                    echo '<a href="?do=Approvecom&comid='.$row['c_id'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>
                      <?php } ?>
                      </div>  
                       
        <?php   }
        //$count =0 in other word no such userid in database
        else{
                echo '<div class="container">';
        $msg= '<div class="alert alert-danger">There is no such ID</div>';
                     Redirect1($msg);    
                  echo '</div>';

       
        } 
        }
    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='updateitm'){
             if($_SERVER['REQUEST_METHOD']=='POST'){
                  echo '<h1 class="text-center"> update Iteme</h1>';
            echo '<div class="container">';
                $id=filter_input(INPUT_POST, 'itmid');
                $itm_nam=filter_input(INPUT_POST, 'name');
                $itm_des=filter_input(INPUT_POST, 'des');
                $itm_pri=filter_input(INPUT_POST, 'pri');
                $itm_cont=filter_input(INPUT_POST, 'cont');
                $itm_stat=filter_input(INPUT_POST, 'status');
                $itm_mem=filter_input(INPUT_POST, 'member');
                $itm_cat=filter_input(INPUT_POST, 'categ');
                $itm_tags=filter_input(INPUT_POST, 'tags');
                
                //validate the form
                $errorarray=array();
                if(empty($itm_nam)){$errorarray[]="Name must be <strong>not Empty</strong>";}
                if(empty($itm_des)){$errorarray[]="Description must be <strong>not Empty</strong>";}
                if(empty($itm_pri)){$errorarray[]="Price must be <strong>not Empty</strong>";}
                if(empty($itm_cont)){$errorarray[]="country must be <strong>not Empty</strong>";}
                if($itm_stat==0){$errorarray[]="you must choose <strong>status</strong>";}
                if($itm_mem==0){$errorarray[]="you must choose <strong>The member</strong>";}
                if($itm_cat==0){$errorarray[]="you must choose <strong>The category</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\">'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                       try{
                $statm=$con->prepare("UPDATE items SET item_name=?,description=?,price=?,country_name=?,status=?,category_id=?,member_id=?,tags=? WHERE item_id=?");
                $statm->execute(array($itm_nam,$itm_des,$itm_pri,$itm_cont,$itm_stat,$itm_cat,$itm_mem,$itm_tags,$id));
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect1($msg,"back");
                     echo '</div>';
                       }catch(PDOException $e)
                             {
                                  echo $e->getMessage();    
                              }
                       }
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect1($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
         elseif ($do==='deleteitm') {
              //delete Member
            echo '<h1 class="text-center"> Delete Item</h1>';
            echo '<div class="container">';
                    $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
                    
                   $check=  CheckItems("item_id", "items", $itemid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM items WHERE item_id=:zitem');
                        $statm->bindParam(":zitem",$itemid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect1($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect1($msg,'back');

                 }
            echo '</div>';
         }
    /*========================================================= 
            *
            * if do==approve
            * 
    * ========================================================= 
    */
          elseif ($do==='Approveitm') {
                       //Aprove users in database
        echo '<h1 class="text-center"> Approve Item</h1>';
            echo '<div class="container">';
                    $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
                    
                   $check=  CheckItems("item_id", "items", $itemid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE items SET Approve_itm=1 WHERE item_id=?');
                        $statm->execute(array($itemid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'Item Approved</div>';
                     Redirect1($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such item';
                   Redirect1($msg);

                 }
            echo '</div>';  
          }
      
    

        ?>




  
  
  

</div>
<div class="page page-map">
  <div class="bg"></div>
  <div class="overlay"></div>
  <div class="title">Traffic map</div>
  <div class="map"></div>
</div>


<div class="page page-tasks">
 
  <?php
         $do=isset($_GET['do'])?$_GET['do']:'managemem';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="managemem"){ //manage memeber page
        $quary='';
        if(isset($_GET['page'])&&$_GET['page']='pending'){
            $quary='AND ApproveStatus=0';
        }
            //fetch data from database to put into table
            $statm=$con->prepare("SELECT * FROM users WHERE groupID!=1 $quary");
            $statm->execute();//select all user expect admin
            $rows=$statm->fetchAll();
            if(!empty($rows)){
            ?>
        <h1 class="text-center">Mange Member Page</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="main-table text-center table table-bordered">
                        <tr>
                               <td>#ID</td>
                               <td>User Name</td>
                               <td>Email</td>
                               <td>Full Name</td>
                               <td>Register Date</td>
                               <td>Control</td>
                        </tr>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['userID'].'</td>';
                                            echo'<td>'.'<a href="message.php?user='.$row['userID'].'&full='.$row['FullName'].'&mail='.$row['Email'].'">'.$row['UserName'].'</a>'.'</td>';
                                             echo'<td>'.$row['Email'].'</td>';
                                             echo'<td>'.$row['FullName'].'</td>';
                                              echo'<td>'.$row['Date'].'</td>';
                            echo'<td> <a href="?do=editmem&userid='.$row['userID'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                              . '<a href="?do=deletemem&userid='.$row['userID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($row['ApproveStatus']==0){
                                                    echo '<a href="?do=activatemem&userid='.$row['userID'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }
                            echo '</td>';          
                            echo '</tr>';
            }
                        ?>
                      
                      
                    </table>
                    
                </div>
                        <a href="?do=Addmem" class="btn btn-primary"><i class="fa fa-plus"></i>New Members</a>
            </div>
       <?php }else {echo '<div class="container><div class="message">there is no users</div></div>';}}//end if do=manage
    /*========================================================= 
            *
            * if do==Add
            * 
    * ========================================================= 
    */
        elseif($do==='Addmem'){
            //Add new Member
            //Add members Form
            ?>
            <h1 class="text-center"><?php echo lang("Mem_add")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=insertmem" method="POST">
                             <!Start User name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="username"  class="form-control" autocomplete="off" required="required" placeholder="UserName to login into shop"/>
                                 </div>
                             </div>
                          <!Start User name password!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Pass")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="password" name="password" class="password form-control"  autocomplete="new-password" placeholder="Password must be hard complex" required="required"/>
                                     <i class="show-pass fa fa-eye fa-2x"></i>
                                 </div>
                             </div>
                <!Start User name E-Mail!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Email")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="email" name="email"  class="form-control" required="required" placeholder="E-mail must be value"/>
                                 </div>
                             </div>
                <!Start User name Full Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_FullN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="full"  class="form-control" required="required" placeholder="Full name appear in profile page"/>
                                 </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("ADD_BUtton")?>" class="btn btn-primary btn-lg"/>
                                 </div>
                             </div>
                         </form>
                      </div>
            
      <?php 
      
      
        }//end if do=add
    /*========================================================= 
            *
            * if do==insert
            * 
    * ========================================================= 
    */
        elseif ($do==='insertmem') {
              //in insert form
            
                if($_SERVER['REQUEST_METHOD']=='POST'){
                echo '<h1 class="text-center"> Insert Member</h1>';
                echo '<div class="container">';
                $user=filter_input(INPUT_POST, 'username');
                $pass=filter_input(INPUT_POST, 'password');
                $email=filter_input(INPUT_POST, 'email');
                $full=filter_input(INPUT_POST, 'full');
                $hashpass=sha1(filter_input(INPUT_POST, 'password'));

                //validate the form
                $errorarray=array();
                if(strlen($user)<4&&strlen($user)!=0){$errorarray[]="UserName can't be <strong>less than 4 character</strong>"; }
                if(strlen($user)>20){$errorarray[]="UserName can\'t be <strong>more than 20 character</strong>"; }
                if(empty($user)){$errorarray[]="UserName must be <strong>not Empty</strong>";}
                if(empty($pass)){$errorarray[]="Password must be <strong>not Empty</strong>";}
                if(empty($email)){$errorarray[]="Email must be <strong>not Empty</strong>";}
                if(empty($full)){$errorarray[]="Full Name must be <strong>not Empty</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\"'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                       //Insert member
                       //check if user exist
                       $check=  CheckItems("UserName", "users", $user);
                       if($check==1){
                           $msg= '<div class="alert alert-danger>"sorry this user is exist</div>';
                            Redirect1($msg,"back");

                       }
                       else{
                       $statm=$con->prepare('INSERT INTO users (UserName,password,Email,FullName,ApproveStatus,Date)VALUES(:zuser,:zpass,:zmail,:zname,1,now())' );
                       $statm->execute(array("zuser"=>$user,"zpass"=>$hashpass,"zmail"=>$email,"zname"=>$full));
                     //echo success message
                       echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect1($msg,"back");
                     echo '</div>';
                       }
                       }
            }//end is not post request 
            else{
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect1($msg,null,5);
            }//end show error
            echo '</div>';
            }//end if do=insert
    /*========================================================= 
            *
            * if do==edit
            * 
    * ========================================================= 
    */
        elseif($do=='editmem'){            //Edit page
            
           $userid= isset($_GET['userid'])&& is_numeric($_GET['userid']) ?intval($_GET['userid']):0;
        $statm=$con->prepare('SELECT * FROM users WHERE userID=?');
        $statm->execute(array($userid));
        $row=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {      
              ?>
        
                <h1 class="text-center"><?php echo lang("mem_edit")?></h1>
                     <div class="container">
                         <form class="form-horizontal" action="?do=updatemem" method="POST">
                             <input type="hidden" name="userid"value="<?php echo $userid?>"/>
                             <!Start User name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_UN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="username" value="<?php echo $row['UserName']?>" class="form-control" autocomplete="off" required="required"/>
                                 </div>
                             </div>
                          <!Start User name password!>
                            <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Pass")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="hidden" name="oldpassword" value="<?php echo $row['password'];?>"/>
                                     <input type="text" name="newpassword" class="form-control"  autocomplete="new-password" placeholder="Leave it blank if you don't change"/>
                                 </div>
                             </div>
                <!Start User name E-Mail!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_Email")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="email" name="email" value="<?php echo $row['Email']?>" class="form-control" required="required"/>
                                 </div>
                             </div>
                <!Start User name Full Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label"><?php echo lang("mem_FullN")?></label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="full" value="<?php echo $row['FullName']?>" class="form-control" required="required"/>
                                 </div>
                             </div>
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" value="<?php echo lang("Save")?>" class="btn btn-primary btn-lg"/>
                                 </div>
                             </div>
                         </form>
                      </div>
            
        <?php   }
        //$count =0 in other word no such userid in database
        else{
                echo '<div class="container">';
        $msg= '<div class="alert alert-danger">There is no such ID</div>';
                     Redirect1($msg);    
                  echo '</div>';

        }
            
        }//end if do=edit
    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='updatemem'){//update page
            
              if($_SERVER['REQUEST_METHOD']=='POST'){
                  echo '<h1 class="text-center"> update Member</h1>';
            echo '<div class="container">';
                $id=filter_input(INPUT_POST, 'userid');
                $user=filter_input(INPUT_POST, 'username');
                $email=filter_input(INPUT_POST, 'email');
                $full=filter_input(INPUT_POST, 'full');
               // password updata
                $pass=empty(filter_input(INPUT_POST, 'newpassword'))?filter_input(INPUT_POST, 'oldpassword'):sha1(filter_input(INPUT_POST, 'newpassword'));
                //validate the form
                $errorarray=array();
                if(strlen($user)<4&&strlen($user)!=0){$errorarray[]="UserName can't be <strong>less than 4 character</strong>"; }
                if(strlen($user)>20){$errorarray[]="UserName can\'t be <strong>more than 20 character</strong>"; }
                if(empty($user)){$errorarray[]="UserName must be <strong>not Empty</strong>";}
                if(empty($email)){$errorarray[]="Email must be <strong>not Empty</strong>";}
                if(empty($full)){$errorarray[]="Full Name must be <strong>not Empty</strong>";}
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\"'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                        $statm2=$con->prepare("SELECT * FROM users WHERE UserName=? AND userID!=?");
                                        $statm2->execute(array($user,$id));
                                        $count=$statm2->rowCount();
                                        if($count==1){
                                            echo '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>this user exist</div>';
                                            Redirect1($mesg,'back');
                                            }  else {
                                            
    


                $statm=$con->prepare("UPDATE users SET UserName=?,Email=?,FullName=?,password=? WHERE userID=?");
                $statm->execute(array($user,$email,$full,$pass,$id));
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>';
                     Redirect1($msg,"back");
                     echo '</div>';
                       }}
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect1($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
        elseif ($do==='deletemem') {
        //delete Member
            echo '<h1 class="text-center"> Delete Member</h1>';
            echo '<div class="container">';
                    $userid= isset($_GET['userid'])&& is_numeric($_GET['userid']) ?intval($_GET['userid']):0;
                    
                   $check=  CheckItems("userID", "users", $userid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM users WHERE userID=:zuser');
                        $statm->bindParam(":zuser",$userid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect1($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect1($msg,'back');

                 }
            echo '</div>';
             }
    /*========================================================= 
            *
            * if do==activate
            * 
    * ========================================================= 
    */
             elseif($do==='activatemem'){
                 //Aprove users in database
        echo '<h1 class="text-center"> Activate Member</h1>';
            echo '<div class="container">';
                    $userid= isset($_GET['userid'])&& is_numeric($_GET['userid']) ?intval($_GET['userid']):0;
                    
                   $check=  CheckItems("userID", "users", $userid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE users SET ApproveStatus=1 WHERE userID=?');
                        $statm->execute(array($userid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Activated</div>';
                     Redirect1($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect1($msg);

                 }
            echo '</div>';             
            
                 }
                 ?>
 
  
  
</div>
<div class="page page-news">
  <div class="bg"></div>
  <div class="overlay">
      
      

  </div>
</div>
<div class="home-stat">
        <div class="container text-center">
                <h1 class="text-center">Dashboard Page</h1>
                <div class="row">
                    <div class="col-md-3">
                        <div class="stat st-members">
                            <div class="info">
                                <i class="fa fa-users"></i>
                            Total Members
                            <span><a href="members.php"><?php echo countitems("userID", "users")-1?></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-Pending">
                            <div class="info">
                                <i class="fa fa-user-plus"></i>
                            Pending Members<span><a href="members.php?do=manage&page=pending">
                                     <?php echo CheckItems('ApproveStatus','users',0)?></a></span></div></div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-items">
                            <div class="info">
                                <i class="fa fa-tag"></i>
                                <div class="info">
                                <i class="fa fa-tag"></i>
                                Total Items<span><a href="items.php"><?php echo countitems('item_id','items')?></a></span></div></div></div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-Comments">
                            <div class="info">
                                <i class="fa fa-comments"></i>
                                Total Comments<span><a href="comments.php"><?php echo countitems('c_id','comments')?></a></span></div></div>
                    </div>  
                      </div>
                </div>
</div>

        <div class="latest">
            <div class="container">
                      <div class="row">
              <div class="col-sm-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-user"></i> Latest <?php echo $num_user?> registered user
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                          <ul class="list-unstyled latest-user">
                          <?php      
                                    if(!empty($latest_user)){

                         foreach ($latest_user as $latus){
                             echo '<li>'.$latus['UserName'].'<span class="btn btn-success edit1 pull-right"><i class="fa fa-edit">';
                                     echo '</i><a href="members.php?do=edit&userid='.$latus['userID'].'"">Edit</a></span>';
                                       if($latus['ApproveStatus']==0){
                                                    echo '<a href="members.php?do=activate&userid='.$latus['userID'].'" class="btn btn-info activate1 pull-right"><i class="fa fa-edit"></i>Approve</a>';
                                                }
                                    echo ''.'</li>';
                                    }}  else {
                           echo 'There is no recently register user';    
                            }
                          ?>
                          </ul>
                      </div>
                  </div>

              </div>
              <div class="col-sm-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-tag"></i> Latest <?php echo $num_items?> recently items 
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                            <ul class="list-unstyled latest-user">
                          <?php   
                          if(!empty($latest_items)){
                         foreach ($latest_items as $latusitm){
                             echo '<li>'.$latusitm['item_name'].'<span class="btn btn-success edit1 pull-right"><i class="fa fa-edit">';
                                     echo '</i><a href="items.php?do=edit&itemid='.$latusitm['item_id'].'"">Edit</a></span>';
                                       if($latusitm['Approve_itm']==0){
                                                    echo '<a href="items.php?do=Approve&itemid='.$latusitm['item_id'].'" class="btn btn-info activate1 pull-right"><i class="fa fa-check"></i>Approve</a>';
                                                }
                                    echo ''.'</li>';
                          }}  else {
                     echo 'There is no recent items added';    
                                }
                          ?>
                          </ul>
                      </div>
                  </div>

              </div>
              </div>
                                      <div class="row">
                                             <div class="col-sm-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-comments"></i> Latest <?php echo $num_comm?> recently Comment 
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                          <?php   
                           $statm11=$con->prepare("SELECT comments.*,Users.UserName AS UN1 FROM comments INNER JOIN Users ON Users.userID=comments.User_id");
                            $statm11->execute();//select all comments expect admin
                            $comments=$statm11->fetchAll();
                          if(!empty($comments)){
                         foreach ($comments as $latcotm){
                             echo '<div class="comment-box">';
                             echo '<span class="member-n">'.$latcotm[UN1].'</span>';
                             echo '<p class="member-c">'.$latcotm['comment'].'</span>';
                            
                                      echo'<div><a href="comments.php?do=edit&comid='.$latcotm['c_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                              . '<a href="comments.php?do=delete&comid='.$latcotm['c_id'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($latcotm['status']==0){
                                                    echo '<a href="comments.php?do=Approve&comid='.$latcotm['c_id'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a></div>';
                                                }
                                            echo '</div>';
                                       }
                          
                                       }  else {
                             echo 'No Latest comments';    
}
                          ?>
                          
                      </div>
                  </div>

              </div>
                                      </div>
      </div>
        
    </div>
<?php
         include $tpl.'Footer.php';
        }
        else{
            
                header("Location:index.php");
                exit;
        }

       ob_end_flush();
