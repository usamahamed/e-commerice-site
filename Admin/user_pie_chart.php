<?php
include 'connectdb.php';
include 'stat_count.php';
require_once ('Includes/libs/src/jpgraph.php');
require_once ('Includes/libs/src/jpgraph_pie.php');
require_once ('Includes/libs/src/jpgraph_pie3d.php');

// Some data
$data = array($users,$trust_user,$admin_user,$pending_users);

// Create the Pie Graph. 
$graph = new PieGraph(350,250);

$theme_class= new VividTheme;
$graph->SetTheme($theme_class);
$graph->title->SetMargin(20); 

// Set A title for the plot
$graph->title->Set("User Statistics");

// Create
$p1 = new PiePlot3D($data);
$p1->SetLabelType(PIE_VALUE_PER);
$lbl = array("Number\n%.1f%%","Trust\n%.1f%%","Admin\n%.1f%%",
         "Pending\n%.1f%%");
$p1->SetLabels($lbl);
$p1->ExplodeAll(25);
$p1->SetSize(0.50);
$p1->SetShadow();

$graph->Add($p1);
$p1->ShowBorder();
$p1->SetColor('black');
$p1->ExplodeSlice(1);
$graph->Stroke();

