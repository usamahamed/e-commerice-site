 <?php 
 /*========================================================= 
 *Dashboard Page 
 * ========================================================= 
 */
    session_start();
        $PageTitel='Dashboard';

    if(isset($_SESSION['user'])){
        include 'inti.php';
        //count latest user and display in dashboard
        $num_user=3;
        $latest_user= getlatest("*","users","userID",$num_user);
       //count latest Items and display in dashboard
        $num_items=6;
        $latest_items=getlatest("*","items","item_id",$num_items);
        //count latest comments
        $num_comm=6;
        $latest_comments=getlatest("*","comments","c_id",$num_comm);
/*Start Dashbooard*/
        ?>
<div class="home-stat">
        <div class="container text-center">
                <h1 class="text-center">Dashboard Page</h1>
                <div class="row">
                    <div class="col-md-3">
                        <div class="stat st-members">
                            <div class="info">
                                <i class="fa fa-users"></i>
                            Total Members
                            <span><a href="members.php"><?php echo countitems("userID", "users")-1?></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-Pending">
                            <div class="info">
                                <i class="fa fa-user-plus"></i>
                            Pending Members<span><a href="members.php?do=manage&page=pending">
                                     <?php echo CheckItems('ApproveStatus','users',0)?></a></span></div></div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-items">
                            <div class="info">
                                <i class="fa fa-tag"></i>
                                <div class="info">
                                <i class="fa fa-tag"></i>
                                Total Items<span><a href="items.php"><?php echo countitems('item_id','items')?></a></span></div></div></div>
                    </div>
                    <div class="col-md-3">
                        <div class="stat st-Comments">
                            <div class="info">
                                <i class="fa fa-comments"></i>
                                Total Comments<span><a href="comments.php"><?php echo countitems('c_id','comments')?></a></span></div></div>
                    </div>  
                      </div>
                </div>
</div>

        <div class="latest">
            <div class="container">
                      <div class="row">
              <div class="col-sm-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-user"></i> Latest <?php echo $num_user?> registered user
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                          <ul class="list-unstyled latest-user">
                          <?php      
                                    if(!empty($latest_user)){

                         foreach ($latest_user as $latus){
                             echo '<li>'.$latus['UserName'].'<span class="btn btn-success edit1 pull-right"><i class="fa fa-edit">';
                                     echo '</i><a href="members.php?do=edit&userid='.$latus['userID'].'"">Edit</a></span>';
                                       if($latus['ApproveStatus']==0){
                                                    echo '<a href="members.php?do=activate&userid='.$latus['userID'].'" class="btn btn-info activate1 pull-right"><i class="fa fa-edit"></i>Approve</a>';
                                                }
                                    echo ''.'</li>';
                                    }
                                    
                                       }  else {
                           echo 'There is no recently register user';    
                            }
                          ?>
                          </ul>
                      </div>
                  </div>

              </div>
              <div class="col-sm-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-tag"></i> Latest <?php echo $num_items?> recently items 
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                            <ul class="list-unstyled latest-user">
                          <?php   
                          if(!empty($latest_items)){
                         foreach ($latest_items as $latusitm){
                             echo '<li>'.$latusitm['item_name'].'<span class="btn btn-success edit1 pull-right"><i class="fa fa-edit">';
                                     echo '</i><a href="items.php?do=edit&itemid='.$latusitm['item_id'].'"">Edit</a></span>';
                                       if($latusitm['Approve_itm']==0){
                                                    echo '<a href="items.php?do=Approve&itemid='.$latusitm['item_id'].'" class="btn btn-info activate1 pull-right"><i class="fa fa-check"></i>Approve</a>';
                                                }
                                    echo ''.'</li>';
                          }}  else {
                     echo 'There is no recent items added';    
                                }
                          ?>
                          </ul>
                      </div>
                  </div>

              </div>
              </div>
                                      <div class="row">
                                             <div class="col-sm-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          <i class="fa fa-comments"></i> Latest <?php echo $num_comm?> recently Comment 
                          <span class="toggle-info pull-right">
                              <i class="fa fa-plus fa-lg"></i>
                          </span>
                      </div>
                      <div class="panel-body">
                          <?php   
                           $statm11=$con->prepare("SELECT comments.*,Users.UserName AS UN1 FROM comments INNER JOIN Users ON Users.userID=comments.User_id");
                            $statm11->execute();//select all comments expect admin
                            $comments=$statm11->fetchAll();
                          if(!empty($comments)){
                         foreach ($comments as $latcotm){
                             echo '<div class="comment-box">';
                             echo '<span class="member-n">'.$latcotm[UN1].'</span>';
                             echo '<p class="member-c">'.$latcotm['comment'].'</span>';
                            
                                      echo'<div><a href="comments.php?do=edit&comid='.$latcotm['c_id'].'" class="btn btn-success"><i class="fa fa-edit"></i>Edit</a>'
                                              . '<a href="comments.php?do=delete&comid='.$latcotm['c_id'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                if($latcotm['status']==0){
                                                    echo '<a href="comments.php?do=Approve&comid='.$latcotm['c_id'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a></div>';
                                                }
                                            echo '</div>';
                                       }
                          
                                       }  else {
                             echo 'No Latest comments';    
}
                          ?>
                          
                      </div>
                  </div>

              </div>
                                      </div>
      </div>
        
    </div>
<?php
         include $tpl.'Footer.php';
        }
        else{
            
                header("Location:index.php");
                exit;
        }

