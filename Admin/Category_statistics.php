<?php
include 'connectdb.php';
include 'stat_count.php';
require_once ('Includes/libs/src/jpgraph.php');
require_once ('Includes/libs/src/jpgraph_bar.php');
 
 // Setup labels
$count=0;
$lbl = array();
$data = array();
foreach ($cat_array as $lbl1){
    $lbl[$count]=$lbl1['Name'];
    $data[$count]=itmsincat($lbl1['ID']);
    
    $count++;
}
// Size of graph
$width=600;
$height=500;
 
// Set the basic parameters of the graph
$graph = new Graph($width,$height);

$graph->SetScale('textlin');
$top = 60;
$bottom = 30;
$left = 100;
$right = 30;
$graph->Set90AndMargin($left,$right,$top,$bottom);
 
// Nice shadow
$graph->SetShadow();
$graph->xaxis->SetTickLabels($lbl);
$graph->yaxis->SetLabelAlign('right','center');
 
// Label align for X-axis
$graph->xaxis->SetLabelAlign('right','center','right');
 
// Label align for Y-axis
$graph->yaxis->SetLabelAlign('center','bottom');
 
// Titles
$graph->title->Set('Number of Items in category');
 
// Create a bar pot
$bplot = new BarPlot($data);
$bplot->SetFillColor('orange');
$bplot->SetWidth(0.4);
$bplot->SetYMin(1990);
 
$graph->Add($bplot);
 
$graph->Stroke();
