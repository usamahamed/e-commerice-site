<?php
include 'connectdb.php';
include 'stat_count.php';
require_once ('Includes/libs/src/jpgraph.php');
require_once ('Includes/libs/src/jpgraph_bar.php');
require_once ('Includes/libs/src/jpgraph_line.php');



$datay=array($c_item1/10,$c_item2/10,$c_item3/10,$c_item4/10,$c_item5/10,$c_item6/10,$c_item7/10,$c_item8/10,$c_item9/10,$c_item10/10,$c_item11/10,$c_item12/10);


// Create the graph. These two calls are always required
$graph = new Graph(650,300,'auto');
$graph->SetScale("textlin");

//$theme_class="DefaultTheme";
//$graph->SetTheme(new $theme_class());

// set major and minor tick positions manually
//$graph->yaxis->SetTickPositions(array(0,5,10,15,20), array(15,45,75,105,135));
$graph->yaxis->scale->SetGrace(30);

$graph->yaxis->SetLabelMargin(10);

$graph->SetBox(false);

//$graph->ygrid->SetColor('gray');
$graph->ygrid->SetFill(false);
$graph->xaxis->SetTickLabels(array('Jan','Feb','Mar','Aprl','May','Jan','Jul','Agu','Setp','Oct','Nov','Dec'));
$graph->yaxis->HideLine(false);
$graph->yaxis->HideTicks(false,false);

// Create the bar plots
$b1plot = new BarPlot($datay);

// ...and add it to the graPH
$graph->Add($b1plot);

$b1plot->SetColor("white");
$b1plot->SetFillGradient("red","white",GRAD_LEFT_REFLECTION);
$b1plot->SetWidth(45);
$graph->title->Set("Items Statistics");

// Display the graph
$graph->Stroke();
