<section>
	<header>
		<nav class="rad-navigation">
                    
			<div class="rad-logo-container rad-nav-min">
                            <a href="#" class="rad-logo pull-left"><i class=" fa fa-recycle"></i>V-Shop</a>
			    <a href="#" class="rad-toggle-btn pull-right"><i class="fa fa-bars"></i></a>
			</div>
                    <a href="#" class="rad-logo-hidden">Radar</a>
			<div class="rad-top-nav-container">
				<a href="" class="brand-icon"><i class="fa fa-recycle"></i></a>
                                
                            
                                
                                
				
			</div>
		</nav>
	</header>
</section>
<aside>
	<nav class="rad-sidebar rad-nav-min">
            <ul class="list-unstyled">
			<li>
				<a href="dashboard.php" class="inbox">
					<i class="fa fa-dashboard"><span class="icon-bg rad-bg-success"></span></i>
					<span class="rad-sidebar-item">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="Categories.php">
					<i class="fa fa-object-group">
						<span class="icon-bg rad-bg-danger"></span>
					</i>
					<span class="rad-sidebar-item">Categories</span>
				</a>
			</li>
			<li><a href="items.php" class="snooz"><i class="fa fa-shopping-basket"><span class="icon-bg rad-bg-primary"></span></i><span class="rad-sidebar-item">Items</span></a></li>
			<li><a href="members.php" class="done"><i class="fa fa-users"><span class="icon-bg rad-bg-warning"></span></i><span class="rad-sidebar-item">Members</span></a></li>
			<li><a href="mails.php" class="done"><i class="fa fa-mail-forward"><span class="icon-bg rad-bg-warning1"></span></i><span class="rad-sidebar-item">mails</span></a></li>
			<li>
				<a href="statistics.php">
					<i class="fa fa-bar-chart-o"><span class="icon-bg rad-bg-violet"></span></i>
					<span class="rad-sidebar-item">Statistics</span>
				</a>
			</li>
                        <li>
				<a href="comments.php">
					<i class="fa fa-comment-o"><span class="icon-bg rad-bg-c"></span></i>
					<span class="rad-sidebar-item">Comments</span>
				</a>
			</li>

		<li>
				<a href="logs.php">
					<i class="fa fa-life-ring"><span class="icon-bg rad-bg-l"></span></i>
					<span class="rad-sidebar-item">Logs</span>
				</a>
			</li>
		<li>
				<a href="statistics.php">
					<i class="fa fa-scissors"><span class="icon-bg rad-bg-s"></span></i>
					<span class="rad-sidebar-item">Settings</span>
				</a>
			</li>
		
<li>
				<a href="mails.php">
					<i class="fa fa-mail-reply"><span class="icon-bg rad-bg-ml"></span></i>
					<span class="rad-sidebar-item">Mails</span>
				</a>
			</li>
		
<li>
				<a href="FAQ.php">
					<i class="fa fa-question"><span class="icon-bg rad-bg-qus"></span></i>
					<span class="rad-sidebar-item">FAQ</span>
				</a>
			</li>
		
<li>
				<a href="Reports.php">
					<i class="fa fa-dribbble"><span class="icon-bg rad-bg-rp"></span></i>
					<span class="rad-sidebar-item">Reports</span>
				</a>
			</li>
		
	<li>
				<a href="members.php?do=edit&userid=15">
					<i class="fa fa-edit"><span class="icon-bg rad-bg-a"></span></i>
					<span class="rad-sidebar-item">Edit Admin</span>
				</a>
			</li>
		
	<li>
				<a href="logout.php">
					<i class="fa fa-sign-out"><span class="icon-bg rad-bg-lo"></span></i>
					<span class="rad-sidebar-item">Logout</span>
				</a>
			</li>
		
		</ul>
	</nav>
</aside>

<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#App-nav" target="#App-nav" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="dashboard.php"><?php echo lang("Home_Admin")?></a>
    </div>
        <img src="layout/Img/shop.jpeg" alt="vshop" class="Logo_image img-responsive pull-right"/>

    <div class="collapse navbar-collapse" id="App-nav">
      <ul class="nav navbar-nav">
          <li><a href="Categories.php"><?php echo lang("Admin_CAT")?></a></li>
          <li><a href="items.php"><?php echo lang("Admin_Items")?></a></li>
          <li><a href="members.php"><?php echo lang("Admin_Mem")?></a></li>
          <li><a href="statistics.php"><?php echo lang("Admin_Statistic")?></a></li>
         <li><a href="comments.php"><?php echo lang("Admin_Comments")?></a></li>
         <li><a href="mails.php">Mails</a></li>
         <li><a href="FAQ.php">FAQ</a></li>
         <li><a href="Reports.php">Report Messages</a></li>
          <li><a href="logs.php"><?php echo lang("Admin_Logs")?></a></li></ul>
        
     
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Control Panel <span class="caret"></span></a>
          <ul class="dropdown-menu">
                        <li><a href="../index.php">Show Shop</a></li>

              <li><a href="members.php?do=edit&userid=<?php echo $_SESSION['Id'] ?>"><?php echo lang("EditProf")?></a></li>
            <li><a href="#"><?php echo lang("Sett")?></a></li>
            <li><a href="logout.php"><?php echo lang("Logout")?></a></li>
           
          </ul>
        </li>
      </ul>
    </div>
  </div>
</nav>