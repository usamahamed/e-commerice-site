<?php
/* 
 * function name-------->genral_feth()
 * function Version----->.01
 * function role----->get All from database
 * Start function
 */
function genral_feth($feild,$table,$where=null,$and=null,$orderfeild=null,$ordring="DESC"){
    
        global $con;
        $statm3=$con->prepare("SELECT $feild FROM $table $where $and ORDER BY $orderfeild $ordring");
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        return $ALLs;

}
/*End genral_feth Function*/
/* 
 * function name-------->getTitel()
 * function Version----->.01
 * function role----->Print the titel of each page depend on specfic variable 
 * Start function
 */
function getTitel(){
    global $PageTitel;
    if(isset($PageTitel)){
        echo $PageTitel;
    }
    else {
        echo "Default";
    }
}
/*End getTitel Function*/
/* 
 * function name-------->Redirect()
 * function Version----->.02
 * function role----->Redirest to home page after specific time and produce error message
 * Start function
 */
function Redirect($mesg,$url=null,$second=3){
     if($url===null){
         $url='index.php';
         $link='Home Page';
     }else{
         if(isset($_SERVER['HTTP_REFERER'])&&$_SERVER['HTTP_REFERER']!==''){
                  $url=$_SERVER['HTTP_REFERER'];
                  $link='Prevouis Page';

         }else{
                      $url='index.php';
                      $link='Home Page';


         }
     }
   echo $mesg;
  echo "<div class='alert alert-info'>you will be redirectd to $link page after$second</div>";
  //echo '<script>window.location=index.php</script>'; 
 // header("Location:index.php",TRUE);
  header("refresh:$second;url=$url",true);
  
  exit;
  
}
/*End RedirectFunction Function*/
/* 
 * function name-------->CheckItems()
 * function Version----->.01
 * function role----->check existance in database
 * Start function
 */
function CheckItems($select,$from,$value){
    global $con;
    $statment=$con->prepare("SELECT $select FROM $from WHERE $select=?");
    $statment->execute(array($value));
    $count=$statment->rowCount();
    return $count;
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitems()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitems($item,$table){
    global $con;
    $statm2=$con->prepare("SELECT COUNT($item) FROM $table");
        $statm2->execute();
        return  $statm2->fetchColumn();
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitemsAll()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitemsAll($item,$table,$select){
    global $con;
    $statm2=$con->prepare("SELECT COUNT($item) FROM $table WHERE $select");
        $statm2->execute();
        return  $statm2->fetchColumn();
}
/*End RedirectFunction Function*/
/* 
 * function name-------->getLatest()
 * function Version----->.01
 * function role----->get latest items from database[users,items,comments]
 * Start function
 */
function getlatest($select,$table,$order,$limt=5){
        global $con;
$statm3=$con->prepare("SELECT $select FROM $table ORDER BY $order DESC LIMIT $limt");
        $statm3->execute();
$rows=$statm3->fetchAll();
return $rows;

}
/*End getLatest Function*/
/* 
 * function name-------->getcats()
 * function Version----->.01
 * function role----->get categories from database
 * Start function
 */
function getcats(){
        global $con;
$statm3=$con->prepare("SELECT ID,Name FROM categories ORDER BY ID ASC");
        $statm3->execute();
$cats=$statm3->fetchAll();
return $cats;

}
/*End getLatest Function*/
/* 
 * function name-------->itmsincat()
 * function Version----->.01
 * function role----->get count items in each category from database
 * Start function
 */
function itmsincat($value){
        global $con;
$statm3=$con->prepare("SELECT COUNT(items.item_id)"
                   . "FROM items INNER JOIN categories ON categories.ID=items.category_id WHERE categories.ID=$value");
                               
        $statm3->execute();
return  $statm3->fetchColumn();

}
/*End getLatest Function*/
/* 
 * function name-------->getUserName()
 * function Version----->.01
 * function role----->Get name of user
 * Start function
 */
function getUserName($uid){
        global $con;
$statm=$con->prepare("SELECT * FROM Users WHERE userID=?");
 $statm->execute(array($uid));
    $row=$statm->fetch();
return $row['UserName'];           

}
/*End getLatest Function*/
/* 
 * function name-------->getUserName()
 * function Version----->.01
 * function role----->Get name of user
 * Start function
 */
function DisplayLog($uid){
        global $con;
$statm=$con->prepare("SELECT * FROM logs WHERE User_ID=?");
 $statm->execute(array($uid));
    $rows=$statm->fetchAll();
return $rows;           

}
/*End getLatest Function*/