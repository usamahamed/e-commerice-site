<?php ob_start(); ?>    

<?php

/*========================================================= 
 *Comments Page 
 * ========================================================= 
 */
        session_start();
        $PageTitel='Reports';

    if(isset($_SESSION['user'])){
        include 'inti.php';
        $do=isset($_GET['do'])?$_GET['do']:'manage';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){ //manage memeber page
       
            //fetch data from database to put into table
            try{
          $statm11=$con->prepare("SELECT report_item.*,Users.FullName AS FUlN,items.member_id AS owner,items.item_name AS ITM,items.item_id AS ITMID FROM report_item INNER JOIN Users ON Users.userID=report_item.uid "
                  . "INNER JOIN items ON items.item_id=report_item.itmid");
            $statm11->execute();//select all items expect admin
            $rows=$statm11->fetchAll();
 $statm1=$con->prepare("SELECT report_review.*,Users.FullName AS FUlN,comments.User_id AS UID,comments.comment AS com,comments.c_id AS comid FROM report_review INNER JOIN Users ON Users.userID=report_review.uid "
                  . "INNER JOIN comments ON comments.c_id=report_review.revid");
            $statm1->execute();//select all items expect admin
            $rows1=$statm1->fetchAll();

            }catch(PDOException $e)
                             {
                             echo $e->getMessage();   }     
                              
                                  if(!empty($rows)){
                             
            ?>
        <h1 class="text-center">reports item messages</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>    <tr>
                               <td>#ID</td>
                               <td>Message</td>
                               <td>item Name</td>
                               <td>Sender</td>
                               <td>owner</td>
                               <td>Control</td>
                                </tr></thead>
                        <?php
                                    foreach ($rows as $row){
                                       $id=$row['owner'];
                                          $stat=$con->prepare('Select * from Users where userID=?');
                                        $stat->execute(array($id));
                                        $name=$stat->fetch();
                                        echo '<tr>';
                                            echo'<td>'.$row['ID'].'</td>';
                          echo'<td data-popup="'.$row['item_report_msg'].'" class="p_des">'.$row['item_report_msg'].'</td>';
                                             echo'<td  data-popup="'.$row['ITM'].'"  class="p_des">'.$row['ITM'].'</td>';
                                             echo'<td class="p_des">'.$row['FUlN'].'</td>';
                                              echo'<td>'.$name['FullName'].'</td>';
                            echo'<td class="p_des">  '
                                              . '<a href="Reports.php?do=deleteitem&id='.$row['ITMID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                    echo '<a href="message.php?user='.$name['userID'].'&full='.$name['FullName'].'&mail='.$name['Email'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Mail</a>';
                                                
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>

            </div>
        <hr>
         <h1 class="text-center">reports reviews messages</h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>    <tr>
                               <td>#ID</td>
                               <td>Message</td>
                               <td>Review</td>
                               <td>Sender</td>
                               <td>Reviewer</td>
                               <td>Control</td>
                                </tr></thead>
                        <?php
                                    foreach ($rows1 as $row){
                                        $id=$row['UID'];
                                          $stat=$con->prepare('Select * from Users where userID=?');
                                        $stat->execute(array($id));
                                        $name=$stat->fetch();
                                        echo '<tr>';
                                            echo'<td class="p_des">'.$row['ID'].'</td>';
                echo'<td data-popup="'.$row['rev_report_msg'].'"  class="p_des">'.$row['rev_report_msg'].'</td>';
                                             echo'<td data-popup="'.$row['com'].'" class="p_des">'.$row['com'].'</td>';
                                             echo'<td class="p_des">'.$row['FUlN'].'</td>';
                                              echo'<td class="p_des">'.$name['FullName'].'</td>';
                            echo'<td class="p_des" > '
                                              . '<a href="Reports.php?do=deletereview&id='.$row['comid'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                              
                                                    echo '<a href="message.php?user='.$name['userID'].'&full='.$name['FullName'].'&mail='.$name['Email'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Mail</a>';
                                                
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>

            </div>
        
       <?php  }else {echo '<div class="container><div class="message">there is no Categories</div></div>';}}//end if do=manage
         /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="answer"){ //manage memeber page
    $qid= isset($_GET['id'])&& is_numeric($_GET['id']) ?intval($_GET['id']):0;
    $ques=$_GET['ques'];

            //fetch data from database to put into table
            try{
   $statm11=$con->prepare("SELECT FAQAnswer.*,Users.FullName AS FUlN FROM FAQAnswer INNER JOIN Users ON Users.userID=FAQAnswer.uid WHERE Qid=?");
            $statm11->execute(array($qid));//select all items expect admin
            $rows=$statm11->fetchAll();
            }catch(PDOException $e)
                             {
                             echo $e->getMessage();   }     
                              
                                  if(!empty($rows)){
                             
            ?>
        <h1 class="text-center"><?php echo $ques; ?></h1><br>
            <div class="container">
                <div class="table-responsive">
                    <table class="datatable table table-bordered text-center">
                            <thead>    <tr>
                               <td>#ID</td>
                               <td>Answer</td>
                               <td>user Name</td>
                               <td>Add Date</td>
                               <td>Control</td>
                                </tr></thead>
                        <?php
                                    foreach ($rows as $row){
                                        echo '<tr>';
                                            echo'<td>'.$row['ID'].'</td>';
                                            echo'<td class="p_des">'.$row['answer'].'</td>';
                                             echo'<td>'.$row['FUlN'].'</td>';
                                              echo'<td>'.$row['date'].'</td>';
                            echo'<td><a href="FAQ?do=deleteanswer&qid='.$row['ID'].'" class="btn btn-danger confirm"><i class="fa fa-close"></i>Delete</a>';
                                       if($row['status']==0){
                                                    echo '<a href="FAQ.php?do=Approveanswer&qid='.$row['ID'].'" class="btn btn-info activate"><i class="fa fa-check"></i>Approve</a>';
                                                }       
                                               
                            echo '</td>';          
                            echo '</tr>';
                                    }
                        ?>
                      
                      
                    </table>
                    
                </div>

            </div>
       <?php  }else {echo '<div class="container><div class="message">there is no comments</div></div>';}}//end if do=manage

 
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
        elseif ($do==='deleteitem') {
        //delete Member
            echo '<h1 class="text-center"> Delete Item</h1>';
            echo '<div class="container">';
                    $id= isset($_GET['id'])&& is_numeric($_GET['id']) ?intval($_GET['id']):0;
                    
                   $check=  CheckItems("item_id", "items", $id);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM items WHERE item_id=:zid');
                        $statm->bindParam(":zid",$id);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
             }
               /*========================================================= 
            *
            * if do==deletecomment
            * 
    * ========================================================= 
    */
        elseif ($do==='deleteanswer') {
        //delete Member
            echo '<h1 class="text-center"> Delete answer</h1>';
            echo '<div class="container">';
                    $qid= isset($_GET['qid'])&& is_numeric($_GET['qid']) ?intval($_GET['qid']):0;
                    
                   $check=  CheckItems("ID", "FAQAnswer", $qid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM FAQAnswer WHERE ID=:zqid');
                        $statm->bindParam(":zqid",$qid);
                        $statm->execute();
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
             }
    /*========================================================= 
            *
            * if do==activate Question
            * 
    * ========================================================= 
    */
             elseif($do==='Approveques'){
                 //Aprove users in database
        echo '<h1 class="text-center"> Approve Question</h1>';
            echo '<div class="container">';
                    $qid= isset($_GET['qid'])&& is_numeric($_GET['qid']) ?intval($_GET['qid']):0;
                    
                   $check=  CheckItems("ID", "FAQ", $qid);

                    if($check>0) {
                        $statm=$con->prepare('UPDATE FAQ SET status=1 WHERE ID=?');
                        $statm->execute(array($qid));
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Activated</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';             
            
                 }
                   /*========================================================= 
            *
            * if do==activate Answer
            * 
    * ========================================================= 
    */
             elseif($do==='deletereview'){
                 //Aprove users in database
        echo '<h1 class="text-center"> Approve Question</h1>';
            echo '<div class="container">';
                    $id= isset($_GET['id'])&& is_numeric($_GET['id']) ?intval($_GET['id']):0;
                    
                   $check=  CheckItems("c_id", "comments", $id);

                    if($check>0) {
                       $statm=$con->prepare('DELETE FROM comments WHERE c_id=?');
                        
                         $statm->execute(array($id));

                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Activated</div>';
                     Redirect($msg);
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';             
            
                 }
         include $tpl.'Footer.php';
        }
        else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 