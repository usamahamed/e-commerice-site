<?php ob_start(); ?>    
<?php 
    session_start();
        $PageTitel='Statistics';
       
    if(isset($_SESSION['user'])){
         include 'inti.php';
$users=countitems("userID","Users");
$trust_user=countitemsAll("userID","Users","trustStatus=1");
$admin_user=countitemsAll("userID","Users","groupID=1");
$pending_users=countitemsAll("userID","Users","ApproveStatus=0");
$user_total=$users+$trust_user+$admin_user+$pending_users;

$c_item1=countitemsAll("item_id","items","MONTH(add_date )='01'");
$c_item2=countitemsAll("item_id","items","MONTH(add_date )='02'");
$c_item3=countitemsAll("item_id","items","MONTH(add_date )='03'");
$c_item4=countitemsAll("item_id","items","MONTH(add_date )='04'");
$c_item5=countitemsAll("item_id","items","MONTH(add_date )='05'");
$c_item6=countitemsAll("item_id","items","MONTH(add_date )='06'");
$c_item7=countitemsAll("item_id","items","MONTH(add_date )='07'");
$c_item8=countitemsAll("item_id","items","MONTH(add_date )='08'");
$c_item9=countitemsAll("item_id","items","MONTH(add_date )='09'");
$c_item10=countitemsAll("item_id","items","MONTH(add_date )='10'");
$c_item11=countitemsAll("item_id","items","MONTH(add_date )='11'");
$c_item12=countitemsAll("item_id","items","MONTH(add_date )='12'");
$iems_total=$c_item1+$c_item2+$c_item3+$c_item4+$c_item5+$c_item6+$c_item7+$c_item8+$c_item9+$c_item10+$c_item11+$c_item12;
      
$count1=0;
$data1 = array();
foreach (getcats() as $lbl1){
    $data1[$count1]=itmsincat($lbl1['ID']);
        $count1++;

}
$total1=array_sum($data1); 
        //start plot statistics
        ?>

              <div class="row">
  <div class="col-xs-4">
    <section class="bubble" style="background-color: #fd8d43;">
        <header><i class="entypo-chart-area"></i><a href="members.php">Members Numbers</a></header>
      <article>
        <ul class="sections">
            <li class="big-num"><h4><a href="members.php"><?php echo $users?></a></h4></li>
          <li class="list">
            <ul>
              <li><i class="fa fa-fw fa-clock-o"></i>Total Number of Members</li>
              <li><i class="fa fa-fw fa-calendar"></i>include Admin,Pending and Trust</li>
            </ul>
          </li>
        </ul>
        <span class="inlinebar">1,3,5,3,5,1,4,3,2,1,3,1,2,6</span>
      </article>
      <footer>
        <ul class="sections">
          <li><i class="fa fa-fw fa-clock-o"></i> 12 Days Ago</li>
          <li class="big-num"><?php echo $users?> 
            <i class="entypo-up-dir" style="color: #0f0;"></i>
          </li>
        </ul>
      </footer>
    </section>
  </div>
  <div class="col-xs-4">
    <section class="bubble" style="background-color: #32b0ed;">
        <header><i class="entypo-chart-bar"></i><a href="Categories.php">Categories numbers</a></header>
      <article>
        <ul class="sections">
            <li class="big-num"><h4><a href="Categories.php"><?php echo $total1?></a></h4></li>
          <li class="list">
            <ul>
              <li><i class="fa fa-fw fa-clock-o"></i>Total Number of Category</li>
              <li><i class="fa fa-fw fa-calendar"></i>Include All Category have Items</li>
            </ul>
          </li>
        </ul>
        <span class="inlinebar">1, 1, 1, 1, 2, 2, 3, 3, 3, 3, 4, 5, 5, 6</span>
      </article>
      <footer>
        <ul class="sections">
          <li></li>
          <li class="big-num"><?php echo $total1?> 
            <i class="entypo-up-dir" style="color: #0f0;"></i>
          </li>
        </ul>
      </footer>
    </section>
  </div>
  <div class="col-xs-4">
    <section class="bubble">
        <header><i class="entypo-chart-line"></i><a href="items.php">Items numbers</a></header>
      <article>
        <ul class="sections">
            <li class="big-num"><h4><a href="items.php"><?php echo $iems_total?></a></h4></li>
          <li class="list">
            <ul>
              <li><i class="fa fa-fw fa-clock-o"></i> Total Number of Items</li>
              <li><i class="fa fa-fw fa-calendar"></i>Except Pending items</li>
            </ul>
          </li>
        </ul>
        <span class="inlinebar">6, 2, 1, 3, 1, 2, 3, 4, 1, 5, 3, 5, 3, 1</span>
      </article>
      <footer>
        <ul class="sections">
          <li></li>
          <li class="big-num"><?php echo $iems_total?>
            <i class="entypo-up-dir" style="color: #0f0;"></i>
          </li>
        </ul>
      </footer>
    </section>
  </div>
</div>
       <?php
        //start user statistics
 //echo '<h1 class="text-center">Statistics Page</h1><br>';   
 echo '<div class="statistics_continer">'; 
 echo '<h3><i class="fa fa-pie-chart fa-lg" aria-hidden="true"></i>User Statistics</h3>';
 echo '<p class="lead usr_p">The diagrms blow show an statistics for all members in site and <br> '
 . '  counting how many members,pending,admin and trusted users</p>';
    
 
 echo '<img src="user_par_chart.php" />';
 echo '<img  src="user_pie_chart.php" />';
 ?>
<!Echo the list statistics for users!>
 <ul class="list-unstyled usr_list">
     <li><i class="fa fa-users fa-fw"></i><span>All Members Number </span>: <?php echo $users.'<br>';?></li>
                    <li><i class="fa fa-lock fa-fw"></i><span>Adminstrator: </span>: <?php echo $admin_user.'<br>';?></li>
                    <li><i class="fa fa-user fa-fw"></i><span>Trusted: </span>: <?php echo $trust_user.'<br>';?></li>
                    <li><i class="fa fa-exclamation fa-fw"></i><span>Pending: </span>: <?php echo $pending_users.'<br>';?></li>
                </ul>

 <?php
 echo '</div>';
 //end user statictics
 echo '<hr>';
 //start item statistics
 echo '<div class="statistics_continer">'; 
 echo '<h3><i class="fa fa-bar-chart fa-lg" aria-hidden="true"></i>Items Statistics</h3>';
 echo '<p class="lead itm_p">The diagrms blow show an statistics for how many Items added <br> '
 . '  per annually and month</p>';
    
 
 echo '<img src="items_statistics.php" />';
 ?>
      <ul class="list-unstyled itm_list">
                    <li><i class="fa fa-unlock-alt fa-fw"></i><span>Total Items</span>: <?php echo $iems_total.'<br>';?></li>
                    <li><i class="fa fa-envelope-o fa-fw"></i><span>Year</span>: <?php echo "2016".'<br>';?></li>
                    </ul>
 <?php
 
 echo '</div>';
 //end item statistics
 //end Category statictics
 echo '<hr>';
 //start item statistics
 echo '<div class="statistics_continer">'; 
 echo '<h3><i class="fa fa-area-chart fa-lg" aria-hidden="true"></i>Category Statistics</h3>';
 echo '<p class="lead cat_p">The diagrms blow show an statistics for how many Items added <br> '
 . '  per annually and month</p>';
    
 
 echo '<img class="img_ITM" src="Category_statistics.php" />';
 echo '<ul class="list-unstyled cat_list">';
 $count=0;
$lbl = array();
$data = array();
$class=array("fa fa-industry","fa fa-book","fa fa-themeisle","fa fa-wpbeginner","fa fa-snapchat-square","fa fa-snapchat-ghost","fa fa-university","fa fa-car");
foreach (getcats() as $lbl1){
    $lbl[$count]=$lbl1['Name'];
    $data[$count]=itmsincat($lbl1['ID']);
    echo "<li><i class='$class[$count]' aria-hidden='true'></i><span>$lbl[$count]</span>: $data[$count]</li>";
        $count++;

}
$total=array_sum($data);
echo "<li><i class='fa fa-snapchat-ghost' aria-hidden='true'></i><span>Total</span>:$total</li>";

echo '</ul>';
 echo '</div>';
 //end Category statistics
       include $tpl.'Footer.php';
    }
else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 


