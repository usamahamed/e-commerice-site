<?php 
/*========================================================= 
 *profile Page 
 * ========================================================= 
 */
ob_start();
session_start();
include 'inti.php';
$PageTitel='All Messages';
    $do=isset($_GET['do'])?$_GET['do']:'manage';

if(isset($_SESSION['useruser'])){  
     /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){
    $getUser=$con->prepare('SELECT * FROM Users WHERE username=?');
    $getUser->execute(array($sessionuser));
    $info=$getUser->fetch();
    $id=$info['userID'];
    $statm=$con->prepare("UPDATE messages SET status=1 WHERE recive_to=?");
   $statm->execute(array($id));
                
    
?>
    <div id="Mesg_container">
  <div id="account-panel">
    
      <ul class="list-unstyled" id="accounts">
      <div id="main-nav">
          <ul class="list-unstyled">
              <a href="messages.php"><li><i class="fa fa-comments"></i>Recive</li></a>
          <a href="?do=send"><li><i class="fa fa-paper-plane"></i>Sents</li></a>
         <a href="?do=deleteall"> <li><i class="fa fa-trash"></i>delete All</li></a>
        </ul>
          <ul class="list-unstyled">
              <a href="index.php"><li><i class="fa fa-sign-out"></i>Exit</li></a>
        </ul>
         
      </div>
       <li class="account">
        <div id="toggle-nav" class="toggle">
        <span class="hamburger hamburger-1"></span>
        <span class="hamburger hamburger-2"></span>
        <span class="hamburger hamburger-3"></span>
      </div>
      </li>
      <li class="account">
        <div class="circle">
            <a href="messages.php"><i class="fa fa-comments" aria-hidden="true"></i></a>
        </div>
      </li>
      <li class="account">
        <div class="circle">
            <a href="?do=deleteall"> <i class="fa fa-trash" aria-hidden="true"></i></a>
        </div>
      </li>
      <li class="account">
        <div class="circle">
            <a href="?do=send"><i class="fa fa-paper-plane"></i></a>
        </div>
      </li>
    </ul>

  </div>
  <div id="message-panel">
      <ul class="list-unstyled" id="messages">
      <?php
      $statm3=$con->prepare("SELECT * FROM messages WHERE recive_to=$id ORDER BY Date DESC");
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        
      foreach ($ALLs as $m){
          $getUser=$con->prepare('SELECT * FROM Users WHERE userID=?');
    $getUser->execute(array($m['sender_id']));
    $info=$getUser->fetch();
    if($info['img']===''){
        $im='av.png';
    }else{
       $im= $info['img'];
    }
          echo '<li>
              
        <div class="contact-info">
        <span><a href=?do=delete&mid='.$m['ID'].'><i class="fa fa-trash"></a></i></span>
        <span><a href="?do=reply&full='.$info['FullName'].'&mid='.$m['sender_id'].'"><i class="fa fa-reply"></i></a></span>
          
        </div>
        <div class="quick-message">
        
          <div class="avatar"><img class="avatar-img" src="layout/img/'.$im.'" alt=""/>
            <span class="from-account"></span>
          </div>
          <div class="message">';
          if($m['sent_from']==='usama hamed'){
            echo '<h2 class="Titel">From:<a href="profileg.php?id='.$m['sender_id'].'">'.'Admin'.'</a></h2>';
          
          }else{
            echo '<h2 class="Titel">From:<a href="profileg.php?id='.$m['sender_id'].'">'.$m['sent_from'].'</a></h2>';

          }
          $date = new DateTime($m['Date']);
          echo '<span class="pull-right date data_span">'.$date->format('g:ia l jS F').'</span>';
          echo '<span class="titl">'.$m['Title'].'</span><p class="Content">'.$m['content'].
                  '</p>';
          
                  echo  '<div class="links1"><a class="btn btn-danger " href=?do=delete&mid='.$m['ID'].'><i class="fa fa-trash" aria-hidden="true"></i>Delete</a>'.
            '<a class="btn btn-primary" href="?do=reply&full='.$info['FullName'].'&mid='.$m['sender_id'].'"><i class="fa fa-reply" aria-hidden="true"></i>Reply</a></div>';
                  echo '</div>
        </div>
        <div class="manage-message">
           <span><a href=?do=delete&mid='.$m['ID'].'><i class="fa fa-trash"></a></i></span>
        <span><a href="?do=reply&full='.$info['FullName'].'&mid='.$m['sender_id'].'"><i class="fa fa-reply"></i></a></span>
         </div>
      </li>';
          
          
          
          
      }
      
      
      
      
      ?>
    
  
    </ul>
  </div>
 <div id="content-panel">
    <h2></h2>
    <h3></h3>
    <span></span>
    <hr>
    <p></p>
    <div></div>
  </div>

</div>
<?php

        }//end do = manage
        //start delete message
        elseif ($do==='delete'){
                //delete Member
            echo '<h1 class="text-center"> Delete message</h1>';
            echo '<div class="container">';
            $mid= isset($_GET['mid'])&& is_numeric($_GET['mid']) ?intval($_GET['mid']):0;
                    
                   $check=  CheckItems("ID", "messages", $mid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM messages WHERE ID=:zID');
                        $statm->bindParam(":zID",$mid);
                        $statm->execute();
                         $mesg_track="-(User delete Message )=>";
                    UpMsgLog($mesg_track,$_SESSION['uid'], $_SESSION['log']);
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg,'back');
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
            
        }
        //END Delete Message
        
        
        else if($do==='reply'){
            //get sender name
  $id1= isset($_GET['mid'])&& is_numeric($_GET['mid']) ?intval($_GET['mid']):0;
  $getUser=$con->prepare('SELECT * FROM Users WHERE userID=?');
    $getUser->execute(array($id1));
    $info=$getUser->fetch();
    $reciver_ID=$info['userID'];
    $reciver_name=$info['FullName'];
    $reciver_Email=$info['Email'];
   
    //end get sender Name
        $Name= $_SESSION['Name'];
         $sender_id= $_SESSION['Id'];
         
        $sender_Name=$_GET['full'];
        if($_SERVER['REQUEST_METHOD']=='POST'){
  
        //get forms input
        $titel=$_POST['titel'];
        $message=$_POST['message'];
        $user=$_POST['userid'];
        $use1r=$_POST['usern1'];
        $sen_id=$_POST['send_id'];
       $reciver_ID1=$_POST['recv_id'];
       try{
$statm=$con->prepare('INSERT INTO messages (Title,content,Date,sent_from,sender_id,recive_to)'
                    . 'VALUES(?,?,now(),?,?,?)' );
  $statm->execute(array($titel,$message,$use1r,$user,$reciver_ID1));  
   $mesg_track="-(User reply message)=>";
                    UpMsgLog($mesg_track,$_SESSION['uid'], $_SESSION['log']);
   }  catch (PDOException $e){
            echo $e->getMessage();
            echo $e->getLine();
        }
  if($statm){
         echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'Message Sent</div>';
                 Redirect($msg,"back");
  echo '</div>';
  } else {
      echo 'error inserting';    
  }
        }  
        ?>
<h1 class="text-center">Send Member Message</h1>
 <div class="message_container">
 
  <div class="icon_wrapper">
    <svg class="icon" viewBox="0 0 145.192 145.192">
      <path d="M126.82,32.694c-2.804,0-5.08,2.273-5.08,5.075v2.721c-1.462,0-2.646,1.185-2.646,2.647v1.995    c0,1.585,1.286,2.873,2.874,2.873h20.577c1.462,0,2.646-1.185,2.646-2.647v-3.041c0-1.009-0.816-1.825-1.823-1.825v-2.722    c0-2.802-2.276-5.075-5.079-5.075h-1.985v-3.829c0-3.816-3.095-6.912-6.913-6.912h-0.589h-20.45c0-2.67-2.164-4.835-4.833-4.835    H56.843c-2.67,0-4.835,2.165-4.835,4.835H34.356v-3.384h-9.563v3.384v1.178h-7.061v1.416c-2.67,0.27-10.17,1.424-13.882,5.972    c-1.773,2.17-2.44,4.791-1.983,7.793c0.463,3.043,1.271,6.346,2.128,9.841c2.354,9.616,5.024,20.515,0.549,28.077    C2.647,79.44-3.125,90.589,2.201,99.547c4.123,6.935,13.701,10.44,28.5,10.44c1.186,0,2.405-0.023,3.658-0.068v9.028h-0.296    c-2.516,0-4.558,2.039-4.558,4.558v4.566h100.04v-4.564c0-2.519-2.039-4.558-4.558-4.558h-0.297V84.631h0.297    c2.519,0,4.558-2.037,4.558-4.556v-0.009c0-2.516-2.039-4.556-4.556-4.556l-36.786-0.009V61.973c0-2.193-1.777-3.971-3.972-3.971    v-4.711h0.456c1.629,0,2.952-1.32,2.952-2.949h14.227V34.459h1.658c2.672,0,4.834-2.165,4.834-4.834h20.45v3.069H126.82z     M34.06,75.511c-2.518,0-4.558,2.04-4.558,4.556v0.009c0,2.519,2.042,4.556,4.558,4.556h0.296v24.12l-0.042-1.168    c-15.994,0.574-26.122-2.523-30.106-9.229C-0.464,90.5,4.822,80.347,6.55,77.423c4.964-8.382,2.173-19.774-0.29-29.825    c-0.843-3.442-1.639-6.696-2.088-9.638c-0.354-2.35,0.129-4.3,1.484-5.958c3.029-3.714,9.509-4.805,12.076-5.1v1.233h7.061v1.49    v2.684c-2.403,1.114-4.153,2.997-4.676,5.237H18.15c-0.584,0-1.056,0.474-1.056,1.056v0.83c0,0.584,0.475,1.056,1.056,1.056h1.984    c0.561,2.18,2.304,3.999,4.658,5.092v0.029c0,0-2.282,20.823,16.479,22.099v1.102c0,1.177,0.955,2.133,2.133,2.133h3.297    c1.178,0,2.133-0.956,2.133-2.133V50.135c0-1.177-0.955-2.132-2.133-2.132h-3.297c-1.178,0-2.133,0.955-2.133,2.132    c-1.575-0.235-5.532-1.17-6.635-4.547c2.36-1.092,4.109-2.913,4.669-5.097h1.308c0.722,0,1.309-0.584,1.309-1.308v-0.578    c0-0.584-0.475-1.056-1.056-1.056h-1.539c-0.542-2.332-2.416-4.271-4.968-5.363v-2.559h17.651c0,2.67,2.166,4.835,4.836,4.835 h2.392v15.88h13.639c0,1.629,1.321,2.949,2.951,2.949h0.899v4.711c-2.194,0-3.972,1.778-3.972,3.971v13.529L34.06,75.511z     M95.188,101.78c0,8.655-7.012,15.665-15.664,15.665c-8.653,0-15.667-7.01-15.667-15.665c0-8.647,7.014-15.664,15.667-15.664    C88.177,86.116,95.188,93.132,95.188,101.78z M97.189,45.669h-9.556c0-0.896-0.726-1.62-1.619-1.62H74.494    c-0.896,0-1.621,0.727-1.621,1.62h-8.967v-11.21h33.283V45.669z"></path>
      <path d="M70.865,101.78c0,4.774,3.886,8.657,8.66,8.657c4.774,0,8.657-3.883,8.657-8.657c0-4.773-3.883-8.656-8.657-8.656    C74.751,93.124,70.865,97.006,70.865,101.78z"></path>
    </svg>
  </div>
     <form action="messages.php?do=reply"  method="POST" id="contact_form">
    <div class="name">
      <label for="name"></label>
      <input type="text" disabled="disabled" placeholder="My name is"
             name="name" id="name_input" value="<?php echo $reciver_name; ?>" autocomplete="off">
    </div>
    <div class="email">
      <label for="email"></label>
      <input type="email" disabled="disabled"  placeholder="My e-mail is" 
             name="email" id="email_input" value="<?php echo $reciver_Email; ?>" autocomplete="off">
    </div>
    <div class="titel">
      <label for="name"></label>
      <input type="text" placeholder="Titel" name="titel" id="telephone_input" required autocomplete="off">
    </div>
   
    <div class="message_cont">
      <label for="message"></label>
      <textarea name="message" placeholder="I'd like to chat about" id="message_input" cols="30" rows="5" required></textarea>
    </div>
     <input type="hidden" name="userid" value="<?php echo $id ?>"/>
     <input type="hidden" name="usern1" value="<?php echo $sender_Name ?>"/>
  <input type="hidden" name="send_id" value="<?php echo $_SESSION['Id'] ?>"/>
  <input type="hidden" name="recv_id" value="<?php echo $reciver_ID ?>"/>
    <div class="submit">
      <input type="submit" value="Send Message" id="form_button" />
    </div>
  </form><!-- // End form -->
</div><!-- // End #container -->
                
                
                
                
                <?php
            
        }
           /*========================================================= 
            *
            * if do==send
            * 
    * ========================================================= 
    */
        elseif($do=="send"){
    $getUser=$con->prepare('SELECT * FROM Users WHERE username=?');
    $getUser->execute(array($sessionuser));
    $info=$getUser->fetch();
    $id=$info['userID'];
               
    
?>
    <div id="Mesg_container">
  <div id="account-panel">
    
      <ul class="list-unstyled" id="accounts">
   <div id="main-nav">
          <ul class="list-unstyled">
              <a href="messages.php"><li><i class="fa fa-comments"></i>Recive</li></a>
          <a href="?do=send"><li><i class="fa fa-paper-plane"></i>Sents</li></a>
         <a href="?do=deleteall"> <li><i class="fa fa-trash"></i>delete All</li></a>
        </ul>
          <ul class="list-unstyled">
              <a href="index.php"><li><i class="fa fa-sign-out"></i>Exit</li></a>
        </ul>
         
      </div>
       <li class="account">
        <div id="toggle-nav" class="toggle">
        <span class="hamburger hamburger-1"></span>
        <span class="hamburger hamburger-2"></span>
        <span class="hamburger hamburger-3"></span>
      </div>
      </li>
      <li class="account">
        <div class="circle">
            <a href="messages.php"><i class="fa fa-comments" aria-hidden="true"></i></a>
        </div>
      </li>
      <li class="account">
        <div class="circle">
            <a href="?do=deleteall"> <i class="fa fa-trash" aria-hidden="true"></i></a>
        </div>
      </li>
      <li class="account">
        <div class="circle">
            <a href="?do=send"><i class="fa fa-paper-plane"></i></a>
        </div>
      </li>
    </ul>

  </div>
  <div id="message-panel">
      <ul class="list-unstyled" id="messages">
      <?php
      $statm3=$con->prepare("SELECT * FROM messages WHERE sender_id=$id ORDER BY Date DESC");
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        
      foreach ($ALLs as $m){
                 $getUser11=$con->prepare('SELECT * FROM Users WHERE userID=?');
    $getUser11->execute(array($m['recive_to']));
    $info11=$getUser11->fetch();
    if($info11['img']===''){
        $im1='av.png';
    }else{
       $im1= $info11['img'];
    }
          echo '<li>
              
        <div class="contact-info">
        <span><a href=?do=delete&mid='.$m['ID'].'><i class="fa fa-trash"></a></i></span>
        <span><a href="?do=reply&full='.$info['FullName'].'&mid='.$m['sender_id'].'"><i class="fa fa-reply"></i></a></span>
          
        </div>
        <div class="quick-message">
          <div class="avatar"><img class="avatar-img" src="layout/img/'.$im1.'" alt=""/>
            <span class="from-account"></span>
          </div>
          <div class="message">';
          $getUser=$con->prepare('SELECT * FROM Users WHERE userID=?');
            $getUser->execute(array($m['recive_to']));
         $info=$getUser->fetch();
         $full=$info['userID'];
         $id=$info['FullName'];
    
          if($id==='usama hamed'){
            echo '<h2 class="Titel">To:<a href="profileg.php?id='.$full.'">'.'Admin'.'</a></h2>';
          
          }else{
              
            echo '<h2 class="Titel">To:<a href="profileg.php?id='.$full.'">'.$id.'</a></h2>';

          }
          $date = new DateTime($m['Date']);
          echo '<span class="pull-right date data_span">'.$date->format('g:ia l jS F').'</span>';
          echo '<span class="titl">'.$m['Title'].'</span><p class="Content">'.$m['content'].
                  '</p>';
          
                  echo  '<div class="links1"><a class="btn btn-danger " href=?do=deletesent&mid='.$m['ID'].'><i class="fa fa-trash" aria-hidden="true"></i>Delete</a>'.
            '</div>';
                  echo '</div>
        </div>
        <div class="manage-message">
           <span><a href=?do=delete&mid='.$m['ID'].'><i class="fa fa-trash"></a></i></span>
        <span><a href="?do=reply&full='.$info['FullName'].'&mid='.$m['sender_id'].'"><i class="fa fa-reply"></i></a></span>
         </div>
      </li>';
          
          
          
          
      }
      
      
      
      
      ?>
    
  
    </ul>
  </div>
 <div id="content-panel">
    <h2></h2>
    <h3></h3>
    <span></span>
    <hr>
    <p></p>
    <div></div>
  </div>

</div>
<?php

        }//end do = sent
        
             //start delete all message
        elseif ($do==='deleteall'){
                //delete Member
            echo '<h1 class="text-center"> Delete ALL message</h1>';
            echo '<div class="container">';
            $mid= $_SESSION['uid'];
                    
                   $check=  CheckItems("recive_to", "messages", $mid);

                    if($check>0) {
                        $statm=$con->prepare('DELETE FROM messages WHERE recive_to=:zID');
                        $statm->bindParam(":zID",$mid);
                        $statm->execute();
                        $mesg_track="-(User delete all messages)=>";
                    UpMsgLog($mesg_track,$_SESSION['uid'], $_SESSION['log']);
                        echo '<div class="container">';
                         $msg= '<div class="alert alert-success">'.$statm->rowCount().'record Deleted</div>';
                     Redirect($msg,'back');
                     echo '</div>';
                 }else{
                   $msg= 'There is no such id';
                   Redirect($msg,'back');

                 }
            echo '</div>';
            
        }
        //END Delete Message
?>

     
<?php }else{
     header('Location:login.php');
     exit;
}
       include $tpl.'Footer.php';
       ob_end_flush();
        
