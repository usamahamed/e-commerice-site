<?php 
ob_start();
session_start();
$PageTitel='Login';
if(isset($_SESSION['useruser'])){  
        header('Location:index.php',true);exit; 
    }
include 'inti.php';
        //check if request method is post
        if($_SERVER['REQUEST_METHOD']=='POST'){

            if(isset($_POST['login'])){

            $user=$_POST['username'];
            $pass=$_POST['password'];
            $hashpass=  sha1($pass);
        //}
        //check if user inside database
        $statm=$con->prepare('SELECT userID,UserName,password FROM users WHERE UserName=? AND password=?');
        $statm->execute(array($user,$hashpass));
        $count=$statm->rowCount();
        $get_UID=$statm->fetch();
        
        if($count>0){
            $_SESSION['useruser']=$user; //register username in session array
            $_SESSION['uid']=$get_UID['userID']; //register userID in session array
            $mesg_track.="-(User Name $user successfully login in)=>";
              $_SESSION['activity'].=$mesg_track;
              $logid = uniqid();
               $_SESSION['log']=$logid;

$statm=$con->prepare('INSERT INTO logs (Server_name,Server_protocol,Request_method,log_time,activity,sessionlog,User_ID)VALUES(:z1,:z2,:z3,now(),:z5,:z55,:z6)' );
$statm->execute(array("z1"=>$_SERVER['SERVER_NAME'],"z2"=>$_SERVER['SERVER_PROTOCOL'],"z3"=>$_SERVER['REQUEST_METHOD'],"z5"=>$mesg_track,"z55"=>$logid,"z6"=>$_SESSION['uid']));
            header('Location:index.php',true);
        }
        }
        
        else
            {
            $u=filter_input(INPUT_POST, 'username');
            $p=filter_input(INPUT_POST, 'password');
            $p1=filter_input(INPUT_POST, 'password1');
            $em=filter_input(INPUT_POST, 'email');
            $err_arr=array();
            //USER NAME LESS THAN 4 CHAR CHECK
            if(isset($u)){
                $user_n=  filter_var($u,FILTER_SANITIZE_STRING);
                if(strlen($user_n)<4){ $err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>User Name must Less Than 4 Character'; }}
                
           //PASSWORD CHECK
            if(isset($p)&&isset($p1)){
                if(empty($p)){$err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>password can\'t be Empty';}
                
                if(sha1($p)!==sha1($p1)){ $err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Sorry password is not matched'; }}
              //FILTER EMAIL
                if(isset($em)){
                $email_n=  filter_var($em,FILTER_SANITIZE_EMAIL);
                if(filter_var($email_n,FILTER_VALIDATE_EMAIL)!=true){ $err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Enter Valid Email'; }}
                
                
               //Check user in database
                  if(empty($err_arr))   //after validation no error
                       {
                       //check if user exist
                       $check=  CheckItems("UserName", "users", $u);
                       if($check==1){
                           $err_arr[]='<i class="fa fa-exclamation-triangle" aria-hidden="true"></i>this user is exist';
                       }
                        else{
                      $statm=$con->prepare('INSERT INTO users (UserName,password,Email,ApproveStatus,Date)VALUES(:zuser,:zpass,:zmail,0,now())' );
                       $statm->execute(array("zuser"=>$u,"zpass"=>sha1($p),"zmail"=>$em));
                      $tatment=$con->prepare('SELECT * FROM Users WHERE UserName=?');
                      $tatment->execute(array($u));
                      $usersinfo=$tatment->fetch();
                      $uiid=$usersinfo['userID'];
                      $staatm=$con->prepare('INSERT INTO messages (Title,content,Date,sent_from,sender_id,recive_to)'
                    . 'VALUES("Welcome","Welcome to US shop store",now(),"admin","28",?)' );
                     $staatm->execute(array($uiid));
                 //    $statm1=$con->prepare('INSERT INTO notification (body,date,Email,uid)VALUES(:zbody,now(),:zuser)' );
                   //    $statm1->execute(array("zuser"=>$u,"zpass"=>sha1($p),"zmail"=>$em));
                      //echo success message
                     echo '<div class="container">';
                   $msg= 'congratulation you are now register user'.$uiid.'';
                     echo '</div>';
                       }
                       } 
                       }
                
        }  else {
            echo 'you cant show this page directily';            
}
        
           ?>

<div class="container login-pa">
     <h1 class="text-center"><span data-view="show" class="selected">login</span> | 
            <span data-view="hide" class="l-Signup">Signup</span></h1>
      <!start login form!>
      <form class="login" action="login.php" method="POST">     
         <input class="form-control input-lg" type="text" name="username" autocomplete="off" placeholder="Enter User Name" required="required"/>
            <i class="fa fa-user"></i>
       <input class="form-control input-lg" type="password" name="password" autocomplete="new-password" placeholder="Type complex Password" required="required"/>
            <i class="fa fa-key"></i>
         <input class="btn btn-primary btn-lg" name="login" type="submit" value="login"/>
    </form>
     <!start Signup form!>

     <form class="Signup" action="login.php" method="POST">
         <div class="us"><input pattern=".{4,}" title="Username must not less than 4 char" class="form-control fc-UserN input-lg" type="text" name="username" autocomplete="off" placeholder="Enter User Name" /></div>
         <div class="us"><input minlength="4" title="Password must gt 4 char" class="form-control input-lg" type="password" name="password" autocomplete="new-password" placeholder="Type your Password"/></div>
         <div class="us"><input minlength="4" title="Password must gt 4 char" class="form-control input-lg" type="password" name="password1" autocomplete="new-password" placeholder="Type your Password Again"/></div>
         <div class="us"><input class="form-control input-lg" type="email" name="email"  placeholder="Enter your Vaild email" required="required"/></div>
         <input class="btn btn-success btn-lg" name="signup" type="submit" value="signup"/>
    </form>
     <div class="e-error text-center">
         <?php
         if(!empty($err_arr)){
             foreach ($err_arr as $err){
                 echo '<div class="err">';
                 echo $err.'<br>';
                 echo '</div>';
             }
         }
         if(isset($msg)){
             echo '<div class="message sucess">'.$msg.'</div>';
         }
         ?>
     </div>
</div>
<?php include $tpl.'Footer.php'; ?>
 <?php   ob_end_flush();


