<?php 
/*========================================================= 
 *profile Page 
 * ========================================================= 
 */
ob_start();
session_start();
include 'inti.php';
$PageTitel='Profile';
if(isset($_SESSION['useruser'])){  
    $getUser=$con->prepare('SELECT * FROM Users WHERE username=?');
    $getUser->execute(array($sessionuser));
    $info=$getUser->fetch();
    $do=isset($_GET['do'])?$_GET['do']:'manage';
     if($do=="manage"){
?>
<h1 class="text-center">My Profile</h1>

<div class="info block">

    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                My Information
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><i class="fa fa-unlock-alt fa-fw"></i><span>Log Name</span>: <?php echo $info['UserName'].'<br>';?></li>
                    <li><i class="fa fa-envelope-o fa-fw"></i><span>Email</span>: <?php echo $info['Email'].'<br>';?></li>
                    <li><i class="fa fa-user fa-fw"></i><span>Full Name</span>: <?php echo $info['FullName'].'<br>';?></li>
                    <li><i class="fa fa-calendar fa-fw"></i><span>Register Date</span>: <?php echo $info['Date'].'<br>';?></li>
                    <li><i class="fa fa-tags fa-fw"></i><span>Favourite Category</span>: <?php echo $info['Date'].'<br>';?></li>
                </ul>
                <a href="profile.php?do=edit"class="btn btn-default">Edit Information</a>
             </div>
        </div>
    </div>
</div>

<div id="MY_ITEMS" class="ads block">
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Adds
                </div>
            <div class="panel-body">
                
        <?php
        if(!empty(getitms('member_id',$info['userID']))){
            echo '<div class="row">'; 
        foreach (getitms('member_id',$info['userID'],1) as $itm){
            
              echo '<figure class="Item_container_home hover1">';

            echo '<div class="image">';
                echo '<img src="layout/img/test.jpg" alt=""/><i class="ion-ios-basketball-outline"></i></div>';
                  echo '<figcaption>';
                  echo '<h3><a href="Item_details.php?itemid='.$itm['item_id'].'">'.$itm['item_name'].'</a></h3>';
                            echo '<p class="warp_content">'.$itm['description'].'</p>';
                            echo ' <div class="price">$'.$itm['price'].'</div>';
                                     echo '<fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" checked /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
  </fieldset>';
                            echo '<a href="#" class="add-to-cart">Add to Cart</a>';
                        echo '<div class="date">Added '.$itm['add_date'].'</div>';
               

                                echo '</figcaption>';
                                 echo ' </figure>';
            
            
            
            
            
            
            
            
            ///////////////////////////////////////////
          /*  echo '<div class="col-sm-6 col-md-3">';
                echo '<div class="thumbnail item-box">';
                if($itm['Approve_itm']==0){echo '<span class="Approve-item">Wating Approval..</span>';}
                  echo '<span class="price">'.$itm['price'].'</span>';
                    echo '<img class="img-responsive" src="layout/img/av.png" alt=""/>';
                        echo '<div class="caption">';
                            echo '<h3><a href="Item_details.php?itemid='.$itm['item_id'].'">'.$itm['item_name'].'</a></h3>';
                            echo '<p>'.$itm['description'].'</p>';
                             echo '<div class="date">'.$itm['add_date'].'</div>';
                        echo '</div>';  
                        
                echo '</div>';
                
                   echo '</div>';
        */}
        
        }  else {
    echo 'There is no Ads create <a href="newAds.php">New Ads</a>';
}
echo '</div>';

        ?>
            </div>
        </div>
    </div>
</div>

<div class="comns block">
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">
                Latest Comments
            </div>
            <div class="panel-body">
                <?php
                $statm11=$con->prepare("SELECT comment  FROM comments where User_id=?");
            
            $statm11->execute(array($info['userID']));//select all items expect admin
            $comments=$statm11->fetchAll();
            if(!empty($comments)){
                foreach ($comments as $com){
                    echo '<p class="lead">'.$com['comment'].'</p>';
                }
            }  else {
            echo 'There is no comments';
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php
     }
     elseif($do=="edit"){
   //   $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
        $statm=$con->prepare('SELECT * FROM Users WHERE userID=?');
        $uid=$_SESSION['uid'];
        $statm->execute(array($uid));
        $user=$statm->fetch();
        $count=$statm->rowCount();            
            if($count>0) {   
              ?>
<h1 class="text-center EDIT_PROFILE" ><?php echo 'Edit profile '.$user['FullName']?></h1>
                     <div class="container">
                         <form class="form-horizontal" enctype="multipart/form-data"  action="?do=update" method="POST">
               

                             <!Start Name!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">log name</label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="name" 
                                            class="form-control" 
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Name Of The Item"
                                            value="<?php echo $user['UserName'];?>"/>
                                 </div>
                             </div>
                          
               <!Start Description!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">E-mail</label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="email"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Description Of The Item"
                                             value="<?php echo $user['Email'];?>"/>
                                 </div>
                             </div>
            <!Start Price!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">Full Name</label>
                                 <div class="col-sm-10 col-md-4">
                                     <input type="text" name="Full"
                                            class="form-control"
                                            autocomplete="off" 
                                            required="required" 
                                            placeholder="Price Of The Item"
                                            value="<?php echo $user['FullName'];?>"/>
                                 </div>
                             </div>
          <!Start country made!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">Picture</label>
                                 <div class="col-sm-10 col-md-4">
                                     <input class="input-group" type="file" name="user_image" accept="image/*" />
                             <!Start country made!>
                             

                                 </div>
                             </div>
                          
         
       
       
      <!Start Category Feild!>
                             <div class="form-group form-group-lg">
                                 <label class="col-sm-2 control-label">Favourite Category</label>
                                 <div class="col-sm-10 col-md-4">
                                     <select name="Favourite" required="required">
                                        <?php
                                        $statm1=$con->prepare('SELECT * FROM categories');
                                        $statm1->execute();
                                        $c= $statm1->fetchAll();
                                        foreach ($c as $cats){
                                           // echo "<option value='".$cats['ID']."'>".$cats['Name']."</option>";
                                            
                                             echo "<option value='".$cats['Name']."'";
                                            
                                            echo ">"; echo $cats['Name']."</option>";
                                            
                                        }
                                        ?>
                                     </select>
                                            
                                 </div>
                             </div>
                    
       
                <!Start User submit!>
                             <div class="form-group form-group-lg">
                                 <div class="col-sm-offset-2 col-sm-10">
                                     <input type="submit" 
                                            name="btnsave"
                                            value="<?php echo lang("itm_up")?>" 
                                            class="btn btn-primary btn-sm" />
                                 </div>
                             </div>
                         </form>
                     </div>
                        <?php
     }}
    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='update'){
             if(isset($_POST['btnsave'])){
                  echo '<h1 class="text-center"> update Iteme</h1>';
            echo '<div class="container">';
                $id=$_SESSION['uid'];
                $usr_nam=filter_input(INPUT_POST, 'name');
                $email=filter_input(INPUT_POST, 'email');
                $Fullname=filter_input(INPUT_POST, 'Full');
                $Favourite=filter_input(INPUT_POST, 'Favourite');
                $imgFile = $_FILES['user_image']['name'];
		$tmp_dir = $_FILES['user_image']['tmp_name'];
		$imgSize = $_FILES['user_image']['size'];
                
                //validate the form
                $errorarray=array();
                if(empty($usr_nam)){$errorarray[]="Name must be <strong>not Empty</strong>";}
                if(empty($email)){$errorarray[]="Description must be <strong>not Empty</strong>";}
                if(empty($Fullname)){$errorarray[]="Price must be <strong>not Empty</strong>";}
                if(empty($Favourite)){$errorarray[]="country must be <strong>not Empty</strong>";}
                
                   foreach ($errorarray as $err){
                       echo '<div class=\"alert alert-danger\">'.$err.'</div><br>';
                   }
                //updata database
                   if(empty($errorarray))   //after validation no error
                       {
                       //make image uploading
                        $statm8=$con->prepare('SELECT * FROM Users WHERE userID=?');
                         $uid=$_SESSION['uid'];
                         $statm8->execute(array($uid));
                          $userimg=$statm8->fetch(PDO::FETCH_ASSOC);
                          extract($userimg);
                          $oldimg=$userimg['img'];
                          	if($imgFile)
		{
			$upload_dir = 'layout/Img/'; // upload directory	
			$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
			$valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
			$userpic = rand(1000,1000000).".".$imgExt;
			if(in_array($imgExt, $valid_extensions))
			{			
				if($imgSize < 5000000)
				{
					unlink($upload_dir.$userimg['img']);
					move_uploaded_file($tmp_dir,$upload_dir.$userpic);
				}
				else
				{
					$errMSG = "Sorry, your file is too large it should be less then 5MB";
				}
			}
			else
			{
				$errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";		
			}	
		}
		else
		{
			// if no image selected the old image remain as it is.
			$userpic = $oldimg ;// old image from database
		}
                       
                       
                       
                       
                       //end image uploading update
                       
                    if(!isset($errMSG))
		{   
                        try{
                $statm=$con->prepare("UPDATE Users SET UserName=?,Email=?,FullName=?,img=? WHERE userID=?");
                $statm->execute(array($usr_nam,$email,$Fullname,$userpic,$uid));
                        }catch(PDOException $e){
                        }
                //echo success message
                echo '<div class="container">';
                   $msg= '<div class="alert alert-success">'.$statm->rowCount().'record update</div>'.'user picture is'.$userpic;
                   echo 'user picture is'.$userpic;
                     Redirect($msg,"back",10);
                     echo '</div>';
                }
                       }
            }//end if not post 
            else{
                     echo '<div class="container">';
                $msg='<div class="alert alert-danger">you cant Browes this page directly</div>';
                 Redirect($msg,null);
                      echo '</div>';

        }
                    echo '</div>';
        }    
     
?>
    
<?php }else{
     header('Location:login.php');
     exit;
}
       include $tpl.'Footer.php';
       ob_end_flush();
        
