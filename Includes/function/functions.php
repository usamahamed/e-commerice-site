<?php
/* 
 * function name-------->genral_feth()
 * function Version----->.01
 * function role----->get All from database
 * Start function
 */
function genral_feth($feild,$table,$where=null,$and=null,$orderfeild=null,$ordring="DESC"){
    
        global $con;
        $statm3=$con->prepare("SELECT $feild FROM $table $where $and ORDER BY $orderfeild $ordring");
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        return $ALLs;

}
/*End genral_feth Function*/
/* 
 * function name-------->getAll()
 * function Version----->.01
 * function role----->get All from database
 * Start function
 */
function getAll($table,$order=null){
    
        global $con;
        $statm3=$con->prepare("SELECT * FROM $table  ORDER BY $order DESC");
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        return $ALLs;

}
function getAllitems($table,$where,$order=null){
    
        global $con;
        $statm3=$con->prepare("SELECT * FROM $table WHERE $where  ORDER BY $order DESC");
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        return $ALLs;

}
/*End getLatest Function*/
/* 
 * function name-------->searchitems()
 * function Version----->.01
 * function role----->get All from database
 * Start function
 */
function searchitems($item,$cat){
    
        global $con;
       $statm3=$con->prepare('SELECT items.* ,categories.Name AS CAT_NM,Users.UserName AS US_NM FROM items '
                . 'INNER JOIN categories ON categories.ID=items.category_id '
               . 'INNER JOIN Users ON  Users.userID=items.member_id '
               . 'WHERE items.item_name LIKE "%'.$item.'%" AND categories.Name LIKE "%'.$cat.'%"');
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        return $ALLs;

}
/*End getLatest Function*/
/* 
 * function name-------->getmessage()
 * function Version----->.01
 * function role----->get All from database
 * Start function
 */
function getmessage($table,$where,$order=null){
    
        global $con;
        $statm3=$con->prepare("SELECT * FROM $table WHERE $where ORDER BY $order DESC");
        $statm3->execute();
        $ALLs=$statm3->fetchAll();
        return $ALLs;

}
/*End getLatest Function*/
/* 
 * function name-------->getcats()
 * function Version----->.01
 * function role----->get categories from database
 * Start function
 */
function getcats(){
        global $con;
$statm3=$con->prepare("SELECT * FROM categories ORDER BY ID ASC");
        $statm3->execute();
$cats=$statm3->fetchAll();
return $cats;

}
/*End getLatest Function*/
/* 
 * function name-------->getitms()
 * function Version----->.01
 * function role----->get Items from database
 * Start function
 */
function getitms($where,$val,$appreove=null){
    if($appreove==null){
        $sql='AND Approve_itm=1';
    }else{ $sql=null; }
        global $con;
$statm3=$con->prepare("SELECT * FROM items WHERE $where=? $sql ORDER BY item_id DESC");
        $statm3->execute(array($val));
$itms=$statm3->fetchAll();
return $itms;

}
/*End getLatest Function*/
/* 
 * function name-------->check_user()
 * function Version----->.01
 * function role----->chech user Activation in database
 * Start function
 */
function check_user($user){
    global $con;
    $statm1=$con->prepare('SELECT UserName,ApproveStatus FROM users WHERE UserName=? AND ApproveStatus=0');
        $statm1->execute(array($user));
        $stat=$statm1->rowCount();
        return $stat;
}
/*End getLatest Function*/



/* 
 * function name-------->getTitel()
 * function Version----->.01
 * function role----->Print the titel of each page depend on specfic variable 
 * Start function
 */
function getTitel(){
    global $PageTitel;
    if(isset($PageTitel)){
        echo $PageTitel;
    }
    else {
        echo "Default";
    }
}
/*End getTitel Function*/
/* 
 * function name-------->Redirect()
 * function Version----->.02
 * function role----->Redirest to home page after specific time and produce error message
 * Start function
 */
function Redirect($mesg,$url=null,$second=3){
     if($url===null){
         $url='index.php';
         $link='Home Page';
     }else{
         if(isset($_SERVER['HTTP_REFERER'])&&$_SERVER['HTTP_REFERER']!==''){
                  $url=$_SERVER['HTTP_REFERER'];
                  $link='Prevouis Page';

         }else{
                      $url='index.php';
                      $link='Home Page';


         }
     }
   echo $mesg;
  echo "<div class='alert alert-info'>you will be redirectd to $link page after$second</div>";
  //echo '<script>window.location=index.php</script>'; 
 // header("Location:index.php",TRUE);
  header("refresh:$second;url=$url",true);
  
  exit;
  
}
/*End RedirectFunction Function*/

/* 
 * function name-------->CheckItems()
 * function Version----->.01
 * function role----->check existance in database
 * Start function
 */
function CheckItems($select,$from,$value){
    global $con;
    $statment=$con->prepare("SELECT $select FROM $from WHERE $select=?");
    $statment->execute(array($value));
    $count=$statment->rowCount();
    return $count;
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitems()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitems($item,$table){
    global $con;
    $statm2=$con->prepare("SELECT COUNT($item) FROM $table");
        $statm2->execute();
        return  $statm2->fetchColumn();
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitems()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitems1($rate,$cid){
    global $con;
    $s1=$con->prepare("SELECT ID FROM rating WHERE Rating=$rate AND r_review=$cid");
                $s1->execute();
        return  $s1->rowCount();
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitems()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitemswhere($item,$table ,$ID){
    global $con;
    $statm2=$con->prepare("SELECT COUNT($item) FROM $table WHERE recive_to=$ID AND status=0");
        $statm2->execute();
        return  $statm2->fetchColumn();
}
/*End RedirectFunction Function*/
/* 
 * function name-------->countitems()
 * function Version----->.01
 * function role----->count number of rows
 * item=the item to count      table=the table to select from
 * Start function
 */
function countitemswhere1($item,$table ,$ID,$and=null){
    global $con;
    $statm2=$con->prepare("SELECT COUNT($item) FROM $table WHERE uid=$ID $and");
        $statm2->execute();
        return  $statm2->fetchColumn();
}
/*End RedirectFunction Function*/
/*End RedirectFunction Function*/
/* 
 * function name-------->getLatest()
 * function Version----->.01
 * function role----->get latest items from database[users,items,comments]
 * Start function
 */
function getlatest($select,$table,$order,$limt=5){
        global $con;
$statm3=$con->prepare("SELECT $select FROM $table ORDER BY $order DESC LIMIT $limt");
        $statm3->execute();
$rows=$statm3->fetchAll();
return $rows;

}
/*End getLatest Function*/

/* 
 * function name-------->UpMsgLog()
 * function Version----->.01
 * function role----->Update user activity log
 * Start function
 */
function UpMsgLog($msg,$uid,$session){
        global $con;
$statm=$con->prepare("UPDATE logs SET activity=CONCAT(activity,?) WHERE User_ID=$uid AND sessionlog=?");
 $statm->execute(array($msg,$session));
               

}
/*End getLatest Function*/
/* 
 * function name-------->CheckItems()
 * function Version----->.01
 * function role----->check existance in database
 * Start function
 */
function gCheckItems($select,$from,$where){
    global $con;
    $statment=$con->prepare("SELECT $select FROM $from WHERE $where");
    $statment->execute();
    $row=$statment->fetch();

    return $row["$select"];
}
/*End RedirectFunction Function*/
