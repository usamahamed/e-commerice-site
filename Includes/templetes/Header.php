<html>
    <head>
        
        <meta charset="UTF-8">
    <link rel="stylesheet" href="<?php echo $css;?>bootstrap.css">
        <link rel="stylesheet" href="<?php echo $css;?>font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo $css;?>jquery-ui.css">
            <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.0-beta.19/angular.js"></script>

        <link rel="stylesheet" href="<?php echo $css;?>jquery.selectBoxIt.css">
        <link rel="stylesheet" href="<?php echo $css;?>rrssb.css" />

        <link rel="stylesheet" href="<?php echo $css;?>frontend.css">
        <title><?php getTitel()?></title>
    </head>
    <body>
         <!Begin search form!>
 <?php                     if(isset($_SESSION['useruser'])){ 
 $style="style='margin:94px 13px 0px 306px'";
 }
                      if(!isset($_SESSION['useruser'])){ 
     $style1="style='margin: 44px 347px 44px 347px; position: absolute;'";
 }
?>
    <div id="DIV_3" <?php echo $style;?>>
  <div id="DIV_4"></div>
  <form accept-charset="utf-8" action="search.php" class="nav-searchbar " method="GET" name="site-search" role="search" id="FORM_5">
    
    <div class="nav-left" id="DIV_6">
      <div class="nav-search-scope" id="DIV_7">
        
<div class="nav-search-facade"  id="DIV_8">
  <span class="nav-search-label" id="SPAN_9">All</span>
  <i class="nav-icon" id="I_10"></i>
</div>

        
<select class="nav-search-dropdown searchSelect"  data-nav-selected="0" data-nav-tabindex="10" id="SELECT_11" name="url" title="Search in" tabindex="1">
<option selected="selected" value="all" id="OPTION_12">All Departments</option>
<option value="appliances" id="OPTION_14">Appliances</option>
<option value="mobile-apps" id="OPTION_15">Apps &amp; Games</option>
<option value="arts-crafts" id="OPTION_16">Arts, Crafts &amp; Sewing</option>
<option value="automotive" id="OPTION_17">Automotive</option>
<option value="baby-products" id="OPTION_18">Baby</option>
<option value="beauty" id="OPTION_19">Beauty</option>
<option value="stripbooks" id="OPTION_20">Books</option>
<option value="popular" id="OPTION_21">CDs &amp; Vinyl</option>
<option value="mobile" id="OPTION_22">Cell Phones &amp; Accessories</option>
<option value="fashion" id="OPTION_23">Clothing, Shoes &amp; Jewelry</option>
<option value="fashion-womens" id="OPTION_24">&nbsp;&nbsp;&nbsp;Women</option>
<option value="fashion-mens" id="OPTION_25">&nbsp;&nbsp;&nbsp;Men</option>
<option value="fashion-girls" id="OPTION_26">&nbsp;&nbsp;&nbsp;Girls</option>
<option value="fashion-boys" id="OPTION_27">&nbsp;&nbsp;&nbsp;Boys</option>
<option value="fashion-baby" id="OPTION_28">&nbsp;&nbsp;&nbsp;Baby</option>
<option value="collectibles" id="OPTION_29">Collectibles &amp; Fine Art</option>
<option value="computers" id="OPTION_30">Computers</option>
<option value="financial" id="OPTION_31">Credit and Payment Cards</option>
<option value="digital-music" id="OPTION_32">Digital Music</option>
<option value="electronics" id="OPTION_33">Electronics</option>
<option value="gift-cards" id="OPTION_34">Gift Cards</option>
<option value="grocery" id="OPTION_35">Grocery &amp; Gourmet Food</option>
<option value="hpc" id="OPTION_36">Health &amp; Personal Care</option>
<option value="garden" id="OPTION_37">Home &amp; Kitchen</option>
<option value="industrial" id="OPTION_38">Industrial &amp; Scientific</option>
<option value="digital-text" id="OPTION_39">Kindle Store</option>
<option value="fashion-luggage" id="OPTION_40">Luggage &amp; Travel Gear</option>
<option value="magazines" id="OPTION_41">Magazine Subscriptions</option>
<option value="movies-tv" id="OPTION_42">Movies &amp; TV</option>
<option value="mi" id="OPTION_43">Musical Instruments</option>
<option value="office-products" id="OPTION_44">Office Products</option>
<option value="lawngarden" id="OPTION_45">Patio, Lawn &amp; Garden</option>
<option value="pets" id="OPTION_46">Pet Supplies</option>
<option value="pantry" id="OPTION_47">Prime Pantry</option>
<option value="software" id="OPTION_48">Software</option>
<option value="sporting" id="OPTION_49">Sports &amp; Outdoors</option>
<option value="tools" id="OPTION_50">Tools &amp; Home Improvement</option>
<option value="toys-and-games" id="OPTION_51">Toys &amp; Games</option>
<option value="videogames" id="OPTION_52">Video Games</option>
<option value="wine" id="OPTION_53">Wine</option>
</select>


      </div>
    </div>
    <div class="nav-right" id="DIV_54">
      <div class="nav-search-submit" id="DIV_55">

        <span class="nav-search-submit-text" id="SPAN_56">Go</span>
        <input type="submit" class="nav-input nav-input_class" value="Go" data-nav-tabindex="12" tabindex="1" id="INPUT_57">
      </div>
    </div>
    <div class="nav-fill" id="DIV_58">
      <div class="nav-search-field" id="DIV_59">
          <input type="text"  value="" name="field-keywords" placeholder="Search" autocomplete="off" class="nav-input nav-input11" data-nav-tabindex="11" tabindex="1">
      </div>
    </div>
    <div id="DIV_61"></div>
  </form>
</div>

   <!End search form!>
        <section class="flat-hide">
	<header>
		<nav class="rad-navigation">
                    
			<div class="rad-logo-container rad-nav-min">
                            <a href="#" class="rad-logo pull-left"><i class=" fa fa-recycle"></i>US-Shop</a>
			    <a href="#" class="rad-toggle-btn1 pull-right"><i class="fa fa-bars"></i></a>
			</div>
                    <a href="#" class="rad-logo-hidden">Radar</a>
			<div class="rad-top-nav-container">
				<a href="" class="brand-icon"><i class="fa fa-recycle"></i></a>
                                
                            
                                
                                
				
			</div>
		</nav>
	</header>
</section>
<aside class="flat-hide">
	<nav  class="rad-sidebar rad-nav-min">
            <ul class="list-unstyled">
                <li class="classshow1">
				<a href="dashboard.php" class="inbox">
					<i class="fa fa-dashboard"><span class="icon-bg rad-bg-success"></span></i>
					<span class="rad-sidebar-item">Dashboard</span>
				</a>
			</li>
			<li>
				<a href="Categories.php">
					<i class="fa fa-object-group">
						<span class="icon-bg rad-bg-danger"></span>
					</i>
					<span class="rad-sidebar-item">Categories</span>
				</a>
			</li>
			<li><a href="items.php" class="snooz"><i class="fa fa-shopping-basket"><span class="icon-bg rad-bg-primary"></span></i><span class="rad-sidebar-item">Items</span></a></li>
			<li><a href="members.php" class="done"><i class="fa fa-users"><span class="icon-bg rad-bg-warning"></span></i><span class="rad-sidebar-item">Members</span></a></li>
			<li><a href="mails.php" class="done"><i class="fa fa-mail-forward"><span class="icon-bg rad-bg-warning1"></span></i><span class="rad-sidebar-item">mails</span></a></li>
			<li>
				<a href="statistics.php">
					<i class="fa fa-bar-chart-o"><span class="icon-bg rad-bg-violet"></span></i>
					<span class="rad-sidebar-item">Statistics</span>
				</a>
			</li>
                        <li>
				<a href="comments.php">
					<i class="fa fa-comment-o"><span class="icon-bg rad-bg-c"></span></i>
					<span class="rad-sidebar-item">Comments</span>
				</a>
			</li>

		<li>
				<a href="logs.php">
					<i class="fa fa-life-ring"><span class="icon-bg rad-bg-l"></span></i>
					<span class="rad-sidebar-item">Logs</span>
				</a>
			</li>
		<li>
				<a href="statistics.php">
					<i class="fa fa-scissors"><span class="icon-bg rad-bg-s"></span></i>
					<span class="rad-sidebar-item">Settings</span>
				</a>
			</li>
		
<li>
				<a href="mails.php">
					<i class="fa fa-mail-reply"><span class="icon-bg rad-bg-ml"></span></i>
					<span class="rad-sidebar-item">Mails</span>
				</a>
			</li>
		
<li>
				<a href="FAQ.php">
					<i class="fa fa-question"><span class="icon-bg rad-bg-qus"></span></i>
					<span class="rad-sidebar-item">FAQ</span>
				</a>
			</li>
		
<li>
				<a href="Reports.php">
					<i class="fa fa-dribbble"><span class="icon-bg rad-bg-rp"></span></i>
					<span class="rad-sidebar-item">Reports</span>
				</a>
			</li>
		
	<li>
				<a href="members.php?do=edit&userid=15">
					<i class="fa fa-edit"><span class="icon-bg rad-bg-a"></span></i>
					<span class="rad-sidebar-item">Edit Admin</span>
				</a>
			</li>
		
	<li>
				<a href="logout.php">
					<i class="fa fa-sign-out"><span class="icon-bg rad-bg-lo"></span></i>
					<span class="rad-sidebar-item">Logout</span>
				</a>
			</li>
		
		</ul>
	</nav>
</aside>
        <div class="outer-bar">
            <div class="container">
                   <!img class="img-thumbnail logo-png-img" src="layout/img/logo.png" alt="" />

                 <?php 
                    
                    if(isset($_SESSION['useruser'])){ 
                   //     include '../Includes/function/functions.php';
                      //  include '../Admin/connectdb.php';
                        ?>
                        
                <!Start setting Theme!>
    <ul class= "links">
					
					<li class="rad-dropdown"><a class="rad-menu-item rad-menu-item1" href="#"></a>
						<ul class="rad-dropmenu-item rad-settings">
							<li class="rad-dropmenu-header"><a href="#">Settings</a></li>
							<li class="rad-notification-item text-left">
								<div class="pull-left"><i class="fa fa-link"></i></div>
								<div class="pull-right">
									<label class="rad-chk-pin pull-right">
										<input type="checkbox" /><span></span></label>
								</div>
								<div class="rad-notification-body">
									<div class="lg-text">Flat Theme</div>
									<div class="sm-text">Flattify it</div>
								</div>
							</li>
							<li id="rad-color-opts" class="rad-notification-item text-left hide">
								<div class="pull-left"><i class="fa fa-puzzle-piece"></i></div>
								<div class="pull-right">
									<div class="rad-color-swatch">
										<label class="colors rad-bg-crimson rad-option-selected">
											<input type="radio" checked name="color" value="crimson" />
										</label>
										<label class="colors rad-bg-teal">
											<input type="radio" name="color" value="teal" />
										</label>
										<label class="colors rad-bg-purple">
											<input type="radio" name="color" value="purple">
										</label>
										<label class="colors rad-bg-orange">
											<input type="radio" name="color" value="orange" />
										</label>
										<label class="colors rad-bg-twitter">
											<input type="radio" name="color" value="twitter" />
										</label>
									</div>
								</div>
								<div class="rad-notification-body">
									<div class="lg-text">color</div>
									<div class="sm-text">Make it colorful</div>
								</div>
							</li>
       
						</ul>
					</li>
               
				</ul>
                
    <!END setting Theme!>
    
      <!Start setting Theme!>
    <ul class= "links1">
					
					<li class="rad-dropdown1"><a class="rad-menu-item2" href="#"></a>
						<ul class="rad-dropmenu-item rad-settings1 list-unstyled">
							<li class="rad-dropmenu-header1"><a href="#">Notifications</a></li>
							<li class="rad-notification-item text-left">
								
								<div class="rad-notification-body rad-notification-body11">
                                                                    <div class="lg-text1">
                                                                        
                                                                        <i class="fa fa-bell"></i> Check New 10 Items added
                                                                        <span class="closenotification pull-right">x</span>
                                                                    </div>
                                                                     <div class="lg-text1">
                                                                        
                                                                       <i class="fa fa-bell"></i> message sent sucessfuly 
                                                                         <span class="closenotification pull-right">x</span>
                                                                    </div>
								</div>
							</li>
						
       
						</ul>
					</li>
               
				</ul>
                
    <!END setting Theme!>
    
    
                
 
                
                <ul class="pull-left links">
                    
<li class="rad-dropdown"><a class="rad-menu-item" href="#">
      <?php
      $uid=$_SESSION['uid'];
      $img=gCheckItems('img','Users',"userID=$uid");
      if($img===null){
          $img='av.png';
      }
    //  echo '<img class="img-thumbnail img-circle" src="layout/img/{$img}" alt=""/>';
      ?>
     <img class="img-thumbnail img-circle" src="layout/img/<?php echo $img ?>" alt=""/>
     
                        <div class="btn-group btn-default my-profile text-right">
                            
                    <span class="btn dropdown-toggle" data-toggle="dropdown">
                       Welcome <?php echo $sessionuser ?>
                       <i class="fa fa-caret-down" aria-hidden="true"></i></span> </div>   
                       
                       
    </a>
    
   
						<ul class="rad-dropmenu-item">
                                                    
							<li class="rad-dropmenu-header"><a href="#">Your Option</a></li>
							<li class="rad-notification-item text-left">
								<a class="rad-notification-content" href="#">
									<div class="pull-left"><i class="fa fa-html5 fa-2x"></i>
									</div>
									<div class="rad-notification-body">
                                                                            <a href="Profile.php">
										<div class="lg-text">My profile</div>
										<div class="sm-text">your profile page</div>
                                                                            </a>
									</div>
								</a>
							</li>
							<li class="rad-notification-item text-left">
								<a class="rad-notification-content" href="#">
									<div class="pull-left"><i class="fa fa-bitbucket fa-2x"></i>
									</div>
									<div class="rad-notification-body">
                                                                            <a href="newAds.php">
										<div class="lg-text">New Ads</div>
                                                                                <div class="sm-text">Add new Advertise</div></a>
									</div>
								</a>
							</li>
							<li class="rad-notification-item text-left">
								<a class="rad-notification-content" href="#">
									<div class="pull-left"><i class="fa fa-google fa-2x"></i>
									</div>
									<div class="rad-notification-body">
                                                                            <a href="Profile.php#MY_ITEMS">
										<div class="lg-text">Show Adds</div>
										<div class="sm-text">Show my Advertise</div>
                                                                            </a>
									</div>
								</a>
							</li>
                                                        <li class="rad-notification-item text-left">
                                                    		<a class="rad-notification-content" href="#">
									<div class="pull-left"><i class="fa fa-lock fa-2x"></i>
									</div>
									<div class="rad-notification-body">
                                                                            <a href="logout.php">
										<div class="lg-text">Logout</div>
										<div class="sm-text">Log out site</div>
                                                                            </a>
									</div>
								</a>
							</li>
							<li class="rad-dropmenu-footer"><a href="#">See all notifications</a></li>
						</ul>
					</li>
					
				</ul>
   
  
  
                <?php
       
      
                  $getUser=$con->prepare('SELECT * FROM Users WHERE username=?');
    $getUser->execute(array($sessionuser));
    $info=$getUser->fetch();
    $id=$info['userID'];
                
                ?>      
          
                
     <ul class="navbar list_icon">
         <span class="rad-menu-badge bt-danger pull-right"><?php echo countitemswhere("ID","messages",$id);?></span>
   <a href="cart.php?do=showcart" class="cartlink"><span  class="rad-menu-badge1 bt-danger1 pull-right"><?php echo countitemswhere1("ID","cart",$id,"AND Quantity!=0");?></span></a>
   <a href="cart.php?do=showwishlist" class="cartlink"><span class="rad-menu-badge2 bt-danger2 pull-right"><?php echo countitemswhere1("ID","cart",$id,"AND wishlist=1");?></span></a>
    <li class="message_li active"><a href="messages.php?uid='<?php echo $id?>'" class="fa fa-envelope"> </a></li>
    <li><a href="#" class="fa fa-cart-plus"> 
        </a>
        <div class="cart1"> 
                <div id="cart">
                       <div id="cart">
                    <?php
                    $statment=$con->prepare("SELECT cart.*,items.item_name AS name,items.item_id AS ITMID,items.image AS IMG,items.description AS des"
                    . ",items.price AS pri"
                    . " FROM cart"
                    . " INNER JOIN items ON items.item_id=cart.itmid"
                    . " WHERE uid=? AND Quantity!=0");
             $statment->execute(array($_SESSION['uid']));//select all items expect admin
            $itms=$statment->fetchAll();
                     if(!empty($itms)){
                         foreach ($itms as $item){
               $imagarray=explode(",", $item['IMG']);
                    ?>
    	  <div class="cart-item">
              <div class="img-wrap">
                  <img src="layout/Img/items/<?php echo $imagarray[0] ?>" alt="">
              </div><span><?php echo $item['name'] ?></span>
              <strong>$<?php echo $item['pri'] ?></strong><div class="cart-item-border">
                  
              </div><div class="delete-item"></div>
              <input type="hidden" class="itmid" name="itmid" value="<?php echo $item['ITMID']?>"/>
         <input type="hidden" class="userid" name="itmid" value="<?php echo $_SESSION['uid']?>"/>
                  
          </div>
                     <?php }}
                     else{
    echo '<span class="empty">No items in cart.
        </span>';
                     }
                     ?>
    	
        <div class="showwallitemscart"><div class="showwallitemscartclick"><i class="fa fa-shopping-basket"></i>Show All</div></div></div>
                </div></div></li>
          <li><a href="#" class="fa fa-heart heart"></a></li>
  <li><a href="#" class="fa fa-bell-o"></a></li>
  <li><a href="#" class="fa fa-cog"></a></li>
</ul>
    
                
       
                
                
                
                
                
                
               
                </div>
                        
                       <?php 
                       
                       
                        }  else{
                            echo '<a style="color: transparent;" href="login.php"><span class="pull-right">Login/signup</span></a>';
                        }        
                 ?>
               
            </div>
        </div>

                   
<nav id="ddmenu">
    <div class="menu-icon"></div>
    <a style="color: transparent;"  href="login.php"><span class="pull-right Loginspan">Login/signup</span></a><br>

    <ul class="pull-left">
        <li class="pull-left"><a href="index.php"><span class="top-heading"> <img class="logodesign" src="layout/Img/logo1.png" alt=""/></span></a></li>
                         <?php   if(!isset($_SESSION['useruser'])){ ?>

              <li class="pull-right"><a href="login.php"><span class="top-heading"><i class="fa fa-legal fa-2x" aria-hidden="true"></i> Login/signup</span></a></li>
                         <?php } ?>
              <a href="login.php"><img <?php echo $style1;?> class="pull-right" src="layout/Img/b-multipack-swms._CB279840584_.png"></a><

  
           <!li class='lamp'><!span><!span><!li>
           
    </ul>
   
</nav>
  <nav id="ddmenu1">   
<ul class="pull-left Submeanue list-unstyled">
    <?php $val='login'; $ref='login.php';
    if(isset($_SESSION['useruser'])){
        $val='Logout'; 
        $ref='logout.php';?>
                <li class="pull-right"><a href="<?php echo $ref; ?>"><span class="Submeanue1"><i class="fa fa-history" aria-hidden="true"></i><?php echo $val; ?></span></a></li>
    <?php } ?>
         <li class="pull-right"><a href="login.php"><span class="Submeanue1"><i class="fa fa-shopping-bag" aria-hidden="true"></i>Help </span></a></li>
        <li class="pull-right"><a href="login.php"><span class="Submeanue1"><i class="fa fa-delicious" aria-hidden="true"></i> Today's Deals</span></a></li>
        <li class="pull-right"><a href="login.php"><span class="Submeanue1"><i class="fa fa-credit-card" aria-hidden="true"></i> Cards</span></a></li>
        <li class="pull-right"><a href="login.php"><span class="Submeanue1"><i class="fa fa-dollar" aria-hidden="true"></i> Sell</span></a></li>
        <li class="pull-right"><a href="login.php"><span class="Submeanue1"><i class="fa fa-question-circle-o" aria-hidden="true"></i> <?php  if(isset($_SESSION['useruser'])){echo $_SESSION['useruser'];}else{    echo 'account'; } ?>'s US-shop</span></a></li>

                <li class="fa-outdent1 pull-right"><a href="login.php"><span class="Submeanue1"><i class="fa fa-outdent" aria-hidden="true"></i>  Browsing History</span></a>
                
                    <div style="display: none; position: absolute" class="showhistoryitems">
                        

<?php if(isset($_SESSION['useruser'])){?>



<!History Items!>

<div class="container item-cont">
        <div class="container">
  <div class="row">
    <div class="col-md-10">
        
      <div class="carousel slide multi-item-carousel" id="theCarousel1">
        <div class="carousel-inner">
         
          
          
       


        <?php
        $count=0;
        $count1=-1;
        $active="active";
        $uid=$_SESSION['uid'];
        $statm11=$con->prepare("SELECT * FROM itemhistory WHERE uid=$uid");
         $statm11->execute();
          $history=$statm11->fetch();
          $historyarrayid=explode(",", $history['itemlogs']);
        
 
        foreach (getAll('items','item_id') as $itm){
            if(in_array($itm['item_id'],$historyarrayid)){
            $imagarray=explode(",", $itm['image']);
            if($imagarray[0]===''){
                $imagarray[0]='test.jpg';
            }
            
                        echo ' <div class="item'.' '.$active.'"> <div class="col-xs-4"><figure data-class="'.$count.'" class="Item_container_home hover1">';
            
            echo '<div class="image">';
                echo '<img src="layout/img/items/'.$imagarray[0].'" alt=""/><i class="ion-ios-basketball-outline"></i></div>';
            echo '<span><span>';
                  echo '<figcaption>';
                  echo '<h3 class="warp_content"><a class="item_a_name" href="Item_details.php?itemid='.$itm['item_id'].'">'.$itm['item_name'].'</a></h3>';
                            echo '<p class="warp_content">'.$itm['description'].'</p>';
                            echo ' <div class="price price_price"><span class="lable-price">Price:</span>$'.$itm['price'].'                  <i class="prime a-icon a-icon-prime" data-bind="visible: isPrime, css: prime.auiIconClass" title="Prime" style="display: inline-block;"></i>
</div>';
                                                            echo '<fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" checked /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
  </fieldset><br>';
                                                            
                                    
                                                            
                     // echo '<a href="?do=quick&itemid='.$itm['item_id'].'" class="add-to-cart">Quick View</a>';
                 //    echo '<a href="#" class="add-to-cart">Quick View</a>';
      echo '<button data-class="'.$count.'" type="button" class=" add-to-cart btn btn-default Quick_view" data-toggle="modal" data-target="#myModal">
Quick view</button>';                                            
                            $count++;
                            $active=null;
     echo '<a class=" item_a_name1 btn btn-default" href="Item_details.php?itemid='.$itm['item_id'].'">See product detail</a>';
                        echo '<div class="date">Added '.$itm['add_date'].'</div>';
                        if(isset($_SESSION['useruser'])){  
                            echo '<a><i class="fa fa-heart fa-heart1"></i></a>';
                      echo '<input type="hidden" name="itemid" class="itemid" value="'.$itm['item_id'].'"/>';
                      echo '<input type="hidden" name="uid" class="uid" value="'. $_SESSION['uid'].'"/>';
                        }
                                echo '</figcaption>';
                                 echo ' </figure></div></div>';
                
        }
        
                        }
          
                       
        ?>

     </div>
     </div>
        <a class="left carousel-control" href="#theCarousel1" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
        <a class="right carousel-control" href="#theCarousel1" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
      </div>
       <div class="col-md-2">
          <div class="advr">
              <a class="paypalad" href="http://optimized-by.rubiconproject.com/t/9456/44042/311008-15.3837914.4104036?url=https%3A%2F%2Fwww.paypal.com" target="_blank"><img src="http://assets.rubiconproject.com/campaigns/9456/37/35/92/1438267550campaign_file_mhobat.jpg" border="0" alt=""></a> 
         <a class="paypalad1" href="http://optimized-by.rubiconproject.com/t/9456/44042/310996-15.3837906.4104028?url=http%3A%2F%2Fpages.ebay.com%2Fnew_buyer%2Fgetting_started.html" target="_blank"><img src="http://assets.rubiconproject.com/campaigns/9456/37/35/88/1438267218campaign_file_z6qr7k.jpg" border="0" alt=""></a>
          </div>
          
      </div>
    </div>
  </div>
</div>
<?php } else {
     echo 'please login to show your history';        
} ?>
                    </div>
                </li>

        
        
        
        <li class='lamp'><span><span><li>
    </ul>
  </nav>
          
                 
         <?php
         foreach (getcats() as $cat){
            
//echo '<li><a href="categories.php?pageid='.$cat['ID'].'&pagename='.str_replace(' ','-',$cat['Name']).'">'.$cat['Name'].'</a></li>';

             }
         ?>
        
     
