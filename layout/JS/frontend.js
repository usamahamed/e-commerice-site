var removedItem,
    sum = 0;
    var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(function(){
   'use strict';
   //fire selectbox plugin
    $("select").selectBoxIt({
    showEffect: "fadeIn",
    showEffectSpeed: 400,
    hideEffect: "fadeOut",
    hideEffectSpeed: 400,
    autoWidth:false
        });
$('.fa-outdent1').hover(function(e){
    e.preventDefault();
    $('.showhistoryitems').slideToggle();
    $('.paypalad').hide();
    $('.paypalad1').hide();
    
});
    $('.delete-item').click(function(){
        var item= $(this).next().val();
 var user= $(this).next().next().val();
$(this).parent().fadeOut(300, function(){
						$(this).remove();
						if($("#cart .cart-item").size() === 0){
							$("#cart .empty").fadeIn(500);
							$("#checkout").fadeOut(500);
						}
                                                
					    });
                                               showdelete1(item,user);

                                        });
    /* ----  Image Gallery Carousel   ---- */

	
	$('.sizes a span, .categories a span').each(function(i, el){
		$(el).append('<span class="x"></span><span class="y"></span>');
		
		$(el).parent().on('click', function(){
			if($(this).hasClass('checked')){				
				$(el).find('.y').removeClass('animate');	
				setTimeout(function(){
					$(el).find('.x').removeClass('animate');							
				}, 50);	
				$(this).removeClass('checked');
				return false;
			}
			
			$(el).find('.x').addClass('animate');		
			setTimeout(function(){
				$(el).find('.y').addClass('animate');
			}, 100);	
			$(this).addClass('checked');
			return false;
		});
	});
	$('.fa-cart-plus').click(function(){
            
           $('.cart1 ,#cart').slideToggle();
            
        });
        
       
        
        
        $('.showwallitemscartclick').hover(function(){
            $(location).attr('href', 'cart.php?do=showcart');
        });
        
	$('.add_to_cart').click(function(){
            var itmid=$(this).next().val();
           var uid=$(this).next().next().val();
        //   $('.showwallitemscart').css('margin-top',175+'px');
	//$('.showwallitemscart').css('margin-bottom',175+'px');
        var productCard = $(this).parent();
		var position = productCard.offset();
		var productImage = $(productCard).find('img').get(0).src;
		var productName = $(productCard).find('.product_name').get(0).innerHTML;				

		$("body").append('<div class="floating-cart"></div>');		
		var cart = $('div.floating-cart');		
		productCard.clone().appendTo(cart);
		$(cart).css({'top' : position.top + 'px', "left" : position.left + 'px'}).fadeIn("slow").addClass('moveToCart');		
		setTimeout(function(){$("body").addClass("MakeFloatingCart");}, 800);
		setTimeout(function(){
			$('div.floating-cart').remove();
			$("body").removeClass("MakeFloatingCart");
			var cartItem = "<div class='cart-item'><div class='img-wrap'><img src='"+productImage+"' alt='' /></div><span>"+productName+"</span><strong>$39</strong><div class='cart-item-border'></div><div class='delete-item'></div></div>";			
			$("#cart .empty").hide();
			$("#cart").append(cartItem);
			$("#checkout").fadeIn(500);
 		    setTimeout(function(){
				$("#cart .cart-item").last().removeClass("flash");
			}, 10 );
			
		}, 1000);
	           Addtocart1(itmid,uid,productImage,productName);

               // addtoCart(itmid,uid);
	});
  /*8**End Image Gallary******/
         //show login or signup form
   $('.login-pa h1 span').click(function(){
       $(this).addClass('selected').siblings().removeClass('selected');
      if($(this).data('view')==='hide'){
          $('.login-pa .Signup').fadeIn(10);
          $('.login-pa .login').fadeOut(10);
          
      }else if($(this).data('view')==='show'){
          $('.login-pa .Signup').fadeOut(10);
          $('.login-pa .login').fadeIn(10);
      }
   });
   $('.ui-switchable-nav li:first-child ').hover(function(){
              $(this).next().removeClass('ui-switchable-active');
         
       $(this).addClass('ui-switchable-active');
     $('.ui-switchable-content').children('.hi').css('opacity','1');
     $('.ui-switchable-content').children('.sh').css('opacity','0');
      
       
   });
     $('.ui-switchable-nav li:last-child ').hover(function(){
          $(this).prev().removeClass('ui-switchable-active');

       $(this).addClass('ui-switchable-active');
     $('.ui-switchable-content').children('.hi').css('opacity','0');
     $('.ui-switchable-content').children('.sh').css('opacity','1');
      
       
   });
      $("#leftnavigator .toggleLi").hover(function() {
        $(this).css({
            'borderColor': '#006799'
        });
        $(this).children("div").fadeToggle();
        $(this).find('a:first').css({
            'right': '-1px',
            'background': '#fff',
            'color': '#006799'
        });
        $(this).find('img.lazy').lazyload();
    }, function() {
        $(this).css({
            'borderColor': '#fff'
        });
        $(this).find('a:first').css({
            'right': '0px',
            'color': '#0A0A0A'
        });
        $(this).children("div").fadeToggle();
    });
    
   //Edit profile
   $('.hotcate_list a').click(function(){
       $(this).addClass('active');
       $(this).siblings().removeClass('active');
       $('.itemcontainer').find('[class^=showitemstap]').eq($(this).index()).slideDown();
        $('.itemcontainer').find('[class^=showitemstap]').eq($(this).index()).siblings().slideUp();
   });
   $(".next11").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

/*********Image Gallary*********************/

	
	$('.product').each(function(i, el){					

		// Lift card and show stats on Mouseover
		$(el).find('.make3D').hover(function(){
				$(this).parent().css('z-index', "20");
				$(this).addClass('animate');
				$(this).find('div.carouselNext, div.carouselPrev').addClass('visible');			
			 }, function(){
				$(this).removeClass('animate');			
				$(this).parent().css('z-index', "1");
				$(this).find('div.carouselNext, div.carouselPrev').removeClass('visible');
		});	
		
		// Flip card to the back side
		$(el).find('.view_gallery').click(function(){	
			
			$(el).find('div.carouselNext, div.carouselPrev').removeClass('visible');
			$(el).find('.make3D').addClass('flip-10');			
			setTimeout(function(){					
			$(el).find('.make3D').removeClass('flip-10').addClass('flip90').find('div.shadow').show().fadeTo( 80 , 1, function(){
					$(el).find('.product-front, .product-front div.shadow').hide();															
				});
			}, 50);
			
			setTimeout(function(){
				$(el).find('.make3D').removeClass('flip90').addClass('flip190');
				$(el).find('.product-back').show().find('div.shadow').show().fadeTo( 90 , 0);
				setTimeout(function(){				
					$(el).find('.make3D').removeClass('flip190').addClass('flip180').find('div.shadow').hide();						
					setTimeout(function(){
						$(el).find('.make3D').css('transition', '100ms ease-out');			
						$(el).find('.cx, .cy').addClass('s1');
						setTimeout(function(){$(el).find('.cx, .cy').addClass('s2');}, 100);
						setTimeout(function(){$(el).find('.cx, .cy').addClass('s3');}, 200);				
						$(el).find('div.carouselNext, div.carouselPrev').addClass('visible');				
					}, 100);
				}, 100);			
			}, 150);			
		});			
		
		// Flip card back to the front side
		$(el).find('.flip-back').click(function(){		
			
			$(el).find('.make3D').removeClass('flip180').addClass('flip190');
			setTimeout(function(){
				$(el).find('.make3D').removeClass('flip190').addClass('flip90');
		
				$(el).find('.product-back div.shadow').css('opacity', 0).fadeTo( 100 , 1, function(){
					$(el).find('.product-back, .product-back div.shadow').hide();
					$(el).find('.product-front, .product-front div.shadow').show();
				});
			}, 50);
			
			setTimeout(function(){
				$(el).find('.make3D').removeClass('flip90').addClass('flip-10');
				$(el).find('.product-front div.shadow').show().fadeTo( 100 , 0);
				setTimeout(function(){						
					$(el).find('.product-front div.shadow').hide();
					$(el).find('.make3D').removeClass('flip-10').css('transition', '100ms ease-out');		
					$(el).find('.cx, .cy').removeClass('s1 s2 s3');			
				}, 100);			
			}, 150);			
			
		});				
	
		makeCarousel(el);
	});
	
	$('.add-cart-large').each(function(i, el){
		$(el).click(function(){
			var carousel = $(this).parent().parent().find(".carousel-container");
			var img = carousel.find('img').eq(carousel.attr("rel"))[0];						
			var position = $(img).offset();	

			var productName = $(this).parent().find('h4').get(0).innerHTML;				
	
			$("body").append('<div class="floating-cart"></div>');		
			var cart = $('div.floating-cart');		
			$("<img src='"+img.src+"' class='floating-image-large' />").appendTo(cart);
			
			$(cart).css({'top' : position.top + 'px', "left" : position.left + 'px'}).fadeIn("slow").addClass('moveToCart');		
			setTimeout(function(){$("body").addClass("MakeFloatingCart");}, 800);
			
			setTimeout(function(){
			$('div.floating-cart').remove();
			$("body").removeClass("MakeFloatingCart");


			var cartItem = "<div class='cart-item'><div class='img-wrap'><img src='"+img.src+"' alt='' /></div><span>"+productName+"</span><strong>$39</strong><div class='cart-item-border'></div><div class='delete-item'></div></div>";			

			$("#cart .empty").hide();			
			$("#cart").append(cartItem);
			$("#checkout").fadeIn(500);
			
			$("#cart .cart-item").last()
				.addClass("flash")
				.find(".delete-item").click(function(){
					$(this).parent().fadeOut(300, function(){
						$(this).remove();
						if($("#cart .cart-item").size() === 0){
							$("#cart .empty").fadeIn(500);
							$("#checkout").fadeOut(500);
						}
					})
				});
 		    setTimeout(function(){
				$("#cart .cart-item").last().removeClass("flash");
			}, 10 );
			
		}, 1000);
			
			
		});
	});
	



$('.edit_profile_personal').click(function(){
    $('#msform').show(300);
    $('#mainwrap').hide(300);

});
$('.exiteditprofile').click(function(){
    $('#msform').hide(300);
    $('#mainwrap').show(300);
  

});
  
   // Instantiate the Bootstrap carousel
$('.multi-item-carousel').carousel({
  interval: false
});

// search button
$('.nav-input_class').on('click',function(){
    $input_search=$('.nav-input11').text();
   $input_search=$('.nav-search-dropdown').val();
   alert("zxcxzzxc"+$input_search+$input_search);
    
    
});
//profile tap script
  $(".section").not(":first").hide();
			    $("ul#menu li:first").addClass("active").show(); 
			 
			    $("ul#menu li").click(function() {
			        $("ul#menu li.active").removeClass("active");
			        $(this).addClass("active");
			        $(".section").slideUp();       
			        $($('a',this).attr("href")).slideDown('slow');
			 
			        return false;
			    });

// Do the same for the next, next item.
$('.multi-item-carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo($(this));
  } else {
  	$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
  }
});
        var container = $('#DIV_3');

container.find('select').data('selectBox-selectBoxIt').destroy();

        
        $('.carousel').carousel();
        //switch opacity in meanu header
        $('.fa-cart-plus').click(function(){
            $('.rad-menu-badge,.rad-menu-badge2').css('opacity','.3');
            $('.rad-menu-badge1').css('opacity','1');});
        
        $('.heart').click(function(){
            $('.rad-menu-badge,.rad-menu-badge1').css('opacity','.3');
            $('.rad-menu-badge2').css('opacity','1');});
         $('.rad-menu-badge,.rad-menu-badge1,.rad-menu-badge2').click(function(){
             
            $(this).css('opacity','1');
            
            
                });
    
        
          //menue budge
      if($('.rad-menu-badge').text()==='0'){
          $('.rad-menu-badge').hide();
      }
       
    
    // Lamp menue motion
     if($('#ddmenu1>ul>li').hasClass('selected11')){
$('.selected11').addClass('active11');
var currentleft=$('.selected11').position().left+"px";
var currentwidth=$('.selected11').css('width');
$('.lamp').css({"left":currentleft,"width":currentwidth});
}
else{
  $('#ddmenu1>ul>li').first().addClass('active11');
  var currentleft=$('.active11').position().left+"px";
var currentwidth=$('.active11').css('width');
$('.lamp').css({"left":currentleft,"width":currentwidth});
}

$('#ddmenu1>ul>li').hover(function(){
  $('#ddmenu1 ul li').removeClass('active11');
  $(this).addClass('active11');
var currentleft=$('.active11').position().left+"px";
var currentwidth=$('.active11').css('width');
$('.lamp').css({"left":currentleft,"width":currentwidth});
},function(){
if($('#ddmenu1>ul>li').hasClass('selected11')){
$('.selected11').addClass('active11');
var currentleft=$('.selected11').position().left+"px";
var currentwidth=$('.selected11').css('width');
$('.lamp').css({"left":currentleft,"width":currentwidth});
}
else{
  $('#ddmenu1>ul>li').first().addClass('active11');
  var currentleft=$('.active11').position().left+"px";
var currentwidth=$('.active11').css('width');
$('.lamp').css({"left":currentleft,"width":currentwidth});
}
});
    //end lamp motion
    
        
        //Quick View 
        $('.Quick_view').click(function(){
            $('.modal').html($('price').html());
           var src= $(this).next().attr('src');
           $('.img-thumbnail').attr('src',src);
            
        });
     $(".hover1").mouseleave(
    function () {
      $(this).removeClass("hover1");
    }
  );
//faq toggle stuff 
$('.togglefaq').click(function(e) {
e.preventDefault();
var notthis = $('.active').not(this);
notthis.find('.icon-minus').addClass('icon-plus').removeClass('icon-minus');
notthis.toggleClass('active').next('.faqanswer').slideToggle(300);
 $(this).toggleClass('active').next().slideToggle("fast");
$(this).children('i').toggleClass('icon-plus icon-minus');
});
//send feedback
$('.content_show_yes').click(function(){
  
    $(this).parent().hide();
     $(this).parent().next().show();
    sleep(5000);
  
});
//toggle comment show
$('.show_comments').click(function(){
   $(this).parent().next().next().fadeToggle(300);
   
});
//FAQ switch
$('.switchlinlkask').click(function(e){
    var cont=$(this).text();
    if(cont==='Ask Question'){
        $(this).text('back').addClass('btn btn-primary');
    }else{
        $(this).text('Ask Question').removeClass('btn btn-primary');
    }
    e.preventDefault();
    e.stopPropagation();
    $('.askqus').fadeToggle(300);
    
    $('.FAQ').fadeToggle(300);
    

    
    
    
});

/*$('.togglefaq').dblclick(function(e){
    e.preventDefault();
    e.stopPropagation();
    var cont=$(this).html();
    $(this).parent().next().fadeToggle(300);
    $(this).parent().fadeToggle(300);
    $('.switchlinlkask').fadeToggle();
    $(this).parent().next().find('p').html(cont);
    
    
});*/


$('.content_show_no').click(function(){
     $(this).parent().hide();
    $(this).parent().next().show();
});
  
  
   //placholder handel
   $('[placeholder]').focus(function(){
       $(this).attr('data-cash', $(this).attr('placeholder'));
       $(this).attr('placeholder','');
   }).blur(function(){
       
      $(this).attr('placeholder',$(this).attr('data-cash')); 
   });
    //add astric to required feild
    $("input").each(function(){
       if($(this).attr('required')==='required'){
           $(this).after('<span class="asterisk">*</span>');
       }
       
    });
    //header panel
 $(".rad-toggle-btn1").on('click', function() {
		$(".rad-logo-container").toggleClass("rad-nav-min");
		$(".rad-sidebar").toggleClass("rad-nav-min");
		$(".rad-body-wrapper").toggleClass("rad-nav-min");
		setTimeout(function() {
			initializeCharts();
		}, 200);
	});
      
$('.closenotification').click(function(){
    $(this).parent().hide(300);
}); 
 //end header panel
    //show product popup screen 
    $(".add-to-cart").click(function (e) {
       //  $('.outer-pop').css('opacity','1');
       // $('body').attr('background-color',"red");
       // e.stopPropagation();
       $pos=$(this).offset();
         $(".pop").fadeIn(300).scrollTop($pos);
         //positionPopup();
         
            });

     $(".pop > span, .pop").click(function () {
         $(".pop").fadeOut(300);
      //  $(location).attr('href', 'index.php');
    


     });

    //show profile menu
    $("li.rad-dropdown > a.rad-menu-item").on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		//$(".rad-dropmenu-item").removeClass("active");
		$(this).next(".rad-dropmenu-item").toggleClass("active");
	});
        
         var storedclass = readCookie("cookiclass");
         $('body').addClass(storedclass);
        $('.rad-chk-pin input[type=checkbox]').change(function(e) {
            var classflat="flat-theme";
            if($('body').hasClass('flat-theme')){
                classflat='';
            }
            createCookie("cookiclass", classflat, 365);
		$('body').toggleClass("flat-theme");
		$("#rad-color-opts").toggleClass("hide");
	});
        //start Theme switch
        var mapSeries = {
		"AF": 16.63,
		"AL": 11.58,
		"DZ": 158.97,
		"AO": 85.81,
		"AG": 1.1,
		"AR": 351.02,
		"AM": 8.83,
		"AU": 1219.72,
		"AT": 366.26,
		"AZ": 52.17,
		"BS": 7.54,
		"IN": 4949,
		"US": 852
	};
        	var settings = {
		map: 'world_mill_en',
		container: $('#world-map'),
		backgroundColor: 'transparent',
		colors: '#3377CC',
		zoomOnScroll: false,
		regionStyle: {
			initial: {
				fill: "#c6c6c6"
			}
		},
		series: {
			regions: [{
				values: mapSeries,
				scale: ['#C8EEFF', '#0071A4'],
				normalizeFunction: 'polynomial'
			}]
		}
	};
        	var world = new jvm.Map(settings);
                var colorMap = {
		crimson: "crimson", 
		teal: "#1fb5ad", 
		orange: "#ff503f", 
		purple: "rebeccapurple", 
		twitter: "#55acee"
	};
        
        	$('.rad-color-swatch input[type=radio]').change(function(e) {
		if ($('.rad-chk-pin input[type=checkbox]').is(":checked")) {
			$('body').removeClass().addClass("flat-theme").addClass(this.value);
			$('.rad-color-swatch label').removeClass("rad-option-selected");
			$(this).parent().addClass("rad-option-selected");
			$(window).scrollTop(0);
			
			
		} else {
			return false;
		}
	});
       
    //start list 
    var navBar = $('.list_icon');
navBar.append('<li class="marker"></li>');
//show setting Theme
navBar.on("click" , ".fa-cog" , function(){
  $("li.rad-dropdown > a.rad-menu-item1").next(".rad-dropmenu-item").toggleClass("active");
  });
  navBar.on("click" , ".fa-bell-o" , function(){
  $("li.rad-dropdown1 > a.rad-menu-item2").next(".rad-dropmenu-item").toggleClass("active");
  });
  
 
navBar.on("click" , "a,li" , function(){

    var navTap = $(this).closest('.list_icon');
    var mrkWidth = $(this).parent('li').width();
    var marker = navTap.find('.marker');
    var nx = $(".list_icon").offset();
    var lx = $(this).parent('li').offset();
    var left = lx.left - nx.left;
    
   navBar.find('li').removeClass('active');
  
   $(this).parent().addClass('active');
  
    marker.removeClass("anim").css({ "width" : mrkWidth , "left" : left });
    setTimeout(function(){
      marker.addClass("anim");
    },200);

});
 $('.list_icon ,i,li,a').on("click" , function(){
       $(this).find('.rad-menu-badge').remove();
        
   
  });
//image zoom in item detail page

var src = $('.thumb-show').find('img').attr("src");
$('.thumb-show').append('<img class="zoom" src="'+src+'" />');

$('.thumb-show').mouseenter(function(){
  
    $(this).mousemove(function(event){
        var offset = $(this).offset();
        var left = event.pageX - offset.left;
        var top = event.pageY - offset.top;
      
        $(this).find('.zoom').css({
          width: '200%',
           height: '200%',
          opacity: 1,
          left: -left,
          top: -top
        });
    });
});

$('.thumb-show').mouseleave(function(){                     
   $(this).find('.zoom').css({
     width: '100%',
     height: '100%',
     opacity: 0,
     left: 0,
     top: 0
   })                               
 });
 
 $('.sub_images div').on("click",function(){
     $(this).nextAll().css("border","none");
     $(this).prevAll().css("border","none");
     $(this).css("border","2px solid black");
     var src=$(this).find('img').attr('src');
     $('.thumb-show').find('img').attr("src",src);
 });
 $('.sub_images div').hover(function(){
     var src=$(this).find('img').attr('src');
     $('.thumb-show').find('img').attr("src",src);
 });
    //confirmatin mess before delete member
    $('.confirm').click(function(){

        
    });
  
   //Start Ad item page
   $('.l-name').keyup(function(){
       
       $('.live-preview .caption h3').text($(this).val());
   });
    $('.l-des').keyup(function(){
       
       $('.live-preview .caption p').text($(this).val());
   });
     $('.l-price').keyup(function(){
       
       $('.live-preview span').text('$'+$(this).val());
   });
   
   
   //start message rules
    $('#messages li').click(function(){
        $('#content-panel h2').html($(this).find('h2').html());
        $('#content-panel p').html($(this).find('p').html());
        $('#content-panel span').html($(this).find('.titl').html());
       $('#content-panel div').html($(this).find('.links1').html());
       $('#content-panel h3').html($(this).find('.data_span').html());
        
        
    });
    
    //start rating 

    var myVar = 0;

$('#dropdown').on('change', function() {

	myVar = $('#dropdown').val();

	if(myVar){
		//Removing all classes from the element
		$('#needle').attr('class', '');
	};

	switch(myVar){

		case "1":
			$('#needle').addClass('poor');
                        
			break;

		case "2":
			$('#needle').addClass('average');
			break;

		case "3":
			$('#needle').addClass('helpful');
			break;

		case "4":
			$('#needle').addClass('buddy');
			break;

		default:
			console.log('Error');
			break;

	};

});
  
$('#dropdown1').on('change', function() {

	myVar = $('#dropdown1').val();

	if(myVar){
		//Removing all classes from the element
		$('#needle1').attr('class', '');
	};

	switch(myVar){

		case "1":
			$('#needle1').addClass('onestar');
                        
			break;

		case "2":
			$('#needle1').addClass('twostar');
			break;

		case "3":
			$('#needle1').addClass('threestar');
			break;

		case "4":
			$('#needle1').addClass('fourstar');
			break;
                case "5":
			$('#needle1').addClass('fivestar');
			break;

		default:
			console.log('Error');
			break;

	};

});
      
    
    //add qick view 
       $('figure').click(function(){
           $('body').css('background','#FFF');
        $('#myModalLabel').html($(this).find('h3').html());
    $('.modal-body').find('.con').html($(this).find('figcaption').html());
   
    });
    
  $(".quick-message").on("swipeleft",function(){
    $(this).next().addClass('show');
  });    
 $(".quick-message").on("swiperight",function(){
    $(this).prev().addClass('show');
  }); 

  $(".contact-info").on("swipeleft",function(){
    $(this).removeClass('show');
  });   

 $(".manage-message").on("swiperight",function(){
    $(this).removeClass('show');
  }); 
  $('.from-account').eq(0).css('background-color', 'hotpink');
  $('.from-account').eq(1).css('background-color', '#3496db');
  $('.from-account').eq(2).css('background-color', '#3496db');
  $('.from-account').eq(3).css('background-color', '#ec8242');
  $('.from-account').eq(4).css('background-color', '#3496db');
  $('.from-account').eq(5).css('background-color', 'hotpink');
  $('.from-account').eq(6).css('background-color', 'hotpink');
  $('.from-account').eq(7).css('background-color', '#ec8242');
  $('.from-account').eq(8).css('background-color', 'hotpink');
  $('.from-account').eq(9).css('background-color', '#ec8242');
   
  $('.toggle').click(function() {
    $('#main-nav').slideToggle();
    $('.hamburger-1').toggleClass('cross-right');
    $('.hamburger-2').toggleClass('cross-hide');
    $('.hamburger-3').toggleClass('cross-left');   
  });
  $('#mail-boxes li').click(function() {
    $('#mail-boxes li').removeClass('active-mailbox');
    $(this).addClass('active-mailbox');
  });
   
   //begin cart calcuation
   
   // calculate the values at the start
  calculatePrices();
  
  // Click to remove an item
  $('a.remove').on("click", function() {
      // store the html
  removedItem = $(this).closest("tr").clone();
 var item= $(this).next().val();
 var user= $(this).next().next().val();
  // fade out the item row
  $(this).closest("tr").fadeOut(500, function() {
    $(this).remove();
    sum = 0;
    calculatePrices();
  });
  // fade in the removed confirmation alert
  $(".removeAlert").fadeIn();
  
  
  // default to hide the removal alert after 5 sec
  setTimeout(function(){
    hideRemoveAlert();
   showdelete(item,user);

  }, 1000); 
  });
    $('a.remove1').on("click", function() {
      // store the html
  removedItem = $(this).closest("tr").clone();
 var item= $(this).next().val();
 var user= $(this).next().next().val();
  // fade out the item row
  $(this).closest("tr").fadeOut(500, function() {
    $(this).remove();
    sum = 0;
    calculatePrices();
  });
  // fade in the removed confirmation alert
  $(".removeAlert").fadeIn();
  
  // default to hide the removal alert after 5 sec
  setTimeout(function(){
    hideRemoveAlert();
   showdelete1(item,user);
   

  }, 1000); 
  });
  
      $('a.remove3').on("click", function() {
      // store the html
 var item= $(this).next().val();
 var user= $(this).next().next().val();
  // fade out the item row
  // fade in the removed confirmation alert
  $(".AddAlert").fadeIn();
  
  // default to hide the removal alert after 5 sec
  setTimeout(function(){
    hideAddAlert();
   Addtocart(item,user);
   

  }, 1000); 
  });
  
  
  
  $(document).on("change", "input.quantity", function(){
      var item= $(this).next().next().val();
 var user= $(this).next().next().next().val();
    var val = $(this).val(),
        pricePer,
        total
    
    if ( val <= "0") {
          $(this).closest("tr").fadeOut(500, function() {
    $(this).remove();
    sum = 0;
    calculatePrices();
  });
        removedItem = $(this).closest("tr").clone();
        $(".removeAlert").fadeIn();
       showdelete1(item,user);
        setTimeout(function(){
    hideRemoveAlert();}, 1000); 
    } else {
         updatequantity(item,user,val);
      // reset the prices
      sum = 0;
      
      // get the price for the item
      pricePer = $(this).parents("td").prev("td").text();
      // set the pricePer to a nice, digestable number
      pricePer = formatNum(pricePer);
      // calculate the new total
      total = parseFloat(val * pricePer).toFixed(2);
      // set the total cell to the new price
      $(this).parents("td").siblings(".itemTotal").text("$" + total);
      
      // recalculate prices for all items
      calculatePrices();
    }
  });
 //Sent FAQ Question
      $('.FAQ_Send_Question').on("click", function(e) {
      // store the html
      e.preventDefault();
      e.stopPropagation();
 var item= $(this).next().val();
 var user= $(this).next().next().val();
 var value=$('.FAQquestion1').val();

  $(".resultFAQ").show();
  
  setTimeout(function(){
   SendFAQ(item,user,value);
   

  }, 2800); 
  });
   
   
      
   //Sent FAQ Question
      $('.FAQ_Send_Question1').on("click", function() {
      // store the html
      
 var qid= $(this).next().val();
 var uid= $(this).next().next().val();
 var value=$(this).prev().val();

  $(this).parent().find(".resultFAQAns1").fadeToggle();
  
  setTimeout(function(){
   SendFAQAns(qid,uid,value);
   

  }, 2800); 
  $(this).hide();
  $(this).prev().hide();
   $(this).parent().find(".resultFAQAns1").hide();
   $(this).parent().find(".resultFAQAns2").show();
   
  });
  
  
     //Add New Item Ajax check for error
       $('.l-name').blur(function() {
      var name=$('.l-name').val();
      SendItemsAddedname(name);
     }).focus(function(){$('.Add_item_errormesg').html('');});
     
   $('.l-des').blur(function() {
      var des=$('.l-des').val();
      SendItemsAddeddes(des);
     }).focus(function(){$('.Add_item_errormesg').html('');});

  
});


//begin cart calculation function

function removeItem() {
  // fade out the item row
  $(this).closest("tr").fadeOut(500, function() {
    $(this).remove();
    sum = 0;
    calculatePrices();
  });
  // fade in the removed confirmation alert
  $(".removeAlert").fadeIn();
  
  // default to hide the removal alert after 5 sec
  setTimeout(function(){
    hideRemoveAlert();   
  }, 2000); 
}

function hideRemoveAlert() {
  $(".removeAlert").fadeOut(500);
}
function hideAddAlert() {
  $(".AddAlert").fadeOut(500);
}

function updateSubTotal(){
  $("table.items td.itemTotal").each(function(){
    var value = $(this).text();
    // set the pricePer to a nice, digestable number
    value = formatNum(value);

    sum += parseFloat(value);
    $("table.pricing td.subtotal").text("$" + sum.toFixed(2));
  });
}

function addTax() {
  var tax = parseFloat(sum * 0.05).toFixed(2);
  $("table.pricing td.tax").text("$" + tax);
}

function calculateTotal() {
  var subtotal = $("table.pricing td.subtotal").text(),
      tax = $("table.pricing td.tax").text(),
      shipping = $("table.pricing td.shipping").text(),
      total;
  
  subtotal = formatNum(subtotal);
  tax = formatNum(tax);
  shipping = formatNum(shipping);
   
  total = (subtotal + tax + shipping).toFixed(2);
  
  $("table.pricing td.orderTotal").text("$" + total);
}

function calculatePrices() {
  updateSubTotal();
  addTax();
  calculateTotal();
}

function formatNum(num) {
  return parseFloat(num.replace(/[^0-9-.]/g, ''));
}

    function showdelete(item,user){
           //begin ajax remove items
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=deletewishlist&itm="+item+"&userid="+user;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
          $cont=$('.rad-menu-badge2').text();
          $('.rad-menu-badge2').text(parseInt($cont)-1);
       
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }
       function showdelete1(item,user){
           //begin ajax remove items
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=deletecart&itm="+item+"&userid="+user;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
          $cont=$('.rad-menu-badge1').text();
          $('.rad-menu-badge1').text(parseInt($cont)-1);
       
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }
    
    
         function Addtocart(item,user){
           //begin ajax remove items
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=addtocart&itm="+item+"&userid="+user;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
          $cont=$('.rad-menu-badge1').text();
          $('.rad-menu-badge1').text(parseInt($cont)+1);
       
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }
           function Addtocart1(item,user,productImage,productName){
           //begin ajax remove items
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=addtocart&itm="+item+"&userid="+user;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
          	
           $cont=$('.rad-menu-badge1').text();
          $('.rad-menu-badge1').text(parseInt($cont)+1);
			  var productCard = $('.add_to_cart').parent();
		var position = productCard.offset();
		var productImage = $(productCard).find('img').get(0).src;
		var productName = $(productCard).find('.product_name').get(0).innerHTML;				

		$("body").append('<div class="floating-cart"></div>');		
		var cart = $('div.floating-cart');		
		productCard.clone().appendTo(cart);
		$(cart).css({'top' : position.top + 'px', "left" : position.left + 'px'}).fadeIn("slow").addClass('moveToCart');		
		setTimeout(function(){$("body").addClass("MakeFloatingCart");}, 800);
		setTimeout(function(){
			$('div.floating-cart').remove();
			$("body").removeClass("MakeFloatingCart");
			var cartItem = "<div class='cart-item'><div class='img-wrap'><img src='"+productImage+"' alt='' /></div><span>"+productName+"</span><strong>$39</strong><div class='cart-item-border'></div><div class='delete-item'></div></div>";			
			$("#cart .empty").hide();
			$("#cart").append(cartItem);
			$("#checkout").fadeIn(500);
 		    setTimeout(function(){
				$("#cart .cart-item").last().removeClass("flash");
			}, 10 );
			
		}, 1000);
       
      
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }
             function SendFAQ(item,user,value){
           //begin ajax remove items
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=SendFAQ&itm="+item+"&userid="+user+"&val="+value;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
         $('.askqus').fadeToggle(300);
         $('.FAQ').fadeToggle(300);
       
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }
    
                 function SendFAQAns(qid,uid,value){
           //begin ajax remove items
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=sendAnswer&qid="+qid+"&userid="+uid+"&val="+value;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
      
       
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }
    
    
   //add items ajax form
 function SendItemsAddedname(name){
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request"); else {
    var url= "AJAXCheck.php?do=sendItemAddedname&name="+name;
    usernameRequest.onreadystatechange = function(){ if (usernameRequest.readyState === 4) {if (usernameRequest.status === 200) {
    $('.Add_item_errormesg').append(usernameRequest.responseText);}}};
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }
   
function SendItemsAddeddes(des){
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request"); else {
    var url= "AJAXCheck.php?do=sendItemAddeddes&des="+des;
    usernameRequest.onreadystatechange = function(){ if (usernameRequest.readyState === 4) {if (usernameRequest.status === 200) {
    $('.Add_item_errormesg').append(usernameRequest.responseText);}}};
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }

   
    
    
    
    
        function updatequantity(item,user,val){
           //begin ajax remove items
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=updatequantity&itm="+item+"&userid="+user+"&qnt="+val;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
          $cont=$('.rad-menu-badge2').text();
          $('.rad-menu-badge2').text(parseInt($cont)-1);
       
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
    }


//AJAX Functionality Test
window.onload = initPage;
var usernameValid = false;
var itmid=$('.content_show_yes').attr('itmid');
var rid=$('.content_show_yes').attr('rid');
var uidid=$('.content_show_yes').attr('uid');

function initPage() {

  $('.fc-UserN').blur(checkUsername);
  $('.fc-UserN').focus(checkUsername1);
  $('.fa-heart1').click(function(){
  var itm=$(this).parent().next().val();    
  var user=$(this).parent().next().next().val(); 
      $(this).css("color","red");
     usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "AJAXCheck.php?do=addwishlist&itm="+itm+"&user="+user;
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
          $cont=$('.rad-menu-badge2').text();
          $('.rad-menu-badge2').text(parseInt($cont)+1);
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
  });
  
  
 //$('.content_show_yes').click(updaterate);
 }
function checkUsername1(){
    $('.asterisk').hide();
}
function checkUsername() {
  $('.fc-UserN').after('<span class="asterisk asterisk1 "><img src="status.gif" class="thinking"/></span>');
    usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var theName =  $('.fc-UserN').val();
    var username = escape(theName);
    var url= "AJAXCheck.php?do=checkuser&username=" + username;
    usernameRequest.onreadystatechange = showUsernameStatus;
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
  }
}
function addtowishlist() {
    usernameRequest = createRequest();
  if (usernameRequest === null)
    alert("Unable to create request");
  else {
    var url= "wishlist.php";
    usernameRequest.onreadystatechange = function(){
             if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
        $(".item-cont").find('.fa-heart').css("color","red");
      } else {
   
      }
    }
  } 
    };
    usernameRequest.open("GET", url, true);
    usernameRequest.send(null);
        

  }
}
function showUsernameStatus() {
    
  if (usernameRequest.readyState === 4) {
    if (usernameRequest.status === 200) {
      if (usernameRequest.responseText === "okay") {
          $('.fc-UserN').next().hide();
  $('.fc-UserN').after('<span class="asterisk "><i class="fa fa-check"></i></span>');
      } else {
    $('.fc-UserN').next().hide();
  $('.fc-UserN').after('<span class="asterisk"><i class="fa fa-close"></i></span>');
       // document.getElementById("username").focus();
        //document.getElementById("username").select();
      }
    }
  }
}
function showUpdateStatus() {
  if (usernameRequest.readyState === 4) {

    if (usernameRequest.status === 200) {
alert('okay');
      if (usernameRequest.responseText === "okay") {
          alert('okay');
      } else {
   
       // document.getElementById("username").focus();
        //document.getElementById("username").select();
      }
    }
  }
}



//Begin Utility Function
function createRequest() {
  try {
    request = new XMLHttpRequest();
  } catch (tryMS) {
    try {
      request = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (otherMS) {
      try {
        request = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (failed) {
        request = null;
      }
    }
  }	
  return request;
}


function getActivatedObject(e) {
  var obj;
  if (!e) {
    // early version of IE
    obj = window.event.srcElement;
  } else if (e.srcElement) {
    // IE 7 or later
    obj = e.srcElement;
  } else {
    // DOM Level 2 browser
    obj = e.target;
  }
  return obj;
}

//header ajax
var lorem = "Easier recognition means suppliers are 63% more likely to contact buyers with a verified Business Identity.",
	fox = " It is an open credit line that you can draw from any time as long as the amount is over US $5,000 and there is credit available",
        ins="Secure Payment Service makes sure that your payment is held securely and released to the inspector only after you confirm satisfactory receipt of the inspection report.",
	rel="check every inspector's qualifications to confirm their authenticity and capabilities.",
        afor="We offer a wide price range from different types of inspectors to satisfy the specific inspection requirements of different buyers.",
        ops="Buyers who are verified enjoy more promotional events, held jointly with Alibaba.com's partners.",
        titleContainer = document.getElementsByClassName("title-container")[0],
	summaryContainer = document.getElementsByClassName("summary")[0],
	timeContainer = document.getElementsByClassName("time")[0];

var news = [{
		title: "Bussiness Identity",
		summary: lorem
	}, {
		title: "e-Credit Line",
		summary: fox
	}, {
		title: "Inspection service",
		summary: ins
	}, {
		title: "Reliable Inspectors",
		summary: rel
	}, {
		title: "Affordable Service",
		summary: afor
	}, {
		title: "Promotional events",
		summary: ops
	}],
	elems = [],
	count = 4;

function createElems() {
    
	elems = news.map( v => {
		var item = document.createElement("div");
		item.classList.add("title-item");
		item.classList.add("text");
		item.innerHTML = v.title;
		item.summary = v.summary;
		return item;
	});
	elems[0].classList.add("active");
	elems[count].classList.add("deactive");

	while (titleContainer.firstChild) {
		titleContainer.removeChild(titleContainer.firstChild);
	}

	for (var i = 0; i < count + 1; i++) {
		titleContainer.appendChild(elems[i]);
	}
	summaryContainer.innerHTML = elems[0].summary;
}
createElems();

var cases = {
	"activeItemDown": function() {
		elems[0].classList.add("deactive");
		elems[count].classList.remove("deactive");
		summaryContainer.classList.add("fade-out");
		summaryContainer.classList.remove("fade-in");
	},
	"width": function() {
		elems[0].remove();
		elems[0].classList.remove("active");
		elems.push(elems.shift());
		elems[count].classList.add("deactive");
		titleContainer.appendChild(elems[count]);
		elems[0].classList.add("active");
		summaryContainer.innerHTML = elems[0].summary;
		summaryContainer.classList.add("fade-in");
		summaryContainer.classList.remove("fade-out");
	}
};

["animationend", "transitionend"].forEach(v => document.addEventListener(v, eventHandler));

function eventHandler(e) {
	var prop = e.animationName || e.propertyName || "";
	(e.target.classList.contains("active") && cases[prop]) ? cases[prop](): null;
}

function updateTime(){
	timeContainer.innerHTML = new Date().toTimeString().substr(0,5);
	setTimeout(updateTime, 10000);
}
updateTime();


function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}
	function makeCarousel(el){
	
		
		var carousel = $(el).find('.carouselimg ul');
		var carouselSlideWidth = 315;
		var carouselWidth = 0;	
		var isAnimating = false;
		var currSlide = 0;
		$(carousel).attr('rel', currSlide);
		
		// building the width of the casousel
		$(carousel).find('li').each(function(){
			carouselWidth += carouselSlideWidth;
		});
		$(carousel).css('width', carouselWidth);
		
		// Load Next Image
		$(el).find('div.carouselNext').on('click', function(){
			var currentLeft = Math.abs(parseInt($(carousel).css("left")));
			var newLeft = currentLeft + carouselSlideWidth;
			if(newLeft === carouselWidth || isAnimating === true){return;}
			$(carousel).css({'left': "-" + newLeft + "px",
								   "transition": "300ms ease-out"
								 });
			isAnimating = true;
			currSlide++;
			$(carousel).attr('rel', currSlide);
			setTimeout(function(){isAnimating = false;}, 300);			
		});
		
		// Load Previous Image
		$(el).find('div.carouselPrev').on('click', function(){
			var currentLeft = Math.abs(parseInt($(carousel).css("left")));
			var newLeft = currentLeft - carouselSlideWidth;
			if(newLeft < 0  || isAnimating === true){return;}
			$(carousel).css({'left': "-" + newLeft + "px",
								   "transition": "300ms ease-out"
								 });
			isAnimating = true;
			currSlide--;
			$(carousel).attr('rel', currSlide);
			setTimeout(function(){isAnimating = false;}, 300);						
		});
	}