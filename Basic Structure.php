<?php ob_start(); ?>    
      <li class="pull-left classshow">
            <span class="top-heading"><i class="fa fa-futbol-o fa-lg" aria-hidden="true"></i>Sports</span>
            <i class="caret"></i>
            <div class="dropdown">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Sports</h3></li>
                        <li><a href="#">Athletic Clothing</a></li>
                        <li><a href="#">Exercise & Fitness</a></li>
                        <li><a href="#">Hunting & Fishing</a></li>
                        <li><a href="#">Team Sports</a></li>
                        <li><a href="#">Fan Shop</a></li>
                        <li><a href="#">Golf</a></li>
                        <li><a href="#">Sports Collectibles</a></li>
                        <li><a href="#">Cycling</a></li>
                    </ul>
                  <ul class="column">
                        <li><h3>CYCLING</h3></li>
                        <li><a href="#">bikes</a></li>
                        <li><a href="#">clothing</a></li>
                        <li><a href="#">Etiam massa</a></li>
                        <li><a href="#">accessories</a></li>
                        <li><a href="#">tires & tubes</a></li>
                        <li><a href="#">bike racks</a></li>
                        <li><a href="#">helmets & accessories</a></li>
                        <li><a href="#">bike frames</a></li>
                    </ul>
                    <ul class="column">
                        <li><h3>Outdoors</h3></li>
                        <li><a href="#">Camping & Hiking</a></li>
                        <li><a href="#">Cycling</a></li>
                        <li><a href="#">Outdoor Clothing</a></li>
                        <li><a href="#">Scooters</a></li>
                        <li><a href="#">Winter Sports</a></li>
                        <li><a href="#">Climbing</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">All Outdoor Recreation</a></li>
                    </ul>
                    <ul class="column mayHide">
                        <li><br /><img src="layout/Img/sports.png" /></li>
                    </ul>
                </div>
            </div>
        </li>


         <li class="pull-left">
            <span class="top-heading"><i class="fa fa-gavel fa-lg" aria-hidden="true"></i>Garden & Tools</span>
            <i class="caret"></i>
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Garden pets</h3></li>
                        <li><a href="#">Bedding & Bath</a></li>
                        <li><a href="#">Exercise & Fitness</a></li>
                        <li><a href="#">Appliances</a></li>
                        <li><a href="#">Patio, Lawn & Garden</a></li>
                        <li><a href="#">Fine Art</a></li>
                        <li><a href="#">Arts, Crafts & Sewing</a></li>
                        <li><a href="#">Pet Supplies</a></li>
                        <li><a href="#">Wedding Registry</a></li>
                    </ul>
                  <ul class="column">
                        <li><h3>home improvement</h3></li>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Power</a></li>
                        <li><a href="#">Hand Tools</a></li>
                        <li><a href="#">Lamps</a></li>
                        <li><a href="#">Light Fixtures</a></li>
                        <li><a href="#">Kitchens</a></li>
                        <li><a href="#">Bath Fixtures</a></li>
                        <li><a href="#">Hardware</a></li>
                    </ul>
                
                    <ul class="column mayHide">
                        <li><br /><img src="layout/Img/home.png" /></li>
                    </ul>
                </div>
            </div>
        </li>
    
            <li class="pull-left">
            <span class="top-heading"><i class="fa fa-book fa-lg" aria-hidden="true"></i>Books</span>
            <i class="caret"></i>
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Books</h3></li>
                        <li><a href="#">Arts & Photography</a></li>
                        <li><a href="#">Biographies & Memoirs</a></li>
                        <li><a href="#">Children's Books</a></li>
                        <li><a href="#">Text books</a></li>
                          <li><a href="#">Kindle books</a></li>
                          <li><a href="#">Magazines</a></li>
                  <li><a href="#">Audible</a></li>
                  <li><a href="#">Audible ulimited</a></li>
                    </ul>
                 
                    <ul class="column mayHide">
                        <li><br /><img src="layout/Img/books.png" /></li>
                    </ul>
                </div>
            </div>
        </li>
                <li class="pull-left  right-aligned">
            <span class="top-heading"><i class="fa fa-laptop fa-lg" aria-hidden="true"></i>Electronics & Computers</span>
            <i class="caret"></i>
            <div class="dropdown offset300">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Electronics</h3></li>
                        <li><a href="#">TV & Video</a></li>
                        <li><a href="#">Home Audio & Theater</a></li>
                        <li><a href="#">Camera, Photo & Video</a></li>
                        <li><a href="#">Cell Phones & Accessories</a></li>
                        <li><a href="#">Headphones</a></li>
                        <li><a href="#">Video Games</a></li>
                        <li><a href="#">Bluetooth & Wireless Speakers</a></li>
                        <li><a href="#">Car Electronics</a></li>
                    <li><a href="#">Computer Parts & Components</a></li>
                    <li><a href="#">Printers & Ink</a></li>
                    <li><a href="#">Car Electronics</a></li>
                   <li><a href="#">Office & School Supplies</a></li>
                    </ul>
                  <ul class="column">
                        <li><h3>Computers</h3></li>
                        <li><a href="#">Musical Instruments</a></li>
                        <li><a href="#">Internet & TV</a></li>
                        <li><a href="#">Phone Services</a></li>
                        <li><a href="#">Wearable Technology</a></li>
                        <li><a href="#">Electronics Showcase</a></li>
                        <li><a href="#">Computers & Tablets</a></li>
                        <li><a href="#">Monitors</a></li>
                        <li><a href="#">Accessories</a></li>
                        <li><a href="#">Networking</a></li>
                        <li><a href="#">Drives & Storage</a></li>
                    </ul>
                
                    <ul class="column mayHide">
                        <li><br /><img src="layout/Img/com.png" /></li>
                    </ul>
                </div>
            </div>
            

        </li>
                   <li class="pull-left  right-aligned">
            <span class="top-heading"><i class="fa fa-hand-o-up fa-lg" aria-hidden="true"></i>Handmade</span>
            <i class="caret"></i>
            <div class="dropdown right-aligned">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Electronics</h3></li>
                        <li><a href="#">Jewelry</a></li>
                        <li><a href="#">Handbags & Accessories</a></li>
                        <li><a href="#">Home Décor</a></li>
                        <li><a href="#">Artwork</a></li>
                        <li><a href="#">Stationery & Party Supplies</a></li>
                        <li><a href="#">Kitchen & Dining</a></li>
                        <li><a href="#">Furniture</a></li>
                        <li><a href="#">Bedding</a></li>
                    <li><a href="#">Baby</a></li>
                    <li><a href="#">Printers & Ink</a></li>
                    <li><a href="#">Toys & Games</a></li>
                   <li><a href="#">All Handmade</a></li>
                    </ul>
              
                
                    <ul class="column mayHide">
                        <li><br /><img src="layout/Img/hand.png" /></li>
                    </ul>
                </div>
            </div>
        </li>
        
              <li class="pull-left  right-aligned">
            <span class="top-heading"><i class="fa fa-map-signs fa-lg" aria-hidden="true"></i>Home Services</span>
            <i class="caret"></i>
            <div class="dropdown right-aligned">
                <div class="dd-inner">
                    <ul class="column">
                        <li><h3>Services</h3></li>
                        <li><a href="#">Home Improvement & Repair</a></li>
                        <li><a href="#">Yard & Outdoors</a></li>
                        <li><a href="#">Computer & Electronics</a></li>
                        <li><a href="#">Assembly</a></li>
                        <li><a href="#">Cleaning</a></li>
                        <li><a href="#">Plumbing</a></li>
                        <li><a href="#">Electrical</a></li>
                        <li><a href="#">Home Theater</a></li>
                    <li><a href="#">Request an estimate</a></li>
                    
                   <li><a href="#">All Services</a></li>
                    </ul>
              
                
                    <ul class="column mayHide">
                        <li><br /><img src="layout/Img/serv.png" /></li>
                    </ul>
                </div>
            </div>
        </li>
                   <li class="pull-left  left-aligned">
            <span class="top-heading"><i class="fa fa-diamond fa-lg" aria-hidden="true"></i>Other Departments</span>
            <i class="caret"></i>
            <div class="dropdown left-aligned">
                <div class="dd-inner">
                    <ul class="column">
                        <li><a href="#">Services</a></li>
                        <li><a href="#">Home Improvement & Repair</a></li>
                        <li><a href="#">Yard & Outdoors</a></li>
                        <li><a href="#">Computer & Electronics</a></li>
                        <li><a href="#">Assembly</a></li>
                        <li><a href="#">Cleaning</a></li>
                        <li><a href="#">Plumbing</a></li>
                        <li><a href="#">Electrical</a></li>
                        <li><a href="#">Home Theater</a></li>
                    <li><a href="#">Request an estimate</a></li>
                    
                   <li><a href="#">All Services</a></li>
                    </ul>
              
                
                    
                </div>
            </div>
        </li>
<?php 
/*========================================================= 
 *Items Page 
 * ========================================================= 
 */
    session_start();
        $PageTitel='items';

    if(isset($_SESSION['useruser'])){
        include 'inti.php';
    $do=isset($_GET['do'])?$_GET['do']:'manage';
    /*========================================================= 
            *
            * if do==manage
            * 
    * ========================================================= 
    */
        if($do=="manage"){
             echo gCheckItems("comment","comments","item_id=35");
           echo "welcome to item page";
        }
    /*========================================================= 
            *
            * if do==add
            * 
    * ========================================================= 
    */
        elseif($do==='Add'){}
    /*========================================================= 
            *
            * if do==Insert
            * 
    * ========================================================= 
    */
        elseif ($do==='insert') {}
    /*========================================================= 
            *
            * if do==edit
            * 
    * ========================================================= 
    */
        elseif($do=='edit'){}
    /*========================================================= 
            *
            * if do==update
            * 
    * ========================================================= 
    */
        elseif($do=='update'){}
    /*========================================================= 
            *
            * if do==delete
            * 
    * ========================================================= 
    */
         elseif ($do==='delete') {}
    /*========================================================= 
            *
            * if do==approve
            * 
    * ========================================================= 
    */
          elseif ($do==='Approve') {}
      
       include $tpl.'Footer.php';
    }
else{
            
                header("Location:index.php");
                exit;                
        }
        ?>
<?php ob_end_flush(); 


