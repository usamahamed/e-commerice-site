<?php 
/*========================================================= 
 *Item Page 
 * ========================================================= 
 */
ob_start();
session_start();
include 'inti.php';
$PageTitel='Show Items';
 $itemid= isset($_GET['itemid'])&& is_numeric($_GET['itemid']) ?intval($_GET['itemid']):0;
        $statm=$con->prepare('SELECT items.* ,categories.Name AS CAT_NM,Users.FullName AS US_NM,Users.userID AS UID FROM items '
                . 'INNER JOIN categories ON categories.ID=items.category_id INNER JOIN Users ON  Users.userID=items.member_id WHERE item_id=? AND Approve_itm=1');
        $statm->execute(array($itemid));
        $count=$statm->rowCount();            
        if($count>0){
        $items=$statm->fetch();
?>
   <div id="wrap">
    <div class="thumb-show"><img id ="x" src="1.jpg" alt="img"/></div>
    <div class="preview"></div>
  <div id="product_layout_3">
    
    <div class="product_desc">
      <h1><?php echo $items['item_name'] ?></h1>
      <span class="sale_price">$<?php echo $items['price']?></span>
      <span class="stars">
             <fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" checked /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
  </fieldset>
      
      </span>
      <div class="product_options">
        <div class="select">
                 <select id="size">
                   <option value = "1">Small</option>
                   <option value = "2">Medium</option>
                   <option value = "3">Large</option>
                   <option value = "4">X-Large</option>
                 </select>
                 </div>
          </div>
          <div class="buying">
                 <div class="quantity">
                   <label for="quantity">QTY:</label>
                   <input type="text">
                 </div>
                 <div class="cart">
                   <a href="#" class="add">Add to Cart <i class="fa fa-shopping-cart fa-lg"></i></a>
                 </div>
          </div>
          <div class="other_options">
          <span class="SKU">SKU:12345</span>
          <span class="QTY">QTY:35</span>
          </div>
          <div class="description">
              <p class="lead"><?php echo $items['description']?>.</P>
         <?php 
             $tag=  explode(",", $items['tags']);
             
             foreach ($tag as $t){
                 $t= str_replace(' ', '', $t);
                 $tlower= strtolower($t);
                    if(!empty($t)){
             echo '<a href="tags.php?name='.$tlower.' "><i class="fa fa-plus" aria-hidden="true"></i>'.$t.'</a>  ';
             }}
             ?>
                 </P>
        </div>
     
          <div class="social">
                   <span class="share">Share This:</span><span class="buttons"><img src="http://i.imgur.com/M8D8rr8.jpg"/></span>
           </div>
      </div>
      <div class="tabs_item" ng-app ng-init="tab=1">
	
<ul class="tabs-nav">
  <li><a ng-click="tab=1" ng-class="{active: tab == 1}">Product Details</a></li>
  <li><a ng-click="tab=2" ng-class="{active: tab == 2}">Write your Review</a></li>
  <li><a ng-click="tab=3" ng-class="{active: tab == 3}">Customer Reviews</a></li>
</ul>

	<div class="tabs-container">
		<div class="tab-content" ng-show="tab == 1">
        <h3>Add By: <a href="profileg.php?id=<?php echo $items['UID'] ?>"><?php echo $items['US_NM']?></a></h3>
      <?php  $date = new DateTime($items['add_date']);
        echo '<span class="pull-left date data_span">'.$date->format('g:ia l jS F').'</span>';
      ?>
        
          
        <div> <fieldset class="rating">
    <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
    <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
    <input type="radio" id="star4" name="rating" value="4" checked /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
    <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
    <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
    <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
    <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
    <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
    <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
    <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
            </fieldset></div>
			<p class="lead"><?php echo $items['description']?></p>
		</div>

		<div class="tab-content" ng-show="tab == 2">
			<h3>Write your Review</h3>
                           <?php if(isset($_SESSION['useruser'])){ ?>
                    <!Start Comments Show!>
           <hr class="C-hr">
          <div class="row">
              <div class="col-md-offset-3">
                  <div class="add-comment">
                  <h3>Add your Review</h3>
                  <form action="<?php echo $_SERVER['PHP_SELF'].'?itemid='.$items['item_id'] ?>" method="POST" >
                      <textarea name="comment"class="form-control" required="required"></textarea>
                      <input class="btn btn-primary"type="submit" value="Add Review"/>
                  </form>
                  <?php if($_SERVER['REQUEST_METHOD']=='POST'){
                      $comment=  filter_var($_POST['comment'],FILTER_SANITIZE_STRING);
                 $User_id=$_SESSION['uid'];
                 $item_id=$items['item_id'];
                 if(!empty($comment)){
                     $stat=$con->prepare('INSERT INTO comments(comment,status,date,item_id,User_id) VALUES (:zcom,0,now(),:zitem,:zuser)');
                     $stat->execute(array('zcom'=>$comment,'zitem'=>$item_id,zuser=>$User_id));
                      $itm=$items['item_name'];
                     if($stat){
                         echo '<div class="alert alert-success">Comment Sucess add</div>';
                    // $mesg_track.="-(User add comment in item $itm)=>";
                    //  $_SESSION['activity'].=$mesg_track;

                    // UpMsgLog($_SESSION['activity'],$_SESSION['uid']);
                     }
                 }
                  }?>
                  </div>
              </div>
          </div>
          <?php }else { echo '<a href="login.php">Login</a> or <a href="login.php">Register</a> to add comment';} ?>
		</div>

		<div class="tab-content" ng-show="tab == 3">
			<h3>Customer Reviews</h3>
         
		</div>

	</div>


</div>
      <hr class="C-hr">
      <div class="item_Statatistics"
      <div class='ui'>
  <div class='ui_box'>
    <div class='ui_box__inner'>
      <h2>
        Conversion Rate
      </h2>
      <p>Lorem ipsum dolor sit amet</p>
      <div class='stat'>
        <span>58%</span>
      </div>
      <div class='progress'>
        <div class='progress_bar'></div>
      </div>
      <p>Lorem ipsum dolor sit amet. Some more super groovy information about this stat.</p>
    </div>
    <div class='drop'>
      <div class='arrow'></div>
    </div>
  </div>
  <div class='ui_box'>
    <div class='ui_box__inner'>
      <h2>
        Sales By Type
      </h2>
      <p>Lorem ipsum dolor sit amet</p>
      <div class='stat_left'>
        <ul>
          <li>
            Electical
          </li>
          <li>
            Clothing
          </li>
          <li>
            Entertainment
          </li>
          <li>
            Kitchen
          </li>
        </ul>
      </div>
      <div class='progress_graph'>
        <div class='progress_graph__bar--1'></div>
        <div class='progress_graph__bar--2'></div>
        <div class='progress_graph__bar--3'></div>
        <div class='progress_graph__bar--4'></div>
      </div>
      <p>Lorem ipsum dolor sit amet. Some more super groovy information.</p>
    </div>
    <div class='drop'>
      <div class='arrow'></div>
    </div>
  </div>
  <div class='ui_box'>
    <div class='ui_box__inner'>
      <h2>
        Total Sales
      </h2>
      <p>Lorem ipsum dolor sit amet</p>
      <div class='stat'>
        <span>$34,403.93</span>
      </div>
      <div class='progress'>
        <div class='progress_bar--two'></div>
      </div>
      <p>Lorem ipsum dolor sit amet. Some more super groovy information about this stat.</p>
    </div>
    <div class='drop'>
      <div class='arrow'></div>
    </div>
  </div>
</div>
  </div>
            <hr class="C-hr">

      <h2>Customer Reviews</h2>
                     <?php  
               $statm=$con->prepare("SELECT comments.*,Users.FullName AS UN1,Users.userID AS UID1 FROM comments INNER JOIN Users ON Users.userID=comments.User_id WHERE item_id=? AND status=1 ORDER BY c_id DESC");
               $statm->execute(array($items['item_id']));//select all items expect admin
               $coms=$statm->fetchAll();
              
                  ?>
          <!Start Comments Show!>
          
           <?php   
           foreach ($coms as $c){?>
          <div class="com-box">
               <div class="row">
 <div class="col-sm-7">
     
     <div class="comments-container">
		<ul id="comments-list" class="comments-list">
			<li>
				<div class="comment-main-level">
					<!-- Avatar -->
					<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
					<!-- Contenedor del Comentario -->
					<div class="comment-box">
						<div class="comment-head">
							<h6 class="comment-name by-author"><a href="http://creaticode.com/blog">Agustin Ortiz</a></h6>
							<span>hace 20 minutos</span>
							<i class="fa fa-reply"></i>
							<i class="fa fa-heart"></i>
						</div>
						<div class="comment-content">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
						</div>
					</div>
				</div>
				<!-- Respuestas de los comentarios -->
				<ul class="comments-list reply-list">
					<li>
						<!-- Avatar -->
						<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
						<!-- Contenedor del Comentario -->
						<div class="comment-box">
							<div class="comment-head">
								<h6 class="comment-name"><a href="http://creaticode.com/blog">Lorena Rojero</a></h6>
								<span>hace 10 minutos</span>
								<i class="fa fa-reply"></i>
								<i class="fa fa-heart"></i>
							</div>
							<div class="comment-content">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
							</div>
						</div>
					</li>

					<li>
						<!-- Avatar -->
						<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_1_zps8e1c80cd.jpg" alt=""></div>
						<!-- Contenedor del Comentario -->
						<div class="comment-box">
							<div class="comment-head">
								<h6 class="comment-name by-author"><a href="http://creaticode.com/blog">Agustin Ortiz</a></h6>
								<span>hace 10 minutos</span>
								<i class="fa fa-reply"></i>
								<i class="fa fa-heart"></i>
							</div>
							<div class="comment-content">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
							</div>
						</div>
					</li>
				</ul>
			</li>

			<li>
				<div class="comment-main-level">
					<!-- Avatar -->
					<div class="comment-avatar"><img src="http://i9.photobucket.com/albums/a88/creaticode/avatar_2_zps7de12f8b.jpg" alt=""></div>
					<!-- Contenedor del Comentario -->
					<div class="comment-box">
						<div class="comment-head">
							<h6 class="comment-name"><a href="http://creaticode.com/blog">Lorena Rojero</a></h6>
							<span>hace 10 minutos</span>
							<i class="fa fa-reply"></i>
							<i class="fa fa-heart"></i>
						</div>
						<div class="comment-content">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit omnis animi et iure laudantium vitae, praesentium optio, sapiente distinctio illo?
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
    
    
    
     
     
     
     </div>                   
                   
                   
                   <div class="c_meter">
<div class="col-sm-3">
    <h3>Rating this Review</h3>
    <div class="c_meter">
    <table class="rating-details-histogram ">
  <tbody>
    <tr class="n-5">
      <th scope="row" class="histogram-label nowrap">
        5 stars
      </th>
      <td>
        <div class="histogram-bar" style="width: 100%;">
          <span class="text-inside">1146</span>
        </div>
      </td>
    </tr>

    <tr class="n-4">
      <th scope="row" class="histogram-label nowrap">
        4 stars
      </th>
      <td>
        <div class="histogram-bar" style="width: 78%;">
          <span class="text-inside">897</span>
        </div>
      </td>
    </tr>

    <tr class="n-3">
      <th scope="row" class="histogram-label nowrap">
        3 stars
      </th>
      <td>
        <div class="histogram-bar" style="width: 34%;">
          <span class="text-inside">395</span>
        </div>
      </td>
    </tr>

    <tr class="n-2">
      <th scope="row" class="histogram-label nowrap">
        2 stars
      </th>
      <td>
        <div class="histogram-bar" style="width: 12%;"></div>
        <span class="text-outside">134</span>
      </td>
    </tr>

    <tr class="n-1">
      <th scope="row" class="histogram-label nowrap">
        1 star
      </th>
      <td>
        <div class="histogram-bar" style="width: 3%;"></div>
        <span class="text-outside">34</span>
      </td>
    </tr>
  </tbody>
</table>
        <a href="rallating.php?do=manage&name=<?php echo $c['UN1']; ?>&idonwm=<?php echo $c['UID1']; ?>&cid=<?php echo $c['c_id']; ?>" class="btn btn-default">Write Comment</a>
    </div>
    <!start view review comments!>
    <?php
    $statm2=$con->prepare("SELECT * FROM com_on_review WHERE rid=?");
               $statm2->execute(array($c['c_id']));//select all items expect admin
               $coms1=$statm2->fetchAll();
                 foreach ($coms1 as $c1){
                     
                 }
  ?>
    <!END view review comments!>
    
</div>
                   </div>
            
             </div>
          </div>
          <hr class="C-hr">
          
         <?php  }
             ?>
       <?php 
        }//Not count >0 no such item 
        else{
            echo 'no such ID Or comment is not approved';
        }
       include $tpl.'Footer.php';
       ob_end_flush();