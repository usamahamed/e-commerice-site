<?php
$dbm='mysql:host=localhost;dbname=shop';
$user='root';
$pass='root';
$option=array(
    
    PDO::MYSQL_ATTR_INIT_COMMAND=>'SET NAMES utf8'
);
try{
    $con=new PDO($dbm,$user,$pass,$option);
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
 catch (PDOException $e){
     echo "fail to connect".$e->getMessage();
 }
     $do=isset($_REQUEST['do'])?$_REQUEST['do']:'manage';
if($do==="deletewishlist"){
 $itemid= isset($_REQUEST['itm'])&& is_numeric($_REQUEST['itm']) ?intval($_REQUEST['itm']):0;
 $userid= isset($_REQUEST['userid'])&& is_numeric($_REQUEST['userid']) ?intval($_REQUEST['userid']):0;
 
 $statm2=$con->prepare("SELECT * FROM cart WHERE itmid=? AND uid=? AND wishlist=1 AND Quantity!=0");
          $statm2->execute(array($itemid,$userid));
          $count=$statm2->rowCount();
          if($count===1){
            $statm=$con->prepare("UPDATE cart SET wishlist=0  WHERE uid=? AND itmid=?");
                $statm->execute(array($userid,$itemid));
                if ($statm) {
	echo 'okay';
        } else {
	echo 'denie';
        }
          }
 
 else{
 
      $statm3=$con->prepare("DELETE FROM cart WHERE itmid=? AND uid=? AND wishlist=1");
          $statm3->execute(array($itemid,$userid));
  
if ($statm3) {
	echo 'okay';
} else {
	echo 'denie';
}

 }
}
elseif($do==="addwishlist"){
    
    $itemid= isset($_REQUEST['itm'])&& is_numeric($_REQUEST['itm']) ?intval($_REQUEST['itm']):0;
 $userid= isset($_REQUEST['user'])&& is_numeric($_REQUEST['user']) ?intval($_REQUEST['user']):0;
 
 $statm2=$con->prepare("SELECT * FROM cart WHERE itmid=? AND uid=?");
          $statm2->execute(array($itemid,$userid));
          $count=$statm2->rowCount();
          
          if($count===1){
   
               $statm=$con->prepare("UPDATE cart SET wishlist=1  WHERE uid=? AND itmid=?");
                $statm->execute(array($userid,$itemid));
                if ($statm) {
	echo 'okay';
        } else {
	echo 'denie';
        }
          }
          
  else{
      $statm3=$con->prepare("INSERT INTO cart(wishlist,date,itmid,uid) VALUES(1,now(),?,?)");
          $statm3->execute(array($itemid,$userid));
  
if ($statm3) {
	echo 'okay';
} else {
	echo 'denie';
}

  }


}
elseif($do==="deletecart"){
 $itemid1= isset($_REQUEST['itm'])&& is_numeric($_REQUEST['itm']) ?intval($_REQUEST['itm']):0;
 $userid1= isset($_REQUEST['userid'])&& is_numeric($_REQUEST['userid']) ?intval($_REQUEST['userid']):0;
 
 $statm2=$con->prepare("SELECT * FROM cart WHERE itmid=? AND uid=? AND Quantity!=0 AND wishlist=1");
          $statm2->execute(array($itemid1,$userid1));
          $count=$statm2->rowCount();
          if($count===1){
            $statm=$con->prepare("UPDATE cart SET Quantity=0  WHERE uid=? AND itmid=?");
                $statm->execute(array($userid1,$itemid1));
                if ($statm) {
	echo 'okay';
        } else {
	echo 'denie';
        }
          }
 
 else{
 
      $statm3=$con->prepare("DELETE FROM cart WHERE itmid=? AND uid=? AND wishlist!=1");
          $statm3->execute(array($itemid1,$userid1));
  
if ($statm3) {
	echo 'okay';
} else {
	echo 'denie';
}

 }
}
elseif($do==="updatequantity"){
 $itemid1= isset($_REQUEST['itm'])&& is_numeric($_REQUEST['itm']) ?intval($_REQUEST['itm']):0;
 $userid1= isset($_REQUEST['userid'])&& is_numeric($_REQUEST['userid']) ?intval($_REQUEST['userid']):0;
 $Qunt= isset($_REQUEST['qnt'])&& is_numeric($_REQUEST['qnt']) ?intval($_REQUEST['qnt']):0;
 
     $statm=$con->prepare("UPDATE cart SET Quantity=? WHERE uid=? AND itmid=?");
                $statm->execute(array($Qunt,$userid1,$itemid1));
                if ($statm) {
	echo 'okay';
        } else {
        echo 'denie';
        
        }
}
elseif ($do==="addtocart") {
               $itmid= isset($_REQUEST['itm'])&& is_numeric($_REQUEST['itm']) ?intval($_REQUEST['itm']):0;
               $uid= isset($_REQUEST['userid'])&& is_numeric($_REQUEST['userid']) ?intval($_REQUEST['userid']):0;
  $statment1=$con->prepare("SELECT * FROM cart WHERE uid=$uid AND itmid=$itmid ");
          $statment1->execute();
          $count1=$statment1->rowCount();
          if($count1===1){
              $statm=$con->prepare("UPDATE cart SET Quantity=Quantity+1  WHERE uid=? AND itmid=?");
                $statm->execute(array($uid,$itmid));
                 $mesg= '<div class="alert alert-danger"><i class="fa fa-exclamation-triangle fa-lg" aria-hidden="true"></i>You increast item quantity to one</div>';
                  Redirect($mesg,'back');
          }
          else{
          $stat=$con->prepare("INSERT INTO cart(date,itmid,uid) VALUES(now(),?,?)");
          $stat->execute(array($itmid,$uid));
                   if ($stat) {
	echo 'okay';
        } else {
        echo 'denie';
        
        }
        
          }
}
elseif($do==="SendFAQ"){
 $itemid1= isset($_REQUEST['itm'])&& is_numeric($_REQUEST['itm']) ?intval($_REQUEST['itm']):0;
 $userid1= isset($_REQUEST['userid'])&& is_numeric($_REQUEST['userid']) ?intval($_REQUEST['userid']):0;
 $val=$_REQUEST['val'];
     $statm=$con->prepare("INSERT INTO FAQ(Question,Itmid,uid) VALUES(?,?,?)");
                $statm->execute(array($val,$itemid1,$userid1));
                if ($statm) {
	echo 'okay';
        } else {
        echo 'denie';
        
        }
}

elseif($do==="sendAnswer"){
 $QID= isset($_REQUEST['qid'])&& is_numeric($_REQUEST['qid']) ?intval($_REQUEST['qid']):0;
 $userid1= isset($_REQUEST['userid'])&& is_numeric($_REQUEST['userid']) ?intval($_REQUEST['userid']):0;
 $val=$_REQUEST['val'];
        $statment1=$con->prepare("SELECT * FROM FAQAnswer WHERE uid=$userid1 AND Qid=$QID ");
          $statment1->execute();
          $count1=$statment1->rowCount();
          if($count1===1){
              $statm=$con->prepare("UPDATE FAQAnswer SET answer=?  WHERE uid=? AND Qid=?");
                $statm->execute(array($val,$userid1,$QID));
                     if ($statm) {
	echo 'okay';
        } else {
        echo 'denie';
        
          }
          }
          else{
 
 
     $statm=$con->prepare("INSERT INTO FAQAnswer(answer,date,Qid,uid) VALUES(?,now(),?,?)");
                $statm->execute(array($val,$QID,$userid1));
                if ($statm) {
	echo 'okay';
        } else {
        echo 'denie';
        
          }
          
        }
}

elseif ($do==="checkuser") {
 $statm3=$con->prepare("SELECT * FROM Users WHERE UserName=?");
        $statm3->execute(array($_REQUEST['username']));
    $count=$statm3->rowCount();
if ($count===1) {
	echo 'denied';
} else {
	echo 'okay';
}
}
elseif ($do==="sendItemAddedname") {
 $val=$_REQUEST['name'];
 if(strlen($val)<4 ){
     echo 'Name not less than 4 character<br>';
 }
 
}
elseif ($do==="sendItemAddeddes") {
 $val1=$_REQUEST['des'];

 if(strlen($val1) < 10 &&$val1!=null){
     echo 'Description not less than 10 character<br>';
 } 
}




